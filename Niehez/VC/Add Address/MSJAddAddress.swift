//
//  MSJAddAddress.swift
//  MSJ
//
//  Created by Mac on 1/1/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit
import AAPickerView



class MSJAddAddress: BaseVC,UITextFieldDelegate
{
    
    @IBOutlet weak var paymentTextField: CustomAAPickerView!
    @IBOutlet weak var isDefaultTextField: CustomAAPickerView!
    @IBOutlet weak var addressTextField: CustomAAPickerView!
    @IBOutlet weak var districtTextField: CustomAAPickerView!
    @IBOutlet weak var zipCodeTextField: MSJCustomPickerTextField!
    @IBOutlet weak var streetTextField: MSJCustomPickerTextField!
    @IBOutlet weak var poBoxTextField: MSJCustomPickerTextField!
    @IBOutlet weak var buildingNoTextField: MSJCustomPickerTextField!
    @IBOutlet weak var nameTextField: MSJCustomPickerTextField!
    @IBOutlet weak var mobileNumberTextField: MSJCustomPickerTextField!
    @IBOutlet weak var emailTextField: MSJCustomPickerTextField!
    
    @IBOutlet weak var textFieldCity: CustomAAPickerView!
    
    @IBOutlet weak var textFieldLocation: MSJCustomTextField!
    @IBOutlet weak var buttonAdress: MSJRegularButton!
    
    
    var addressObject : UserAddressModel!
    
    var citiesArray = [CitiesModel]()
    var districtArray = [DistrictModel]()
    var cityId = ""
    var districtId = ""
    var userData = MSJSharedManager.getUser()
        
    var lat:String = ""
    var lng:String = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.backButtonOnNavigationBar()
        
        
        self.getCities()
        
        self.configurePicker()
        
        if addressObject != nil
        {
            textFieldCity.text = addressObject.CityTitle
            textFieldLocation.text = addressObject.Street
            lat = addressObject.Latitude
            lng = addressObject.Longitude
            nameTextField.text = addressObject.RecipientName
            zipCodeTextField.text = addressObject.ZipCode
            poBoxTextField.text = addressObject.POBox
            streetTextField.text = addressObject.Street
            buildingNoTextField.text = addressObject.BuildingNo
            emailTextField.text = addressObject.Email
            mobileNumberTextField.text = addressObject.MobileNo
            districtTextField.text = addressObject.DistrictTitle
            
            if addressObject.IsDefault == "1" {
                isDefaultTextField.text = "Yes"
            } else {
                isDefaultTextField.text = "No"
            }
            if addressObject.UseForPaymentCollection == "1" {
                paymentTextField.text = "Yes"
            } else {
                paymentTextField.text = "No"
            }
            
            addressTextField.text = addressObject.AddressType
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        
        
        setLanguage()
        if addressObject != nil
        {
            self.setAttributedTitleForRightBar(complete: "Update Address".localized(), CompFont: UIFont.init(name: MSJSharedManager.boldFont(), size: 15.0)!, CompColor: MSJColor.TAB_BAR_BACKGROUND_COLOR, sub: "", subFont: UIFont.init(name: MSJSharedManager.mediumFont(), size: 15.0)!, SubColor: MSJColor.TAB_BAR_BACKGROUND_COLOR)
            
        }
        else
        {
            self.setAttributedTitleForRightBar(complete: "Create Address".localized(), CompFont: UIFont.init(name: MSJSharedManager.boldFont(), size: 15.0)!, CompColor: MSJColor.TAB_BAR_BACKGROUND_COLOR, sub: "", subFont: UIFont.init(name: MSJSharedManager.mediumFont(), size: 15.0)!, SubColor: MSJColor.TAB_BAR_BACKGROUND_COLOR)
            
        }
    }
    
    
    @objc func cityImageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        
        print("Imageview tap.")
        
        self.view.endEditing(true)
        getLocation()
        
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        
        if textField .isEqual(textFieldLocation)
        {
            getLocation()
            self.view.endEditing(true)
            return false
        }
        
        return true
    }

    
    //Api Call
    
    func getCities()
    {
        
        
        
        let url = BASE_URL + GET_CITIES
        
        let parameters: [String:Any] = ["NoVersion":"true"]
        
        ApiManager.getRequest(urlString: url, isAlertShow: false, parameters: parameters) { dict in
            
            self.citiesArray = CitiesModel.parseData(response: dict)
            
            /// Cities Model Picker View
            var CitiesNameStrings = [String]()
            for cities in self.citiesArray {
                CitiesNameStrings.append(cities.Title)
                if cities.Title == self.addressObject.CityTitle {
                    self.cityId = cities.CityID
                }
            }
            self.textFieldCity.pickerType = .string(data: CitiesNameStrings)
            self.textFieldCity.valueDidSelected = { data in
                self.textFieldCity.text = CitiesNameStrings[data as! Int]
                if let foo = self.citiesArray.enumerated().first(where: {$0.element.Title == self.textFieldCity.text}) {
                    self.cityId = foo.element.CityID
                    self.getDistrict()
                }
            }
            
        } errorCallBack: { errorString in
            print("Error")
        }
        
        
    }
    
    func getDistrict()
    {
        
        
        
        let url = BASE_URL + DISTRICTS
        
        let parameters: [String:Any] = ["NoVersion":"true", "CityID": self.cityId]
        
        ApiManager.getRequest(urlString: url, isAlertShow: false, parameters: parameters) { dict in
            
            self.districtArray = DistrictModel.parseData(response: dict)

//             District Model Picker View
            var districtNameStrings = [String]()
            for district in self.districtArray {
                districtNameStrings.append(district.Title)
                if district.Title == self.addressObject.DistrictTitle {
                    self.districtId = district.DistrictID
                }
            }
            self.districtTextField.pickerType = .string(data: districtNameStrings)
            self.districtTextField.valueDidSelected = { data in
                self.districtTextField.text = districtNameStrings[data as! Int]
                if let foo = self.districtArray.enumerated().first(where: {$0.element.Title == self.districtTextField.text}) {
                    self.districtId = foo.element.DistrictID
                }
            }
            
        } errorCallBack: { errorString in
            print("Error")
        }
        
        
    }
    
    @IBAction func buttonAddress(_ sender: Any)
    {
        
        
        if self.whiteSpaceValidae(textfield: self.nameTextField.text!)==false
        {
            self.showAlert(title: "Error".localized(), message: "Enter Name".localized())
        }
        else if self.whiteSpaceValidae(textfield: self.mobileNumberTextField.text!)==false
        {
            self.showAlert(title: "Error".localized(), message: "Enter Mobile Number".localized())
        }
        else if self.whiteSpaceValidae(textfield: self.emailTextField.text!)==false
        {
            self.showAlert(title: "Error".localized(), message: "Enter Email".localized())
        }
        if self.whiteSpaceValidae(textfield: self.textFieldCity.text!)==false
        {
            self.showAlert(title: "Error".localized(), message: "Enter City".localized())
        }
        else  if self.whiteSpaceValidae(textfield: self.districtTextField.text!)==false
        {
            self.showAlert(title: "Error".localized(), message: "Enter your district".localized())
        }
        else  if self.whiteSpaceValidae(textfield: self.buildingNoTextField.text!)==false
        {
            self.showAlert(title: "Error".localized(), message: "Enter your Building No".localized())
        }
        else  if self.whiteSpaceValidae(textfield: self.streetTextField.text!)==false
        {
            self.showAlert(title: "Error".localized(), message: "Enter your street".localized())
        }
        else  if self.whiteSpaceValidae(textfield: self.poBoxTextField.text!)==false
        {
            self.showAlert(title: "Error".localized(), message: "Enter your PO Box".localized())
        }
        else  if self.whiteSpaceValidae(textfield: self.zipCodeTextField.text!)==false
        {
            self.showAlert(title: "Error".localized(), message: "Enter your Zip Code".localized())
        }
        else if self.lat == ""
        {
            self.showAlert(title: "Error".localized(), message: "Pin your location".localized())
            
        }
        else  if self.whiteSpaceValidae(textfield: self.addressTextField.text!)==false
        {
            self.showAlert(title: "Error".localized(), message: "Select your address type".localized())
        }
        else  if self.whiteSpaceValidae(textfield: self.isDefaultTextField.text!)==false
        {
            self.showAlert(title: "Error".localized(), message: "Select is default".localized())
        }
        else  if self.whiteSpaceValidae(textfield: self.paymentTextField.text!)==false
        {
            self.showAlert(title: "Error".localized(), message: "Select Payment Collection".localized())
        }

            
        else
        {
            self.saveAddress()
            
        }
    }
    
    private func configurePicker() {
        
        /// addressType Picker View
        let addressTypeArray = ["Home", "Office"]
        
        self.addressTextField.pickerType = .string(data: addressTypeArray)
        self.addressTextField.valueDidSelected = { data in
            self.addressTextField.text = addressTypeArray[data as! Int]
        }
        
        /// isDefault Picker View
        let isDefaultArray = ["Yes", "No"]
        
        self.isDefaultTextField.pickerType = .string(data: isDefaultArray)
        self.isDefaultTextField.valueDidSelected = { data in
            self.isDefaultTextField.text = isDefaultArray[data as! Int]
        }
        
        /// paymentCollect Picker View
        let paymentCollectArray = ["Yes", "No"]
        
        self.paymentTextField.pickerType = .string(data: paymentCollectArray)
        self.paymentTextField.valueDidSelected = { data in
            self.paymentTextField.text = paymentCollectArray[data as! Int]
        }
        
    }
    func saveAddress()
    {
        
        
        if (UserDefaults.standard.value(forKey: USER_DATA) != nil)
        {
            
            
            var addressDictionary = [String: Any]()
            addressDictionary["UserID"] = self.userData.UserID
            addressDictionary["RecipientName"] = self.nameTextField.text
            addressDictionary["MobileNo"] = self.mobileNumberTextField.text
            addressDictionary["Email"] = self.emailTextField.text
            addressDictionary["CityID"] = self.cityId
            addressDictionary["DistrictID"] = self.districtId
            addressDictionary["BuildingNo"] = self.buildingNoTextField.text
            addressDictionary["Street"] = self.streetTextField.text
            addressDictionary["POBox"] = self.poBoxTextField.text
            addressDictionary["ZipCode"] = self.zipCodeTextField.text

      
            
            addressDictionary["Latitude"] = self.lat
            addressDictionary["Longitude"] = self.lng
            
            if isDefaultTextField.text == "Yes" {
                addressDictionary["IsDefault"] = "1"
            } else {
                addressDictionary["IsDefault"] = "0"
            }
            
            if paymentTextField.text == "Yes" {
                addressDictionary["UseForPaymentCollection"] = "1"
            } else {
                addressDictionary["UseForPaymentCollection"] = "0"
            }
            
            addressDictionary["AddressType"] = self.addressTextField.text
            
            addressDictionary["CompanyID"] = "2"
            
            print(addressDictionary)
            
            var url :String
            if addressObject != nil
            {
                url = BASE_URL + UPDATE_ADDRESS
                addressDictionary["AddressID"] =  addressObject.AddressID
            }
            else
            {
                url = BASE_URL + POST_ADDRESS
            }
                        
            ApiManager.postServiceCall(urlString: url, isAlertShow: true, parameters: addressDictionary, header: nil) { response in
                print(response)
                if let message = response["message"] as? String {
                    self.showAlert(title: "Alert!", message: message)
                }
            } errorCallBack: { error in
                self.showAlert(title: "Error!", message: error)
            }

    }
    }
    
    func getLocation()
    {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "MSJMapVC") as? MSJMapVC
        {
            
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    
    func setLanguage()
    {
        
        
        
        if MSJSharedManager.getArabic()
        {
            
            
            districtTextField.textAlignment = NSTextAlignment.right
           
            buildingNoTextField.textAlignment = NSTextAlignment.right
           
           
            textFieldCity.textAlignment = NSTextAlignment.right
            textFieldLocation.textAlignment = NSTextAlignment.right
        }
        else
        {
            
            
            districtTextField.textAlignment = NSTextAlignment.left
            buildingNoTextField.textAlignment = NSTextAlignment.left
           
          
            textFieldCity.textAlignment = NSTextAlignment.left
            textFieldLocation.textAlignment = NSTextAlignment.left
            
            
            
            
        }
        
        
        textFieldCity.addLeftAndRightView()
        districtTextField.addLeftAndRightView()
        
        if addressObject != nil
        {
            buttonAdress.setTitle("Update Address".localized(), for: UIControl.State.normal)
            
        }
        else
        {
            buttonAdress.setTitle("Add Address".localized(), for: UIControl.State.normal)
            
        }
        
        
        let cityPaddingView: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let dropDownImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 18, height: 18))
        dropDownImageView.image = UIImage(named: LOCATION_ICON)
        dropDownImageView.contentMode = UIView.ContentMode.scaleAspectFit
        cityPaddingView.addSubview(dropDownImageView)
        textFieldLocation.rightView = cityPaddingView
        textFieldLocation.rightViewMode = .always
        textFieldLocation.delegate = self
        
        
        let cityGesture = UITapGestureRecognizer(target: self, action: #selector(cityImageTapped(tapGestureRecognizer:)))
        dropDownImageView.isUserInteractionEnabled = true
        dropDownImageView.addGestureRecognizer(cityGesture)
        
        
    }
    
    
}


extension MSJAddAddress : LOCATIONSELECT
{
    func locationSelect(address: String, latitude: Double, lognitude: Double) {
        
        self.textFieldLocation.text = address
        self.lat = String(describing:latitude)
        self.lng = String(describing:lognitude)
    }
    
    
}

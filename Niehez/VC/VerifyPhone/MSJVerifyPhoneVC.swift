//
//  MSJVerifyPhoneVC.swift
//  MSJ
//
//  Created by Mac on 08/03/2018.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class MSJVerifyPhoneVC: BaseVC {
    
    @IBOutlet weak var labelPhoneNumber: MSJRegularLabel!
    @IBOutlet weak var buttonSubmiut: MSJRegularButton!
    @IBOutlet weak var textFieldVerifyCode: MSJCustomTextField!
    
    @IBOutlet weak var buttonResendCodeButton: MSJRegularButton!
    var isComingFromLogin = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.backButtonOnNavigationBar()
        
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        setLanguage()
        
        self.setTabbarToVisible(isVisible: false)
        if isComingFromLogin
        {
            self.backButtonOnNavigationBar()
        }
        else
        {
            let rightBarButton = UIBarButtonItem(title: "Cancel".localized(), style: .plain, target: self, action: #selector(barButtonItemClicked))
            rightBarButton.setTitleTextAttributes([
                NSAttributedString.Key.font : UIFont(name: MSJSharedManager.boldFont(), size: 15)!,
                NSAttributedString.Key.foregroundColor : MSJColor.TAB_BAR_BACKGROUND_COLOR,
            ], for: .normal)
            self.navigationItem.leftBarButtonItem = rightBarButton
        }
        
        self.setAttributedTitleForRightBar(complete: "Verify Phone".localized(), CompFont: UIFont.init(name: MSJSharedManager.boldFont(), size: 15.0)!, CompColor: MSJColor.TAB_BAR_BACKGROUND_COLOR, sub: "Verify Phone".localized(), subFont: UIFont.init(name: MSJSharedManager.boldFont(), size: 15.0)!, SubColor: MSJColor.TAB_BAR_BACKGROUND_COLOR)
        
        self.showAlert(title: "Alert".localized(), message: MSJSharedManager.getUser().Code)
    }
    
    @objc func barButtonItemClicked()
    {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers ;
        for aViewController in viewControllers
        {
            if(aViewController is MSJLogInVC)
            {
                self.navigationController!.popToViewController(aViewController, animated: true);
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonSubmit(_ sender: Any)
    {
        
        if textFieldVerifyCode.text == MSJSharedManager.getUser().Code
        {
                               
            profileVerification()
            
        }
        else
        {
            self.showAlert(title: "Error".localized(), message: "Code mismatch".localized())
        }
        
      
    }
    
    
    
    func profileVerification()
    {
        
        
     
            
            
            var dataDictionary = [String: String]()
            
            dataDictionary["user_id"] = MSJSharedManager.getUser().UserID
            dataDictionary["is_verified"] = "1"
            dataDictionary["verification_code"] = ""
            
            print(dataDictionary)
            
            let url = BASE_URL + UPDATE_USER_PROFILE_DATA
            
        MSJApiManager.sharedInstance.postServiceCall(urlString: url, isAlertShow: true, companyId: "", parameters: dataDictionary, header: nil, successCallback: { (dict) in
                
                
                
                //                self.buttonSubmiut.stopAnimation(animationStyle: .expand, completion:
                //                    {
                
                
                
                
                var userObject:MSJUser = MSJUser.parseDataOfUserInfo(userDict: dict )
                
                userObject =  MSJSharedManager.setUser(userObject: userObject)
                
                self.saveCustomerData(dict: dict)
        
                self.alertWithSingleBackButtonFuction(title: "Success".localized(), message: "Your number is verified".localized(), successCallback: self.goBack)
                
                
                // })
                
                
                
            }) { (error) in
                
                //                    self.buttonSubmiut.stopAnimation(animationStyle: .expand, completion:
                //                        {
                
                
                self.showAlert(title: "Error".localized(), message: error)
                
                //  })
                
                
            }
            
            //  })
        
        
        
    }
    
    
    func goBack()
    {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func buttonResendCode(_ sender: Any)
    {
            
            var dataDictionary = [String: String]()
            
          
            
            
            
            
            
            dataDictionary["user_id"] = MSJSharedManager.getUser().UserID
            
            
            
            let url = BASE_URL + VERIFICATION_CODE_SEND
            
        MSJApiManager.sharedInstance.postServiceCall(urlString: url, isAlertShow: true, companyId: "", parameters: dataDictionary, header: nil, successCallback: { (dict) in
                
                print(dict)
                
                self.showAlert(title: "Success", message: "Code sent successfully")
                
                
                
                
            }) { (error) in
                
                
                
                
                self.showAlert(title: "Error".localized(), message: error)
                
                
                
                
            }
            
            
        
        
        
    }
    
    func setLanguage()
    {
        if MSJSharedManager.getArabic()
        {
            
            textFieldVerifyCode.textAlignment = NSTextAlignment.right
            
            
        }
        else
        {
            
            
            textFieldVerifyCode.textAlignment = NSTextAlignment.left
            
            
        }
        
        textFieldVerifyCode.placeholder = "Enter Code".localized()
        
        buttonSubmiut.setTitle("Submit".localized(), for: UIControl.State.normal)
        buttonResendCodeButton.setTitle("Resend Code".localized(), for: UIControl.State.normal)
        
       
        if let phoneStr =  MSJSharedManager.getUser().Mobile
        {
                var phone = String(phoneStr.dropFirst(3))
                phone = "0" + phone
                labelPhoneNumber.text = "Your phone number is".localized() + " " + phone
        }
        
        
      
    }
}

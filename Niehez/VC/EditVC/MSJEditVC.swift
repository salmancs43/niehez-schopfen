//
//  MSJEditVC.swift
//  MSJ
//
//  Created by Mac on 12/29/17.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class MSJEditVC: BaseVC , UITextFieldDelegate {
    
    @IBOutlet weak var textFieldFullName: MSJCustomTextField!
    
    @IBOutlet weak var textFieldEmail: MSJCustomTextField!
    @IBOutlet weak var textFieldPhone: MSJCustomTextField!
    @IBOutlet weak var textFieldCity: MSJCustomPickerTextField!
    
    
    @IBOutlet weak var buttonMale: UIButton!
    @IBOutlet weak var buttonFemale: UIButton!
    
    @IBOutlet weak var buttonUpdateProfile: MSJRegularButton!
    var addressListArray = Array<Any>()
    
    @IBOutlet weak var labelGender: MSJBoldLabel!
    @IBOutlet weak var labelFemale: MSJRegularLabel!
    @IBOutlet weak var labelMale: MSJRegularLabel!
    
    
    
    var citiesArray = [Cities]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //       self.navigationItem.title = "Account Settings"
        
        
        textFieldEmail.isUserInteractionEnabled = false
        
        
        getCities()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        setLanguage()
        self.setTabbarToVisible(isVisible: false)
        self.backButtonOnNavigationBar()
        self.setAttributedTitleForRightBar(complete: "Update Profile".localized(), CompFont: UIFont.init(name: MSJSharedManager.regularFont(), size: 15.0)!, CompColor: MSJColor.TAB_BAR_BACKGROUND_COLOR, sub: "Update".localized(), subFont: UIFont.init(name: MSJSharedManager.boldFont(), size: 15.0)!, SubColor: MSJColor.TAB_BAR_BACKGROUND_COLOR)
        
        let userObject = MSJSharedManager.getUser()
        if userObject.UserID != nil
        {
            // let phoneCount = userObject.phone?.count
            
            
            let phoneStr = String(userObject.Mobile!.dropFirst(3))
            self.textFieldPhone.text = "0" + phoneStr
            
            
            textFieldFullName.text = userObject.FullName
            textFieldEmail.text = userObject.Email
            
            textFieldCity.text = userObject.CityTitle
            
            
            
            if userObject.Gender == "male"
            {
                buttonMale.isSelected = true
            }
            else if userObject.Gender == "female"
            {
                buttonFemale.isSelected = true
            }
        }
        
        
    }
    
    
    //Mark - textfield delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField.isEqual(textFieldFullName)
        {
            if string == ""
            {
                
            }
            else
            {
                if textFieldFullName.text!.count > FULLNAME_LENGTH
                {
                    return false
                }
            }
            
        }
        else if textField.isEqual(textFieldPhone)
        {
            let set = CharacterSet(charactersIn: "0123456789")
            if string.rangeOfCharacter(from: set.inverted) != nil
            {
                return false
            }
            if string == ""
            {
                
            }
            else
            {
                
                
            }
        }
        
        return true
        
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    
    
    
    //Api Call
    
    func getCities()
    {
        
        
        
        MSJApiManager.sharedInstance.getRequest(urlString: BASE_URL + CITIES, isAlertShow: true, parameters: nil, successCallback: { (response) in
            
            
            self.citiesArray = Cities.parseData(response: response)
            self.configPickers()
            
        }) { (error) in
            
            self.showAlert(title: "Error".localized(), message: error)
        }
        
        
    }
    
    
    func configPickers() {
        
        
        
        
        
        // textFieldCity
        
        var cityTitleArray = [String]()
        for item in self.citiesArray
        {
            cityTitleArray.append(item.title)
        }
        self.textFieldCity.pickerType = .string(data: cityTitleArray)
        self.textFieldCity.pickerRow.font = UIFont(name: MSJSharedManager.mediumFont(), size: 14)
        
        textFieldCity.toolbar.barTintColor = .darkGray
        textFieldCity.toolbar.tintColor = .darkGray
        self.textFieldCity.valueDidSelected = { (index) in
            
            
            
            
        }
    }
    
    func updateUserData()
    {
        let userObject = MSJSharedManager.getUser()
        var dataDictionary = [String: String]()
        if self.whiteSpaceValidae(textfield: self.textFieldFullName.text!)==false
        {
            self.showAlert(title: "Error".localized(), message: "Enter your name".localized())
            
            
        }
        else if self.isValidEmail(testStr: self.textFieldEmail.text!)==false
        {
            self.showAlert(title: "Error".localized(), message: "Enter valid email".localized())
            
            
        }
            
        else if self.phoneValidate(value: self.textFieldPhone.text!)==false
        {
            self.showAlert(title: "Error".localized(), message: "Enter valid phone e.g 05xxxxxxx".localized())
            
            
        }
        else if self.whiteSpaceValidae(textfield: self.textFieldCity.text!)==false
        {
            self.showAlert(title: "Error".localized(), message: "Enter your city".localized())
            
            
        }
        else
        {
            
            
            let outputPhone =   "966" + String(describing: self.textFieldPhone.text!.dropFirst())
            
            dataDictionary["full_name"] = self.textFieldFullName.text
            dataDictionary["phone"] = outputPhone
            
            
            
            dataDictionary["user_id"] = userObject.UserID
            
            if buttonMale.isSelected
            {
                dataDictionary["gender"] = "male"
            }
            else if buttonFemale.isSelected
            {
                dataDictionary["gender"] = "female"
            }
            
            if let object = citiesArray.filter({ $0.title == textFieldCity.text }).first {
                
                dataDictionary["city_id"] = String(describing:object.id)
                
            } else {
                print("not found")
            }
            
            
            
            buttonUpdateProfile.startAnimation()
            let qualityOfServiceClass = DispatchQoS.QoSClass.background
            let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
            backgroundQueue.async(execute: {
                
                
                
                let url = BASE_URL + UPDATE_USER_PROFILE_DATA
                
                MSJApiManager.sharedInstance.postServiceCall(urlString: url, isAlertShow: false, companyId: "", parameters: dataDictionary, header: nil, successCallback: { (dict) in
                    print(dict)
                    
                    self.buttonUpdateProfile.stopAnimation(animationStyle: .expand, completion:
                        {
                            
                            self.showAlert(title: "Success".localized(), message:"Profile updated successfully".localized())
                    })
                    
                    var userObject:MSJUser = MSJUser.parseDataOfUserInfo(userDict: dict )
                    
                    userObject =  MSJSharedManager.setUser(userObject: userObject)
                    
                    self.saveCustomerData(dict: dict)
                    
                    
                    
                    
                }) { (error) in
                    
                    
                    self.buttonUpdateProfile.stopAnimation(animationStyle: .expand, completion:
                        {
                            self.showAlert(title: "Error".localized(), message:error)
                            
                            
                    })
                }
            })
        }
    }
    
    
    
    @IBAction func updateButton(_ sender: Any)
    {
        self.updateUserData()
    }
    
    
    
    
    @IBAction func buttonMale(_ sender: Any)
    {
        
        buttonMale.isSelected = true
        buttonFemale.isSelected = false
    }
    
    @IBAction func buttonFemale(_ sender: Any)
    {
        buttonMale.isSelected = false
        buttonFemale.isSelected = true
    }
    func setLanguage()
    {
        if MSJSharedManager.getArabic()
        {
            
            textFieldEmail.textAlignment = NSTextAlignment.right
            
            textFieldFullName.textAlignment = NSTextAlignment.right
            textFieldPhone.textAlignment = NSTextAlignment.right
            textFieldCity.textAlignment = NSTextAlignment.right
            
            
            
            
        }
        else
        {
            
            
            textFieldEmail.textAlignment = NSTextAlignment.left
            
            textFieldFullName.textAlignment = NSTextAlignment.left
            textFieldPhone.textAlignment = NSTextAlignment.left
            textFieldCity.textAlignment = NSTextAlignment.left
            
            
            
        }
        
        textFieldCity.addLeftAndRightView()
        
        textFieldEmail.placeholder = "Email".localized()
        
        textFieldFullName.placeholder = "Full Name".localized()
        textFieldPhone.placeholder = "Phone".localized()
        textFieldCity.placeholder = "City".localized()
        
        labelGender.text = "Gender".localized()
        labelMale.text = "Male".localized()
        labelFemale.text = "Female".localized()
        buttonUpdateProfile.setTitle("Update".localized(), for: UIControl.State.normal)
    }
    
}




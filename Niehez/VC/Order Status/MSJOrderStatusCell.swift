//
//  MSJOrderStatusCell.swift
//  MSJ
//
//  Created by Mac on 1/11/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

protocol OrderStatusDelegate {
    func openDetail(button:UIButton,index1:Int)
}

class MSJOrderStatusCell: UITableViewCell {

    @IBOutlet weak var threeDotButton: UIButton!
    @IBOutlet weak var labelAssignTo: MSJRegularLabel!
    
    @IBOutlet weak var outerImage: UIView!
    @IBOutlet weak var labelItemCount: MSJRegularLabel!
    @IBOutlet weak var labelItemTitle: MSJRegularLabel!
    @IBOutlet weak var labelAssignToName: MSJRegularLabel!
    @IBOutlet weak var labelOrderNumber: MSJBoldLabel!
    @IBOutlet weak var labelOrderNumberTitle: MSJRegularLabel!
    @IBOutlet weak var labelTotalBill: MSJRegularLabel!
    
    @IBOutlet weak var labelTotalBillPrice: MSJRegularLabel!
    
    @IBOutlet weak var labelOrderStatusPlaced: MSJBoldLabel!
    
     @IBOutlet weak var labelOrderStatusCaptainAssigned: MSJBoldLabel!
    
    
    @IBOutlet weak var labelOrderStatusPicked: MSJBoldLabel!
    
    @IBOutlet weak var labelOrderStatusDelivered: MSJBoldLabel!
    
    @IBOutlet weak var fullOrderProgress: UIView!
    @IBOutlet weak var thirdOrderProgress: UIView!
    @IBOutlet weak var secondOrderProgress: UIView!
    @IBOutlet weak var firstOrderProgress: UIView!
    
    @IBOutlet weak var driverImage: UIImageView!
    
    @IBOutlet weak var mainView: UIView!
    
    
    @IBOutlet weak var orderStatusImage: UIImageView!
    
    @IBOutlet weak var labelProductPrice: MSJBoldLabel!
    @IBOutlet weak var labelProductPiece: MSJRegularLabel!
    @IBOutlet weak var labelProductTitle: MSJBoldLabel!
    @IBOutlet weak var productImage: UIImageView!
    
    var cellDelegate:OrderStatusDelegate?
    var index1:IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
     
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func threeDotAction(_ sender: UIButton) {
        cellDelegate?.openDetail(button: threeDotButton, index1: (index1?.row)!)
    }
    
}

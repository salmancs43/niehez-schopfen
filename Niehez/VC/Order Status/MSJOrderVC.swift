//
//  MSJOrderVC.swift
//  MSJ
//
//  Created by Muhammad Salman on 15/06/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import DropDown

class MSJOrderVC: BaseVC {
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var bgImage: UIImageView!
    
    var orderArray = [MSJOrderStatusObject]()
    
    let threeDotDropDown = DropDown()
    
    lazy var dropDowns: [DropDown] = {
        return [
            
            self.threeDotDropDown
        ]
    }()
    
    var isBackButton = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isBackButton
        {
            self.backButtonOnNavigationBar()
            self.setTabbarToVisible(isVisible: false)
        }
        
        self.navigationItem.title = "My Orders".localized()
        
        addRefreshControl(isCollectionView: false, collectionView: nil, tableView: self.tableView, refrence: self) {
                  
                  
                  self.getOrders()
                  
              }
        
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getOrders()
        self.backgroundColorOrImageSetting(view: self.view, imageView: self.bgImage)
      
    }
    
    

    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func getOrders()
    {
        
        if UserDefaults.standard.value(forKey: USER_DATA) != nil
        {
            let params = ["CompanyID": "2", "NoVersion": "true" , "Type" : "user" , "UserID" : MSJSharedManager.getUser().UserID ?? ""]
            
            let url = BASE_URL + GET_ORDERS
            ApiManager.getRequest(urlString: url, isAlertShow: true, parameters: params, successCallback:
                
                {(dict) in
                    
                    
                    print(dict)
                    self.endRefreshing()
                    self.orderArray.removeAll()
                    self.orderArray = MSJOrderStatusObject.parseData(response: dict)
                    self.tableView.reloadData()
                    
                    
                    
                    
                    
                    
            }) { (error) in
                
                 self.endRefreshing()
                print(error)
            }
            
            
        }
    }
    
}

extension MSJOrderVC : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.orderArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell =  tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MSJOrderStatusCell
        
        
        cell.mainView.layer.cornerRadius = 12
        cell.mainView.clipsToBounds = true
        
        cell.cellDelegate = self
        cell.index1 = indexPath
        
        
        cell.outerImage.layer.cornerRadius =  cell.outerImage.frame.size.height / 2
        cell.outerImage.clipsToBounds = true
        
        cell.thirdOrderProgress.layer.cornerRadius =  cell.thirdOrderProgress.frame.size.height / 2
        cell.thirdOrderProgress.clipsToBounds = true
        
        cell.firstOrderProgress.layer.cornerRadius =  cell.firstOrderProgress.frame.size.height / 2
        cell.firstOrderProgress.clipsToBounds = true
        
        cell.secondOrderProgress.layer.cornerRadius =  cell.secondOrderProgress.frame.size.height / 2
        cell.secondOrderProgress.clipsToBounds = true
        
        cell.fullOrderProgress.layer.cornerRadius =  cell.fullOrderProgress.frame.size.height / 2
        cell.fullOrderProgress.clipsToBounds = true
        
        cell.fullOrderProgress.backgroundColor = .lightGray
        cell.labelOrderStatusDelivered.textColor = self.hexStringToUIColors(hex: MSJSharedManager.getAppSetting().AlertPositiveButtonColor)
        
        
        cell.firstOrderProgress.backgroundColor = self.hexStringToUIColors(hex: MSJSharedManager.getAppSetting().AlertPositiveButtonColor)
        
        cell.secondOrderProgress.backgroundColor = self.hexStringToUIColors(hex: MSJSharedManager.getAppSetting().AlertPositiveButtonColor)
        
        cell.thirdOrderProgress.backgroundColor = self.hexStringToUIColors(hex: MSJSharedManager.getAppSetting().AlertPositiveButtonColor)
        
        let object = self.orderArray[indexPath.row]
        
        
        
        cell.labelOrderNumber.text = object.OrderNumber
        cell.labelItemCount.text = "1"
        cell.labelTotalBillPrice.text = "= " + object.TotalAmount + " " + "SAR"
        
        if object.ReturnedReason == ""{
            
            if object.AssignedDriverName != ""
            {
                cell.labelAssignToName.text = object.AssignedDriverName
            }
            else
            {
                cell.labelAssignToName.text = "N/A"
            }
        }
        else{
            if object.AssignedDriverName != ""
            {
                cell.labelAssignToName.text = object.AssignedDriverName
            }
            else
            {
                cell.labelAssignToName.text = "N/A"
            }
        }
        
        
        cell.firstOrderProgress.isHidden = true
        cell.secondOrderProgress.isHidden = true
        cell.thirdOrderProgress.isHidden = true
        
       // pending,packed,dispatched,delivered,pos,cancelled,cancelled_not_collect,returned
        if object.OrderStatusEn == "pending"  &&  object.AssignedDriverName == ""
        {
            cell.firstOrderProgress.isHidden = false
        }
        else if object.OrderStatusEn == "packed"
        {
            cell.secondOrderProgress.isHidden = false
        }
        if object.OrderStatusEn == "dispatched"
        {
            cell.thirdOrderProgress.isHidden = false
        }
        if object.OrderStatusEn == "delivered"
        {
            cell.fullOrderProgress.backgroundColor = self.hexStringToUIColors(hex: MSJSharedManager.getAppSetting().AlertPositiveButtonColor)
            cell.labelOrderStatusDelivered.textColor = self.hexStringToUIColors(hex: MSJSharedManager.getAppSetting().AlertPositiveButtonColor)
        }
        
        if object.OrderStatusEn == "returned"
        {
            cell.fullOrderProgress.backgroundColor = UIColor.red
            cell.labelOrderStatusDelivered.textColor = UIColor.red
        }
        
        
        if object.ReturnedReason == ""{
            
//            if object.driver_profile_pic != ""
//            {
//                pinRemoteImg(url: IMG_BASE_URL + object.driver_profile_pic!, imgView: cell.driverImage, placeholder: USER_IMAGE_PLACEHOLDER_IMAGE)
//            }
//            else
//            {
                cell.driverImage.image = USER_IMAGE_PLACEHOLDER_IMAGE
           // }
            
        }
        else{
            
            
//            if object.return_driver_pic != ""
//            {
//                pinRemoteImg(url: IMG_BASE_URL + object.return_driver_pic!, imgView: cell.driverImage, placeholder: USER_IMAGE_PLACEHOLDER_IMAGE)
//            }
//            else
//            {
                cell.driverImage.image = USER_IMAGE_PLACEHOLDER_IMAGE
           // }
            
        }
        
        
        cell.labelOrderStatusPlaced.text = "Placed".localized()
        cell.labelOrderStatusCaptainAssigned.text = "Captain Assigned".localized()
        cell.labelOrderStatusPicked.text = "Picked".localized()
        
        if object.OrderStatusEn == "returned"{
            
            cell.labelOrderStatusDelivered.text = "Returned".localized()
        }
        else{
            cell.labelOrderStatusDelivered.text = "Delivered".localized()
        }
        
        cell.labelOrderNumberTitle.text = "Order No".localized()
        cell.labelItemTitle.text = "Items".localized()
        cell.labelTotalBill.text = "Total Bill".localized()
        cell.labelAssignTo.text = "Assigned To".localized()
        
        
        if MSJSharedManager.getArabic()
        {
            cell.labelOrderStatusPlaced.textAlignment = .right
            cell.labelOrderStatusDelivered.textAlignment = .left
            cell.labelTotalBill.textAlignment = .left
            cell.labelTotalBillPrice.textAlignment = .left
            
        }
        else
        {
            cell.labelOrderStatusPlaced.textAlignment = .left
            cell.labelOrderStatusDelivered.textAlignment = .right
            cell.labelTotalBill.textAlignment = .right
            cell.labelTotalBillPrice.textAlignment = .right
        }
        
        
        
        
        return cell
    }
}


//extension MSJOrderVC : UITableViewDelegate
//{
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "MSJOrderDetailVC") as! MSJOrderDetailVC
//        vc.object = self.orderArray[indexPath.row]
//        self.navigationController?.pushViewController(vc, animated: true)
//    }
//
//}
extension MSJOrderVC : OrderStatusDelegate
{
    
    
    func openDetail(button: UIButton, index1: Int) {
        
        threeDotDropDown.backgroundColor = UIColor.white
        
        threeDotDropDown.anchorView = button
        threeDotDropDown.direction = .bottom
        
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        
        if orderArray[index1].OrderStatusEn == "delivered"{
            
            threeDotDropDown.dataSource = [
                "Order Details","Return Order"
                
            ]
        }
        else{
            threeDotDropDown.dataSource = [
                "Order Details"
                
            ]
            
        }
        
        // Action triggered on selection
        
        
        threeDotDropDown.selectionAction = {  (index, item) in
            //self?.rightBarButton.setTitle(item, for: .normal)
            
            if index == 0 {
                
                print(index)
                
                        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "MSJOrderDetailVC") as! MSJOrderDetailVC
                vc.object = self.orderArray[index1]
                        self.navigationController?.pushViewController(vc, animated: true)
                
            }
            
            
            else{
                
                print(index)
                
                let vc  = self.storyboard?.instantiateViewController(withIdentifier: "ReturnViewController") as! ReturnViewController
                vc.order = self.orderArray[index1]
                        self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        threeDotDropDown.show()
    }
    

    
    

    
}

//
//  MSJOrderDetailVC.swift
//  MSJ
//
//  Created by Muhammad Salman on 17/06/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import Alamofire

class MSJOrderDetailVC: BaseVC {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var labelOrderNumber: MSJBoldLabel!
    @IBOutlet weak var labelOrderNumberTitle: MSJBoldLabel!
    @IBOutlet weak var bgImage: UIImageView!
    
    var object = MSJOrderStatusObject()
    var orderDetailObj = OrderDetailsModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.backButtonOnNavigationBar()
        self.labelOrderNumberTitle.text = "Order No".localized()
        
        mainView.layer.cornerRadius = 8
        mainView.clipsToBounds = true
        
        tableView.tableFooterView = UIView()
        
        self.getOrderDetails()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setTabbarToVisible(isVisible: false)
        
        self.backgroundColorOrImageSetting(view: self.view, imageView: self.bgImage)
        
    }
    
    func getOrderDetails() {
        
        let url = BASE_URL + GET_ORDER_DETAILS
        
        var parameters: [String: Any] = ["NoVersion": "true"]
        parameters["UserID"] = MSJSharedManager.getUser().UserID ?? ""
        parameters["OrderID"] = self.object.OrderID
        
        ApiManager.getRequest(urlString: url, isAlertShow: true, parameters: parameters) { response in
            
            print(response)
            
            if let orderData = response["order_data"] as? NSDictionary {
                
                self.orderDetailObj = OrderDetailsModel.parseOrderDetailData(dict: orderData)
                
                self.labelOrderNumber.text = self.orderDetailObj.orderInnerDetailObj.OrderNumber
                self.tableView.reloadData()
                
            }
            
        } errorCallBack: { error in
            print("Error")
        }

        
    }
    
}

extension MSJOrderDetailVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.orderDetailObj.orderItemsArray.count + 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! MSJOrderStatusCell
            
            cell.outerImage.layer.cornerRadius = cell.outerImage.layer.frame.size.height / 2
            cell.outerImage.clipsToBounds = true
            cell.driverImage.downLoadImageIntoImageView(url: IMG_BASE_URL + self.orderDetailObj.orderInnerDetailObj.DriverImage)
            cell.labelAssignToName.text = self.orderDetailObj.orderInnerDetailObj.AssignedDriverName
            cell.labelItemCount.text = String(describing: self.orderDetailObj.orderItemsArray.count)
            cell.labelTotalBillPrice.text = "= " + self.orderDetailObj.orderInnerDetailObj.TotalAmount + " SAR"
            
            cell.firstOrderProgress.isHidden = true
            cell.secondOrderProgress.isHidden = true
            cell.thirdOrderProgress.isHidden = true
            
            cell.fullOrderProgress.layer.cornerRadius = cell.fullOrderProgress.frame.height / 2
            
            if self.orderDetailObj.orderInnerDetailObj.OrderStatusEn == "Pending" {
                
                cell.firstOrderProgress.isHidden = false
                cell.firstOrderProgress.layer.cornerRadius = cell.firstOrderProgress.frame.height / 2
                
            } else if self.orderDetailObj.orderInnerDetailObj.OrderStatusEn == "Packed" {
                
                cell.secondOrderProgress.isHidden = false
                cell.secondOrderProgress.layer.cornerRadius = cell.secondOrderProgress.frame.height / 2
                
            } else if self.orderDetailObj.orderInnerDetailObj.OrderStatusEn == "Dispatched" {
                
                cell.thirdOrderProgress.isHidden = false
                cell.thirdOrderProgress.layer.cornerRadius = cell.thirdOrderProgress.frame.height / 2
            
            } else if self.orderDetailObj.orderInnerDetailObj.OrderStatusEn == "Delivered" {
                
                cell.labelOrderStatusDelivered.text = "Delivered".localized()
                
            } else if self.orderDetailObj.orderInnerDetailObj.OrderStatusEn == "Returned" {
                
                cell.labelOrderStatusDelivered.text = "Returned".localized()
                cell.fullOrderProgress.backgroundColor = UIColor.red
                cell.labelOrderStatusDelivered.textColor = UIColor.red
                
            } else {
                
                // nothing here for now
                
            }
            
            return cell
            
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell") as! MSJOrderStatusCell
            
            let singleObj = self.orderDetailObj.orderItemsArray[indexPath.row - 1]
            
            cell.labelProductTitle.text = singleObj.Title
            cell.labelProductPiece.text = singleObj.Quantity + " pc" + " x " + singleObj.Price + " SAR + " + singleObj.TotalTaxAmount + " SAR"
            cell.labelProductPrice.text = "= " + singleObj.TotalAmount + " SAR"
            
//            cell.productImage.downLoadImageIntoImageView(url: IMG_BASE_URL + image_url)
            
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

//extension MSJOrderDetailVC : UITableViewDataSource
//{
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 0
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        if indexPath.row == 0
//        {
//            let cell =  tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MSJOrderStatusCell
//            cell.mainView.layer.cornerRadius = 12
//            cell.mainView.clipsToBounds = true
//
//
//
//
//
//
//            cell.outerImage.layer.cornerRadius =  cell.outerImage.frame.size.height / 2
//            cell.outerImage.clipsToBounds = true
//
//            cell.thirdOrderProgress.layer.cornerRadius =  cell.thirdOrderProgress.frame.size.height / 2
//            cell.thirdOrderProgress.clipsToBounds = true
//
//            cell.firstOrderProgress.layer.cornerRadius =  cell.firstOrderProgress.frame.size.height / 2
//            cell.firstOrderProgress.clipsToBounds = true
//
//            cell.secondOrderProgress.layer.cornerRadius =  cell.secondOrderProgress.frame.size.height / 2
//            cell.secondOrderProgress.clipsToBounds = true
//
//            cell.fullOrderProgress.layer.cornerRadius =  cell.fullOrderProgress.frame.size.height / 2
//            cell.fullOrderProgress.clipsToBounds = true
//
//
//
//
//
//
//
//            if object.AssignedDriverName != ""
//            {
//                cell.labelAssignToName.text = object.AssignedDriverName
//            }
//            else
//            {
//                cell.labelAssignToName.text = "N/A"
//            }
//
//            cell.labelItemCount.text = ""
//            cell.labelTotalBillPrice.text = "= " + (object.TotalAmount ?? "0") + " " + "SAR"
//
//            cell.firstOrderProgress.isHidden = true
//            cell.secondOrderProgress.isHidden = true
//            cell.thirdOrderProgress.isHidden = true
//
//
//            if object.OrderStatusEn == "Received" &&  object.AssignedDriverName == ""
//            {
//                cell.orderStatusImage.image = UIImage.init(named: "01 placed")
//                cell.firstOrderProgress.isHidden = false
//            }
//            else if object.OrderStatusEn == "Received"
//            {
//
//                cell.orderStatusImage.image = UIImage.init(named: "02 driver assigned")
//
//                cell.secondOrderProgress.isHidden = false
//
//
//            }
//            if object.OrderStatusEn == "Dispatched"
//            {
//
//                cell.orderStatusImage.image = UIImage.init(named: "03 picked- on the way")
//                cell.thirdOrderProgress.isHidden = false
//            }
//            if object.OrderStatusEn == "Delivered"
//            {
//
//
//                cell.orderStatusImage.image = UIImage.init(named: "04 delivered")
//
//                cell.fullOrderProgress.backgroundColor = .clear
//            }
//
//
//            if object.OrderStatusEn == "Returned Order"
//            {
//                cell.fullOrderProgress.backgroundColor = UIColor.red
//                cell.labelOrderStatusDelivered.textColor = UIColor.red
//
//            }
//
//
//            if object.ReturnedReason == ""{
////                if object.driver_profile_pic != ""
////                {
////                    pinRemoteImg(url: IMG_BASE_URL + object.driver_profile_pic!, imgView: cell.driverImage, placeholder: USER_IMAGE_PLACEHOLDER_IMAGE)
////                }
////                else
////                {
//                    cell.driverImage.image = USER_IMAGE_PLACEHOLDER_IMAGE
//                }
//            }
//            else{
////                if object.return_driver_pic != ""
////                {
////                    pinRemoteImg(url: IMG_BASE_URL + object.return_driver_pic!, imgView: cell.driverImage, placeholder: USER_IMAGE_PLACEHOLDER_IMAGE)
////                }
////                else
////                {
//                    cell.driverImage.image = USER_IMAGE_PLACEHOLDER_IMAGE
//               // }
//            }
//
//            cell.labelOrderStatusPlaced.text = "Placed".localized()
//            cell.labelOrderStatusCaptainAssigned.text = "Captain Assigned".localized()
//            cell.labelOrderStatusPicked.text = "Picked".localized()
//
//            if object.OrderStatusEn == "Returned Order"{
//                cell.labelOrderStatusDelivered.text = "Returned".localized()
//            }
//            else{
//                cell.labelOrderStatusDelivered.text = "Delivered".localized()
//            }
//
//            cell.labelItemTitle.text = "Items".localized()
//            cell.labelTotalBill.text = "Total Bill".localized()
//            cell.labelAssignTo.text = "Assigned To".localized()
//
//
//            if MSJSharedManager.getArabic()
//            {
//                cell.labelOrderStatusPlaced.textAlignment = .right
//                cell.labelOrderStatusDelivered.textAlignment = .left
//                cell.labelTotalBill.textAlignment = .left
//                cell.labelTotalBillPrice.textAlignment = .left
//
//            }
//            else
//            {
//                cell.labelOrderStatusPlaced.textAlignment = .left
//                cell.labelOrderStatusDelivered.textAlignment = .right
//                cell.labelTotalBill.textAlignment = .right
//                cell.labelTotalBillPrice.textAlignment = .right
//            }
//
//
//
//            return cell
//        }
//        else
//        {
//            let cell =  tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath) as! MSJOrderStatusCell
//
//
//            cell.productImage.layer.cornerRadius = 12
//            cell.clipsToBounds = true
//
//            let object = self.object.orderItemsArray[indexPath.row - 1]
//            cell.labelProductTitle.text = object.product_title
//
//            var quantityString = (object.quantity ?? "0") + "Pcs"
//            quantityString = quantityString + " x " + (object.price ?? "0")
//            quantityString = quantityString + "SAR"
//
//            cell.labelProductPiece.text = quantityString
//
//            let totalPrice =  Float(object.quantity ?? "0")! * Float(object.price ?? "0")!
//
//            cell.labelProductPrice.text = "= " + String(describing:totalPrice) + " SAR"
//
//
//            if object.product_image != ""
//            {
//                pinRemoteImg(url: IMG_BASE_URL + object.product_image!, imgView: cell.productImage, placeholder: PLACEHOLDER_IMAGE)
//            }
//            else
//            {
//                cell.productImage.image = PLACEHOLDER_IMAGE
//            }
//
//
//
//
//
//            return cell
//
//        }
//    }
//}
//
//
//
//extension MSJOrderDetailVC : UITableViewDelegate
//{
//
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//
//        return 100
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//        return UITableView.automaticDimension
//    }
//
//
//}

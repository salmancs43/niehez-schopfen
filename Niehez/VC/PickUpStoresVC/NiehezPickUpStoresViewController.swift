//
//  NiehezPickUpStoresViewController.swift
//  Niehez
//
//  Created by Macbook on 10/05/2022.
//  Copyright © 2022 Mac. All rights reserved.
//

import UIKit
import AAPickerView

class NiehezPickUpStoresViewController: BaseVC {

    @IBOutlet weak var cityTextFieldOuterView: UIView!
    @IBOutlet weak var cityTextField: CustomAAPickerView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var bgView: UIView!
    
    var cityId = ""
    var citiesArray = [CitiesModel]()
    var storesArray = [StoresModel]()
    
    var userData = MSJSharedManager.getUser()
    
    var isPickUp = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tableFooterView = UIView()
        self.getCities()
        self.getShipmentMethod()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
         self.backButtonOnNavigationBar()
        
       
        self.setTabbarToVisible(isVisible: false)
        
        self.cityTextFieldOuterView.layer.cornerRadius = 20
        self.cityTextFieldOuterView.layer.masksToBounds = true
        
        self.backgroundColorOrImageSetting(view: self.view, imageView: self.bgImage)
    }
    
  
    
    func getCities() {
        let url = BASE_URL + GET_CITIES
        
        let parameters: [String:Any] = ["NoVersion":"true"]
        
        ApiManager.getRequest(urlString: url, isAlertShow: false, parameters: parameters) { dict in
            
            self.citiesArray = CitiesModel.parseData(response: dict)
            
            /// Cities Model Picker View
            var CitiesNameStrings = [String]()
            for cities in self.citiesArray {
                CitiesNameStrings.append(cities.Title)
//                if cities.Title == self.userData.CityTitle {
//                    self.cityId = cities.CityID
//                }
            }
            self.cityTextField.pickerType = .string(data: CitiesNameStrings)
            self.cityTextField.valueDidSelected = { data in
                self.cityTextField.text = CitiesNameStrings[data as! Int]
                if let foo = self.citiesArray.enumerated().first(where: {$0.element.Title == self.cityTextField.text}) {
                    self.cityId = foo.element.CityID
                    self.getStores()
                }
            }
            
        } errorCallBack: { errorString in
            print("Error")
        }
    }
    
    func getStores() {
        let url = BASE_URL + GET_STORES
        
        let parameters: [String:Any] = ["NoVersion":"true", "CityID": "1337", "CompanyID": "2"]
        
        ApiManager.getRequest(urlString: url, isAlertShow: true, parameters: parameters) { dict in
            print(dict)
            
            self.storesArray = StoresModel.parseData(response: dict)
            
            if self.storesArray.count == 0 {
                self.showAlert(title: "Alert!", message: "Stores not found")
            }
               
            self.tableView.reloadData()
            
                        
        } errorCallBack: { errorString in
            print("Error")
        }
    }
    
    func getShipmentMethod() {
        let url = BASE_URL + SHIPMENT_METHOD
        
        let parameters: [String:Any] = ["NoVersion":"true", "CompanyID": "2"]
        
        ApiManager.getRequest(urlString: url, isAlertShow: true, parameters: parameters) { dict in
            print(dict)
                        
        } errorCallBack: { errorString in
            print("Error")
        }
    }
    
    func getProductAvailability(index: Int) {
        
        let url = BASE_URL + GET_PRODUCT_AVAILABILITY
        
        let parameters: [String:Any] = ["NoVersion":"true", "StoreID": self.storesArray[index].StoreID, "UserID": MSJSharedManager.getUser().UserID ?? ""]
        
        ApiManager.getRequest(urlString: url, isAlertShow: true, parameters: parameters) { dict in
            print(dict)
            
            if let productsArray = dict["products"] as? NSArray {
                
                for item in productsArray
                {
                    if let dictionary = item as? NSDictionary
                    {
                        let IsAvailable = dictionary["IsAvailable"] as? Int ?? -1
                        
                        let message = dictionary["Message"] as? String ?? ""
                        
                        if IsAvailable == 1 {
                            if self.isPickUp {
                                self.twoButtonAlert(title: "Success", message: "Are you sure you want to place order?", btnTitle: "No", buttonTwo: "Yes") {
                                    self.placeOrder(StoreID: self.storesArray[index].StoreID)
                                }
                            } else {
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MSJAddressOneVC") as? MSJAddressOneVC ?? MSJAddressOneVC()
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        } else {
                            self.showAlert(title: "Alert", message: message)
                        }
                    }
                }
                
            }
                        
        } errorCallBack: { errorString in
            print("Error")
            self.showAlert(title: "Alert", message: errorString)
        }
    }
    
    func placeOrder(StoreID: String) {
        
        let params: [String: Any] = ["UserID": MSJSharedManager.getUser().UserID ?? "",
                                     "CompanyID": "2",
                                     "CollectFromStore": "1",
                                     "StoreID": StoreID,
//                                     "OrderCoupon": "FALSE",
                                     "PaymentMethodForBooking": "COD",
                                     "ShipmentMethodIDForBooking": "3"]

        
        ApiManager.postServiceCall(urlString: BASE_URL + POST_ORDER, isAlertShow: true, parameters: params, header: nil) { response in
            print(response)
            if let message = response["message"] as? String {
                self.showAlert(title: "Alert", message: message)
            
            }
        } errorCallBack: { error in
            print(error)
            self.showAlert(title: "Error", message: error)
        }

    }

}

extension NiehezPickUpStoresViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.storesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! NiehezStoresTableViewCell
        
        cell.mainContainer.layer.cornerRadius = 8
        cell.mainContainer.clipsToBounds = true
        
        cell.lblStoresTitles.text = self.storesArray[indexPath.row].Title
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.getProductAvailability(index: indexPath.row)
    }
}

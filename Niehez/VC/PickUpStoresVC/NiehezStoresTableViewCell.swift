//
//  NiehezStoresTableViewCell.swift
//  Niehez
//
//  Created by Macbook on 12/05/2022.
//  Copyright © 2022 Mac. All rights reserved.
//

import UIKit

class NiehezStoresTableViewCell: UITableViewCell {

    @IBOutlet var mainContainer: UIView!
    @IBOutlet weak var lblStoresTitles: MSJRegularLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

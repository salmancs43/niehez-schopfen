//
//  MSJAddressOneVC.swift
//  MSJ
//
//  Created by Mac on 1/5/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class MSJAddressOneVC: BaseVC,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lableAddressCount: UILabel!
    
    @IBOutlet weak var labelAddNewAddressBottom: MSJBoldLabel!
    @IBOutlet weak var downAddressButtonView: UIView!
    
    @IBOutlet weak var labelDeliverTo: MSJBoldLabel!
    var addressListArray = [UserAddressModel]()
    var selectedIndex: Int = 0
    var totalPrice: Float = 0
    var totalItems: Int = 0
    
    var selectedButtonShow : Bool = false
    
    var isLanguageChange : Bool = false
    
//    var initialSetupViewController: PTFWInitialSetupViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.isHidden = true
        
        downAddressButtonView.layer.borderColor = UIColor(red: 70/255, green: 172/255, blue: 88/255, alpha: 1.0).cgColor
        downAddressButtonView.layer.borderWidth = 1.0
        
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = UITableView.automaticDimension
        
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonOnNavigationBar()
        
        getAddress()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.addressListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : MSJAddressOneCell
        
        
        
        
        cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MSJAddressOneCell
        cell.mainView.layer.cornerRadius = 20
        
        
        let addressObj = self.addressListArray[indexPath.row]
        print(addressObj)
        cell.lableSelectThisAddress.text = "Select This Address".localized()
        cell.lableAddressTitle.text = "My Address ".localized() +  String(describing: indexPath.row+1)
        
        cell.lableDistrict.text =  "District".localized() + " " + (addressObj.DistrictTitle)
        
        
        cell.lableCity.text = "City".localized() + " " + (addressObj.CityTitle)
        cell.labelLocation.text = "Location".localized() + " " + (addressObj.Street)
        cell.labelBuildingNo.text = "Building No./Floor No./Appartment No.".localized() + " " + addressObj.BuildingNo
       
        cell.labelDeliveryCharges.text = "Delivery fees".localized() + " " + "100.0" + " " + "SAR"
     
        
        
        cell.buttonMore .addTarget(self,  action: #selector(buttonMore(button:)), for:.touchUpInside)
        cell.buttonMore.tag = indexPath.row
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        selectedIndex = indexPath.row
        
        let alert = UIAlertController(title: "My Address ".localized() +  String(describing: indexPath.row+1), message: "", preferredStyle: .actionSheet)
        
        
         let editAction = UIAlertAction(title: "Edit".localized(), style: .default, handler: handleEdit)
        let deleteAction = UIAlertAction(title: "Delete".localized(), style: .destructive, handler: handleDelete)
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: cancelDelete)
        
        alert.addAction(deleteAction)
        alert.addAction(editAction)
        alert.addAction(cancelAction)
          
        
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: 2.0, y: 2.0, width: self.view.bounds.size.width / 2.0, height: self.view.bounds.size.height / 2.0)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    
    func getAddress()
    {
        
        
        if UserDefaults.standard.value(forKey: USER_DATA) != nil
        {
            let parameters: [String:Any] = ["NoVersion":"true", "UserID": MSJSharedManager.getUser().UserID ?? ""]
            
            let url = BASE_URL + GET_ALL_CUST_ADDRESS
            ApiManager.getRequest(urlString: url, isAlertShow: true, parameters: parameters, successCallback: {(dict) in
                    
                    let addressList = dict["address_info"] as! NSArray
                    self.addressListArray.removeAll()
                    for item in addressList
                    {
                        let addressDict = item as! NSDictionary
                        self.addressListArray.append(UserAddressModel.parseUserAddressData(userDict: addressDict))
                        
                    }
                    self.setAddressLabel()
                    
                    
                    
                    
                    
            }) { (error) in
                
                print(error)
            }
            
            
        }
        
    }
    @IBAction func buttonAddAddress(_ sender: Any)
    {
        
        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "MSJAddAddress") as! MSJAddAddress
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //Mark - show activity
    
    @objc func buttonMore(button: UIButton)
    {
        
        selectedIndex = button.tag
        
        let alert = UIAlertController(title: "My Address ".localized() +  String(describing: button.tag+1), message: "", preferredStyle: .actionSheet)
        
        
        //let editAction = UIAlertAction(title: "Edit".localized(), style: .default, handler: handleEdit)
        let deleteAction = UIAlertAction(title: "Delete".localized(), style: .destructive, handler: handleDelete)
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: cancelDelete)
        
        alert.addAction(deleteAction)
        alert.addAction(cancelAction)
        //alert.addAction(editAction)
        
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: 2.0, y: 2.0, width: self.view.bounds.size.width / 2.0, height: self.view.bounds.size.height / 2.0)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    //Mark - show activity
    
    @objc func buttonSelectAddress(button: UIButton)
    {
        
        selectedIndex = button.tag
        
        let object = addressListArray[selectedIndex]
        let convertedDeliveryCharges: Float = 100.0//Float(object.district_delivery_charges) ?? 0.0
        let totalWithOutVat = totalPrice + convertedDeliveryCharges
        let calculateVAT = totalWithOutVat * 0.15
        let grandTotal = totalWithOutVat + calculateVAT
        
        
        
        var messageString = "Are you sure you want to coutinue?".localized()
        messageString = messageString + " " + "Your total bill after including delivery fees and VAT will be".localized() + " "
        messageString = messageString + String(format: "%.2f", grandTotal) + " " +  "SAR"
        
        self.twoButtonAlert(title: "Alert".localized(), message: messageString, btnTitle: "Cancel".localized(), buttonTwo: "Yes".localized(), successCallback: self.postOrder)
        
        
        
        
    }
    
    
    
    
    
    func handleDelete(alertAction: UIAlertAction!) -> Void
    {
        
        self.twoButtonAlert(title: "Alert".localized(), message: "Are you sure you want to delete?".localized(), btnTitle: "Cancel".localized(), buttonTwo: "Delete".localized(), successCallback: self.deletePressed)
        
    }
    
    func deletePressed()
    {
        
        let addressObj = self.addressListArray[selectedIndex]
        
        let url = BASE_URL + REMOVE_ADDRESS
        
        var addressDictionary = [String: Any]()
        
        addressDictionary["AddressID"] = addressObj.AddressID
        addressDictionary["UserID"] = addressObj.UserID
        addressDictionary["CompanyID"] = "2"
        
        ApiManager.postServiceCall(urlString: url, isAlertShow: true, parameters: addressDictionary, header: nil, successCallback:
            { (dict) in
                
                self.tableView.beginUpdates()
                self.addressListArray.remove(at: self.selectedIndex)
                
                let indexPath = NSIndexPath.init(row: self.selectedIndex, section: 0)
                
                self.tableView.deleteRows(at: [indexPath as IndexPath], with: .automatic)
                
                self.tableView.endUpdates()
                
                self.setAddressLabel()
                
                self.showAlert(title: "Success".localized(), message: "Address removed successfully".localized())
                
                
        }, errorCallBack: { (error) in
            
            self.showAlert(title: "Error".localized(), message: error)
        })
    }
    
    
    
    func handleEdit(alertAction: UIAlertAction!) -> Void
    {
        
        let addressObject = self.addressListArray[selectedIndex]
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MSJAddAddress") as! MSJAddAddress
        vc.addressObject = addressObject
        self.addressListArray.removeAll()
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func cancelDelete(alertAction: UIAlertAction!) -> Void
    {
        
    }
    
    func setAddressLabel()
    {
        
        labelAddNewAddressBottom.text = "Add new address".localized()
        
        var str : String = ""
        if selectedButtonShow == true
        {
            str = "My Bag".localized()
            labelDeliverTo.text = "Deliver To".localized()
        }
        else
        {
            str = "My Address"
            labelDeliverTo.text = "My Address"
        }
        if (self.addressListArray.count > 0)
        {
            
            self.tableView.isHidden = false
            self.setAttributedTitleForRightBar(complete: str, CompFont: UIFont.init(name:MSJSharedManager.boldFont(), size: 15.0)!, CompColor: MSJColor.TAB_BAR_BACKGROUND_COLOR, sub: "", subFont: UIFont.init(name: MSJSharedManager.mediumFont(), size: 15.0)!, SubColor: MSJColor.TAB_BAR_BACKGROUND_COLOR)
            self.lableAddressCount.isHidden = true
            self.tableView.reloadData()
            //            let strCount =  String(self.addressListArray.count) + " items"
            //
            //            self.lableAddressCount.attributedText = self.makeAttributedLable(complete: strCount, CompFont: UIFont.init(name: MSJCustomFont.HEAVY_FONT, size: 13.0)!, CompColor: UIColor.white, sub: "items", subFont: UIFont.init(name: MSJCustomFont.REGULAR_FONT, size: 13.0)!, SubColor: UIColor.white)
            
        }
        else
        {
            
            self.lableAddressCount.text = ""
            self.tableView.isHidden = false
            
        }
    }
    
    
    
    func openPayTabs()
    {
        
//        let object = addressListArray[selectedIndex]
//        let convertedDeliveryCharges: Float = 100.0//Float(object.district_delivery_charges) ?? 0.0
//        let totalWithOutVat = totalPrice + convertedDeliveryCharges
//        let calculateVAT = totalWithOutVat * 0.15
//        let grandTotal = totalWithOutVat + calculateVAT
//
//
//        //        if amount < 0
//        //        {
//        //            amount = 100
//        //
//        //        }
//        // self.bookingStatusUpdate(bookingStatus:String(describing: BOOKING_PAYMENT_SATUS) , bookingObject: object)
//        //        return
//        let bundle = Bundle(url: Bundle.main.url(forResource: "Resources", withExtension: "bundle")!)
//        self.initialSetupViewController = PTFWInitialSetupViewController.init(
//            bundle: bundle,
//            andWithViewFrame: self.view.frame,
//            andWithAmount: grandTotal,
//            andWithCustomerTitle: MSJSharedManager.getUser().FullName ?? "",
//            andWithCurrencyCode: "SAR",
//            andWithTaxAmount: 0,
//            andWithSDKLanguage: "en",
//            andWithShippingAddress: "Not Available",
//            andWithShippingCity: "Jeddah",
//            andWithShippingCountry: "SAU",
//            andWithShippingState: "Not Available",
//            andWithShippingZIPCode: "Not Available",
//            andWithBillingAddress: "Not Available",
//            andWithBillingCity: "Jeddah",
//            andWithBillingCountry: "SAU",
//            andWithBillingState: "Not Available",
//            andWithBillingZIPCode: "Not Available",
//            andWithOrderID: String(describing: Date().timeIntervalSince1970),
//            andWithPhoneNumber: MSJSharedManager.getUser().Mobile ?? "",
//            andWithCustomerEmail: MSJSharedManager.getUser().Email ?? "",
//            andIsTokenization: false,
//            andIsPreAuth: false,
//            andWithMerchantEmail: PAYTAB_EMAIL,
//            andWithMerchantSecretKey: PAYTAB_SECRETKEY,
//            andWithAssigneeCode: "SDK",
//            andWithThemeColor:UIColor.LIGHT_GREEN_COLOR,
//            andIsThemeColorLight: false)
//
//
//        self.initialSetupViewController.didReceiveBackButtonCallback = {
//
//        }
//
//        self.initialSetupViewController.didStartPreparePaymentPage = {
//            // Start Prepare Payment Page
//            MSJSharedManager.showHUD()
//
//
//            // Show loading indicator
//        }
//        self.initialSetupViewController.didFinishPreparePaymentPage = {
//            // Finish Prepare Payment Page
//
//            MSJSharedManager.hideHud()
//
//
//            // Stop loading indicator
//        }
//
//        self.initialSetupViewController.didReceiveFinishTransactionCallback = {(responseCode, result, transactionID, tokenizedCustomerEmail, tokenizedCustomerPassword, token, transactionState) in
//            print("Response Code: \(responseCode)")
//            print("Response Result: \(result)")
//
//            // In Case you are using tokenization
//            print("Tokenization Cutomer Email: \(tokenizedCustomerEmail)");
//            print("Tokenization Customer Password: \(tokenizedCustomerPassword)");
//            print("TOkenization Token: \(token)");
//            if responseCode == 100
//            {
//                //self.bookingStatusUpdate(bookingStatus:String(describing: BOOKING_PAYMENT_SATUS))
//
//                self.submitOrder(transactionId: String(describing:transactionID))
//            }
//
//        }
//
//        DispatchQueue.main.async {
//
//            self.view.addSubview(self.initialSetupViewController.view)
//            self.addChild(self.initialSetupViewController)
//
//            self.initialSetupViewController.didMove(toParent: self)
//        }
        
        
        // self.present(initialSetupViewController, animated: true, completion: nil)
        
    }
    
    
    
    func postOrder()
    {
        
        self.openPayTabs()
        
    }
    
    
    func submitOrder(transactionId:String)
    {
        if (UserDefaults.standard.value(forKey: USER_DATA) == nil)
        {
            
            let vc =  self.storyboard?.instantiateViewController(withIdentifier: "MSJLogInVC") as! MSJLogInVC
            
            self.navigationController?.pushViewController(vc, animated: true)
            return;
        }
        
        
        
        let object = addressListArray[selectedIndex]
        let convertedDeliveryCharges: Float = 100.0//Float(object.district_delivery_charges) ?? 0.0
        let totalWithOutVat = totalPrice + convertedDeliveryCharges
        let calculateVAT = totalWithOutVat * 0.15
        let grandTotal = totalWithOutVat + calculateVAT
        
        
        let addressObj = self.addressListArray[selectedIndex]
        let dateTimeDict = self.convertDateAndTimeIntoTimeStamp(date: NSDate.init())
        var cartDict = [String: Any]()
        cartDict["user_id"] =  self.customerId()
        cartDict["address_id"] = addressObj.AddressID
        cartDict["transaction_id"] = transactionId
        cartDict["total_amount"] = String(format: "%.2f", grandTotal)
        cartDict["total_items"] = String(describing: self.totalItems)
        
         cartDict["vat_total"] = String(format: "%.2f", calculateVAT)
         cartDict["delivery_charges"] = String(format: "%.2f", convertedDeliveryCharges)
        
        cartDict["order_date"] = dateTimeDict[TIMESTAMP] as? String
        
        
     
       
        print(cartDict)
        
        let url = BASE_URL + POST_ORDER
        MSJApiManager.sharedInstance.postServiceCall(urlString: url, isAlertShow: true, companyId: "", parameters: cartDict, header: nil, successCallback:
            { (dict) in
                
                print(dict)
                MSJSharedManager.sharedManager.cartArray.removeAll()
                
                self.alertWithSingleBackButtonFuction(title: "Success".localized(), message: "Order placed successfully".localized(), successCallback: self.goesBack)
                
                
                
        }, errorCallBack: { (error) in
            
            self.showAlert(title: "Error".localized(), message: error)
            
        })
    }
    
    func goesBack()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setLanguage()
    {
        
    }
    
    
    
    
}


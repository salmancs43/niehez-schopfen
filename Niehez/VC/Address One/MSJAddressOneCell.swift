//
//  MSJAddressOneCell.swift
//  MSJ
//
//  Created by Mac on 1/10/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class MSJAddressOneCell: UITableViewCell {

    
    @IBOutlet weak var labelLocation: MSJRegularLabel!
    @IBOutlet weak var mainView: MSJCellViewShadow!
   
    @IBOutlet weak var labelDeliveryCharges: MSJRegularLabel!
    @IBOutlet weak var buttonMore: UIButton!
    @IBOutlet weak var lableAddressTitle: MSJBoldLabel!

    @IBOutlet weak var lableDistrict: MSJRegularLabel!
   
    
    @IBOutlet weak var lableCity: MSJRegularLabel!
    
    @IBOutlet weak var labelBuildingNo: MSJRegularLabel!
 
    
    @IBOutlet weak var buttonSelectThisAddress: UIButton!
    @IBOutlet weak var lableSelectThisAddress: MSJBoldLabel!
    @IBOutlet weak var buttonView: MSJShadow!
    @IBOutlet weak var buttonView_Height: NSLayoutConstraint!
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

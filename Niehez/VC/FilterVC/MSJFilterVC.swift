//
//  MSJFilterVC.swift
//  MSJ
//
//  Created by Mac on 08/02/2018.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit


protocol filerdelegate
{
    func filter(isFilter : Bool)
}



class MSJFilterVC: BaseVC ,UITextFieldDelegate{

     var delegate : filerdelegate?
    var obj = MSJSharedManager.getAppSetting()
    
    @IBOutlet weak var textFieldBrand: MSJCustomPickerTextField!
    @IBOutlet weak var textFieldSubCategory: MSJCustomPickerTextField!
    @IBOutlet weak var textFieldCategories: MSJCustomPickerTextField!
    
    
    @IBOutlet weak var resetView: MSJButtonView!
    
    var subCategoryArray = Array<Any>()
    var subCategoryTitleArray = Array<Any>()
    
    var caterogyArray = Array<Any>()
     var caterogyTitleArray = Array<Any>()
    
    var brandArray = Array<Any>()
     var brandTitleArray = Array<Any>()
    
    @IBOutlet weak var buttonReset: MSJRegularButton!
    
    @IBOutlet weak var buttonApply: MSJRegularButton!
    
   
    
    
    var categoryId : String = "0"
    var subCategoryId : String = "0"
    var brandId : String = "0"
    
    var isLanguageChange : Bool = false
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
      
        
       
        
        resetView.layer.borderWidth = 1.0
        resetView.layer.borderColor = UIColor.lightGray.cgColor
        
        
        
        self.textFieldBrand.delegate = self
      //  self.textFieldBrand.pickerType = .string(data: <#T##[String]#>)
        
        
        
        
        
        
      
        
        
        self.textFieldCategories.delegate = self
       // self.textFieldCategories.pickerType = .StringPicker
        
        
      
        
        self.textFieldSubCategory.delegate = self
       // self.textFieldSubCategory.pickerType = .StringPicker
       
        
       
        
        
        textFieldCategories.text = MSJSharedManager.sharedManager.category
        textFieldBrand.text = MSJSharedManager.sharedManager.brand
        textFieldSubCategory.text = MSJSharedManager.sharedManager.subCategory
        
        
         textFieldSubCategory.isUserInteractionEnabled = false
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
         self.setTabbarToVisible(isVisible: false)
        
        setLanguage()
          self.backButtonOnNavigationBar()
        if isLanguageChange != MSJSharedManager.getArabic()
        {
            self.caterogyArray.removeAll()
            self.caterogyTitleArray.removeAll()
            
           
            self.brandArray.removeAll()
            self.brandTitleArray.removeAll()
            
        
            self.subCategoryArray.removeAll()
            self.subCategoryTitleArray.removeAll()
            
            self.getAllCategories()
            isLanguageChange = MSJSharedManager.getArabic()
        }
        else
        {
            if  self.brandTitleArray.count == 0 || self.caterogyTitleArray.count == 0
            {
                 self.getAllCategories()
            }
        }
        
    }

    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField .isEqual(textFieldSubCategory)
        {
//            if textFieldSubCategory.text == ""
//            {
            textFieldSubCategory.pickerType = .string(data: subCategoryTitleArray as? [String] ?? [""])
           
                textFieldSubCategory.pickerRow.font = UIFont(name: MSJSharedManager.regularFont(), size: 20)
                textFieldSubCategory.toolbar.barTintColor = .darkGray
                textFieldSubCategory.toolbar.tintColor = MSJColor.TAB_BAR_BACKGROUND_COLOR
           // }
        }
        else if textField .isEqual(textFieldCategories)
        {
//            if textFieldCategories.text == ""
//            {
            
            textFieldCategories.pickerType = .string(data: caterogyTitleArray as? [String] ?? [""])
            
            
               
            
                textFieldCategories.pickerRow.font = UIFont(name: MSJSharedManager.regularFont(), size: 20)
                textFieldCategories.toolbar.barTintColor = .darkGray
                textFieldCategories.toolbar.tintColor = MSJColor.TAB_BAR_BACKGROUND_COLOR
            //}
        }
        else if textField .isEqual(textFieldBrand)
        {
//            if textFieldBrand.text == ""
//            {
             textFieldBrand.pickerType = .string(data: brandTitleArray as? [String] ?? [""])
            
        
                textFieldBrand.pickerRow.font = UIFont(name:MSJSharedManager.regularFont(), size: 20)
                textFieldBrand.toolbar.barTintColor = .darkGray
                textFieldBrand.toolbar.tintColor = MSJColor.TAB_BAR_BACKGROUND_COLOR
            //}
        }
     
        
        
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if textField.isEqual(textFieldCategories)
        {
            textFieldSubCategory.isUserInteractionEnabled = false
            if self.whiteSpaceValidae(textfield: self.textFieldCategories.text!)==false
            {
                
            }
            else
            {
                textFieldSubCategory.text = ""
                self.getSubCategories()
            }
        }
    }

    func getAllCategories()
    {
        let url = BASE_URL + MAIN_CATEGORY
        
        self.caterogyArray.removeAll()
        self.caterogyTitleArray.removeAll()
        
        
        let parameters: [String:Any] = [COMPANY_ID:obj.CompanyID]
        MSJApiManager.sharedInstance.getRequest(urlString: url, isAlertShow: true, parameters: parameters, successCallback: { (dict) in
            let complaintList = dict["categories"] as! NSArray
            for item in complaintList{
                let itemDict = item as! NSDictionary
                let object = MSJFilterObject()
                object.id = itemDict["category_id"] as? String
                if MSJSharedManager.getArabic() == true
                {
                     object.title = itemDict["title_ar"] as? String
                }
                else
                {
                     object.title = itemDict["title_en"] as? String
                }

               
               
                self.caterogyArray.append(object)
                self.caterogyTitleArray.append(object.title!)
               
                
            }
            self.getBrands()
            
            if self.whiteSpaceValidae(textfield: self.textFieldCategories.text!)==false
            {
               
            }
            else
            {
                 self.getSubCategories()
            }
            
        }, errorCallBack: { (error) in
            print(error)
        })
        
        
    }
    
    func getBrands(){
        
        self.brandArray.removeAll()
        self.brandTitleArray.removeAll()
        
        
     
        let url = BASE_URL + GET_ALL_BRAND
        let parameters: [String:Any] = [COMPANY_ID:obj.CompanyID]
        
        MSJApiManager.sharedInstance.getRequest(urlString: url, isAlertShow: true, parameters: parameters, successCallback: { (dict) in
            let complaintList = dict["brands"] as! NSArray
            for item in complaintList{
                let itemDict = item as! NSDictionary
                 let object = MSJFilterObject()
                object.id = itemDict["brand_id"] as? String
               
                if MSJSharedManager.getArabic() == true
                {
                    object.title = itemDict["title_ar"] as? String
                }
                else
                {
                    object.title = itemDict["title_en"] as? String
                }
                self.brandArray.append(object)
                self.brandTitleArray.append(object.title!)
               
                
            }
            
          
            
        }, errorCallBack: { (error) in
            print(error)
        })
        
        
    }
    
    
    func getSubCategories()
    {
        self.subCategoryArray.removeAll()
        self.subCategoryTitleArray.removeAll()
        if self.caterogyArray.count > 0
        {
            for i in 0 ..< self.caterogyArray.count
            {
                let obj = self.caterogyArray[i] as! MSJFilterObject
                if obj.title == self.textFieldCategories.text
                {
                    categoryId = obj.id!
                }
            }
        }

        
        let url = BASE_URL + GET_ALL_SUBCATEGORIES + "?parent_id=" +  categoryId
        
        let parameters: [String:Any] = [COMPANY_ID:obj.CompanyID]
        
        MSJApiManager.sharedInstance.getRequest(urlString: url, isAlertShow: true, parameters: parameters, successCallback: { (dict) in
            let complaintList = dict["categories"] as! NSArray
            for item in complaintList{
                let itemDict = item as! NSDictionary
                let object = MSJFilterObject()
                object.id = itemDict["category_id"] as? String
                if MSJSharedManager.getArabic() == true
                {
                    object.title = itemDict["title_ar"] as? String
                }
                else
                {
                    object.title = itemDict["title_en"] as? String
                }
                self.subCategoryArray.append(object)
                self.subCategoryTitleArray.append(object.title!)
               
              
            }
            
            self.textFieldSubCategory.isUserInteractionEnabled = true
       
          
            
            
            
        }, errorCallBack: { (error) in
            print(error)
        })
        
        
    }

    @IBAction func applyButton(_ sender: Any)
    {
        MSJSharedManager.setBrand(brand: textFieldBrand.text!)
        MSJSharedManager.setCategory(cat: textFieldCategories.text!)
        MSJSharedManager.setSubCategory(subCat: textFieldSubCategory.text!)
        
        
        if self.caterogyArray.count > 0
        {
            for i in 0 ..< self.caterogyArray.count
            {
                let obj = self.caterogyArray[i] as! MSJFilterObject
                if obj.title == self.textFieldCategories.text
                {
                    categoryId = obj.id!
                }
            }
        }
        
        
        if self.subCategoryArray.count > 0
        {
            for i in 0 ..< self.subCategoryArray.count
            {
                let obj = self.subCategoryArray[i] as! MSJFilterObject
                if obj.title == self.textFieldSubCategory.text
                {
                    subCategoryId = obj.id!
                }
            }
        }
        
        
        
        if self.brandArray.count > 0
        {
            for i in 0 ..< self.brandArray.count
            {
                let obj = self.brandArray[i] as! MSJFilterObject
                if obj.title == self.textFieldBrand.text
                {
                    brandId = obj.id!
                }
            }
        }

        if categoryId != "0" || subCategoryId != "0" || brandId != "0"
        {
            if categoryId != "0"
            {
                  MSJSharedManager.sharedManager.url = BASE_URL + FILTER + "?category_id=" +  String(describing: self.categoryId)
            }
          
            if subCategoryId != "0"
            {
                 MSJSharedManager.sharedManager.url =  MSJSharedManager.sharedManager.url! + "&subcategory_id=" +  String(describing: self.subCategoryId)
            }
            
            if brandId != "0"
            {
                MSJSharedManager.sharedManager.url =  MSJSharedManager.sharedManager.url! + "&brand_id=" +  String(describing: self.brandId)
            }
           
            
           
        }
        
         self.delegate?.filter(isFilter: true)
         self.navigationController?.popViewController(animated: true)
       
        
       
    }
    @IBAction func resetButton(_ sender: Any)
    {
        MSJSharedManager.setBrand(brand: "")
        MSJSharedManager.setCategory(cat: "")
        MSJSharedManager.setSubCategory(subCat: "")
        textFieldBrand.text = ""
        textFieldCategories.text = ""
        textFieldSubCategory.text = ""
        MSJSharedManager.sharedManager.url = ""
         self.delegate?.filter(isFilter: true)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func setLanguage()
    {
        
        
        if MSJSharedManager.getArabic()
        {
            
          
            textFieldCategories.textAlignment = NSTextAlignment.right
            textFieldBrand.textAlignment = NSTextAlignment.right
            textFieldSubCategory.textAlignment = NSTextAlignment.right
          
//        
//            let paddingView: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
//            let dropDownImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 18, height: 18))
//            dropDownImageView.image = UIImage(named: DROP_DOWN_ICON)
//            dropDownImageView.contentMode = UIViewContentMode.scaleAspectFit
//            paddingView.addSubview(dropDownImageView)
//            
//            textFieldBrand.leftView = paddingView
//            textFieldBrand.leftViewMode = .always
//            
//             textFieldBrand.rightView = nil
//            
//            
//            
//            
//            let paddingViewCategory: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
//            let dropDownImageViewCategory = UIImageView(frame: CGRect(x: 0, y: 0, width: 18, height: 18))
//            dropDownImageViewCategory.image = UIImage(named: DROP_DOWN_ICON)
//            dropDownImageViewCategory.contentMode = UIViewContentMode.scaleAspectFit
//            paddingViewCategory.addSubview(dropDownImageViewCategory)
//            
//            textFieldCategories.leftView = paddingViewCategory
//            textFieldCategories.leftViewMode = .always
//            
//             textFieldCategories.rightView = nil
//            
//            let paddingViewSubCategory: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
//            let dropDownImageViewSubCategory = UIImageView(frame: CGRect(x: 0, y: 0, width: 18, height: 18))
//            dropDownImageViewSubCategory.image = UIImage(named: DROP_DOWN_ICON)
//            dropDownImageViewSubCategory.contentMode = UIViewContentMode.scaleAspectFit
//            paddingViewSubCategory.addSubview(dropDownImageViewSubCategory)
//            
//            textFieldSubCategory.leftView = paddingViewSubCategory
//            textFieldSubCategory.leftViewMode = .always
//             textFieldSubCategory.rightView = nil
        }
        else
        {
            textFieldCategories.textAlignment = NSTextAlignment.left
            textFieldBrand.textAlignment = NSTextAlignment.left
            textFieldSubCategory.textAlignment = NSTextAlignment.left
            
    
        }
        
        
        let paddingView: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let dropDownImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 18, height: 18))
        dropDownImageView.image = UIImage(named: DROP_DOWN_ICON)
        dropDownImageView.contentMode = UIView.ContentMode.scaleAspectFit
        paddingView.addSubview(dropDownImageView)
        
        textFieldBrand.rightView = paddingView
        textFieldBrand.rightViewMode = .always
        textFieldBrand.leftView = nil
        
        
        
        
        let paddingViewCategory: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let dropDownImageViewCategory = UIImageView(frame: CGRect(x: 0, y: 0, width: 18, height: 18))
        dropDownImageViewCategory.image = UIImage(named: DROP_DOWN_ICON)
        dropDownImageViewCategory.contentMode = UIView.ContentMode.scaleAspectFit
        paddingViewCategory.addSubview(dropDownImageViewCategory)
        
        textFieldCategories.rightView = paddingViewCategory
        textFieldCategories.rightViewMode = .always
        textFieldCategories.leftView = nil
        
        
        let paddingViewSubCategory: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let dropDownImageViewSubCategory = UIImageView(frame: CGRect(x: 0, y: 0, width: 18, height: 18))
        dropDownImageViewSubCategory.image = UIImage(named: DROP_DOWN_ICON)
        dropDownImageViewSubCategory.contentMode = UIView.ContentMode.scaleAspectFit
        paddingViewSubCategory.addSubview(dropDownImageViewSubCategory)
        
        textFieldSubCategory.rightView = paddingViewSubCategory
        textFieldSubCategory.rightViewMode = .always
        textFieldSubCategory.leftView = nil
        
        textFieldCategories.placeholder = "Categories".localized()
        textFieldSubCategory.placeholder = "Sub Categories".localized()
        textFieldBrand.placeholder = "Brand".localized()
       
        buttonApply.setTitle("Apply".localized(), for: UIControl.State.normal)
        buttonReset.setTitle("Reset".localized(), for: UIControl.State.normal)
        
         self.setAttributedTitleForRightBar(complete: "Filter".localized(), CompFont: UIFont.init(name: MSJSharedManager.regularFont(), size: 15.0)!, CompColor: MSJColor.TAB_BAR_BACKGROUND_COLOR, sub: "Filter".localized(), subFont: UIFont.init(name: MSJSharedManager.boldFont(), size: 15.0)!, SubColor: MSJColor.TAB_BAR_BACKGROUND_COLOR)
        
         
    }
    
}

//
//  MSJRegisterVC.swift
//  MSJ
//
//  Created by Mac on 20/12/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit
import Firebase

class MSJRegisterVC: BaseVC , UITextFieldDelegate {
    
    
    @IBOutlet weak var textFieldEmail: MSJCustomTextField!
    
    @IBOutlet weak var textFieldFullName: MSJCustomTextField!
    
    @IBOutlet weak var textFieldPhone: MSJCustomTextField!
    
    @IBOutlet weak var textFieldCity: MSJCustomPickerTextField!
    
    
    
    @IBOutlet weak var textFieldPassword: MSJCustomTextField!
    
    @IBOutlet weak var textFieldConfirmPassword: MSJCustomTextField!
    
    @IBOutlet weak var buttonMale: UIButton!
    @IBOutlet weak var buttonFemale: UIButton!
    @IBOutlet weak var labelGender: MSJBoldLabel!
    
    @IBOutlet weak var labelTitleSignUp: MSJBoldLabel!
    @IBOutlet weak var buttonSignUp: MSJRegularButton!
    let img = "cart_blue"
    
    @IBOutlet weak var labelFemale: MSJRegularLabel!
    @IBOutlet weak var labelMale: MSJRegularLabel!
    
    
    
    var citiesArray = [Cities]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        let image : UIImage = UIImage(named: "Logo.png")!
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        imageView.contentMode = .scaleAspectFit
        imageView.image = image
        self.navigationItem.titleView = imageView
        let widthConstraint = imageView.widthAnchor.constraint(equalToConstant:110)
        let heightConstraint = imageView.heightAnchor.constraint(equalToConstant: 30)
        heightConstraint.isActive = true
        widthConstraint.isActive = true
        
        
        getCities()
        
        
        
        
        //        buttonMale.isSelected = true
        
        //   textFieldPhone.text = "+966581057443"
        
        //textFieldEmail.keyboardType = UIKeyboardType.emailAddress
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonOnNavigationBar()
        setLanguage()
        
        
        
    }
    
    
    //Api Call
    
    func getCities()
    {
        
        
        
        MSJApiManager.sharedInstance.getRequest(urlString: BASE_URL + CITIES, isAlertShow: true, parameters: nil, successCallback: { (response) in
            
            
            self.citiesArray = Cities.parseData(response: response)
            self.configPickers()
            
        }) { (error) in
            
            self.showAlert(title: "Error".localized(), message: error)
        }
        
        
    }
    
    
    func configPickers() {
        
        
        
        
        
        // textFieldCity
        
        var cityTitleArray = [String]()
        for item in self.citiesArray
        {
            cityTitleArray.append(item.title)
        }
        self.textFieldCity.pickerType = .string(data: cityTitleArray)
        self.textFieldCity.pickerRow.font = UIFont(name: MSJSharedManager.mediumFont(), size: 14)
        
        textFieldCity.toolbar.barTintColor = .darkGray
        textFieldCity.toolbar.tintColor = .darkGray
        self.textFieldCity.valueDidSelected = { (index) in
            
            
            
            
        }
        
        
        
        
    }
    
    
    //Mark - textfield delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField.isEqual(textFieldFullName)
        {
            if string == ""
            {
                
            }
            else
            {
                if textFieldFullName.text!.count > FULLNAME_LENGTH
                {
                    return false
                }
            }
            
        }
        else if textField.isEqual(textFieldPhone)
        {
            let set = CharacterSet(charactersIn: "0123456789")
            if string.rangeOfCharacter(from: set.inverted) != nil
            {
                return false
            }
            if string == ""
            {
                
            }
            else
            {
                
                
            }
        }
        //        else if textField.isEqual(textFieldUserName)
        //        {
        //            let set = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyz1234567890_.@0123456789")
        //            if string.rangeOfCharacter(from: set.inverted) != nil
        //            {
        //                return false
        //            }
        //            if string == ""
        //            {
        //
        //            }
        //            else
        //            {
        //                if (textFieldUserName.text!.count > 12)
        //                {
        //
        //                    return false
        //
        //                }
        //
        //            }
        //
        //        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    
    @IBAction func buttonGender(_ sender: Any)
    {
        let button = sender as! UIButton
        if button.tag == 10
        {
            buttonMale.isSelected = true
            buttonFemale.isSelected = false
        }
        else
        {
            buttonMale.isSelected = false
            buttonFemale.isSelected = true
        }
    }
    
    
    @IBAction func buttonSignUp(_ sender: Any)
    {
        var dataDictionary = [String: String]()
        if self.whiteSpaceValidae(textfield: self.textFieldFullName.text!)==false
        {
            self.showAlert(title: "Error".localized(), message: "Enter your name".localized())
            
            
        }
        else if self.isValidEmail(testStr: self.textFieldEmail.text!)==false
        {
            self.showAlert(title: "Error".localized(), message: "Enter valid email".localized())
            
            
        }
        else if self.whiteSpaceValidae(textfield: self.textFieldPhone.text!)==false
        {
            self.showAlert(title: "Error".localized(), message: "Enter valid phone e.g 05xxxxxxx".localized())
            
            
        }
            
        else if self.phoneValidate(value: self.textFieldPhone.text!)==false
        {
            self.showAlert(title: "Error".localized(), message: "Enter valid phone e.g 05xxxxxxx".localized())
            
            
        }
        else if self.whiteSpaceValidae(textfield: self.textFieldCity.text!)==false
        {
            self.showAlert(title: "Error".localized(), message: "Enter your city".localized())
            
            
        }
            
        else if self.whiteSpaceValidae(textfield: self.textFieldPassword.text!)==false
        {
            self.showAlert(title: "Error".localized(), message: "Enter password".localized())
            
            
        }
        else if (self.textFieldPassword.text!.count < 8) || (self.textFieldPassword.text!.count > 16)
        {
            
            self.showAlert(title: "Error".localized(), message: "Pasword should  contains 8 to 16 characters".localized())
            
            
        }
            
        else if self.whiteSpaceValidae(textfield: self.textFieldConfirmPassword.text!)==false
        {
            self.showAlert(title: "Error".localized(), message: "Enter confirm Password".localized())
            
            
        }else if self.textFieldPassword.text != self.textFieldConfirmPassword.text
        {
            self.showAlert(title: "Error".localized(), message: "Password mismatch".localized())
            
            
            
        }
        else
        {
            
            
            
            
            let outputPhone =   "966" + String(describing: self.textFieldPhone.text!.dropFirst())
            
            
            
            let phone = outputPhone
            dataDictionary["full_name"] = self.textFieldFullName.text
            dataDictionary["email"] = self.textFieldEmail.text
            dataDictionary["phone"] = phone
            dataDictionary["password"] = self.textFieldPassword.text
            dataDictionary["device_type"] = "ios"
            dataDictionary["device_token"] = MSJSharedManager.getFCMTokenFromUserDefaults()
            dataDictionary["role_id"] = "3"
            
            if buttonMale.isSelected
            {
                dataDictionary["gender"] = "male"
            }
            else if buttonFemale.isSelected
            {
                dataDictionary["gender"] = "female"
            }
            
            
            if let object = citiesArray.filter({ $0.title == textFieldCity.text }).first {
                
                dataDictionary["city_id"] = String(describing:object.id)
                
            } else {
                print("not found")
            }
            
            
            if UserDefaults.standard.value(forKey: WITHOUT_LOGIN_ID) != nil
            {
                let tempId = String(describing:UserDefaults.standard.value(forKey: WITHOUT_LOGIN_ID)!)
                dataDictionary["temp_order_key"] = tempId
            }
            
            
            buttonSignUp.startAnimation()
            let qualityOfServiceClass = DispatchQoS.QoSClass.background
            let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
            backgroundQueue.async(execute: {
                
                
                
                let url = BASE_URL + POST_USER_SIGN_UP
                
                MSJApiManager.sharedInstance.postServiceCall(urlString: url, isAlertShow: false, companyId: "", parameters: dataDictionary, header: nil, successCallback: { (dict) in
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    self.buttonSignUp.stopAnimation(animationStyle: .expand, completion:
                        {
                            
                            
                            self.removeWithoutLoginId()
                            let userObject:MSJUser = MSJUser.parseDataOfUserInfo(userDict: dict )
                            MSJSharedManager.setUser(userObject: userObject)
                            if userObject.IsMobileVerified == "0"
                            {
                                
                                
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MSJVerifyPhoneVC") as! MSJVerifyPhoneVC
                                vc.isComingFromLogin = false
                                self.navigationController?.pushViewController(vc, animated: true)
                                
                            }
                            else
                            {
                                
                                
                                
                                
                                self.saveCustomerData(dict: dict)
                                self.getCartCount()
                                self.navigationController!.popToRootViewController(animated: true);
                            }
                            
                            
                            
                            //                            let viewControllers: [UIViewController] = self.navigationController!.viewControllers ;
                            //                            for aViewController in viewControllers {
                            //                                if(aViewController is MSJComplaintVC){
                            //                                    self.navigationController!.popToViewController(aViewController, animated: true);
                            //                                }
                            //                            }
                            
                            
                    })
                }) { (error) in
                    print(error)
                    self.showAlert(title: "Error", message: error)
                    self.buttonSignUp.stopAnimation(animationStyle: .expand, completion:
                        {
                            
                    })
                }
            })
        }
    }
    
    
    func setLanguage()
    {
        if MSJSharedManager.getArabic()
        {
            
            textFieldEmail.textAlignment = NSTextAlignment.right
            
            textFieldFullName.textAlignment = NSTextAlignment.right
            textFieldPhone.textAlignment = NSTextAlignment.right
            textFieldCity.textAlignment = NSTextAlignment.right
            
            textFieldPassword.textAlignment = NSTextAlignment.right
            textFieldConfirmPassword.textAlignment = NSTextAlignment.right
            
        }
        else
        {
            
            
            textFieldEmail.textAlignment = NSTextAlignment.left
            
            textFieldFullName.textAlignment = NSTextAlignment.left
            textFieldPhone.textAlignment = NSTextAlignment.left
            textFieldCity.textAlignment = NSTextAlignment.left
            
            textFieldPassword.textAlignment = NSTextAlignment.left
            textFieldConfirmPassword.textAlignment = NSTextAlignment.left
            
        }
        
        textFieldCity.addLeftAndRightView()
        
        
        textFieldEmail.placeholder = "Email".localized()
        
        textFieldFullName.placeholder = "Full Name".localized()
        textFieldPhone.placeholder = "Phone".localized()
        textFieldCity.placeholder = "City".localized()
        
        textFieldPassword.placeholder = "Password".localized()
        textFieldConfirmPassword.placeholder = "Confirm Password".localized()
        labelTitleSignUp.text = "Sign Up".localized()
        labelGender.text = "Gender".localized()
        buttonSignUp.setTitle("Sign Up".localized(), for: UIControl.State.normal)
        labelMale.text = "Male".localized()
        labelFemale.text = "Female".localized()
        
    }
    
}

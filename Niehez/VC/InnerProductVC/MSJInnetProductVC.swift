//
//  MSJInnetProductVC.swift
//  MSJ
//
//  Created by Mac on 08/11/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit
import AnimatableReload
class MSJInnetProductVC: BaseVC , UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout , PinterestLayoutDelegate, UIScrollViewDelegate
{
    
    var categoryObject = CategoryModel()
    var subCategoryObject = CategoryModel()
    var subCategoryProductArray = [ProductModel]()
    var obj = MSJSharedManager.getAppSetting()
    
    var pageNo = 1
    var pagination = true
    
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var mainProductName:String!
    var subCatProductName:String!
    
    var isLanguageChange : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        
        self.setAttributedTitleForRightBar(complete: subCategoryObject.Title, CompFont: UIFont.init(name: MSJSharedManager.regularFont(), size: 15.0)!, CompColor: .white, sub: subCategoryObject.Title.localized(), subFont: UIFont.init(name: MSJSharedManager.boldFont(), size: 15.0)!, SubColor: .white)
        
        
        if let layout = collectionView?.collectionViewLayout as? PinterestLayout {
            layout.delegate = self
        }
        
        self.backgroundColorOrImageSetting(view: self.view, imageView: self.bgImage)
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonOnNavigationBar()
        self.setTabbarToVisible(isVisible: false)
        setLanguage()
        if isLanguageChange != MSJSharedManager.getArabic()
        {
            self.subCategoryProductArray.removeAll()
            self.getSubCategoryProduct()
            isLanguageChange = MSJSharedManager.getArabic()
        }
        else
        {
            if self.subCategoryProductArray.count == 0
            {
                self.getSubCategoryProduct()
            }
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        
        return self.subCategoryProductArray.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let cell = collectionView .dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! MSJProductCell
        
        
        let productObject = self.subCategoryProductArray[indexPath.row]
        
        
        cell.cartView.layer.cornerRadius = cell.cartView.frame.size.height / 2
        cell.cartView.clipsToBounds = true
        
        cell.labelLogo.isHidden = true
        cell.logoImageView.isHidden = true
        
        cell.labelModelNumber.text = productObject.SKU
        cell.labelProductTitle.text = productObject.Title
        cell.labelPrice.text = String(describing: productObject.Price) + " " + "SAR".localized()
        cell.labelModelNumber.font = UIFont.init(name: MSJSharedManager.regularFont(), size: cell.labelModelNumber.font.pointSize)
        
        
        
        cell.labelPrice.font = UIFont.init(name: MSJSharedManager.boldFont(), size: cell.labelPrice.font.pointSize)
        
        cell.labelLogo.font = UIFont.init(name: MSJSharedManager.boldFont(), size: cell.labelLogo.font.pointSize)
        
        
        cell.productImageView.downLoadImageIntoImageView(url: productObject.ImageName, placeholder: PLACEHOLDER_IMAGE)
        
        cell.labelLogo.text = productObject.BrandTitle
        
        
        
        
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        
        
        
        if indexPath.row == 1
        {
            return UIScreen.main.bounds.size.width / 1.36
        }
        
        
        
        return UIScreen.main.bounds.size.width / 1.56
        
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailNewDesignViewController") as! ProductDetailNewDesignViewController
        vc.categoryObjectById = subCategoryProductArray[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
        
        
        //        self.performSegue(withIdentifier: "MSJPorductDetailVC", sender: self)
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let position = scrollView.contentOffset.y
        if position > (self.collectionView.contentSize.height - 100 - scrollView.frame.size.height) {
            if pagination {
                self.pageNo = self.pageNo + 1
                self.getSubCategoryProduct()
            }
        }
    }
    
    
    func getSubCategoryProduct()
    {
        let url = BASE_URL + PRODUCTS
        
        //        + "?SubCategoryID=" +  subCategoryObject.CategoryID
        
        var parameters: [String:Any] = ["CompanyID":"2"]
        parameters["NoVersion"] = "true"
        parameters["PageNo"] = self.pageNo
        
        ApiManager.getRequest(urlString: url, isAlertShow: true, parameters: parameters, successCallback: { (dict) in
            
            let productList = dict["products"] as! NSArray
            
            for item in productList
            {
                let mainProductDict = item as! NSDictionary
                
                self.subCategoryProductArray.append(ProductModel.parseProductData(dict: mainProductDict))
            }
            var countPercentage = 0.0
            countPercentage = Double(self.subCategoryProductArray.count)/Double(20)
            if countPercentage != 0.0 {
                self.pagination = false
            }
            
            if self.subCategoryProductArray.count > 0
            {
                //                  self.setTitle()
                
            }
            
            AnimatableReload.reload(collectionView: self.collectionView, animationDirection: ANIMATION_DOWN)
            
        }) { (errorString) in
            
            print(errorString)
            
        }
    }
    
    
    //    func setTitle()
    //    {
    //        let str = mainProductName + " \\ "  + subCatProductName
    //
    //        labelProduct.attributedText = makeAttributedLable(complete: str, CompFont: UIFont.init(name: MSJSharedManager.mediumFont(), size: 15.0)!, CompColor: MSJColor.white, sub: subCatProductName, subFont: UIFont.init(name: MSJSharedManager.regularFont(), size: 15.0)!, SubColor: MSJColor.white)
    //    }
    
    func setLanguage()
    {
        
        
        
    }
    
    
}

class View: UIView
{
    override func layoutSubviews()
    {
        super.layoutSubviews()
        self.roundCorners([.topLeft, .bottomLeft], radius: 10)
    }
}


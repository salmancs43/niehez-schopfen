//  Created on 18/08/2022.

import UIKit

class ProductDetailNewDesignViewController: BaseVC {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bgImage: CustomImageView!
    @IBOutlet weak var addToCartOuterView: UIView!
    @IBOutlet weak var addToCartLabel: MSJBoldLabel!
    
    let obj = MSJSharedManager.getAppSetting()
    var categoryObjectById : ProductModel!
    var productModel = ProductModel()
    var selectedVariantData = ProductVariantData()
    var selectedAddOnDataArray = [ProductAddOnData]()
    
    var quantity: Int = 1
    var cartObject : MSJAddToCartObject!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.backgroundColorOrImageSetting(view: self.view, imageView: self.bgImage)
        self.addToCartOuterView.backgroundColor = hexStringToUIColors(hex: MSJSharedManager.getAppSetting().AlertPositiveButtonColor)
        
        if self.cartObject != nil {
            
            self.quantity = Int(self.cartObject.Quantity ?? "1") ?? 1
            self.addToCartLabel.text = "Update Bag".localized()
            
        } else {
            
            self.addToCartLabel.text = "Add to Bag".localized()
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tableView.tableFooterView = UIView()
        
        self.backButtonOnNavigationBar()
        self.setTabbarToVisible(isVisible: false)
        self.getSubCategoryProduct()
    
    }

    func getSubCategoryProduct() {
        
        let url = BASE_URL + PRODUCTS + "?ProductID=" + self.categoryObjectById.ProductID
        
        var parameters: [String: Any] = ["CompanyID": "2"]
        parameters["NoVersion"] = "true"
        parameters["PageNo"] = "1"
        
        ApiManager.getRequest(urlString: url, isAlertShow: true, parameters: parameters, successCallback: { (dict) in
            
            if let productList = dict["products"] as? NSArray {
                
                for item in productList {
                    
                    if let mainProductDict = item as? NSDictionary {
                        
                        self.productModel = ProductModel.parseProductData(dict: mainProductDict)
                        
                    }
                    
                }
                
            }
            
            self.tableView.reloadData()
            
        }) { (errorString) in
            
            print(errorString)
            
        }
        
    }
    
    func addOrEditCart() {
        
        let url: String
        var cartDictionary = [String: String]()
        
        cartDictionary["Quantity"] = String(describing: self.quantity)
        cartDictionary["UserID"] = MSJSharedManager.getUser().UserID ?? ""
        
        if self.cartObject != nil {
            
            cartDictionary["TempOrderID"] = self.cartObject.TempOrderID ?? ""
            
            url = BASE_URL + EDIT_CART
            
        } else {
           
            url = BASE_URL + ADD_TO_CART
            cartDictionary["ProductID"] = productModel.ProductID
            cartDictionary["CompanyID"] = productModel.CompanyID
            cartDictionary["TempItemPrice"] = productModel.Price
            cartDictionary["ItemType"] = productModel.ProductType
            
        }
        
        ApiManager.postServiceCall(urlString: url, isAlertShow: true, parameters: cartDictionary, header: nil) { dict in
        
            print(dict)
            
            if let message = dict["message"] as? String {
                self.showAlert(title: "Success", message: message)
            }
                            
        } errorCallBack: { error in
            self.showAlert(title: "Error".localized(), message: error)
        }
        
    }
    
}

extension ProductDetailNewDesignViewController {
    
    @IBAction func typeButtonAction(_ sender: UIButton) {
        
        if String(describing: sender.tag).count == 3 {
            
            let tagToUse = sender.tag - 100
            
            if self.productModel.AddOnData[tagToUse].isSelected {
                
                self.productModel.AddOnData[tagToUse].isSelected = false
                self.selectedVariantData = ProductVariantData()
                
                self.selectedAddOnDataArray.removeAll { item in
                    item.SKU == self.productModel.AddOnData[tagToUse].SKU
                }
                
            } else {
                
                self.productModel.AddOnData[tagToUse].isSelected = true
                self.selectedAddOnDataArray.append(self.productModel.AddOnData[tagToUse])
                
            }
            
        } else {
            
            if self.productModel.VariantData[sender.tag].isSelected {
                
                self.productModel.VariantData[sender.tag].isSelected = false
                self.selectedVariantData = ProductVariantData()
                
            } else {
                
                self.productModel.VariantData.forEach { item in
                    item.isSelected = false
                }
                
                self.productModel.VariantData[sender.tag].isSelected = true
                self.selectedVariantData = self.productModel.VariantData[sender.tag]
                
            }
            
        }
        
        self.tableView.reloadData()
        
    }
    
    @IBAction func buttonMinus(_ sender: Any) {
        
        if ((self.quantity - 1) != 0) {
            self.quantity -= 1
        }
        
        self.tableView.reloadData()
        
    }
    
    @IBAction func buttonPlus(_ sender: Any) {
        
        if ((self.quantity + 1) != 99) {
            self.quantity += 1
        }
        
        self.tableView.reloadData()
        
    }
    
    @IBAction func addToCartButtonAction(_ sender: UIButton) {
        
        if (UserDefaults.standard.value(forKey: USER_DATA) == nil) {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MSJLogInVC") as? MSJLogInVC
            self.navigationController?.pushViewController(vc!, animated: true)

        } else {
            
            self.addOrEditCart()

        }
        
    }
    
}

extension ProductDetailNewDesignViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1 + 1 + self.productModel.VariantData.count + 1 + self.productModel.AddOnData.count + 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! ProductDetailNewDesignViewControllerTableViewCell
            
            cell.titleLabel.text = self.productModel.Title
            cell.categoryLabel.text = self.productModel.CategoryTitle + " / " + productModel.SubCategoryTitle
            
            cell.imagesCollectionViewOuterView.layer.cornerRadius = 8
            cell.imagesCollectionViewOuterView.layer.borderColor = UIColor.black.cgColor
            cell.imagesCollectionViewOuterView.layer.borderWidth = 1
            cell.imagesCollectionViewOuterView.clipsToBounds = true
            
            cell.imagesCollectionView.delegate = self
            cell.imagesCollectionView.dataSource = self
            cell.imagesCollectionView.reloadData()
            
//            cell.itemsLeftLabel.text = "Last 10 Pieces Left"
            cell.itemsLeftLabel.text = ""
            cell.descriptionTextLabel.text = self.productModel.Description.htmlToString
            cell.specificationTextLabel.text = self.productModel.Specifications.htmlToString
            
            return cell
            
        } else if indexPath.row == 1 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! ProductDetailNewDesignViewControllerTableViewCell
            
            cell.multiselectionCellOuterView.backgroundColor = .LIGHT_GRAY_COLOR
            cell.mutiselectionCellTitleLabel.text = "Type"
            
            return cell
            
        } else if ((indexPath.row >= 2) && (indexPath.row < 2 + self.productModel.VariantData.count)) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell3") as! ProductDetailNewDesignViewControllerTableViewCell
            
            let singleVariantData = self.productModel.VariantData[indexPath.row - 2]
            
            cell.typeCellOuterView.backgroundColor = .LIGHT_GRAY_COLOR
            
            cell.typeCellButtonOuterView.layer.borderColor = UIColor.black.cgColor
            cell.typeCellButtonOuterView.layer.borderWidth = 1
            cell.typeCellButtonOuterView.layer.cornerRadius = cell.typeCellButtonOuterView.frame.height / 2
            cell.typeCellButtonOuterView.clipsToBounds = true
            cell.typeCellButtonInnerView.layer.borderColor = UIColor.black.cgColor
            cell.typeCellButtonInnerView.layer.cornerRadius = cell.typeCellButtonInnerView.frame.height / 2
            cell.typeCellButtonInnerView.clipsToBounds = true
            
            if singleVariantData.isSelected {
                
                cell.typeCellButtonInnerView.layer.borderWidth = 1
                cell.typeCellButtonInnerView.backgroundColor = UIColor.black
                
            } else {
                
                cell.typeCellButtonInnerView.layer.borderWidth = 0
                cell.typeCellButtonInnerView.backgroundColor = UIColor.clear
                
            }
            
            cell.typeCellButton.tag = indexPath.row - 2
            
            cell.typeCellTypeLabel.text = singleVariantData.AttributeTitle
            cell.typeCellPriceLabel.text = singleVariantData.Price + " " + "SAR"
            
            return cell
            
        } else if indexPath.row == 2 + self.productModel.VariantData.count {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! ProductDetailNewDesignViewControllerTableViewCell
            
            cell.multiselectionCellOuterView.backgroundColor = .LIGHT_GRAY_COLOR
            cell.mutiselectionCellTitleLabel.text = "Add On"
            
            return cell
            
        } else if ((indexPath.row >= 3 + self.productModel.VariantData.count) && (indexPath.row < 3 + self.productModel.VariantData.count + self.productModel.AddOnData.count)) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell3") as! ProductDetailNewDesignViewControllerTableViewCell
            
            let singleVariantData = self.productModel.AddOnData[indexPath.row - 3 - self.productModel.VariantData.count]
            
            cell.typeCellOuterView.backgroundColor = .LIGHT_GRAY_COLOR
            
            cell.typeCellButtonOuterView.layer.borderColor = UIColor.black.cgColor
            cell.typeCellButtonOuterView.layer.borderWidth = 1
            cell.typeCellButtonOuterView.layer.cornerRadius = cell.typeCellButtonOuterView.frame.height / 4
            cell.typeCellButtonOuterView.clipsToBounds = true
            cell.typeCellButtonInnerView.layer.borderColor = UIColor.black.cgColor
            cell.typeCellButtonInnerView.layer.cornerRadius = cell.typeCellButtonInnerView.frame.height / 4
            cell.typeCellButtonInnerView.clipsToBounds = true
            
            if singleVariantData.isSelected {
                
                cell.typeCellButtonInnerView.layer.borderWidth = 1
                cell.typeCellButtonInnerView.backgroundColor = UIColor.black
                
            } else {
                
                cell.typeCellButtonInnerView.layer.borderWidth = 0
                cell.typeCellButtonInnerView.backgroundColor = UIColor.clear
                
            }
            
            cell.typeCellButton.tag = indexPath.row - 3 - self.productModel.VariantData.count + 100
            
            cell.typeCellTypeLabel.text = singleVariantData.Title
            cell.typeCellPriceLabel.text = "+" + singleVariantData.Price + " " + "SAR"
            
            return cell
            
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell4") as! ProductDetailNewDesignViewControllerTableViewCell
            
            cell.textFieldQuantity.text = String(describing: self.quantity)
            cell.textFieldQuantity.textAlignment = NSTextAlignment.center
            
            var addOnTotalPrice: Double = 0.0
            self.selectedAddOnDataArray.forEach { singelItem in
                addOnTotalPrice += Double(singelItem.Price) ?? 0.0
            }
            
            let totalPrice = (Double(self.productModel.Price) ?? 0.0) + (Double(self.selectedVariantData.Price) ?? 0.0) + addOnTotalPrice
            cell.totalPriceLabel.text = "+" + String(describing: totalPrice) + " " + "SAR"
            
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if indexPath.row == 1
        {
            if self.productModel.VariantData.count == 0
            {
                return 0
            }
        }
        
        if indexPath.row == 2 + self.productModel.VariantData.count
        {
            if self.productModel.AddOnData.count == 0
            {
                return 0
            }
        }
        
        
        return UITableView.automaticDimension
        
    }
    
}

extension ProductDetailNewDesignViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.productModel.ProductImages.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView .dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! MSJProductDetailCollectionCell
        
        let productObj = self.productModel.ProductImages[indexPath.item]
        
        if productObj.ImageName != "" {
            
            let urlString = IMG_BASE_URL + productObj.ImageName
            let url = URL(string: urlString)
            cell.productImageView.kf.indicatorType = .activity
            cell.productImageView.kf.setImage(with: url, placeholder: PLACEHOLDER_IMAGE, options: [.transition(.fade(0.7))])
        
        } else {
            
            cell.productImageView.image = PLACEHOLDER_IMAGE
            
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: collectionView.frame.width, height: 250)

    }
    
}

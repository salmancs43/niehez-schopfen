//
//  MSJPorductDetailVC.swift
//  MSJ
//
//  Created by Mac on 08/11/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit
import SKPhotoBrowser
import ISPageControl


class MSJPorductDetailVC: BaseVC ,UITableViewDelegate,UITableViewDataSource , UICollectionViewDelegate,UICollectionViewDataSource , UITextFieldDelegate
{
    @IBOutlet var backgroundViewOfTitle: UIView!
    @IBOutlet weak var mainShadowView: UIView!
    let obj = MSJSharedManager.getAppSetting()
    
    @IBOutlet weak var lblPrice: MSJBoldLabel!
    @IBOutlet weak var lblAddCart: MSJBoldLabel!
    @IBOutlet weak var lblDescription: MSJRegularLabel!
    var categoryObjectById : ProductModel!
    var cartObject : MSJAddToCartObject!
//    let productObject = MSJProductDetailsObject()
    
    var productModel = ProductModel()
    @IBOutlet weak var selectQuantityView: UIView!
    
    @IBOutlet weak var bgImage: CustomImageView!
    @IBOutlet weak var addToCardView: UIView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var pageController: ISPageControl!
    @IBOutlet weak var labelTitlte: UILabel!
    @IBOutlet weak var qualityCollectionView: UICollectionView!
    @IBOutlet weak var imageCollectionView: UICollectionView!
    
    @IBOutlet weak var textFieldQuantity: MSJCustomTextField!
    
    @IBOutlet weak var height_quantityTextField: NSLayoutConstraint!
    @IBOutlet weak var mainView: UIView!
    
    var index : Int?
    
    @IBOutlet weak var labelDescriptionTitle: MSJBoldLabel!
    
    
    
    
    @IBOutlet weak var labelBrandString: MSJBoldLabel!
    @IBOutlet weak var labelQuantityTitle: MSJBoldLabel!
    
    var isLanguageChange : Bool = false
    
    
    
    var categoryTitle : String = ""
    
    
    @IBOutlet weak var bottom_cartView: NSLayoutConstraint!
    
    var initalIndex = 0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        
       
        
        self.addToCardView.backgroundColor = hexStringToUIColors(hex: MSJSharedManager.getAppSetting().AlertPositiveButtonColor)
        self.backgroundViewOfTitle.backgroundColor = hexStringToUIColors(hex: MSJSharedManager.getAppSetting().TabBarColor)
        
        let cellSize = CGSize(width:UIScreen.main.bounds.size.width-32 , height:220)
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 0.0
        layout.minimumInteritemSpacing = 0.0
        imageCollectionView.isPagingEnabled = true
        imageCollectionView.setCollectionViewLayout(layout, animated: true)
        
        imageCollectionView.dropShadow(color: UIColor.black, isRoundConer: false)
        imageCollectionView.layer.cornerRadius = 20
        
        
        self.backgroundColorOrImageSetting(view: self.view, imageView: self.bgImage)
      
        
        
        let color = UIColor.white
        textFieldQuantity.attributedPlaceholder = NSAttributedString(string: textFieldQuantity.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : color])
        
        mainView.isHidden = true
        self.addToCardView.isHidden = true
        pageController.radius = 2.5
    
        selectQuantityView.layer.borderWidth = 0.0
        selectQuantityView.layer.cornerRadius = selectQuantityView.frame.size.height / 2
        selectQuantityView.clipsToBounds = true
        selectQuantityView.layer.borderColor = UIColor.TAB_BAR_BACKGROUND_COLOR.cgColor
        
        if cartObject != nil {
            self.textFieldQuantity.text = String(describing: self.cartObject.Quantity ?? "0")
        }
        
      
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.setLanguage()
        self.backButtonOnNavigationBar()
        self.setTabbarToVisible(isVisible: false)
        
        getSubCategoryProduct()
        
        
    
    }
    
 
    
    // Mark : Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 0//self.productObject.productSpecificationArry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! MSJProductDetailCell
//        let object = self.productObject.productSpecificationArry[indexPath.row] as! MSJProductDetailsObject
//        cell.labelSpecificationDetail.attributedText = object.productSpecDescription
//        cell.dotImageView.layer.cornerRadius = cell.dotImageView.frame.size.width/2
//        cell.dotImageView.clipsToBounds = true
        
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return UITableView.automaticDimension
    }
    
    
    // Mark : Collection View Delegate
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == imageCollectionView
        {
            
            return self.productModel.ProductImages.count
        }
        return 0//self.productObject.productFeatureArry.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == imageCollectionView
        {
            
            let cell = collectionView .dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! MSJProductDetailCollectionCell
            
            let productObj = self.productModel.ProductImages[indexPath.item]
            
            if productObj.ImageName != ""
            {
                let urlString = IMG_BASE_URL + productObj.ImageName
                
                let url = URL(string: urlString)
                cell.productImageView.kf.indicatorType = .activity
                cell.productImageView.kf.setImage(with: url, placeholder: PLACEHOLDER_IMAGE, options: [.transition(.fade(0.7))])
            }
            else
            {
                cell.productImageView.image = PLACEHOLDER_IMAGE
            }
            
            return cell
        }
        else
        {
            let cell = collectionView .dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! MSJProductDetailCollectionCell
//
//            let productObj = self.productObject.productFeatureArry[indexPath.item] as!  MSJProductDetailsObject
//
//            if productObj.imageURL != nil
//            {
//                pinRemoteImg(url: IMG_BASE_URL + productObj.imageURL!, imgView: cell.featuredImageView, placeholder: PLACEHOLDER_IMAGE)
//            }
//            else
//            {
//                cell.featuredImageView.image = PLACEHOLDER_IMAGE
//            }
            return cell
            
            
            
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == imageCollectionView
        {
            initalIndex = 0
            self.expandImages()
        }
        
        
        
    }
    
    // Mark - textfield delegates
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField.isEqual(textFieldQuantity)
        {
            if(range.location==0)
            {
                if (string.hasPrefix("0"))
                {
                    return false
                }
            }
            let textstring = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let length = textstring.count
            if length > 2
            {
                return false
            }
        }
        return true
        
    }
    
    
    func getSubCategoryProduct()
    {
        
        let url = BASE_URL + PRODUCTS + "?ProductID=" + categoryObjectById.ProductID
        
        var parameters: [String:Any] = ["CompanyID":"2"]
        parameters["NoVersion"] = "true"
        parameters["PageNo"] = "1"
        
        ApiManager.getRequest(urlString: url, isAlertShow: true, parameters: parameters, successCallback: { (dict) in
            
            //New Parsing
            
            let productList = dict["products"] as! NSArray
            
            for item in productList
            {
                let mainProductDict = item as! NSDictionary
                self.productModel = ProductModel.parseProductData(dict: mainProductDict)
            }
            
            //End new Parsing
            
            var titleString =   self.productModel.CategoryTitle + " \\ "  + self.productModel.SubCategoryTitle
                
            titleString = titleString +  " \\ "  + self.productModel.Title
                
            
            
            
            self.labelTitlte.attributedText = self.makeAttributedLable(complete: titleString, CompFont: UIFont.init(name: MSJSharedManager.boldFont(), size: 15.0)!, CompColor: UIColor.white, sub: titleString, subFont: UIFont.init(name: MSJSharedManager.boldFont(), size: 15.0)!, SubColor: UIColor.white)
            
            //    }
            
            
            
            self.labelBrandString.text =  self.productModel.Title
            
            if self.productModel.ImageName != ""
            {
                self.logoImageView.isHidden = false
                self.labelBrandString.isHidden = true
                
                let urlString = IMG_BASE_URL + self.productModel.ImageName
                let url = URL(string: urlString)
                self.logoImageView.kf.indicatorType = .activity
                self.logoImageView.kf.setImage(with: url, placeholder: PLACEHOLDER_IMAGE, options: [.transition(.fade(0.7))])
            }
            else
            {
                self.logoImageView.isHidden = true
                self.labelBrandString.isHidden = false
                
            }
        
            
            self.lblDescription.text = self.productModel.Description
            self.lblPrice.text = String(describing: self.productModel.Price) + " " + "SAR".localized()
            
            
            self.imageCollectionView.reloadData()
            self.qualityCollectionView.reloadData()
            self.mainView.isHidden = false
            self.addToCardView.isHidden = false
        }) { (errorString) in
            
            print(errorString)
            
        }
    }
    public func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
//        let pageWidth  = imageCollectionView.frame.size.width
//        let currentPage = (imageCollectionView.contentOffset.x) / CGFloat(pageWidth)
//        if (0.0 != fmodf(Float(currentPage), 1.0)) {
//            pageController.currentPage = Int(currentPage + 1);
//        } else {
//            pageController.currentPage = Int(currentPage);
//        }
        
    }
    
    
    
    @IBAction func addToCartAction(_ sender: UIButton)
    {
        if (UserDefaults.standard.value(forKey: USER_DATA) == nil)
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MSJLogInVC") as? MSJLogInVC
            self.navigationController?.pushViewController(vc!, animated: true)

        } else {
            if index != nil
            {
                self.addOrEditCart(selectedIndex: index!)
            }
            else
            {
                self.addOrEditCart(selectedIndex: 0)
            }

        }
        
    }
    
    func addOrEditCart(selectedIndex : Int)
    {
        
        let url : String
        var cartDictionary = [String: String]()
        
        
        if cartObject != nil
        {
            
            if self.whiteSpaceValidae(textfield: self.textFieldQuantity.text!) == false
            {
                self.showAlert(title: "Alert".localized(), message: "Enter quantity".localized())
                return;
            }
            
            cartDictionary["TempOrderID"] = cartObject.TempOrderID ?? ""
            
            let quatityStr =   textFieldQuantity.text?.replacedArabicDigitsWithEnglish
            cartDictionary["Quantity"] = quatityStr
            cartDictionary["UserID"] = MSJSharedManager.getUser().UserID ?? ""
            
            url = BASE_URL + EDIT_CART
        }
        else
        {
           
            
            url = BASE_URL + ADD_TO_CART
            cartDictionary["ProductID"] = productModel.ProductID
            cartDictionary["CompanyID"] = productModel.CompanyID
            cartDictionary["Quantity"] = self.textFieldQuantity.text ?? "1"
            cartDictionary["UserID"] = MSJSharedManager.getUser().UserID ?? ""
            cartDictionary["TempItemPrice"] = productModel.Price
            cartDictionary["ItemType"] = productModel.ProductType
            
        }
        
         ApiManager.postServiceCall(urlString: url, isAlertShow: true, parameters: cartDictionary, header: nil) { dict in
            
            print(dict)
            
            if let message = dict["message"] as? String {
                self.showAlert(title: "Success", message: message)
            }
                            
        } errorCallBack: { error in
            self.showAlert(title: "Error".localized(), message: error)
        }

        ///Commented by Sikandar
//        MSJApiManager.sharedInstance.postServiceCall(urlString: url, isAlertShow: true, companyId: obj.company_id, parameters: cartDictionary, header: nil, successCallback:
//            { (dict) in
//
//
//                var str = "Added to Bag".localized()
//                if self.cartObject != nil
//                {
//                    str = "Bag updated".localized()
//
//                }
//
//                self.getCartCount()
//
//                self.languageAlert(title: "Success".localized(), message: str.localized(), btnTitle: "Continue Shopping".localized(), buttonTwo: "Go to Bag".localized(), isUsedBagColor: true ,  successCallback:self.cartPressed, callBack: self.goesBack)
//
//        }) { (error) in
//
//            self.showAlert(title: "Error".localized(), message: error)
//
//        }
        ///Commented by Sikandar
        
        
        
    }
    
    
    func cartPressed()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MSJAddToCartVC") as! MSJAddToCartVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    func goesBack()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
     func hexStringToUIColor (hex:String) -> UIColor
    {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    
    // Zoom Images
    func expandImages()
    {
        var count =  self.productModel.ProductImages.count
        count = count - 1
        var images = [SKPhoto]()
        for  index in 0...count
        {
            
            let productObj = self.productModel.ProductImages[index]
            
            if productObj.ImageName != ""
            {
                let url = IMG_BASE_URL + productObj.ImageName
                let photo = SKPhoto.photoWithImageURL(url)
                photo.shouldCachePhotoURLImage = true
                images.append(photo)
            }
            
            
        }
        
        let browser = SKPhotoBrowser(photos: images)
        browser.initializePageIndex(initalIndex)
        present(browser, animated: true, completion: {})
    }
    @IBAction func buttonMinus(_ sender: Any)
    {
        if self.whiteSpaceValidae(textfield: self.textFieldQuantity.text!) == false
        {
            
            
            
        }
        else
        {
            var quantity = Int(textFieldQuantity.text!)
            quantity = quantity! - 1
            if quantity == 0
            {
                return
            }
            textFieldQuantity.text = String(describing:quantity!)
        }
    }
    
    @IBAction func buttonPlus(_ sender: Any)
    {
        if self.whiteSpaceValidae(textfield: self.textFieldQuantity.text!) == false
        {
            
            textFieldQuantity.text = "1"
            
        }
        else
        {
            var quantity = Int(textFieldQuantity.text!)
            quantity = quantity! + 1
            if quantity == 99
            {
                return
            }
            textFieldQuantity.text = String(describing:quantity!)
        }
    }
    
    func setLanguage()
    {
       
        
        labelQuantityTitle.text = "Quantity".localized()
        labelDescriptionTitle.text = "Description".localized()
        
        
        if MSJSharedManager.getArabic()
        {
            lblAddCart.font = UIFont.init(name: MSJSharedManager.boldFont(), size: 10)
        }
        if cartObject != nil
        {
            lblAddCart.text = "Update Bag".localized()
        }
        else
        {
            lblAddCart.text = "Add to Bag".localized()
        }
        
        
        textFieldQuantity.textAlignment = NSTextAlignment.center
    }
    
    
}

public extension String {
    
    var replacedArabicDigitsWithEnglish: String {
        var str = self
        let map = ["٠": "0",
                   "١": "1",
                   "٢": "2",
                   "٣": "3",
                   "٤": "4",
                   "٥": "5",
                   "٦": "6",
                   "٧": "7",
                   "٨": "8",
                   "٩": "9"]
        map.forEach { str = str.replacingOccurrences(of: $0, with: $1) }
        
        return str
    }
}

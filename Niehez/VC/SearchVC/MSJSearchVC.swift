//
//  MSJSearchVC.swift
//  MSJ
//
//  Created by Mac on 08/11/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit
import AnimatableReload

class MSJSearchVC: BaseVC ,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UISearchControllerDelegate,UISearchBarDelegate,filerdelegate , PinterestLayoutDelegate {
    

   
    @IBOutlet var labelFilter: MSJRegularLabel!
    
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var labelSearchProduct: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewSearchBar: UIView!
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var viewFilter: UIView!

    var searchArray = Array<Any>()
    var tempArray = Array<Any>()
 
    var obj = MSJSharedManager.getAppSetting()
   var isLanguageChange : Bool = false
     var isComingFromFilter : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()

        searchBar.delegate = self
      
        
       
        viewFilter.dropShadow(color:UIColor.black,isRoundConer:true)
        viewSearchBar.dropShadow(color:UIColor.black,isRoundConer:true)
        
        
        searchBar.searchBarStyle = UISearchBar.Style.prominent
        searchBar.isTranslucent = false
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.font = UIFont.init(name: MSJSharedManager.regularFont(), size: 13.0)
        textFieldInsideSearchBar?.backgroundColor = UIColor.white
        searchBar.barTintColor = UIColor.white
        searchBar.layer.cornerRadius = 20;
        
        
        if let layout = collectionView?.collectionViewLayout as? PinterestLayout {
                   layout.delegate = self
        }
  
       
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonOnNavigationBar()
        setLanaguage()
         self.setTabbarToVisible(isVisible: false)
        if isLanguageChange != MSJSharedManager.getArabic()
        {
            self.searchArray.removeAll()
            self.getAllSearchProducts()
            isLanguageChange = MSJSharedManager.getArabic()
        }
        else
        {
            if isComingFromFilter
            {
                
            }
            else
            {
                if self.searchArray.count == 0
                {
                    self.getAllSearchProducts()
                }
            }
           
        }
        
        self.addMultiplRightBraButtons(with: UIImage.init(named: CART_BlUE), secondImg:nil , ref: self, action1:
            {
                
                let vc  = self.storyboard?.instantiateViewController(withIdentifier: "MSJAddToCartVC")
                self.navigationController?.pushViewController(vc!, animated: true)
                print("Cart bar pressed")
        })
        {
            
            
        }
        
        self.backgroundColorOrImageSetting(view: self.view, imageView: self.bgImage)
        
        
    }

   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {

            return self.searchArray.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
      
        var cell = MSJProductCell()
        
        if indexPath.row == 1
        {
            cell = collectionView .dequeueReusableCell(withReuseIdentifier: "AnotherCell", for: indexPath) as! MSJProductCell
        }
        else
        {
           cell = collectionView .dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! MSJProductCell
        }
         
        
        cell.cartView.layer.cornerRadius = cell.cartView.frame.size.height / 2
        cell.cartView.clipsToBounds = true

        cell.labelLogo.isHidden = true
        cell.logoImageView.isHidden = true
        //    cell.shadowView.dropShadowView(color: UIColor.black, isRoundConer: false)
          //  cell.shadowView.layer.cornerRadius = 10
        let productObject = self.searchArray[indexPath.row] as! MSJProduct
        cell.labelModelNumber.text = productObject.productModel
        cell.labelDetail.text = productObject.productName
        
        cell.labelPrice.text = String(describing: productObject.productPrice!) + " " + "SAR".localized()
        cell.labelLogo.text = productObject.logoString
        cell.labelLogo.font = UIFont.init(name: MSJSharedManager.boldFont(), size: cell.labelLogo.font.pointSize)
        
        
        cell.labelModelNumber.font = UIFont.init(name: MSJSharedManager.regularFont(), size: cell.labelModelNumber.font.pointSize)
        cell.labelDetail.font = UIFont.init(name: MSJSharedManager.regularFont(), size: cell.labelDetail.font.pointSize)
        cell.labelPrice.font = UIFont.init(name: MSJSharedManager.boldFont(), size: cell.labelPrice.font.pointSize)

        if productObject.imagePath != nil
        {
            pinRemoteImg(url: IMG_BASE_URL + productObject.imagePath!, imgView: cell.productImageView, placeholder: PLACEHOLDER_IMAGE)
        }
        else
        {
            cell.productImageView.image = PLACEHOLDER_IMAGE
        }
        
        
        
        if productObject.brandLogo != ""
        {
            pinRemoteImg(url: IMG_BASE_URL + productObject.brandLogo!, imgView: cell.logoImageView, placeholder: BRAND_PLACEHOLDER_IMAGE)
           
            cell.logoImageView.isHidden = false
        }
        else
        {
            cell.labelLogo.isHidden = false
            
        }
        

        
        
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MSJPorductDetailVC") as! MSJPorductDetailVC
//        vc.categoryObjectById = self.searchArray[indexPath.row] as? MSJProduct

//        self.navigationController?.pushViewController(vc, animated: true)
        
        
        //        self.performSegue(withIdentifier: "MSJPorductDetailVC", sender: self)
        
    }
    
  
    
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
           

        
        if indexPath.row == 1
        {
             return UIScreen.main.bounds.size.width / 1.36
        }
        
      
          
           return UIScreen.main.bounds.size.width / 1.56
           
       }
    
    
    
    
    
    func getAllSearchProducts()
    {
        var url:String

        if MSJSharedManager.sharedManager.url != nil && MSJSharedManager.sharedManager.url != ""
        {
            url = MSJSharedManager.sharedManager.url!
        }
        else
        {
             url = BASE_URL + PRODUCTS
        }
        let parameters: [String: Any] = [COMPANY_ID:obj.CompanyID]
        MSJApiManager .sharedInstance.getRequest(urlString: url, isAlertShow: true, parameters: parameters, successCallback: { (dict) in
            
            self.isComingFromFilter = false
            
            let productList = dict["products"] as! NSArray
            
            for item in productList
            {
                var mainCategoryDict = item as! NSDictionary
                let productImagesArray = mainCategoryDict["product_images"] as! NSArray
                mainCategoryDict = mainCategoryDict.removeNull()
                let products = MSJProduct()
                
                
                products.brandLogo = mainCategoryDict["brand_image"] as? String
                products.productId = mainCategoryDict["product_id"] as? String
                
                
                products.productPrice = mainCategoryDict["price"] as? String
               
            
                if MSJSharedManager.getArabic() == true
                {
                    
                    products.productName = mainCategoryDict["sub_category_title_ar"] as? String
                    products.logoString = mainCategoryDict["brand_title_ar"] as? String
                    products.productModel = mainCategoryDict["model_ar"] as? String
                }
                else
                {
                    products.productName = mainCategoryDict["sub_category_title_en"] as? String
                    products.logoString = mainCategoryDict["brand_title_en"] as? String
                    products.productModel = mainCategoryDict["model_en"] as? String
                    
                }
                
                if productImagesArray.count > 0
                {
                    
                    var productImagesDict = productImagesArray.firstObject as! NSDictionary
                    productImagesDict = productImagesDict.removeNull()
                    products.imagePath = productImagesDict["product_image"] as? String
                    
                }
                
                 self.searchArray.append(products)
            
            }
            
            self.tempArray = self.searchArray
            
            
            
           // self.collectionView.reloadData()
            
            AnimatableReload.reload(collectionView: self.collectionView, animationDirection: ANIMATION_DOWN)
            
        }) { (errorString) in
            
            print(errorString)
            
        }
        
        
        
    }
    
    
    //Mark - Search Bar Delegate
    
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar)
    {
       
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        
        if searchBar.text == ""
        {
             self.searchArray = self.tempArray
            
        }
        else
        {
            let searchPredicate = NSPredicate(format:"(self.productName CONTAINS[c] %@) OR (self.productModel CONTAINS[c] %@) ",searchBar.text!,searchBar.text!)
            let array = (self.tempArray as NSArray).filtered(using: searchPredicate)
            
            
            self.searchArray = array

        
            
        }
        

        collectionView.reloadData()
    }
   
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        self.searchBar.endEditing(true)
    }
    @IBAction func filterButton(_ sender: Any)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MSJFilterVC") as! MSJFilterVC
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func filter(isFilter: Bool)
    {
        isComingFromFilter = isFilter
        self.searchArray.removeAll()
        self.collectionView.reloadData()
        self.getAllSearchProducts()
    }
    
    func setLanaguage()
    {
          labelSearchProduct.attributedText = makeAttributedLable(complete: "Search Products".localized(), CompFont: UIFont.init(name: MSJSharedManager.regularFont(), size: 15.0)!, CompColor: UIColor.white, sub: "Search".localized(), subFont: UIFont.init(name: MSJSharedManager.boldFont(), size: 15.0)!, SubColor: UIColor.white)
        
        searchBar.placeholder = "Search Products".localized()
        labelFilter.text = "Filter".localized()
      
        
            
        
        
    }
    
    
    
}

//
//  NiehezDeliveryTypeViewController.swift
//  Niehez
//
//  Created by Macbook on 09/05/2022.
//  Copyright © 2022 Mac. All rights reserved.
//

import UIKit

class NiehezDeliveryTypeViewController: BaseVC {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var pickUpOuterView: UIView!
    @IBOutlet weak var deliveryOuterView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
         self.backButtonOnNavigationBar()
        
        
        self.setTabbarToVisible(isVisible: false)
        
        self.deliveryOuterView.layer.cornerRadius = 24
        self.deliveryOuterView.layer.masksToBounds = true
        self.pickUpOuterView.layer.cornerRadius = 24
        self.pickUpOuterView.layer.masksToBounds = true
        
        self.backgroundColorOrImageSetting(view: self.view, imageView: self.bgImage)
    }
    
    
    
    
    @IBAction func actionPickUp(_ sender: UIButton) {
        
        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "NiehezPickUpStoresViewController") as! NiehezPickUpStoresViewController
        vc.isPickUp = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionDelivery(_ sender: UIButton) {
        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "NiehezPickUpStoresViewController") as! NiehezPickUpStoresViewController
        vc.isPickUp = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

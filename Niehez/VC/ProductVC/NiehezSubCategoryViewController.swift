//
//  NiehezSubCategoryViewController.swift
//  Niehez
//
//  Created by Muhammad Salman on 23/03/2022.
//  Copyright © 2022 Mac. All rights reserved.
//



import UIKit
import AnimatableReload

class NiehezSubCategoryViewController: BaseVC , UITableViewDataSource,UITableViewDelegate
{
 
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bgImage: UIImageView!
    
    
    var isLanguageChange : Bool = false
    var obj = MSJSharedManager.getAppSetting()

    var categoryObject = CategoryModel()
    var subCategoryArray = [CategoryModel]()
    
    @IBOutlet weak var labelNoProductAvailable: MSJRegularLabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        
        self.setAttributedTitleForRightBar(complete: categoryObject.Title, CompFont: UIFont.init(name: MSJSharedManager.regularFont(), size: 15.0)!, CompColor: .white, sub: categoryObject.Title.localized(), subFont: UIFont.init(name: MSJSharedManager.boldFont(), size: 15.0)!, SubColor: .white)
        
        self.addLeftBarIcon()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
       
        setLanaguage()
        self.setTabbarToVisible(isVisible: false)
        self.backButtonOnNavigationBar()
        
        if isLanguageChange != MSJSharedManager.getArabic()
        {
            
            self.getMainSubCategory(categoryId: categoryObject.CategoryID, mainProductName: categoryObject.Title)
            
            isLanguageChange = MSJSharedManager.getArabic()
        }
        else
        {
           
            self.getMainSubCategory(categoryId: categoryObject.CategoryID, mainProductName: categoryObject.Title)
            
        }
        
        self.backgroundColorOrImageSetting(view: self.view, imageView: self.bgImage)
        
     

    }
    
 
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Mark : Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.subCategoryArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! MSJProductTableCell
        
      
        
        cell.mainContainer.layer.cornerRadius = 8
        cell.mainContainer.clipsToBounds = true
        
        let categoryObject = subCategoryArray[indexPath.row]
        cell.labelProductTitle.text = categoryObject.Title
        
        cell.labelProductTitle.font = UIFont.init(name: MSJSharedManager.regularFont(), size: cell.labelProductTitle.font.pointSize)
        
        cell.productImageView.contentMode = .scaleAspectFit
        cell.productImageView.downLoadImageIntoImageView(url: categoryObject.Image, placeholder: PLACEHOLDER_IMAGE)
       
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 90
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MSJInnetProductVC") as! MSJInnetProductVC
        vc.categoryObject = self.categoryObject
        vc.subCategoryObject = subCategoryArray[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    

    
  
    
    func getMainSubCategory(categoryId:String , mainProductName:String)
    {
        self.subCategoryArray.removeAll()
        self.tableView.reloadData()
        let url = BASE_URL + MAIN_CATEGORY + "?ParentID=" +  categoryId
        
        var parameters: [String:Any] = ["CompanyID":"2"]
        parameters["NoVersion"] = "true"
        
        ApiManager.getRequest(urlString: url, isAlertShow: true, parameters: parameters, successCallback: { (dict) in
            
            
            let categoryList = dict["categories"] as! NSArray
         
            
            for item in categoryList
            {
                let mainCategoryDict = item as! NSDictionary
                
                self.subCategoryArray.append(CategoryModel.parseCategoryData(dict: mainCategoryDict))
            }
            
            if self.subCategoryArray.count > 0
            {
                self.labelNoProductAvailable.isHidden = true
            }
            else
            {
                self.labelNoProductAvailable.isHidden = false
            }
            //self.tableView.reloadData()
            
            AnimatableReload.reload(tableView: self.tableView, animationDirection: ANIMATION_DOWN)
            
        }) { (errorString) in
            
            self.labelNoProductAvailable.isHidden = false
            
            
            
        }
        
    }
    
    
    func setLanaguage()
    {
        
//        labelProductCategories.attributedText = makeAttributedLable(complete: "Browse Products".localized(), CompFont: UIFont.init(name: MSJSharedManager.regularFont(), size: 15.0)!, CompColor: UIColor.white, sub: "Browse".localized(), subFont: UIFont.init(name:MSJSharedManager.mediumFont(), size: 15.0)!, SubColor: UIColor.white)
        
        labelNoProductAvailable.text = "No Product Available".localized()
        
    }
    
    

    
    
    
}

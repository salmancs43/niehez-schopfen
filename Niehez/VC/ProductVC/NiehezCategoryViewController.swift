//
//  NiehezCategoryViewController.swift
//  Niehez
//
//  Created by Macbook on 27/03/2022.
//  Copyright © 2022 Mac. All rights reserved.
//

import UIKit
import AnimatableReload
import Kingfisher

class NiehezCategoryViewController: BaseVC , UITableViewDataSource,UITableViewDelegate
{
 
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var bgImage: UIImageView!
    
    
    var isLanguageChange : Bool = false
    var obj = MSJSharedManager.getAppSetting()
    var mainCategoryArray = [CategoryModel]()
 
    
    @IBOutlet weak var labelNoProductAvailable: MSJRegularLabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addLeftBarIcon()
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
       
        setLanaguage()
        self.backgroundColorOrImageSetting(view: self.view, imageView: self.bgImage)
        
        self.setTabbarToVisible(isVisible: true)
        if isLanguageChange != MSJSharedManager.getArabic()
        {
            
            self.getAllMainCategories()
            isLanguageChange = MSJSharedManager.getArabic()
        }
        else
        {
            if self.mainCategoryArray.count == 0
            {
                self.getAllMainCategories()
            }
        }
        
        self.addMultiplRightBraButtons(with: UIImage.init(named: SEARCH_BLUE), secondImg: UIImage.init(named: CART_BlUE), ref: self, action1:
            {
                let vc  = self.storyboard?.instantiateViewController(withIdentifier: "MSJSearchVC")
                self.navigationController?.pushViewController(vc!, animated: true)
                
        })
        {
            
            let vc  = self.storyboard?.instantiateViewController(withIdentifier: "MSJAddToCartVC")
            self.navigationController?.pushViewController(vc!, animated: true)
            
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
    }
    
   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Mark : Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.mainCategoryArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! MSJProductTableCell
        
      
        
        cell.mainContainer.layer.cornerRadius = 8
        cell.mainContainer.clipsToBounds = true
        
        let categoryObject = mainCategoryArray[indexPath.row]
        cell.labelProductTitle.text = categoryObject.Title
        
        
        cell.labelProductTitle.font = UIFont.init(name: MSJSharedManager.regularFont(), size: cell.labelProductTitle.font.pointSize)
        cell.productImageView.contentMode = .scaleAspectFill
        if categoryObject.Image != ""
        {
            
            let urlString = IMG_BASE_URL + categoryObject.Image
//            pinRemoteImg(url: IMG_BASE_URL_New + categoryObject.Image, imgView: cell.productImageView, placeholder: PLACEHOLDER_IMAGE)
            
            let url = URL(string: urlString)
            cell.productImageView.kf.indicatorType = .activity
            cell.productImageView.kf.setImage(with: url, placeholder: PLACEHOLDER_IMAGE, options: [.transition(.fade(0.7))])
        }
        else
        {
            cell.productImageView.image = PLACEHOLDER_IMAGE
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 90
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
       
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NiehezSubCategoryViewController") as! NiehezSubCategoryViewController
        vc.categoryObject = mainCategoryArray[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
           
    }

    func getAllMainCategories()
    {
        self.mainCategoryArray.removeAll()
        let url = BASE_URL + MAIN_CATEGORY
        
        var parameters: [String: Any] = ["CompanyID":"2","ParentID":0]
        parameters["NoVersion"] = "true"
        
        ApiManager.getRequest(urlString: url, isAlertShow: true, parameters: parameters, successCallback: { (dict) in
            
            let categoryList = dict["categories"] as! NSArray


            for item in categoryList
            {
                let mainCategoryDict = item as! NSDictionary

                self.mainCategoryArray.append(CategoryModel.parseCategoryData(dict: mainCategoryDict))
            }

            AnimatableReload.reload(tableView: self.tableView, animationDirection: ANIMATION_DOWN)

            if self.mainCategoryArray.count > 0
            {
                self.labelNoProductAvailable.isHidden = true
            }
            else
            {
                self.labelNoProductAvailable.isHidden = false
            }
            
            
        }) { (errorString) in
            
            print(errorString)
            
        }
    }
    
    

    
    
 
    
    func setLanaguage()
    {
        
//        labelProductCategories.attributedText = makeAttributedLable(complete: "Browse Products".localized(), CompFont: UIFont.init(name: MSJSharedManager.regularFont(), size: 15.0)!, CompColor: UIColor.white, sub: "Browse".localized(), subFont: UIFont.init(name:MSJSharedManager.mediumFont(), size: 15.0)!, SubColor: UIColor.white)
        
        labelNoProductAvailable.text = "No Product Available".localized()
        
    }
    
    

    
    
    
}

extension UIImage {
    var circleMask: UIImage {
        let square = CGSize(width: min(size.width, size.height), height: min(size.width, size.height))
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: square))
        imageView.contentMode = UIView.ContentMode.scaleAspectFill
        imageView.image = self
        imageView.layer.cornerRadius = square.width/2
        imageView.layer.borderColor = UIColor.white.cgColor
        imageView.layer.borderWidth = 5
        imageView.layer.masksToBounds = true
        UIGraphicsBeginImageContext(imageView.bounds.size)
        imageView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result!
    }
}


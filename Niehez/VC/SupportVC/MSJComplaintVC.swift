//
//  MSJComplaintVC.swift
//  MSJ
//
//  Created by Mac on 07/11/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit
import TransitionButton
import AAPickerView


protocol complaintDelegate
{
    func complaintDelegate()
}

class MSJComplaintVC: BaseVC,UITextFieldDelegate,UITextViewDelegate
{
    
    @IBOutlet weak var bgImage: CustomImageView!
    
    @IBOutlet weak var textFieldTicketType: MSJCustomPickerTextField!
    @IBOutlet weak var textFieldAddress: MSJCustomTextField!
    @IBOutlet weak var textFieldComplaintType: MSJCustomPickerTextField!
    @IBOutlet weak var textViewMessage: MSJCustomTextView!
    @IBOutlet weak var buttonSubmit: MSJRegularButton!
    
    
    
    var mainComplaintArray = Array<Any>()
    var complaintTitleArray = Array<Any>()
    
    var delegate : complaintDelegate?
   
    var ticketTypeArray = Array<Any>()
    var ticketTypeTitleArray = Array<Any>()
    
    
    var lat:String = ""
    var lng:String = ""

    var selectedDate = NSDate.init()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
      
        
        self.textFieldComplaintType.delegate = self
        self.textFieldTicketType.delegate = self
       
        getComplaintsType()
      
        NotificationCenter.default.addObserver(self, selector: #selector(self.isInternetComing(notification:)), name: Notification.Name(IS_INTERNET_COMING), object: nil)
        
          self.getUserLocation()
        
        self.backgroundColorOrImageSetting(view: self.view, imageView: self.bgImage)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.setTabbarToVisible(isVisible: false)
    }

    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        
        print("Imageview tap.")
        // Your action
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.backButtonOnNavigationBar()
        setLanguage()
        
        if textViewMessage.text ==  "Message".localized()
        {
            textViewMessage.textColor = UIColor.lightGray
        }
         self.setTabbarToVisible(isVisible: false)
         self.setAttributedTitleForRightBar(complete: "Open a new Ticket".localized(), CompFont: UIFont.init(name: MSJSharedManager.regularFont(), size: 15.0)!, CompColor: MSJColor.TAB_BAR_BACKGROUND_COLOR, sub: "Ticket".localized(), subFont: UIFont.init(name: MSJSharedManager.boldFont(), size: 15.0)!, SubColor: MSJColor.TAB_BAR_BACKGROUND_COLOR)

    }
    
        
        
    
    // MARK - textfield delegates
    
    
    func getLocation()
    {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "MSJMapVC") as? MSJMapVC
        {
            
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    
    
    
    
   public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
   {
        if textField.isEqual(textFieldTicketType)
        {
            return true
        }
        else
        {

            if self.whiteSpaceValidae(textfield: self.textFieldTicketType.text!)==false
            {
                return false
            }
            if textField.isEqual(textFieldAddress)
            {
                
                getLocation()
                self.view.endEditing(true)
                return false
                
            }
    }
    
    
    return true
    
    }
    
    
   

    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if textField.isEqual(textFieldTicketType)
        {

            self.textFieldTicketType.pickerType = .string(data: ticketTypeTitleArray as! [String])
            
      
            textFieldTicketType.pickerRow.font = UIFont(name: MSJSharedManager.regularFont(), size: 20)
            textFieldTicketType.toolbar.barTintColor = .darkGray
            textFieldTicketType.toolbar.tintColor = MSJColor.TAB_BAR_BACKGROUND_COLOR
        }
        else if textField .isEqual(textFieldComplaintType)
        {
            
            self.textFieldComplaintType.pickerType = .string(data: complaintTitleArray as! [String])
            
          
            textFieldComplaintType.pickerRow.font = UIFont(name: MSJSharedManager.regularFont(), size: 20)
            textFieldComplaintType.toolbar.barTintColor = .darkGray
            textFieldComplaintType.toolbar.tintColor = MSJColor.TAB_BAR_BACKGROUND_COLOR

        }

       
        
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason)
    {
        if textField.isEqual(textFieldTicketType)
        {
            

            
            if self.ticketTypeArray.count > 0
            {
                for i in 0 ..< self.ticketTypeArray.count
                {
                    let obj = self.ticketTypeArray[i] as! MSJComplaint
                    if obj.complaintTitle == self.textFieldTicketType.text
                    {
                        textFieldComplaintType.text = ""
                        textFieldComplaintType.placeholder = obj.complaintPlacholder
                        self.getSubComplaints(complaintId: obj.complaintTypeId!)
                       
                    }
                }
            }
           
        }
       
    }
    
    
 
    
//    @objc func showDatePicker(sender: UIDatePicker)
//    {
//        selectedDate = sender.date as NSDate
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateStyle = DateFormatter.Style.long
//        dateFormatter.timeStyle = DateFormatter.Style.short
//        dateFormatter.doesRelativeDateFormatting = true;
//        let dateStr : String = dateFormatter.string(from: sender.date)
//        textFieldDateAndTime.text = dateStr
//
//    }
    
    
    
    
    // Mark - textview delegate
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        
        if textView.text == "Message".localized()
        {
            textView.text = ""
            textView.textColor = UIColor.black
        }
        else
        {
            textView.textColor = UIColor.black
        }
        
    }
    

    @IBAction func buttonSubmit(_ sender: Any)
    {
        if (UserDefaults.standard.value(forKey: USER_DATA) == nil)
        {
            
            let vc =  self.storyboard?.instantiateViewController(withIdentifier: "MSJLogInVC") as! MSJLogInVC
        
           self.navigationController?.pushViewController(vc, animated: true)
            return;
        }
        
        var dataDictionary = [String: String]()

        if self.whiteSpaceValidae(textfield: self.textFieldTicketType.text!)==false
        {
            
            self.showAlert(title: "Error".localized(), message: "Select Ticket Type".localized())

            return
        }
       
        
        
        
        
         if self.whiteSpaceValidae(textfield: self.textFieldComplaintType.text!)==false
        {
             self.showAlert(title: "Error".localized(), message: textFieldComplaintType.placeholder)
           
            return
        }
        
        if self.whiteSpaceValidae(textfield: self.textFieldAddress.text!)==false
        {
            self.showAlert(title: "Error".localized(), message: "Select Location".localized())
            return
            
        }
    
        
         if self.whiteSpaceValidae(textfield: self.textViewMessage.text!)==false || self.textViewMessage.text! == "Message".localized(){
            
                self.showAlert(title: "Error".localized(), message: "Enter message".localized())
                return
           
        }
       
        
        let dateTimeDict = self.convertDateAndTimeIntoTimeStamp(date: NSDate.init())
        dataDictionary["timestamp"] = dateTimeDict[TIMESTAMP] as? String
        
        if self.mainComplaintArray.count > 0
        {
            for i in 0 ..< self.mainComplaintArray.count
            {
                let obj = self.mainComplaintArray[i] as! MSJComplaint
                    if obj.complaintTitle == self.textFieldComplaintType.text
                    {
                        dataDictionary["type_id"] =  obj.complaintTypeId!
                    }
                }
        }
        
        
        if self.ticketTypeArray.count > 0
        {
            for i in 0 ..< self.ticketTypeArray.count
            {
                let obj = self.ticketTypeArray[i] as! MSJComplaint
                if obj.complaintTitle == self.textFieldTicketType.text
                {
                    dataDictionary["ticket_type"] =  obj.complaintTypeId!
                }
            }
        }
        
        
        
        
        


        
        
       
            let userObject  = MSJSharedManager.getUser()
        
        
            dataDictionary["visit_time"] =  ""
            dataDictionary["apartment_no"] = ""
        
        
            dataDictionary["user_id"] = String(describing: self.customerId())
            dataDictionary["full_name"] = userObject.FullName
            dataDictionary["email"] = userObject.Email
            dataDictionary["phone"] = userObject.Mobile
            dataDictionary["city"] = userObject.CityTitle
            dataDictionary["location"] = self.textFieldAddress.text
            dataDictionary["product"] = ""
            dataDictionary["message"] = self.textViewMessage.text
            dataDictionary["status"] = "open"
            dataDictionary["lat"] = lat
            dataDictionary["lng"] = lng
            print(dataDictionary)
            
       
        buttonSubmit.startAnimation()
        let qualityOfServiceClass = DispatchQoS.QoSClass.background
        let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
        backgroundQueue.async(execute: {

            
                
                let url = BASE_URL + POST_CREATE_COMPLAINT
            MSJApiManager.sharedInstance.postServiceCall(urlString: url, isAlertShow: false, companyId: "", parameters: dataDictionary, header: nil, successCallback: { (dict) in
                  
                    self.buttonSubmit.stopAnimation(animationStyle: .expand, completion:
                        {
                             self.delegate?.complaintDelegate()
                            self.navigationController?.popViewController(animated: true)
                            
                            
                    })
                }) { (error) in
                 
                    self.buttonSubmit.stopAnimation(animationStyle: .expand, completion:
                        {
                            self.showAlert(title: "Error".localized(), message: error)
                    })
                }
            })
        
}
        

    
    
    func getComplaintsType()
    {
        let url = BASE_URL + GET_COMPLAINT_TYPE
        
        MSJApiManager.sharedInstance.getRequest(urlString: url, isAlertShow: true, parameters: nil, successCallback: { (dict) in
             
                print(dict)
                
                let complaintList = dict["complaint_types"] as! NSArray
                for item in complaintList
                {
                    let itemDict = item as! NSDictionary
                    let objCompaliant = MSJComplaint()
                    objCompaliant.complaintTypeId = itemDict["complaint_type_id"] as? String
                    if MSJSharedManager.getArabic()
                    {
                        objCompaliant.complaintTitle = itemDict["complaint_title_ar"] as? String
                        objCompaliant.complaintPlacholder = itemDict["placeholder_ar"] as? String
                        
                        
                    }
                    else
                    {
                        objCompaliant.complaintTitle = itemDict["complaint_title_en"] as? String
                        objCompaliant.complaintPlacholder = itemDict["placeholder_en"] as? String
                    }
                    self.ticketTypeArray.append(objCompaliant)
                    self.ticketTypeTitleArray.append(objCompaliant.complaintTitle!)
                    
                }
                
               
                    
            }, errorCallBack: { (error) in
                print(error)
            })

        
    }
    
    
    func getSubComplaints(complaintId:String)
    {
        mainComplaintArray.removeAll()
        complaintTitleArray.removeAll()
        let url = BASE_URL + GET_COMPLAINT_TYPE + "?complaint_type_id=" + complaintId
        
        MSJApiManager.sharedInstance.getRequest(urlString: url, isAlertShow: true, parameters: nil, successCallback: { (dict) in
            print(dict)
       
            
            let complaintList = dict["complaint_types"] as! NSArray
            for item in complaintList
            {
                let itemDict = item as! NSDictionary
                let objCompaliant = MSJComplaint()
                objCompaliant.complaintTypeId = itemDict["complaint_type_id"] as? String
                if MSJSharedManager.getArabic()
                {
                    objCompaliant.complaintTitle = itemDict["complaint_title_ar"] as? String
                }
                else
                {
                    objCompaliant.complaintTitle = itemDict["complaint_title_en"] as? String
                }
                self.mainComplaintArray.append(objCompaliant)
                self.complaintTitleArray.append(objCompaliant.complaintTitle!)
                
            }
            
           
            
            
            
        }, errorCallBack: { (error) in
            print(error)
        })
        
        
    }
    
    
    func setLanguage()
    {
        
   
        if MSJSharedManager.getArabic()
        {


          
            textFieldTicketType.textAlignment = NSTextAlignment.right
            textFieldAddress.textAlignment = NSTextAlignment.right
            textFieldComplaintType.textAlignment = NSTextAlignment.right
      
            textViewMessage.textAlignment = NSTextAlignment.right




        }
        else
        {
        
            textFieldTicketType.textAlignment = NSTextAlignment.left
            textFieldAddress.textAlignment = NSTextAlignment.left
            textFieldComplaintType.textAlignment = NSTextAlignment.left
            textViewMessage.textAlignment = NSTextAlignment.left

        
            
            
        }
        
        
        
        let paddingView: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let dropDownImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 18, height: 18))
        dropDownImageView.image = UIImage(named: DROP_DOWN_ICON)
        dropDownImageView.contentMode = UIView.ContentMode.scaleAspectFit
        paddingView.addSubview(dropDownImageView)
        textFieldComplaintType.rightView = paddingView
        textFieldComplaintType.rightViewMode = .always
        textFieldComplaintType.leftView = nil
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        dropDownImageView.isUserInteractionEnabled = true
        dropDownImageView.addGestureRecognizer(tapGesture)
        
        
        let ticketView: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let dropDownTicket = UIImageView(frame: CGRect(x: 0, y: 0, width: 18, height: 18))
        dropDownTicket.image = UIImage(named: DROP_DOWN_ICON)
        dropDownTicket.contentMode = UIView.ContentMode.scaleAspectFit
        ticketView.addSubview(dropDownTicket)
        textFieldTicketType.rightView = ticketView
        textFieldTicketType.rightViewMode = .always
        textFieldTicketType.leftView = nil
        

        textFieldTicketType.placeholder = "Select Ticket Type".localized()
      
        textFieldAddress.placeholder = "Location".localized()
        textFieldComplaintType.placeholder = "Complaint Type".localized()
      
  
        textViewMessage.text =  "Message".localized()
        buttonSubmit.setTitle("Submit".localized(), for: UIControl.State.normal)
    }
    
    
    
    @objc func isInternetComing(notification: Notification)
    {
        
        
        if mainComplaintArray.count == 0
        {
            if self.ticketTypeArray.count > 0
            {
                for i in 0 ..< self.ticketTypeArray.count
                {
                    let obj = self.ticketTypeArray[i] as! MSJComplaint
                    if obj.complaintTitle == self.textFieldTicketType.text
                    {
                        textFieldComplaintType.text = ""
                        self.getSubComplaints(complaintId: obj.complaintTypeId!)
                        
                    }
                }
            }
           
        }

        if ticketTypeArray.count == 0
        {
            getComplaintsType()
        }
        
    }
    
    
    
}

extension MSJComplaintVC : LOCATIONSELECT
{
    func locationSelect(address: String, latitude: Double, lognitude: Double) {
        
        self.textFieldAddress.text = address
        self.lat = String(describing:latitude)
        self.lng = String(describing:lognitude)
    }
    
    
}

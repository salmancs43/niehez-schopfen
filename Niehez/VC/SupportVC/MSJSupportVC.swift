//
//  MSJSupportVC.swift
//  MSJ
//
//  Created by Mac on 07/11/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit
import AnimatableReload

class MSJSupportVC: BaseVC ,UITableViewDelegate,UITableViewDataSource,complaintDelegate{

    @IBOutlet weak var newComplaintView: UIView!
    @IBOutlet weak var labelTechnicalSupport: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bgImage: CustomImageView!
    
    
    var complaintListArray = Array<Any>()
    
    
    let refreshControl = UIRefreshControl()
    
    var isRefresControl:Bool = true
    
    var isDelegateCalled:Bool = false
    
    var isComingFromPush : Bool = false
    
    
    @IBOutlet weak var labelNoComplaints: MSJRegularLabel!
    var isLanguageChange : Bool = false
    @IBOutlet weak var labelNewComplaintTicket: MSJRegularLabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        newComplaintView.dropShadow(color: UIColor.lightGray, isRoundConer: true)
        
        self.backgroundColorOrImageSetting(view: self.view, imageView: self.bgImage)
        self.addLeftBarIcon()
        
       
//         tableView.alpha = 0.0
//        
//        UIView.animate(withDuration:0., animations: {
//            self.tableView.alpha = 1.0
//        }, completion: { finished in
//            
//        })
        
        tableView.tableFooterView = UIView()

       // self.createChatButton()
        
        refreshControl.addTarget(self, action: #selector(pullToRefresh(_:)), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        
        isRefresControl = true
        

         NotificationCenter.default.addObserver(self, selector: #selector(self.emptyArray(notification:)), name: Notification.Name(REMOVE_SUPPORT), object: nil)
        
        
      //  NotificationCenter.default.addObserver(self, selector: #selector(self.pushCame(notification:)), name: Notification.Name(IS_SUPPORT_PUSH), object: nil)

        
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        setLanaguage()
        self.backButtonOnNavigationBar()
         self.setTabbarToVisible(isVisible: false)
        
        if isComingFromPush
        {
            isComingFromPush = false
            self.psuhDetail()
            
        }
        tableView.reloadData()
        
        if isLanguageChange != MSJSharedManager.getArabic()
        {
            getCompliantList()
           
            isLanguageChange = MSJSharedManager.getArabic()
        }
        else
        {
            if self.complaintListArray.count == 0
            {
                if !isDelegateCalled
                {
                    getCompliantList()
                }
                
            }
        }
        
        self.addMultiplRightBraButtons(with: UIImage.init(named: SEARCH_BLUE), secondImg: UIImage.init(named: CART_BlUE), ref: self, action1:
            {
                let vc  = self.storyboard?.instantiateViewController(withIdentifier: "MSJSearchVC")
                self.navigationController?.pushViewController(vc!, animated: true)
                
        })
        {
           
            let vc  = self.storyboard?.instantiateViewController(withIdentifier: "MSJAddToCartVC")
            self.navigationController?.pushViewController(vc!, animated: true)
            
        }
        
        
        
//        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "MSJComplaintDetailVC") as! MSJComplaintDetailVC
//        self.navigationController?.pushViewController(vc, animated: true)
    }

   

    @objc func pullToRefresh(_ refreshControl: UIRefreshControl)
    {
        isRefresControl = false
         getCompliantList()
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return complaintListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! MSJSupportCell
        
        
        cell.buttonYes.cornerRadius = 15
        cell.buttonYes.clipsToBounds = true
        
        cell.buttonNo.cornerRadius = 15
        cell.buttonNo.clipsToBounds = true
        
        
      
        let complaintObject = complaintListArray[indexPath.row] as? MSJComplaint
        
        var color = UIColor.red
        
        if complaintObject?.status == "Closed".localized()
        {
            color = MSJColor.GREEN_BUTTON_COLOR
        }
       
        if complaintObject?.closed_request == "1"
        {
            
            cell.yes_height.constant = 30
            cell.top_yes_button_height.constant = 8
            cell.labelCloseTicket.text = "Are you sure you want to close this ticket?".localized()
            
            
        }
        else
        {
            cell.yes_height.constant = 0
            cell.top_yes_button_height.constant = 0
            cell.labelCloseTicket.text = ""
        }
        
        cell.buttonYes .addTarget(self,  action: #selector(buttonYes(button:)), for:.touchUpInside)
        cell.buttonYes.tag = indexPath.row
        
        cell.buttonNo .addTarget(self,  action: #selector(buttonNo(button:)), for:.touchUpInside)
        cell.buttonNo.tag = indexPath.row
        
        
        
        cell.labelRefrence.attributedText = makeAttributedLable(complete: "Request No. ".localized()+(complaintObject?.requestNo!)!+" ("+(complaintObject?.status!)!+")", CompFont: UIFont.init(name: MSJSharedManager.boldFont(), size: 17.0)!, CompColor: MSJColor.white, sub: (" ("+(complaintObject?.status!)!+")"), subFont: UIFont.init(name: MSJSharedManager.regularFont(), size: 15.0)!, SubColor: .white)
        
        if complaintObject?.createdDate != ""
        {
              cell.labelDateAndTime.text = self.convertTimeStampIntoDateString(unixtimeInterval:(complaintObject?.createdDate)!, isTime: false)
            
        }
        
        cell.labelDateAndTime.font = UIFont.init(name: MSJSharedManager.regularFont(), size: cell.labelDateAndTime.font.pointSize)
        cell.labelDescription.font = UIFont.init(name: MSJSharedManager.regularFont(), size: cell.labelDescription.font.pointSize)
        cell.labelDescription.textColor = .white
        
        if complaintObject?.unread != "0"
        {
            cell.labelDescription.textColor = UIColor.white
            cell.labelDescription.font = UIFont.init(name: MSJSharedManager.boldFont(), size: cell.labelDescription.font.pointSize)
        }
        
        
      
        
        cell.labelDescription.text = complaintObject?.message
        

        
      
        
        return cell
        
    }
    
   
    
   
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let complaintObject = complaintListArray[indexPath.row] as? MSJComplaint
//        if complaintObject?.status == "Closed"
//        {
//            return;
//        }
        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "MSJComplaintDetailVC") as! MSJComplaintDetailVC
        vc.complaintObject = complaintObject
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
  
    @objc func buttonYes(button: UIButton)
    {
        
         updateTicket(number: button.tag, isPressedYes: true)
    }
    
    @objc func buttonNo(button: UIButton)
    {
        updateTicket(number: button.tag, isPressedYes: false)
    }

    @IBAction func buttonNewComplaint(_ sender: Any)
    {
        if (UserDefaults.standard.value(forKey: USER_DATA) == nil)
        {
            
            let vc =  self.storyboard?.instantiateViewController(withIdentifier: "MSJLogInVC") as! MSJLogInVC
            self.navigationController?.pushViewController(vc, animated: true)
            return;
        }
        
        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "MSJComplaintVC") as! MSJComplaintVC
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
       // self.performSegue(withIdentifier: "MSJComplaintVC", sender: self)
    }
    
    
    func complaintDelegate()
    {
        isDelegateCalled = true
        getCompliantList()
        
    }
    
    
    func removeData()
    {
        self.complaintListArray.removeAll()
        tableView.reloadData()
       
    }
    
    
    @objc func emptyArray(notification: Notification)
    {
        self.labelNoComplaints.isHidden = false
        self.complaintListArray.removeAll()
        tableView.reloadData()
    }
    
    func getCompliantList()
    {
      
        removeData()
       
        if (UserDefaults.standard.value(forKey: USER_DATA) != nil)
        {
            
            let url = BASE_URL + GET_COMPLAINT_BY_ID + "?user_id=" + self.customerId()
        
            MSJApiManager .sharedInstance.getRequest(urlString: url, isAlertShow: self.isRefresControl, parameters: nil, successCallback: { (dict) in
                
               
                print(dict)
                self.isDelegateCalled = false
                self.isRefresControl = true
                self.refreshControl.endRefreshing()
                
                
                let complaintList = dict["tickets"] as! NSArray
                
                for item in complaintList
                {
                    var complaintListDict = item as! NSDictionary
                    complaintListDict = complaintListDict.removeNull()
                    let mainList = MSJComplaint()
                    
                    mainList.address = complaintListDict["location"] as? String
                    mainList.city = complaintListDict["city"] as? String
                    mainList.complaintId = complaintListDict["id"] as? String
                    mainList.supportComplaintTypeId = complaintListDict["type_id"] as? String
                    mainList.customerId = complaintListDict["customerId"] as? String
                    mainList.email = complaintListDict["email"] as? String
                    mainList.fullName = complaintListDict["full_name"] as? String
                    mainList.message = complaintListDict["message"] as? String
                    mainList.mobile = complaintListDict["phone"] as? String
                    mainList.productBrand = complaintListDict["product"] as? String
                    mainList.requestNo = complaintListDict["ticket_id"] as? String
                    mainList.status = complaintListDict["status"] as? String
                    mainList.createdDate = complaintListDict["timestamp"] as? String
                    mainList.lng = complaintListDict["lng"] as? String
                    mainList.lat = complaintListDict["lat"] as? String
                    mainList.ticket_type = complaintListDict["ticket_type"] as? String
                    mainList.visit_time = complaintListDict["visit_time"] as? String
                    mainList.unread = complaintListDict["unread"] as? String
                    mainList.closed_request = complaintListDict["closed_request"] as? String
                    
                    
                    
                    
                    
                    if MSJSharedManager.getArabic() == true
                    {
                        mainList.ticket_type_parent = complaintListDict["ticket_type_parent_ar"] as? String
                        mainList.ticket_type_child = complaintListDict["ticket_type_child_ar"] as? String
                       
                    }
                    else
                    {
                        mainList.ticket_type_parent = complaintListDict["ticket_type_parent_en"] as? String
                        mainList.ticket_type_child = complaintListDict["ticket_type_child_en"] as? String
                       
                    }
                    
                    
                    mainList.status =   mainList.status?.capitalizingFirstLetter().localized()
                    mainList.ticket_type =   mainList.ticket_type?.capitalizingFirstLetter().localized()
                    
                    
                    if mainList.requestNo != nil && mainList.status != nil
                    {
                        self.complaintListArray.append(mainList)
                    }
                    
                }
                
                if self.complaintListArray.count > 0
                {
                    self.labelNoComplaints.isHidden = true
                }
                else
                {
                    self.labelNoComplaints.isHidden = false
                }
                
  //              self.tableView.reloadData()
//                let leftAnimation = TableViewAnimation.Cell.left(duration: 0.5)
//                self.tableView.animate(animation: leftAnimation, indexPaths: nil, completion: nil)
//
                AnimatableReload.reload(tableView: self.tableView, animationDirection: ANIMATION_DOWN)
                
            }) { (errorString) in
                
                self.isRefresControl = true
                self.refreshControl.endRefreshing()
                self.labelNoComplaints.isHidden = false
                
                
            }
        }
        else
        {
            
            self.isRefresControl = true
            self.refreshControl.endRefreshing()
            
            
        }
     
        
    }
    
    func setLanaguage()
    {
        
          labelTechnicalSupport.attributedText = makeAttributedLable(complete: "Technical Support Request".localized(), CompFont: UIFont.init(name:  MSJSharedManager.regularFont(), size: 15.0)!, CompColor: UIColor.white, sub: "Technical".localized(), subFont: UIFont.init(name:  MSJSharedManager.mediumFont(), size: 15.0)!, SubColor: UIColor.white)
        labelNewComplaintTicket.text = "Open a new Ticket".localized()
         labelNewComplaintTicket.font = UIFont.init(name:MSJSharedManager.regularFont(), size:labelNewComplaintTicket.font.pointSize)
        labelNoComplaints.text = "No Complaints".localized()
        
        if MSJSharedManager.getArabic()
        {
            labelTechnicalSupport.textAlignment = NSTextAlignment.right
        }
        else
        {
            labelTechnicalSupport.textAlignment = NSTextAlignment.left
        }
        
        
    }
    
    
     func psuhDetail()
    {
        let dict = UserDefaults.standard.value(forKey: NOTIFICATION_DATA) as! NSDictionary
        print(dict)
        UserDefaults.standard.removeObject(forKey: NOTIFICATION_DATA)
        UserDefaults.standard.synchronize()
        if (dict["gcm.notification.status"] as! String == "1")
        {
            getCompliantList()
        }
        else
        {
            let vc  = self.storyboard?.instantiateViewController(withIdentifier: "MSJComplaintDetailVC") as! MSJComplaintDetailVC
            vc.ticketId =  dict["gcm.notification.ticket_id"] as! String
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    
//
//    @objc func pushCame(notification: Notification)
//    {
//        let dict = UserDefaults.standard.value(forKey: NOTIFICATION_DATA) as! NSDictionary
//        print(dict)
//        UserDefaults.standard.removeObject(forKey: NOTIFICATION_DATA)
//        UserDefaults.standard.synchronize()
//        if (dict["gcm.notification.status"] as! String == "1")
//        {
//            getCompliantList()
//        }
//        else
//        {
//            let vc  = self.storyboard?.instantiateViewController(withIdentifier: "MSJComplaintDetailVC") as! MSJComplaintDetailVC
//            vc.ticketId =  dict["gcm.notification.ticket_id"] as! String
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
//    }
    
    func updateTicket(number:NSInteger , isPressedYes:Bool)
    {
         let complaintObject = complaintListArray[number] as? MSJComplaint
        var dataDictionary = [String: String]()
      
        dataDictionary["ticket_id"] =  complaintObject?.requestNo!
        if isPressedYes
        {
            dataDictionary["closed_request"] = "2"
            
        }
        else
        {
              dataDictionary["closed_request"] = "3"
                dataDictionary["status"] = "open"
        }
        
        let url = BASE_URL + UPDATE_TICKET
        MSJApiManager.sharedInstance.postServiceCall(urlString: url, isAlertShow: true, companyId: "", parameters: dataDictionary, header: nil, successCallback: { (dict) in
            
            if isPressedYes
            {
                 self.showAlert(title: "Success", message: "Ticket closed successfully".localized())
                complaintObject?.closed_request = "2";
            }
            else
            {
                 self.showAlert(title: "Success", message: "Ticket opened successfully".localized())
                complaintObject?.closed_request = "3";
                complaintObject?.status = "Open".localized()

            }
           
            
            
            self.tableView.reloadData()
            
            
        }) { (error) in
            
             self.showAlert(title: "Error", message: error)
            
        }
    }
    
}

extension String
{
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
    
}

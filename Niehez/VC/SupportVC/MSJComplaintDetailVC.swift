//
//  MSJComplaintDetailVC.swift
//  MSJ
//
//  Created by Mac on 19/01/2018.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit
import GrowingTextView
import IHKeyboardAvoiding
import IQKeyboardManager
import AnimatableReload

class MSJComplaintDetailVC: BaseVC,UITableViewDelegate,UITableViewDataSource,GrowingTextViewDelegate{

    
    var ticketId : String = "0"
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var growingTextView: MSJGrowing!
    var complaintObject : MSJComplaint!
    
    @IBOutlet weak var view_height: NSLayoutConstraint!
    @IBOutlet weak var growingView: UIView!
    
    @IBOutlet weak var sendButton: UIButton!
    var commentArray = Array<Any>()
    
    @IBOutlet weak var labelWaitingForReply: MSJRegularLabel!
    
    
    let refreshControl = UIRefreshControl()
    
    var isRefresControl:Bool = true
    
    @IBOutlet weak var bottom_GrowingView: NSLayoutConstraint!
    override func viewDidLoad()
    {
        super.viewDidLoad()
      
      
        
        tableView.estimatedRowHeight = 60
        tableView.estimatedRowHeight = UITableView.automaticDimension
        
        
      
        growingTextView.delegate = self
        growingTextView.trimWhiteSpaceWhenEndEditing = false
        
         growingTextView.maxHeight = 200
        growingTextView.minHeight = 48
        
        growingTextView.text = "Write a Reply".localized()
        growingTextView.textColor = UIColor.lightGray
        
        refreshControl.addTarget(self, action: #selector(pullToRefresh(_:)), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        
        isRefresControl = true
        
        if UIScreen.main.bounds.size.height == 812
        {
           bottom_GrowingView.constant = 6
        }
        
        
        
        let originalImage = UIImage(named: "Send Message Button")
        let tintedImage = originalImage?.withRenderingMode(.alwaysTemplate)
        sendButton.setImage(tintedImage, for: .normal)
       
        if ticketId == "0"
        {
            if complaintObject.status == "Closed".localized()
            {
                sendButton.tintColor = UIColor.lightGray
                sendButton.isUserInteractionEnabled = false
                growingTextView.isUserInteractionEnabled = false
                
            }
            else
            {
                sendButton.tintColor = MSJColor.GREEN_BUTTON_COLOR
                sendButton.isUserInteractionEnabled = true
                growingTextView.isUserInteractionEnabled = true
            }
        }
        
       
        
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.emptyArray(notification:)), name: Notification.Name(REMOVE_SUPPORT), object: nil)
       
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
      super.viewWillAppear(animated)
        
        setLanguage()
        self.tableView.isHidden = true
      self.backButtonOnNavigationBar()
     IQKeyboardManager.shared().isEnabled = false
         self.setTabbarToVisible(isVisible: false)
        if ticketId != "0"
        {
            self.getOneComplaint()
        }
        else
        {
            self.setData()
             complaintObject.unread = "0"
        }
        
       
      
        
//        let image = UIImage(named: "Send Message Button")?.withRenderingMode(.alwaysTemplate)
//        sendButton.setImage(image, for: .normal)
//        sendButton.tintColor = UIColor.lightGray
    }
    
    
    @objc func pullToRefresh(_ refreshControl: UIRefreshControl)
    {
        isRefresControl = false
        getComments()
        
    }

    
    
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
        IQKeyboardManager.shared().isEnabled = true
    }
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if complaintObject != nil
        {
             return 1 + commentArray.count
        }
       
        return  0
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.row == 0
        {
           
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MSJComplaintDetailCell

            if complaintObject != nil
            {

                cell.labelRequestNo.attributedText = makeAttributedLable(complete: "Request No. ".localized()+(complaintObject?.requestNo!)!+" ("+(complaintObject?.status!)!+")", CompFont: UIFont.init(name: MSJSharedManager.boldFont(), size: 17.0)!, CompColor:UIColor.white, sub: (" ("+(complaintObject?.status!)!+")"), subFont: UIFont.init(name: MSJSharedManager.regularFont(), size: 15.0)!, SubColor: UIColor.white)

                if complaintObject?.createdDate != ""
                {
                    

                     cell.labelDate.text = self.convertTimeStampIntoDateString(unixtimeInterval:(complaintObject?.createdDate)!, isTime: false)
                }

               
                cell.labelComplaintType.text = complaintObject?.ticket_type_parent

                cell.labelProductName.text = complaintObject?.productBrand

                cell.labelComplaintDetail.text = complaintObject?.message
                
                if MSJSharedManager.getArabic()
                {
                    
                    cell.labelComplaintType.textAlignment = NSTextAlignment.right
                     cell.labelProductName.textAlignment = NSTextAlignment.right
                     cell.labelComplaintDetail.textAlignment = NSTextAlignment.right
                     cell.labelRequestNo.textAlignment = NSTextAlignment.right
                    
                    
                    
                }
                else
                {
                    cell.labelComplaintType.textAlignment = NSTextAlignment.left
                    cell.labelProductName.textAlignment = NSTextAlignment.left
                    cell.labelComplaintDetail.textAlignment = NSTextAlignment.left
                    cell.labelRequestNo.textAlignment = NSTextAlignment.left
                    
                  
                    
                }
                
                
                
            }
            return cell

        }
        else
        {
            
        
            let object = commentArray[indexPath.row - 1]  as! MSJCommentObject
            if object.customerId == self.customerId()
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "commentmine") as! MSJCommentCell
                cell.labelDate.textColor = UIColor.darkGray
                cell.labelDescription.textColor = UIColor.gray
                cell.labelDescription.text = object.message
                if object.date != ""
                {
                    cell.labelDate.text = self.convertTimeStampIntoDateString(unixtimeInterval: (object.date)!, isTime: false)

                }
                cell.labelName.text = "Me".localized()
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "commentsupport") as! MSJCommentCell
                cell.labelDate.textColor = UIColor.darkGray
                cell.labelDescription.textColor = UIColor.gray
                 cell.labelDescription.text = object.message
                if object.date != ""
                {

                cell.labelDate.text = self.convertTimeStampIntoDateString(unixtimeInterval: (object.date)!, isTime: false)

                }
                
                cell.labelName.text = "Support".localized()
                return cell
            }
            
        }
   }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    func getOneComplaint()
    {
        if (UserDefaults.standard.value(forKey: USER_DATA) != nil)
        {
            let url = BASE_URL + GET_SINGLE_COMPLAINT + "?ticket_id=" + ticketId
            
            MSJApiManager .sharedInstance.getRequest(urlString: url, isAlertShow: true, parameters: nil, successCallback: { (dict) in
                
                
                 
                     let complaintListDict = dict["ticket_info"] as! NSDictionary
                
                    self.complaintObject = MSJComplaint()
                
                    self.complaintObject.address = complaintListDict["location"] as? String
                    self.complaintObject.city = complaintListDict["city"] as? String
                    self.complaintObject.complaintId = complaintListDict["id"] as? String
                    self.complaintObject.supportComplaintTypeId = complaintListDict["type_id"] as? String
                    self.complaintObject.customerId = complaintListDict["customerId"] as? String
                    self.complaintObject.email = complaintListDict["email"] as? String
                    self.complaintObject.fullName = complaintListDict["full_name"] as? String
                    self.complaintObject.message = complaintListDict["message"] as? String
                    self.complaintObject.mobile = complaintListDict["phone"] as? String
                    self.complaintObject.productBrand = complaintListDict["product"] as? String
                    self.complaintObject.requestNo = complaintListDict["ticket_id"] as? String
                    self.complaintObject.status = complaintListDict["status"] as? String
                    self.complaintObject.createdDate = complaintListDict["timestamp"] as? String
                    self.complaintObject.lng = complaintListDict["lng"] as? String
                    self.complaintObject.lat = complaintListDict["lat"] as? String
                    self.complaintObject.ticket_type = complaintListDict["ticket_type"] as? String
                    self.complaintObject.visit_time = complaintListDict["visit_time"] as? String
                    self.complaintObject.unread = complaintListDict["unread"] as? String
                
                if MSJSharedManager.getArabic() == true
                {
                    self.complaintObject.ticket_type_parent = complaintListDict["ticket_type_parent_ar"] as? String
                    self.complaintObject.ticket_type_child = complaintListDict["ticket_type_child_ar"] as? String
                    
                }
                else
                {
                    self.complaintObject.ticket_type_parent = complaintListDict["ticket_type_parent_en"] as? String
                    self.complaintObject.ticket_type_child = complaintListDict["ticket_type_child_en"] as? String
                    
                }
                
                    self.complaintObject.status =   self.complaintObject.status?.capitalizingFirstLetter().localized()
                    self.complaintObject.ticket_type =   self.complaintObject.ticket_type?.capitalizingFirstLetter().localized()
                     self.complaintObject.unread = "0"
                
                if self.ticketId != "0"
                {
                    if self.complaintObject.status == "Closed".localized()
                    {
                        self.sendButton.tintColor = UIColor.lightGray
                        self.sendButton.isUserInteractionEnabled = false
                        self.growingTextView.isUserInteractionEnabled = false
                        
                    }
                    else
                    {
                        self.sendButton.tintColor = MSJColor.GREEN_BUTTON_COLOR
                        self.sendButton.isUserInteractionEnabled = true
                        self.growingTextView.isUserInteractionEnabled = true
                    }
                }
                
                
                     
                  
                     self.setData()
                
                
            }) { (errorString) in
                
                print(errorString)
                
            }
        }
    }
    
    
    func setData()
    {
        tableView.reloadData()
       
        getComments()
        
    }
    
    
    // TextView Delegate
    
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat)
    {
        UIView.animate(withDuration: 0.2)
        {
            self.view.layoutIfNeeded()
          
            self.view_height.constant = height
            
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        KeyboardAvoiding.avoidingView = self.growingView
         self.scrollToBottom()
        print(textView.text)
        if textView.text == "Write a Reply".localized()
        {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidChange(_ textView: UITextView)
    {
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        scrollToBottom()
        
    }
    
    
    func scrollToBottom()
    {
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: self.commentArray.count, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }

    

    @IBAction func sendButton(_ sender: Any)
    {
        self.view.endEditing(true)
        if self.whiteSpaceValidae(textfield: self.growingTextView.text!)==false || self.growingTextView.text == "Write a Reply".localized()
        {
            self.showAlert(title: "Error".localized(), message: "Write a Reply".localized())
            
        }
        else
        {
            let dateTimeDict = self.convertDateAndTimeIntoTimeStamp(date: NSDate.init())
            var dataDictionary = [String: String]()
            dataDictionary["user_id"] = self.customerId()
            dataDictionary["ticket_id"] =  complaintObject.requestNo!
            dataDictionary["message"] = self.growingTextView.text
             dataDictionary["timestamp"]  = dateTimeDict[TIMESTAMP] as? String

        
                let url = BASE_URL + POST_COMMENT
            MSJApiManager.sharedInstance.postServiceCall(urlString: url, isAlertShow: true, companyId: "", parameters: dataDictionary, header: nil, successCallback: { (dict) in
                   
                 
                    let commentObject = MSJCommentObject()
                    commentObject.message = self.growingTextView.text
                    commentObject.customerId = self.customerId()
                    commentObject.date = dateTimeDict[TIMESTAMP] as? String
                    self.commentArray.append(commentObject)
                    
                    self.growingTextView.text = ""
                    
                    self.growingTextView.text = "Write a Reply".localized()
                    self.growingTextView.textColor = UIColor.lightGray
                    self.tableView.reloadData()
                    self.scrollToBottom()
                    
                }) { (error) in
                    
                  // self.showAlert(title: "Error", message: error)
                    
                }
       
        }
      
    }
   
    
    func getComments()
    {
        
        
        if (UserDefaults.standard.value(forKey: USER_DATA) != nil)
        {
          
            let url = BASE_URL + GET_COMMENT + "?ticket_id=" + complaintObject.requestNo!
          
            
            MSJApiManager .sharedInstance.getRequest(urlString: url, isAlertShow: self.isRefresControl, parameters: nil, successCallback: { (dict) in
                
                self.isRefresControl = true
                self.refreshControl.endRefreshing()
             print(dict)
                
                let commetList = dict["ticket_comments"] as! NSArray
                  self.commentArray.removeAll()
                for item in commetList
                {
                    var complaintDict = item as! NSDictionary
                    complaintDict = complaintDict.removeNull()
                    let commentObject = MSJCommentObject()
                    commentObject.message = complaintDict["message"] as? String
                    commentObject.customerId = complaintDict["user_id"] as? String
                    commentObject.date = complaintDict["timestamp"] as? String
                    commentObject.is_read = complaintDict["is_read"] as? String
                    self.commentArray.append(commentObject)
                   
                    
                }
                self.tableView.isHidden = false
                AnimatableReload.reload(tableView: self.tableView, animationDirection: ANIMATION_DOWN)
               // self.tableView.reloadData()
                self.scrollToBottom()
                self.updateReadStatus()
                
            }) { (errorString) in
                
                self.isRefresControl = true
                self.refreshControl.endRefreshing()
               // self.showAlert(title: "Error", message: errorString)
                
            }
            
            
        }

    }
    
   
   @objc func emptyArray(notification: Notification)
   {
    
        self.navigationController?.popViewController(animated: true)
    
    }
    
    
    func updateReadStatus()
    {
        var dataDictionary = [String: String]()
        dataDictionary["user_id"] = self.customerId()
        dataDictionary["ticket_id"] =  complaintObject.requestNo!

        
        
        let url = BASE_URL + UPDATE_READ_STATUS
        MSJApiManager.sharedInstance.postServiceCall(urlString: url, isAlertShow: false, companyId: "", parameters: dataDictionary, header: nil, successCallback: { (dict) in
            
           
            
        }) { (error) in
            
            // self.showAlert(title: "Error", message: error)
            
        }
    }
  
    func setLanguage()
    {
        self.growingTextView.text = "Write a Reply".localized()
        if MSJSharedManager.getArabic()
        {
            
            growingTextView.textAlignment = NSTextAlignment.right
            
           
        }
        else
        {
            
            growingTextView.textAlignment = NSTextAlignment.left
           
        }
    }
    
    
   

}

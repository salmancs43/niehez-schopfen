//
//  NiehezTermsAndConditionsViewController.swift
//  Niehez
//
//  Created by Macbook on 30/05/2022.
//  Copyright © 2022 Mac. All rights reserved.
//

import UIKit

class NiehezTermsAndConditionsViewController: BaseVC {

    @IBOutlet weak var backGroundImage: UIImageView!
    @IBOutlet weak var termsLabel: UILabel!
    
    let object = MSJSharedManager.getAppSetting()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.getTermsAndConditions()
        self.navigationItem.title = "Terms & Conditions"
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        ///Set Navbar
        self.backButtonOnNavigationBar()
        self.setTabbarToVisible(isVisible: false)
        self.backgroundColorOrImageSetting(view: self.view, imageView: self.backGroundImage)
    }

   
    
    func getTermsAndConditions() {
        
        let url = BASE_URL + GET_PAGE_DETAIL
        
        var parameters: [String:Any] = ["NoVersion":"true"]
        parameters["CompanyID"] = "2"
        parameters["Type"] = "Terms"
        
        ApiManager.getRequest(urlString: url, isAlertShow: false, parameters: parameters) { dict in
            
            print(dict)
            
            if let data = dict["page_data"] as? NSDictionary {
                
                let desc = data["Description"] as? String ?? ""
                self.termsLabel.text = desc.htmlToString
                
            }
            
        } errorCallBack: { errorString in
            print("Error")
        }
    }

}

//
//  MSJLogInVC.swift
//  MSJ
//
//  Created by Mac on 11/10/17.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit
import Firebase
import IQKeyboardManager

class MSJLogInVC: BaseVC, UITextFieldDelegate, UITextViewDelegate {
    
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var btnCreateNewAcconut: UIButton!
    @IBOutlet weak var topHeightLayout: NSLayoutConstraint!
    @IBOutlet weak var buttonSignIn: MSJRegularButton!
    @IBOutlet weak var textFieldEmail: MSJCustomTextField!
    @IBOutlet weak var textFieldPassword: MSJCustomTextField!
    @IBOutlet weak var labelTitleLogIn: MSJBoldLabel!
    @IBOutlet weak var buttonForgotPassword: UIButton!
    
    let object = MSJSharedManager.getAppSetting()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
     

        if  UIScreen.main.bounds.size.height == 568 {
            topHeightLayout.constant = 20
        }

    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.backgroundColorOrImageSetting(view: self.view, imageView: self.bgImage)
        self.setLoginLogo()
        self.backButtonOnNavigationBar()
        self.setupCenterImageButton()
        self.setLanguage()
        self.setTabbarToVisible(isVisible: false)
        self.navigationController?.navigationBar.isHidden = true
        let str = makeAttributedLable(complete: "Forgot Password?".localized(), CompFont: UIFont.init(name: MSJSharedManager.regularFont(), size: 13.0)!, CompColor: UIColor.white, sub: "Forgot Password?".localized(), subFont: UIFont.init(name:MSJSharedManager.boldFont(), size: 15.0)!, SubColor: UIColor.white)
        buttonForgotPassword.setAttributedTitle(str, for: .normal)
        
        
         let createStr = makeAttributedLable(complete: "Don't have an account? Register Now".localized(), CompFont: UIFont.init(name: MSJSharedManager.regularFont(), size: 13.0)!, CompColor: UIColor.white, sub: "Register Now".localized(), subFont: UIFont.init(name:MSJSharedManager.boldFont(), size: 15.0)!, SubColor: UIColor.white)
         btnCreateNewAcconut.setAttributedTitle(createStr, for: .normal)
        
//        btnCreateNewAcconut.titleLabel?.attributedText = makeAttributedLable(complete: "Don't have an account? Register Now".localized(), CompFont: UIFont.init(name: MSJSharedManager.regularFont(), size: 13.0)!, CompColor: UIColor.white, sub: "Register Now".localized(), subFont: UIFont.init(name:MSJSharedManager.boldFont(), size: 15.0)!, SubColor: UIColor.white)
        
//
//        buttonForgotPassword.titleLabel?.attributedText = makeAttributedLable(complete: "Forgot Password?".localized(), CompFont: UIFont.init(name: MSJSharedManager.boldFont(), size: 12.0)!, CompColor: UIColor.white, sub: "Forgot Password?".localized(), subFont: UIFont.init(name:MSJSharedManager.boldFont(), size: 12.0)!, SubColor: UIColor.white)
        
       
        self.textFieldEmail.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked))
        self.textFieldPassword.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked))
        
    }
    
    @objc func doneButtonClicked(_ sender: Any) {
       self.view.endEditing(true)
    }
    
    func setLoginLogo() {
        
        
        //Splash Background Color end----
        // set login logo image
        
        
        if object.LoginLogo != ""
        {
            
            logoImage.downLoadImageIntoImageView(url: IMG_BASE_URL + object.LoginLogo)
    
            labelTitleLogIn.isHidden = true
            
        } else {
            logoImage.isHidden = true
        }
        
       
        
    }
    
    @IBAction func buttonSignUp(_ sender: Any) {
        
        let vc =  self.storyboard?.instantiateViewController(withIdentifier: "NiehezSignUpViewController") as! NiehezSignUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func buttonSignIn(_ sender: Any) {
        
        var dataDictionary = [String: String]()
        
        if self.isValidEmail(testStr: self.textFieldEmail.text!) == false {
            self.showAlert(title: "Error".localized(), message: "Enter valid email".localized())
        } else if self.whiteSpaceValidae(textfield: self.textFieldPassword.text!) == false {
            self.showAlert(title: "Error".localized(), message: "Enter password".localized())
        } else {
            
            dataDictionary["Email"] = self.textFieldEmail.text
            dataDictionary["Password"] = self.textFieldPassword.text
            
          ///Commented by Sikandar
//            if UserDefaults.standard.value(forKey: WITHOUT_LOGIN_ID) != nil
//            {
//               let tempId = String(describing:UserDefaults.standard.value(forKey: WITHOUT_LOGIN_ID)!)
//                dataDictionary["temp_order_key"] = tempId
//            }
            
            userLogIn(dictionary:dataDictionary as NSDictionary , button: buttonSignIn)
        
        }
    
    }
    
    @IBAction func buttonCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func userLogIn(dictionary:NSDictionary , button : MSJRegularButton) {
     
        button.startAnimation()
        
        let qualityOfServiceClass = DispatchQoS.QoSClass.background
        let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
        backgroundQueue.async(execute: {
            
            var normalDict = [String: String]()
            normalDict = dictionary as! [String : String]
            
            normalDict["DeviceType"] = DEVICE_TYPE
            normalDict["OS"] = "ios"
            normalDict["DeviceToken"] = UserDefaults.standard.string(forKey: TOKEN) ?? ""
            
            let url = BASE_URL + LOGIN_URL
            
            ApiManager.postServiceCall(urlString: url, isAlertShow: false, parameters:normalDict, header: nil, successCallback: { (dict) in
                
                print(dict)
                
                button.stopAnimation(animationStyle: .expand, completion: {

                    print(dict)
                    self.removeWithoutLoginId()
                    let userObject:MSJUser = MSJUser.parseDataOfUserInfo(userDict: dict)
                    MSJSharedManager.setUser(userObject: userObject)

                    if userObject.IsMobileVerified == "1" {

                        self.twoButtonAlert(title: "Alert".localized(), message: "Verify your number".localized(), btnTitle: "Cancel".localized(), buttonTwo: "Verify".localized(), successCallback: self.verifyPhone)
                    
                    } else if userObject.IsEmailVerified == "1" {

                        self.twoButtonAlert(title: "Alert".localized(), message: "Verify your email".localized(), btnTitle: "Cancel".localized(), buttonTwo: "Verify".localized(), successCallback: self.verifyPhone)
                        
                    } else {

                        self.saveCustomerData(dict: dict)

                        
                        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "MSJTabBar"){
                            self.navigationController?.pushViewController(vc, animated: false)

                        }

                       
                        
                    }

                })
            
            }) { (error) in
              
                button.stopAnimation(animationStyle: .expand, completion: {
                    
                    if error == "Your account is not verified.Otp is sent to your mobile number." {
                        
                        self.twoButtonAlert(title: "Alert".localized(), message: "Verify your number".localized(), btnTitle: "Cancel".localized(), buttonTwo: "Verify".localized(), successCallback: self.verifyPhone)
                        
                    } else {
                        
                        self.showAlert(title: "Error".localized(), message: error)
                        
                    }
    
                })
                
            }
            
        })
        
    }
    
    func verifyPhone() {
    
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MSJVerifyPhoneVC") as! MSJVerifyPhoneVC
        vc.isComingFromLogin = true
        self.navigationController?.pushViewController(vc, animated: true)
    
    }

    @IBAction func buttonForgotPassword(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MSJForgotPasswordVC") as! MSJForgotPasswordVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
//    func updateProfile()
//    {
//        let userObject = MSJSharedManager.getUser()
//        var dataDictionary = [String: String]()
//         dataDictionary["user_id"] = userObject.customerId!
//        dataDictionary["device_type"] = "ios"
//        dataDictionary["device_token"] = MSJSharedManager.getFCMTokenFromUserDefaults()
//
//        let url = BASE_URL + UPDATE_USER_PROFILE_DATA
//
//        MSJApiManager.sharedInstance.postServiceCall(urlString: url, isAlertShow: false, companyId: "", parameters: dataDictionary, header: nil, successCallback: { (dict) in
//
//
//        }) { (error) in
//
//
//        }
//
//    }
    
    func setLanguage() {
        
        if MSJSharedManager.getArabic() {
            
            textFieldEmail.textAlignment = NSTextAlignment.right
            textFieldPassword.textAlignment = NSTextAlignment.right
        
        } else {
            
            textFieldEmail.textAlignment = NSTextAlignment.left
            textFieldPassword.textAlignment = NSTextAlignment.left
        
        }
        
        textFieldEmail.placeholder = "Email".localized()
        textFieldPassword.placeholder = "Password".localized()
        labelTitleLogIn.text = "Log In".localized()
        buttonSignIn.setTitle("Log In".localized(), for: UIControl.State.normal)
        
    }
    
}

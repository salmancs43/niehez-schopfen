//
//  NiehezSignUpViewController.swift
//  Niehez
//
//  Created by Macbook on 31/03/2022.
//  Copyright © 2022 Mac. All rights reserved.
//

import UIKit
import Foundation
import Kingfisher

class NiehezSignUpViewController: BaseVC {
    
    //IBOutlet
    @IBOutlet weak var checkBoxButton: UIButton!
    @IBOutlet weak var backGroundImage: UIImageView!
    @IBOutlet weak var profileTopView: UIView!
    @IBOutlet weak var fullNameOuterView: UIView!
    @IBOutlet weak var userNameOuterView: UIView!
    @IBOutlet weak var emailOuterView: UIView!
    @IBOutlet weak var mobileOuterView: UIView!
    @IBOutlet weak var genderOuterView: UIView!
    @IBOutlet weak var dateOfBirthOuterView: UIView!
    @IBOutlet weak var cityOuterView: UIView!
    @IBOutlet weak var passwordOuterView: UIView!
    @IBOutlet weak var confirmPasswordOuterView: UIView!
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var gendertextField: CustomAAPickerView!
    @IBOutlet weak var dateOfBirthTextField: CustomAAPickerView!
    @IBOutlet weak var cityTextField: CustomAAPickerView!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var imageButton: UIButton!
    @IBOutlet weak var btnSignUp_Update: UIButton!
    @IBOutlet weak var checkboxOuterView: UIView!
    @IBOutlet weak var termsButton: UIButton!
    
    //Variables and Constants
    
    let object = MSJSharedManager.getAppSetting()
    var citiesArray = [CitiesModel]()
    
    var cityId = ""
    
    var isProfileImage = false
    
    var isFromEditProfile = false
    
    var userData = MSJSharedManager.getUser()
    
    var isCheckBox: Bool = false
    
    /// UIImagePickerController instance
    private let imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureExpiryPicker()

        self.setUI()
        
        self.getCities()
        
        //image picker delegate as self
        self.imagePicker.delegate = self
        
        self.imageButton.setTitle("", for: .normal)
        
        if isFromEditProfile {
            
            self.checkboxOuterView.isHidden = true
            self.termsButton.isHidden = true
            self.UpdateData()
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        ///Set Navbar
        self.navigationController?.navigationBar.isHidden = false
        self.backButtonOnNavigationBar()
        
        self.setTabbarToVisible(isVisible: false)
        
        self.backgroundColorOrImageSetting(view: self.view, imageView: self.backGroundImage)
        
    }
    
    ///Updatedata
    func UpdateData() {
        self.passwordOuterView.isHidden = true
        self.confirmPasswordOuterView.isHidden = true
        self.fullNameTextField.text = self.userData.FullName
        self.emailTextField.text = self.userData.Email
        self.gendertextField.text = self.userData.Gender
        self.mobileTextField.text = self.userData.Mobile
        if self.userData.DateOfBirth != "0000-00-00" {
            self.dateOfBirthTextField.text = self.userData.DateOfBirth
        }
        self.cityTextField.text = self.userData.CityTitle
        
        if self.userData.Image != nil && self.userData.Image != "" {
            let urlString = IMG_BASE_URL + self.userData.Image!
            let url = URL(string: urlString)
            DispatchQueue.main.async {
                self.profileImage.kf.indicatorType = .activity
                self.profileImage.kf.setImage(with: url, placeholder: nil, options: [.transition(.fade(0.7))])
                self.isProfileImage = true
            }
        }
    }
    
    func setUI(){
        self.profileTopView.fullRoundView()
        self.profileTopView.layer.masksToBounds = true
        self.profileTopView.layer.borderColor = UIColor.black.cgColor
        self.profileTopView.layer.borderWidth = 2
        
        if self.isFromEditProfile {
            self.btnSignUp_Update.setTitle("Update", for: .normal)
        }
        self.checkBoxButton.setTitle("", for: .normal)
    }
    
  
    
    func getCities() {
        let url = BASE_URL + GET_CITIES
        
        let parameters: [String:Any] = ["NoVersion":"true"]
        
        ApiManager.getRequest(urlString: url, isAlertShow: false, parameters: parameters) { dict in
            
            self.citiesArray = CitiesModel.parseData(response: dict)
            
            /// Cities Model Picker View
            var CitiesNameStrings = [String]()
            for cities in self.citiesArray {
                CitiesNameStrings.append(cities.Title)
                if cities.Title == self.userData.CityTitle {
                    self.cityId = cities.CityID
                }
            }
            self.cityTextField.pickerType = .string(data: CitiesNameStrings)
            self.cityTextField.valueDidSelected = { data in
                self.cityTextField.text = CitiesNameStrings[data as! Int]
                if let foo = self.citiesArray.enumerated().first(where: {$0.element.Title == self.cityTextField.text}) {
                    self.cityId = foo.element.CityID
                }
            }
            
        } errorCallBack: { errorString in
            print("Error")
        }
    }
    
    private func configureExpiryPicker() {
        
        /// Date Of birth
        dateOfBirthTextField.pickerType = .date
        dateOfBirthTextField.valueDidSelected = { date in
            self.dateOfBirthTextField.text = self.dateNewFormatter(date: date as? Date)
        }
        
        /// Gender Picker View
        let GenderArray = ["Male", "Female"]
        
        self.gendertextField.pickerType = .string(data: GenderArray)
        self.gendertextField.valueDidSelected = { data in
            self.gendertextField.text = GenderArray[data as! Int]
        }
        
    }
    
    /// Setting the date formate for the date picker
    /// - Parameter date: Geeting the date from date picker and setting its formate
    /// - Returns: It returns the formateed string
    private func dateNewFormatter(date: Date?) -> String? {
        guard let date = date else {
            return nil
        }
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd"
        return dateFormater.string(from: date)
        
    }
    
    func isValidated() -> Bool {
        
        if fullNameTextField.text == "" {
            self.showAlert(title: "Error".localized(), message: "Enter full name".localized())
            return false
        }
        
        if usernameTextField.text! == "" {
            self.showAlert(title: "Error".localized(), message: "Enter user name".localized())
            return false
        }
        
        let letters = NSCharacterSet.alphanumerics
        if usernameTextField.text!.rangeOfCharacter(from: letters.inverted) != nil {
            self.showAlert(title: "Error".localized(), message: "Username only contains alphanumeric characters".localized())
            return false
        }
        
        if self.isValidEmail(testStr: self.emailTextField.text!)==false
        {
             self.showAlert(title: "Error".localized(), message: "Enter valid email".localized())
            return false
        }
        
        if cityTextField.text! == "" {
            self.showAlert(title: "Error".localized(), message: "Select city".localized())
            return false
        }
        
        if !self.isFromEditProfile {
            if self.whiteSpaceValidae(textfield: self.passwordTextField.text!)==false
            {
                 self.showAlert(title: "Error".localized(), message: "Enter password".localized())
                return false
            }
            
            if self.whiteSpaceValidae(textfield: self.confirmPasswordTextField.text!)==false
            {
                 self.showAlert(title: "Error".localized(), message: "Enter confirm password".localized())
                return false
            }
            
            if passwordTextField.text! != confirmPasswordTextField.text! {
                self.showAlert(title: "Error".localized(), message: "Password not matched".localized())
                return false
            }
        }
        
        if !isProfileImage {
            self.showAlert(title: "Error".localized(), message: "Add Profile Image".localized())
            return false
        }
        
        return true
    }

    @IBAction func actionProfileImage(_ sender: UIButton) {
        
        self.addImagePicker(title: "Select Photo", msg: "", imagePicker: self.imagePicker)
    }
    @IBAction func actionSignUp_Update(_ sender: UIButton) {
        
        if self.isValidated() {
            
            var params = ["FullName": fullNameTextField.text ?? "",
                          "Email": emailTextField.text ?? "",
                          "IOSAppVersion": "true",
                          "CityID": self.cityId,
                          "RoleID": "5"]
            
            if self.isFromEditProfile {
                params["UserID"] = self.userData.UserID
            } else {
                params["Password"] = passwordTextField.text ?? ""
            }
            
            if mobileTextField.text != "" {
                params["Mobile"] = mobileTextField.text ?? ""
            }
            if gendertextField.text != "" {
                params["Gender"] = gendertextField.text ?? ""
            }
            if dateOfBirthTextField.text != "" {
                params["DateOfBirth"] = dateOfBirthTextField.text ?? ""
            }
            
            var url = ""
            
            if isFromEditProfile {
                url = Update_Profile
            } else {
                url = SIGN_UP_URL
            }
            
            ApiManager.SignUpApi(url: url, profileImage: profileImage.image!, params: params) { dict in
                
                if self.isFromEditProfile {
                    self.saveCustomerData(dict: dict)
                    let userObject:MSJUser = MSJUser.parseDataOfUserInfo(userDict: dict)
                    MSJSharedManager.setUser(userObject: userObject)
                    self.alertWithSingleBackButtonFuction(title: "Success", message: "Profile Updated Successfully!") {
                        self.navigationController?.popViewController(animated: true)
                    }
                } else {
                    self.alertWithSingleBackButtonFuction(title: "Success", message: "User Register Successfully!") {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                
            } errorCallBack: { error in
                print(error)
                self.showAlert(title: "Error!", message: error)
            }

            
        }
        
    }
    @IBAction func actionCheckBoxButton(_ sender: UIButton) {
        self.isCheckBox.toggle()
        
        if self.isCheckBox {
            self.checkBoxButton.setImage(UIImage(named: "tick"), for: .normal)
        } else {
            self.checkBoxButton.setImage(UIImage(), for: .normal)

        }
    }
    @IBAction func actionTermsAndConditions(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "NiehezTermsAndConditionsViewController") as? NiehezTermsAndConditionsViewController ?? NiehezTermsAndConditionsViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK: - Image Picker Delegates
extension NiehezSignUpViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    /// This function is using fot picking the picture against all the filds
    /// - Parameters:
    ///   - picker: Image picker
    ///   - info: Info
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            self.profileImage.image = pickedImage
            self.isProfileImage = true
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}

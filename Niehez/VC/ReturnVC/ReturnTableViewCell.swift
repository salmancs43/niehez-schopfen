//
//  ReturnTableViewCell.swift
//  Niehez
//
//  Created by Macbook on 12/10/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class ReturnTableViewCell: UITableViewCell {

    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var lblTitle_Price: UILabel!
    
    @IBOutlet weak var lblItemPrice: UILabel!
    @IBOutlet weak var lblItemQuantity: UILabel!
    
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var quantityTextField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func plusButton(_ sender: UIButton) {
    }
    @IBAction func MinusButton(_ sender: UIButton) {
    }
}


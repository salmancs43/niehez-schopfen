//
//  MSJTabBar.swift
//  MSJ
//
//  Created by Mac on 2/17/1439 AH.
//  Copyright © 1439 Mac. All rights reserved.
//

import UIKit

class MSJTabBar: UITabBarController,UITabBarControllerDelegate
{
    
    var pushDict : NSDictionary  = [:]
    
   // var isComingFromPush : Bool  = false
    
    let object = MSJSharedManager.getAppSetting()
    let basevc = BaseVC()
    
    var lastSelectedTabIndex = 0;

    @IBOutlet var tabBarView: UIView!
    
    @IBOutlet weak var homeView: UIView!
    
    @IBOutlet weak var productView: UIView!
    
  
    
    
    @IBOutlet weak var ticketView: UIView!
    
    @IBOutlet weak var homeImage: UIImageView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var orderImage: UIImageView!
    @IBOutlet weak var settingImage: UIImageView!
    
    @IBOutlet weak var otherView: UIView!
    
    @IBOutlet weak var labelHome: MSJRegularLabel!
    
    @IBOutlet weak var labelProduct: MSJRegularLabel!
    
 
    
    @IBOutlet weak var labelSupport: MSJRegularLabel!
    
    @IBOutlet weak var labelOther: MSJRegularLabel!
    
    @IBOutlet weak var home_center_layout: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        basevc.setUpNavBarAppearance()
        
        tabBarView.tag = -1
        tabBarView.frame = CGRect(x: tabBarView.frame.origin.x, y: tabBarView.frame.origin.y, width: tabBarView.frame.size.width, height: tabBarView.frame.size.height)
       // tabBarView.semanticContentAttribute = .forceRightToLeft
        self.delegate = self
        self.setUpTabBar()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.changeLanguage(notification:)), name: Notification.Name(NOTIFICATION), object: nil)
        
       //   NotificationCenter.default.addObserver(self, selector: #selector(self.changeTab(notification:)), name: Notification.Name(SUPPORT), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.isPushCalled(notification:)), name: Notification.Name(IS_PUSH), object: nil)
        
        setLanguage()

       
    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func setUpTabBar()
    {
        tabBarView.removeFromSuperview()
        
        self.view .addSubview(tabBarView)
        
        if UIScreen.main.bounds.size.height > 811
        {
            home_center_layout.constant = -15
             tabBarView.frame = CGRect(x: tabBarView.frame.origin.x, y:  (self.view.frame.size.height - tabBarView.frame.size.height-30), width:self.view.frame.size.width, height: tabBarView.frame.size.height+30)
        }
        else
        {
            //home_center_layout.constant = -5
             tabBarView.frame = CGRect(x: tabBarView.frame.origin.x, y:  (self.view.frame.size.height - tabBarView.frame.size.height), width:self.view.frame.size.width, height: tabBarView.frame.size.height)
        }
        
        
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let homeTab = storyBoard.instantiateViewController(withIdentifier: "Home")
        let productTab = storyBoard.instantiateViewController(withIdentifier: "Product")
       // let orderTab = storyBoard.instantiateViewController(withIdentifier: "Order")
        let supportTab = storyBoard.instantiateViewController(withIdentifier: "Ticket")
        let otherTab = storyBoard.instantiateViewController(withIdentifier: "Other")
        
        self.viewControllers = [homeTab,productTab,supportTab,otherTab]
        
        tabBarView.backgroundColor = BaseVC().hexStringToUIColors(hex: MSJSharedManager.getAppSetting().TabBarColor)
        
        // Sikandar cooment this code of product image
        
        //let originalImage = UIImage(named: "Group 1")
        //let tintedImage = originalImage?.withRenderingMode(.alwaysTemplate)
       // productImage.image = tintedImage
        //productImage.tintColor = UIColor.init(red: 158.0/255.0, green: 182.0/255.0, blue: 235.0/255.0, alpha: 1.0)
        
//        if isComingFromPush
//        {
//            self.selectItem(index: 2)
//            var timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: false)
//        }
//        else
//        {
             self.selectItem(index: 0)
      //  }
       
        
      
       
    }
   
    
    func selectItem(index:Int)
    {
        
      
         homeView.backgroundColor = UIColor.clear
         productView.backgroundColor = UIColor.clear
         ticketView.backgroundColor = UIColor.clear
         otherView.backgroundColor = UIColor.clear
      
      
        
        let vc = self.viewControllers![index]
        
        if let vc = vc as? UINavigationController
        {
            if lastSelectedTabIndex == index
            {
            vc.popToRootViewController(animated: true)
            }
        }
        
        self.selectedIndex = index
        
        
        self.homeImage.downLoadImageIntoImageView(url:  object.TabImageHomeIcon)
        
        self.productImage.downLoadImageIntoImageView(url:  object.TabImageCatalougeIcon)
        
        self.orderImage.downLoadImageIntoImageView(url: object.TabImageOrdersIcon)
        
        self.settingImage.downLoadImageIntoImageView(url: object.TabImageSettingIcon)

        
        if index == 0
        {
            self.homeImage.downLoadImageIntoImageView(url: object.TabImageHomeSelectedIcon)
           
        }
        else if index == 1
        {
            
            self.productImage.downLoadImageIntoImageView(url:  object.TabImageCatalougeSelectedIcon)
         
          
        }
    
        else if index == 2
        {
            
            self.orderImage.downLoadImageIntoImageView(url: object.TabImageOrdersSelectedIcon)
          
            
            
        }
        else if index == 3
        {
            
            self.settingImage.downLoadImageIntoImageView(url: object.TabImageSettingSelectedIcon)
 
        }
        
    }

    
    @IBAction func tabBarItemTapped(_ sender: UIButton)
    {
        self.selectItem(index: sender.tag)
        lastSelectedTabIndex = sender.tag
    }
    
    @objc func changeLanguage(notification: Notification)
    {
        setLanguage()
    }
    
    
    @objc func changeTab(notification: Notification)
    {
          self.selectItem(index: 2)
    }
    
    
    @objc func isPushCalled(notification: Notification)
    {
     
        self.selectItem(index: 0)
        
        _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(runTimedCode), userInfo: notification.userInfo, repeats: false)
        
    }
    @objc func runTimedCode()
    {
      //  isComingFromPush = false
         NotificationCenter.default.post(name: Notification.Name(IS_SUPPORT_PUSH), object: pushDict)
    }
    
    func setLanguage()
    {
        
      
//        labelHome.text = "Home".localized()
//        labelProduct.text = "Products".localized()
//        labelOther.text = "Settings".localized()
//        labelSupport.text = "My Orders".localized()
//     //   labelOffer.text = "Visit".localized()
//       labelHome.textColor = .TAB_BAR_TEXT
//         labelProduct.textColor = .TAB_BAR_TEXT
//         labelOther.textColor = .TAB_BAR_TEXT
//         labelSupport.textColor = .TAB_BAR_TEXT
        
       // if MSJSharedManager.getArabic()
        //{
            //labelHome.font = UIFont.init(name:"GESSTwoMedium-Medium", size:labelHome.font.pointSize)
            //labelProduct.font = UIFont.init(name:"GESSTwoMedium-Medium", size:labelProduct.font.pointSize)
          //  labelOther.font = UIFont.init(name:"GESSTwoMedium-Medium", size:labelOther.font.pointSize)
         //   labelSupport.font = UIFont.init(name:"GESSTwoMedium-Medium", size:labelSupport.font.pointSize)
       // }
       // else
      //  {
//            labelHome.font = UIFont.init(name:"HelveticaNeue", size:labelHome.font.pointSize)
//            labelProduct.font = UIFont.init(name:"HelveticaNeue", size:labelProduct.font.pointSize)
//            labelOther.font = UIFont.init(name:"HelveticaNeue", size:labelOther.font.pointSize)
//            labelSupport.font = UIFont.init(name:"HelveticaNeue", size:labelSupport.font.pointSize)

       // }
        
    }
    


}

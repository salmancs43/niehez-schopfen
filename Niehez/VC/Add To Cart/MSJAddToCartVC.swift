//
//  MSJAddToCartVC.swift
//  MSJ
//
//  Created by Mac on 1/2/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class MSJAddToCartVC: BaseVC,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var headerInTableView: UIView!
    @IBOutlet weak var labelNoItemAvailabel: MSJRegularLabel!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var labelTotalItem: MSJBoldLabel!
    @IBOutlet weak var viewGrandTotal: UIView!
    @IBOutlet weak var labelGrandTotal: UILabel!
    @IBOutlet weak var labelCartCount: UILabel!
    var totalPrice : Float = 0.0
    var selectedIndex: Int = 0
    
    var cart_Count = 0
    
    let obj = MSJSharedManager.getAppSetting()
    
    var totalItemsOfCart = 0
    
    var cartItemArray = [MSJAddToCartObject]()
    
    @IBOutlet weak var arrowKey_Image: UIImageView!
    @IBOutlet weak var labelSelectDeliveryAddress: MSJMediumLabel!
    
 
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = UITableView.automaticDimension
        setAttributedTitleForRightBar(complete: "My Bag".localized(), CompFont: UIFont.init(name:  MSJSharedManager.boldFont(), size: 15.0)!, CompColor: .white, sub: "", subFont: UIFont.init(name:  MSJSharedManager.boldFont(), size: 15.0)!, SubColor: .white)
        
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.cart(notification:)), name: Notification.Name(CART_NOTIFICATION), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        headerInTableView.backgroundColor = self.hexStringToUIColors(hex: MSJSharedManager.getAppSetting().TabBarColor)
        viewGrandTotal.backgroundColor = self.hexStringToUIColors(hex: MSJSharedManager.getAppSetting().AlertPositiveButtonColor)
        self.backButtonOnNavigationBar()
        self.setLanguage()
        viewGrandTotal.isHidden = true
        MSJSharedManager.sharedManager.cartArray.removeAll()
        tableView.reloadData()
        self.getCartList()
        
        self.backgroundColorOrImageSetting(view: self.view, imageView: self.bgImage)
        self.setTabbarToVisible(isVisible: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  
    
    // MARK: - Table view data source
 
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
     {
       
         return self.cartItemArray.count
        
    }
    
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MSJAddToCartCell
        
         let cartArrayObj = self.cartItemArray[indexPath.row]
        
       
      
        cell.labelProductName.text =   cartArrayObj.Title

        cell.labelPrice.text       =   "= " + String(describing: cartArrayObj.Price!) + " " + "SAR".localized()
         
        cell.blueView.backgroundColor = self.hexStringToUIColors(hex: MSJSharedManager.getAppSetting().TabBarColor)
        cell.blueView.isHidden     =   true
        cell.textFieldQuantity.text = cartArrayObj.Quantity ?? "0"
        cell.viewCart.layer.borderWidth = 0.0
        cell.viewCart.layer.cornerRadius = cell.viewCart.frame.size.height / 2
        cell.viewCart.clipsToBounds = true
        cell.viewCart.layer.borderColor = UIColor.TAB_BAR_BACKGROUND_COLOR.cgColor
     
        if indexPath.row == 0
        {
            cell.blueView.isHidden = false
       
        }
        
        if cartArrayObj.ImageName != nil
        {
            
            cell.imgView.downLoadImageIntoImageView(url: cartArrayObj.ImageName ?? "", placeholder: PLACEHOLDER_IMAGE)
          
        }
        else
        {
            cell.imgView.image = PLACEHOLDER_IMAGE
        }
        
        
        cell.buttonMore .addTarget(self,  action: #selector(buttonMore(button:)), for:.touchUpInside)
        cell.buttonMore.tag = indexPath.row
        cell.buttonPlus .addTarget(self,  action: #selector(buttonPlus(button:)), for:.touchUpInside)
        cell.buttonPlus.tag = indexPath.row
        
        cell.buttonMinus .addTarget(self,  action: #selector(buttonMinus(button:)), for:.touchUpInside)
        cell.buttonMinus.tag = indexPath.row
        
        
        if UIScreen.main.bounds.size.height == 568
        {
            cell.widthContraint.constant = 110
        }
       
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        selectedIndex = indexPath.row
        
        let cartObject =  self.cartItemArray[indexPath.row]
        
        let alert = UIAlertController(title: cartObject.Title ?? "", message: "", preferredStyle: .actionSheet)
        
        
        let editAction = UIAlertAction(title: "Edit".localized(), style: .default, handler: handleEdit)
        let deleteAction = UIAlertAction(title: "Delete".localized(), style: .destructive, handler: handleDelete)
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: cancelDelete)
        
        alert.addAction(deleteAction)
        alert.addAction(cancelAction)
        alert.addAction(editAction)
        
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: 2.0, y: 2.0, width: self.view.bounds.size.width / 2.0, height: self.view.bounds.size.height / 2.0)
        
        self.present(alert, animated: true, completion: nil)

    }
     func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {

        return false
        
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath)
    {
        if editingStyle == .delete
        {
           
        }
    }
    
  
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
  
    
    // Override to support rearranging the table view.
     func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        
    }
    
    
    
    // Override to support conditional rearranging of the table view.
     func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    
    
    
    @IBAction func payFortAction(_ sender: UIButton) {
        let request = NSMutableDictionary.init()
        request.setValue("1000", forKey: "amount")
        request.setValue("AUTHORIZATION", forKey: "command")
        request.setValue("USD", forKey: "currency")
        request.setValue("email@domain.com", forKey: "customer_email")
        request.setValue("en", forKey: "language")
        request.setValue("112233682686", forKey: "merchant_reference")
        request.setValue("token", forKey: "sdk_token")
        request.setValue("gr66zzwW9" , forKey: "token_name")
        request.setValue("" , forKey: "payment_option")

//        self.navigationController?.present(payFort!, animated: true, completion: nil)
//        payFort?.hideLoading = true
        
//        payFort?.callPayFort(withRequest: request, currentViewController: self,
//                            success: { (requestDic, responeDic) in
//                                print("success")
//                                print("responeDic = ",responeDic!)
//                                print("responeDic = ",responeDic!)
//        },
//                            canceled: { (requestDic, responeDic) in
//                                print("canceled")
//                                print("responeDic = ",responeDic!)
//                                print("responeDic = ",responeDic!)
//        },
//                            faild: { (requestDic, responeDic, message) in
//                                print("faild")
//                                print("responeDic = ",responeDic!)
//                                print("responeDic = ",responeDic!)
//                                print("message = ",message!)
//        })
        
        
       
    }
    
    
    func getCartList() {
        
        var url : String = ""
        if (UserDefaults.standard.value(forKey: USER_DATA) != nil)
        {
            
            url = BASE_URL + GET_CART_ITEM_LIST + "?UserID=" + MSJSharedManager.getUser().UserID!
            
        }
        
        let params = ["NoVersion" : "true"]
                
        if url == "" {
            return
        }
        
        ApiManager.getRequest(urlString: url, isAlertShow: true, parameters: params, successCallback: { (dict) in
            
            let cartDict = dict["cart_items"]
            let cartArray = cartDict as? NSArray
            MSJSharedManager.sharedManager.cartArray.removeAll()
            self.cartItemArray.removeAll()
            
            for item in cartArray!
            {
              
                
                let singleItem = item as? NSDictionary
                
                let data = MSJAddToCartObject.parseCartData(dict: singleItem!)
                
                self.cartItemArray.append(data)
                
                let productPrice : Float = Float(data.Quantity ?? "0.0")! * Float(data.Price ?? "0.0")!
                
                self.totalPrice =  self.totalPrice + productPrice
                
                self.cart_Count = Int(data.Quantity ?? "0") ?? 0 + self.cart_Count
            }
            
            
            self.tableView.reloadData()
//            self.setValueInCart()
            self.setCartLabel()
            
            
            
            
            
        }) { (error) in
            print(error)
        }
       
    }
    
  
    
   
    @IBAction func buttonSelectAddress(_ sender: Any)
    {
      
        if (UserDefaults.standard.value(forKey: USER_DATA) != nil)
        {
            
//            let vc  = self.storyboard?.instantiateViewController(withIdentifier: "MSJAddressOneVC") as! MSJAddressOneVC
//            vc.totalPrice = totalPrice
//            vc.totalItems = totalItemsOfCart
//            vc.selectedButtonShow = true
//            self.navigationController?.pushViewController(vc, animated: true)
            
//            let vc  = self.storyboard?.instantiateViewController(withIdentifier: "NiehezDeliveryTypeViewController") as! NiehezDeliveryTypeViewController
//            self.navigationController?.pushViewController(vc, animated: true)
            
            let alert = UIAlertController(title: "Choose option", message: "", preferredStyle: .actionSheet)
            
            
            let pickupAction = UIAlertAction(title: "Pickup".localized(), style: .default, handler: handlePickup)
            let deliveryAction = UIAlertAction(title: "Delivery".localized(), style: .default, handler: handleDelivery)
            let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: handleCancel)
            
            alert.addAction(pickupAction)
            alert.addAction(deliveryAction)
            alert.addAction(cancelAction)
            
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = CGRect(x: 2.0, y: 2.0, width: self.view.bounds.size.width / 2.0, height: self.view.bounds.size.height / 2.0)
            
            self.present(alert, animated: true, completion: nil)
            
        }
        else
        {
            self.moveToLoginScreen()
        }
       
    }
    
    func handlePickup(alertAction: UIAlertAction!) -> Void {
        
        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "NiehezPickUpStoresViewController") as! NiehezPickUpStoresViewController
        vc.isPickUp = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func handleDelivery(alertAction: UIAlertAction!) -> Void {
        
        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "NiehezPickUpStoresViewController") as! NiehezPickUpStoresViewController
        vc.isPickUp = false
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func handleCancel(alertAction: UIAlertAction!) -> Void {
        print("handleCancel")
    }
    
    func verifyPhone()
    {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MSJVerifyPhoneVC") as! MSJVerifyPhoneVC
        vc.isComingFromLogin = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }

    @objc func buttonPlus(button: UIButton)
    {
        let path = NSIndexPath.init(row: button.tag, section: 0)
        let cell = tableView.cellForRow(at: path as IndexPath) as! MSJAddToCartCell

        var quantity = Int(cell.textFieldQuantity.text!)
        quantity = quantity! + 1
        if quantity == 99
        {
            return
        }
        cell.textFieldQuantity.text = String(describing:quantity!)

        let cartArrayObj = self.cartItemArray[button.tag]
        let url : String
        var cartDictionary = [String: String]()
        cartDictionary["TempOrderID"] = cartArrayObj.TempOrderID
        let quatityStr =  cell.textFieldQuantity.text?.replacedArabicDigitsWithEnglish
        cartDictionary["Quantity"] = quatityStr
        cartDictionary["UserID"] = MSJSharedManager.getUser().UserID!
        url = BASE_URL + EDIT_CART
        
        ApiManager.postServiceCall(urlString: url, isAlertShow: true, parameters: cartDictionary, header: nil) { dict in
            print(dict)
        } errorCallBack: { error in
            self.showAlert(title: "Error".localized(), message: error)
        }

    }
    
    
    @objc func buttonMinus(button: UIButton)
    {
        let path = NSIndexPath.init(row: button.tag, section: 0)
        let cell = tableView.cellForRow(at: path as IndexPath) as! MSJAddToCartCell

        var quantity = Int(cell.textFieldQuantity.text!)
        quantity = quantity! - 1
        if quantity == 0
        {
            return
        }
        cell.textFieldQuantity.text = String(describing:quantity!)

        let cartArrayObj = self.cartItemArray[button.tag]
        let url : String
        var cartDictionary = [String: String]()
        cartDictionary["TempOrderID"] = cartArrayObj.TempOrderID
        let quatityStr =    cell.textFieldQuantity.text?.replacedArabicDigitsWithEnglish
        cartDictionary["Quantity"] = quatityStr
        cartDictionary["UserID"] = MSJSharedManager.getUser().UserID!
        url = BASE_URL + EDIT_CART

        ApiManager.postServiceCall(urlString: url, isAlertShow: true, parameters: cartDictionary, header: nil) { dict in
            print(dict)
        } errorCallBack: { error in
            self.showAlert(title: "Error".localized(), message: error)
        }

        
    }

    //Mark - show activity
    
    @objc func buttonMore(button: UIButton)
    {

        selectedIndex = button.tag

         let cartObject =  self.cartItemArray[selectedIndex]

        let alert = UIAlertController(title: cartObject.Title, message: "", preferredStyle: .actionSheet)


        let editAction = UIAlertAction(title: "Edit".localized(), style: .default, handler: handleEdit)
        let deleteAction = UIAlertAction(title: "Delete".localized(), style: .destructive, handler: handleDelete)
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: cancelDelete)

        alert.addAction(deleteAction)
        alert.addAction(cancelAction)
        alert.addAction(editAction)

        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: 2.0, y: 2.0, width: self.view.bounds.size.width / 2.0, height: self.view.bounds.size.height / 2.0)

        self.present(alert, animated: true, completion: nil)

    }
    
    func handleDelete(alertAction: UIAlertAction!) -> Void
    {

         self.twoButtonAlert(title: "Alert".localized(), message: "Are you sure you want to delete?".localized(), btnTitle: "Cancel".localized(), buttonTwo: "Delete".localized(), successCallback: self.deletePressed)

    }
    
    func deletePressed()
    {

        var deleteDict = [String: String]()

        let cartObject =  self.cartItemArray[selectedIndex]
        let url = BASE_URL + REMOVE_CART_ITEM_BY_ID

        deleteDict["TempOrderID"] = cartObject.TempOrderID!
        deleteDict["UserID"] = MSJSharedManager.getUser().UserID!
        deleteDict["NoVersion"] =  "true"
        
        ApiManager.getRequest(urlString: url, isAlertShow: true, parameters: deleteDict) { dict in
            
            self.tableView.beginUpdates()
            
            self.cartItemArray.remove(at: self.selectedIndex)

            let indexPath = NSIndexPath.init(row:  self.selectedIndex, section: 0)

            self.tableView.deleteRows(at: [indexPath as IndexPath], with: .automatic)

            self.tableView.endUpdates()


            if self.cartItemArray.count == 0
            {
                self.totalPrice = 0.0
                self.navigationController?.popViewController(animated: true)
            }
            else
            {
                let productPrice : Float = Float(cartObject.Quantity!)! * Float(cartObject.Price!)!

                self.totalPrice =  self.totalPrice - productPrice

            }
            
        } errorCallBack: { error in
            self.showAlert(title: "Error".localized(), message: error)
        }

    }
    
    func handleEdit(alertAction: UIAlertAction!) -> Void
    {
//        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "MSJPorductDetailVC") as! MSJPorductDetailVC
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailNewDesignViewController") as! ProductDetailNewDesignViewController
        
        let obj = self.cartItemArray[selectedIndex]
        let productObj = ProductModel()
        productObj.ProductID = obj.ProductID ?? ""
        vc.categoryObjectById = productObj
        vc.cartObject = obj
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func cancelDelete(alertAction: UIAlertAction!) -> Void
    {
        
    }
   
    func setCartLabel()
    {
        if ( cartItemArray.count > 0)
        {
            labelNoItemAvailabel.isHidden = true
            viewGrandTotal.isHidden = false
        
            print(totalItemsOfCart)
            let strCount =  String(self.cart_Count) + " items".localized()
            
            self.labelCartCount.attributedText = self.makeAttributedLable(complete: strCount, CompFont: UIFont.init(name:  MSJSharedManager.boldFont(), size: 13.0)!, CompColor: UIColor.white, sub: " items".localized(), subFont: UIFont.init(name:  MSJSharedManager.regularFont(), size: 13.0)!, SubColor: UIColor.white)
            
            let strGrandTotal = "Grand Total".localized() +  "\n"  + String(self.totalPrice) + " "  + "SAR".localized()
            
            self.labelGrandTotal.attributedText = self.makeAttributedLable(complete: strGrandTotal, CompFont: UIFont.init(name: MSJSharedManager.regularFont(), size: 10.0)!, CompColor: UIColor.white, sub:  String(self.totalPrice) + "SAR".localized(), subFont: UIFont.init(name: MSJSharedManager.boldFont(), size: 13.0)!, SubColor: UIColor.white)
//               self.tableView.reloadData()
        }
        else
        {
             labelNoItemAvailabel.isHidden = false
            viewGrandTotal.isHidden = true
            self.labelCartCount.attributedText = self.makeAttributedLable(complete: "No item available".localized(), CompFont: UIFont.init(name: MSJSharedManager.boldFont(), size: 13.0)!, CompColor: UIColor.white, sub: "No item".localized(), subFont: UIFont.init(name:  MSJSharedManager.regularFont(), size: 13.0)!, SubColor: UIColor.white)
            
            
        }
        
        
    }
    
    func setLanguage()
    {
        labelSelectDeliveryAddress.text = "Select Delivery Address".localized()
        labelSelectDeliveryAddress.text = "Next".localized()

        labelTotalItem.text = "Total Items".localized()
        labelNoItemAvailabel.text = "No item available".localized()
        
        if MSJSharedManager.getArabic()
        {
            arrowKey_Image.image = UIImage.init(named: "doubleleft")
        }
        else
        {
            arrowKey_Image.image = UIImage.init(named: "doubleright")
        }
    }
    
    
    func setValueInCart()
    {
        self.getCartCount()
      
    }
    
    
    @objc func cart(notification: Notification)
    {
       self.setCartLabel()
    }
    
}



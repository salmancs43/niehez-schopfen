//
//  MSJForgotPasswordVC.swift
//  MSJ
//
//  Created by Muhammad Salman on 2/28/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class MSJForgotPasswordVC: BaseVC {

    @IBOutlet var labelForgotPassword: MSJBoldLabel!
    @IBOutlet var buttonSubmit: MSJRegularButton!
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet var textFieldForgotPassword: MSJCustomTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

         self.backButtonOnNavigationBar()
//        self.setAttributedTitleForRightBar(complete: "Forgot Password", CompFont: UIFont.init(name: MSJSharedManager.regularFont(), size: 15.0)!, CompColor: MSJColor.TAB_BAR_BACKGROUND_COLOR, sub: "Forgot Password", subFont: UIFont.init(name: MSJSharedManager.boldFont(), size: 15.0)!, SubColor: MSJColor.TAB_BAR_BACKGROUND_COLOR)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setLanguage()
        self.backgroundColorOrImageSetting(view: self.view, imageView: self.bgImage)
       
        
    }
    
 
    
    @IBAction func buttonSubmit(_ sender: Any) {
        
        if self.isValidEmail(testStr: self.textFieldForgotPassword.text!) == false {
            self.showAlert(title: "Error".localized(), message: "Enter valid email".localized())
            return
        } else {
            forgotPassword()
        }
        
    }
    
    func forgotPassword() {
        
        var dataDictionary = [String: String]()
        dataDictionary["Query"] = textFieldForgotPassword.text
        
        let url = BASE_URL + FORGOT_PASSWORD
        
        ApiManager.postServiceCall(urlString: url, isAlertShow: true, parameters: dataDictionary, header: nil, successCallback: { (dict) in
            
            self.showAlert(title: "Success".localized(), message: "Please check your email".localized())
        
        }) { (error) in
            
            self.showAlert(title: "Error".localized(), message: error)
        
        }
    
    }

    func setLanguage() {
        
//        if MSJSharedManager.getArabic() {
//            textFieldForgotPassword.textAlignment = NSTextAlignment.right
//        } else {
//            textFieldForgotPassword.textAlignment = NSTextAlignment.left
//        }
        
        textFieldForgotPassword.placeholder = "Email".localized()
        buttonSubmit.setTitle("Submit".localized(), for: UIControl.State.normal)
        labelForgotPassword.text = "Forgot Password".localized()
        
    }
    
}

//
//  MSJMapVC.swift
//  MSJ
//
//  Created by Mac on 02/04/2018.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit
import MapKit



protocol LOCATIONSELECT {
    func locationSelect(address:String , latitude:Double , lognitude:Double)
}


class MSJMapVC: BaseVC
{
    
     var delegate: LOCATIONSELECT!
   
    @IBOutlet weak var mapView: MKMapView!
    var geoCoder = CLGeocoder()
    

    @IBOutlet weak var buttonConfirm: MSJRegularButton!
    @IBOutlet weak var labelAddress: MSJBoldLabel!
    
    
    var lat = 0.0
    var long = 0.0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.backButtonOnNavigationBar()
        
        
        
     
        let location = CLLocation(latitude: MSJSharedManager.sharedManager.latitude , longitude: MSJSharedManager.sharedManager.lognitude)
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude), span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        self.mapView.setRegion(region, animated: true)
        
        self.parseAddress(location: location)
        
        lat = MSJSharedManager.sharedManager.latitude
        long = MSJSharedManager.sharedManager.lognitude
    
    }
    
    
    @IBAction func buttonConfirmAddress(_ sender: Any) {
        
        
        
        self.delegate.locationSelect(address: self.labelAddress.text!, latitude: self.lat, lognitude: self.long)
        self.navigationController?.popViewController(animated: true)
        
    }
    func parseAddress(location: CLLocation) {
        
        var address  = ""
        
        geoCoder.reverseGeocodeLocation(location) { (response, error) in

            if let name = response?.last?.name {
                print(name)
                address = name
            }
            
            if let subLocality = response?.last?.subLocality {
                print(subLocality)
                address.append(", " + subLocality)
            }
            
            if let locality = response?.last?.locality {
                print(locality)
               
                address.append(", " + locality)
            }
            
            if let postalCode = response?.last?.postalCode {
                print(postalCode)
                address.append(", " + postalCode)
            }
            
            if let administrativeArea = response?.last?.administrativeArea {
                print(administrativeArea)
                 address.append(", " + administrativeArea)
            }
            
            if let country = response?.last?.country {
                print(country)
                address.append(", " + country)
                
            }
            
            self.labelAddress.text = address
            
        }
        
    }



  

}



extension MSJMapVC : MKMapViewDelegate
{
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        
        let coordinate  = mapView.region.center
       var latitude = coordinate.latitude
       var lognitude = coordinate.longitude
        
        lat = latitude
        long = lognitude
        
        
        let location = CLLocation(latitude: latitude , longitude: lognitude)
              let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude), span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
                  self.mapView.setRegion(region, animated: true)
              
               self.parseAddress(location: location)
        
       
        
        
    }
}

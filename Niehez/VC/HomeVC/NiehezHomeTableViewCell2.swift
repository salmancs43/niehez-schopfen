//
//  NiehezHomeTableViewCell2.swift
//  Niehez
//
//  Created by Macbook on 28/04/2022.
//  Copyright © 2022 Mac. All rights reserved.
//

import UIKit

class NiehezHomeTableViewCell2: UITableViewCell {
    
    @IBOutlet weak var featuresCollectioView: UICollectionView!
    @IBOutlet weak var constraintsCollectionViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var labelProducts: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if isHorizantalCollectioView {
            
            let height = featuresCollectioView.collectionViewLayout.collectionViewContentSize.height
            constraintsCollectionViewHeight.constant = height
            self.contentView.setNeedsLayout()
            
        } else {
            
            let height = featuresCollectioView.collectionViewLayout.collectionViewContentSize.height
            let item: Double = Double(featuresProductsItems) / 2.0
            constraintsCollectionViewHeight.constant = height * floor(item)
            self.contentView.setNeedsLayout()
            
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

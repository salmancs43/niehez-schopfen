//
//  NiehezHomeTableViewCell.swift
//  Niehez
//
//  Created by Macbook on 27/04/2022.
//  Copyright © 2022 Mac. All rights reserved.
//

import UIKit

class NiehezHomeTableViewCell: UITableViewCell, UIScrollViewDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageController: UIPageControl!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.pageController.numberOfPages = 3
        
        let cellSize = CGSize(width:UIScreen.main.bounds.size.width , height:190)

        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 0.0
        layout.minimumInteritemSpacing = 0.0
        collectionView.setCollectionViewLayout(layout, animated: true)
    }

}

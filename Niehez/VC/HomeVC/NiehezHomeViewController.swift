//
//  NiehezHomeViewController.swift
//  Niehez
//
//  Created by Macbook on 26/04/2022.
//  Copyright © 2022 Mac. All rights reserved.
//

import UIKit
import IQKeyboardManager
import AnimatableReload

let isHorizantalCollectioView = true
var featuresProductsItems = 0

class NiehezHomeViewController: BaseVC, UIScrollViewDelegate {
    
    var productsArray = [ProductModel]()
    var promotionsArray = [PromotionsModel]()
    var isRefresControl:Bool = true
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bgImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getFeaturedProducts()
        self.getPromotions()
        self.addLeftBarIcon()
        
//        self.createChatButton()
        
        if UserDefaults.standard.value(forKey: FIRST_TIME) == nil
        {
            
            self.languageAlert(title: "Alert".localized(), message: "Select the language\nYou can change your language from language settings.", btnTitle: "العربية", buttonTwo: "English", successCallback: self.englishLanguage, callBack: self.arabicLanguage)
            
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.pushCame(notification:)), name: Notification.Name(IS_SUPPORT_PUSH), object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        IQKeyboardManager.shared().isEnabled = true
        self.setTabbarToVisible(isVisible: true)
        
        
        if UserDefaults.standard.value(forKey: FIRST_TIME) == nil
        {
            
            UserDefaults.standard.set("first", forKey: FIRST_TIME)
            UserDefaults.standard.synchronize()
            
        }
        
        self.addMultiplRightBraButtons(with: UIImage.init(named: SEARCH_BLUE), secondImg: UIImage.init(named: CART_BlUE), ref: self, action1:
            {
                let vc  = self.storyboard?.instantiateViewController(withIdentifier: "MSJSearchVC")
                self.navigationController?.pushViewController(vc!, animated: true)
                
        })
        {
            
            let vc  = self.storyboard?.instantiateViewController(withIdentifier: "MSJAddToCartVC")
            self.navigationController?.pushViewController(vc!, animated: true)
            
        }
        
        self.backgroundColorOrImageSetting(view: self.view, imageView: self.bgImage)
        self.view.layoutIfNeeded()
        self.bgImage.layoutIfNeeded()
        
    }
    
  
    
    func englishLanguage()
    {
//        self.getAds()
        
    }
    
    func arabicLanguage()
    {
        self.setLanguage(isArabic: true)
        self.opensetRTL()
    }
    
    @objc func pushCame(notification: Notification)
    {
        if (UserDefaults.standard.value(forKey: USER_DATA) == nil)
        {
            
            self.moveToLoginScreen()
            return
            
        }
        
    }
    
    func getFeaturedProducts()
    {
        
        let url = BASE_URL + PRODUCTS
        
        let params = ["IsFeatured": "1",
                      "CompanyID":       MSJSharedManager.getAppSetting().CompanyID,
                      "NoVersion": "true"]

        
        ApiManager.getRequest(urlString: url, isAlertShow: self.isRefresControl, parameters: params, successCallback: { (dict) in
            
            print(dict)
            self.isRefresControl = true
            
            let featuredDict = dict["products"] as! NSArray
            
            for item in featuredDict {
                let mainProductDict = item as! NSDictionary
                self.productsArray.append(ProductModel.parseProductData(dict: mainProductDict))
            }
            
            featuresProductsItems = self.productsArray.count
            
            self.tableView.dataSource = self
            self.tableView.delegate = self
            
            if MSJSharedManager.getArabic()
            {
                
                self.tableView.reloadData()
            }
            else
            {
                self.tableView.reloadData()
            }
            
            

        }) { (errorString) in
            
            
            
            print(errorString)
            
        }
    }
    
    func getPromotions()
    {
        
        let url = BASE_URL + PROMOTIONS
        
        let params = ["CompanyID": MSJSharedManager.getAppSetting().CompanyID,
                      "NoVersion": "true"]

        
        ApiManager.getRequest(urlString: url, isAlertShow: self.isRefresControl, parameters: params, successCallback: { (dict) in
            
            print(dict)
            self.isRefresControl = true
            
            let promotionsArray = dict["promotions"] as! NSArray

            for item in promotionsArray {
                let promotionDict = item as! NSDictionary
                self.promotionsArray.append(PromotionsModel.parsePromotionData(dict: promotionDict))
            }
            
            self.tableView.dataSource = self
            self.tableView.delegate = self
            
            if MSJSharedManager.getArabic()
            {
                
                self.tableView.reloadData()
            }
            else
            {
                self.tableView.reloadData()
            }
            
            

        }) { (errorString) in
            
            
            
            print(errorString)
            
        }
    }
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        let indexPath = IndexPath(row: 0, section: 0)
        if let cell = tableView.cellForRow(at: indexPath) as? NiehezHomeTableViewCell {
         
            let pageWidth  = cell.collectionView.frame.size.width
            let currentPage = (cell.collectionView.contentOffset.x) / CGFloat(pageWidth)
            if (0.0 != fmodf(Float(currentPage), 1.0)) {
                cell.pageController.currentPage = Int(currentPage + 1);
            } else {
                cell.pageController.currentPage = Int(currentPage);
            }
            
        }
        
    }

}

//MARK: - Table View Delagtes
extension NiehezHomeViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as? NiehezHomeTableViewCell ?? NiehezHomeTableViewCell()
            
            cell.collectionView.dataSource = self
            cell.collectionView.delegate = self
            
            
            cell.collectionView.reloadData()
            
            return cell
            
        } else if indexPath.row == 1 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as? NiehezHomeTableViewCell2 ?? NiehezHomeTableViewCell2()
            
            cell.featuresCollectioView.dataSource = self
            cell.featuresCollectioView.delegate = self
            
            cell.labelProducts.text = "Feature Products"
            
            AnimatableReload.reload(collectionView: cell.featuresCollectioView, animationDirection: ANIMATION_DOWN)
            
            return cell
            
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as? NiehezHomeTableViewCell2 ?? NiehezHomeTableViewCell2()
            
            cell.featuresCollectioView.dataSource = self
            cell.featuresCollectioView.delegate = self
            
            cell.labelProducts.text = "Top Rated"
            
            AnimatableReload.reload(collectionView: cell.featuresCollectioView, animationDirection: ANIMATION_DOWN)
            
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 0
        {
            if promotionsArray.count == 0
            {
                return 0 
            }
        }
        return UITableView.automaticDimension
    }
    
}

//MARK: - CollectionView Delegates

extension NiehezHomeViewController: UICollectionViewDataSource, UICollectionViewDelegate, CollectionViewWaterfallLayoutDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 0
        {
            return self.promotionsArray.count
        } else {
            return self.productsArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 0 {
            
            let cell = collectionView .dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! MSJProductCell
            
            cell.labelProductTitleBold.text = self.promotionsArray[indexPath.row].Title
            cell.labelDetail.text = self.promotionsArray[indexPath.row].Description
            
            cell.labelProductTitleBold.font = UIFont.init(name: MSJSharedManager.boldFont(), size: cell.labelProductTitleBold.font.pointSize)
            cell.labelDetail.font = UIFont.init(name: MSJSharedManager.regularFont(), size: cell.labelDetail.font.pointSize)
            
            if self.promotionsArray[indexPath.row].ImageName != ""
            {
                let urlString = IMG_BASE_URL + self.promotionsArray[indexPath.row].ImageName
                let url = URL(string: urlString)
                cell.productImageView.kf.indicatorType = .activity
                cell.productImageView.kf.setImage(with: url, placeholder: PLACEHOLDER_IMAGE, options: [.transition(.fade(0.7))])
            }
            else
            {
                cell.productImageView.image = PLACEHOLDER_IMAGE
            }
            
            return cell
        } else if collectionView.tag == 1 {
                        
            var cell = MSJProductCell()
            
            cell = collectionView .dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! MSJProductCell
            
            
            let productObject = self.productsArray[indexPath.row]
            
            
            cell.cartView.layer.cornerRadius = cell.cartView.frame.size.height / 2
            cell.cartView.clipsToBounds = true
            
            cell.labelLogo.isHidden = true
            cell.logoImageView.isHidden = true
            
            cell.labelModelNumber.text = productObject.SubCategoryTitle
            cell.labelModelNumber.isHidden = true
            cell.labelDetail.text = productObject.Title
            cell.labelPrice.text = String(describing: productObject.Price) + " " + "SAR".localized()
            cell.labelLogo.text = productObject.SubCategoryTitle
            
            cell.labelModelNumber.font = UIFont.init(name: MSJSharedManager.regularFont(), size: cell.labelModelNumber.font.pointSize)
            cell.labelDetail.font = UIFont.init(name: MSJSharedManager.regularFont(), size: cell.labelDetail.font.pointSize)
            cell.labelPrice.font = UIFont.init(name: MSJSharedManager.boldFont(), size: cell.labelPrice.font.pointSize)
            
            cell.labelLogo.font = UIFont.init(name: MSJSharedManager.boldFont(), size: cell.labelLogo.font.pointSize)
            
            if productObject.ImageName != ""
            {
                let urlString = IMG_BASE_URL + productObject.ImageName
                let url = URL(string: urlString)
                cell.productImageView.kf.indicatorType = .activity
                cell.productImageView.kf.setImage(with: url, placeholder: PLACEHOLDER_IMAGE, options: [.transition(.fade(0.7))])
            }
            else
            {
                cell.productImageView.image = PLACEHOLDER_IMAGE
            }
            
//            if productObject.brandLogo != ""
//            {
//                pinRemoteImg(url: IMG_BASE_URL + (productObject.brandLogo ?? ""), imgView: cell.logoImageView, placeholder: BRAND_PLACEHOLDER_IMAGE)
//                cell.logoImageView.isHidden = false
//            }
//            else
//            {
//                cell.labelLogo.isHidden = false
//
//            }
            
            
            return cell
            
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        if collectionView.tag == 0 {
            
            return CGSize(width:UIScreen.main.bounds.size.width , height:220)
            
        } else {
            
            let yourWidth = UIScreen.main.bounds.size.width / 2.4
            
            let yourHeight = yourWidth + 60
            
            return CGSize(width: yourWidth, height: yourHeight)
            
        }
                
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag != 0 {
            DispatchQueue.main.async
                {
//                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "MSJPorductDetailVC") as! MSJPorductDetailVC
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailNewDesignViewController") as! ProductDetailNewDesignViewController
                    vc.categoryObjectById = self.productsArray[indexPath.row]
                    self.navigationController?.pushViewController(vc, animated: true)
                    
            }
        }
    }
    
}

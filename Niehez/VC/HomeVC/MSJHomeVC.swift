//
//  MSJHomeVC.swift
//  MSJ
//
//  Created by Mac on 2/17/1439 AH.
//  Copyright © 1439 Mac. All rights reserved.
//

import UIKit
import SKPhotoBrowser
import IQKeyboardManager
import AnimatableReload


class MSJHomeVC: BaseVC , UICollectionViewDelegate,UICollectionViewDataSource , UIScrollViewDelegate , CollectionViewWaterfallLayoutDelegate
{
    
    
    @IBOutlet weak var labelFeaturedProducts: MSJBoldLabel!
    
     @IBOutlet weak var adsCollectionView: UICollectionView!
    
    @IBOutlet weak var featuredProductCollectionView: UICollectionView!
    
    @IBOutlet weak var bgImage: UIImageView!
    
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet var headerView: UIView!
    
    
    var adsArray = Array<Any>()
    
    var productsArray = [ProductModel]()
    
    var isRefresControl:Bool = true
    
    var isLanguageChange : Bool = false
    
    
    var initalIndex = 0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
                        
       let cellSize = CGSize(width:UIScreen.main.bounds.size.width , height:190)

       let layout = UICollectionViewFlowLayout()
       layout.scrollDirection = .horizontal
       layout.itemSize = cellSize
       layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
       layout.minimumLineSpacing = 0.0
       layout.minimumInteritemSpacing = 0.0
       adsCollectionView.setCollectionViewLayout(layout, animated: true)

        
        
        self.addLeftBarIcon()
        
        isRefresControl = true 
        
        self.createChatButton()
        
        
        
        
        if UserDefaults.standard.value(forKey: FIRST_TIME) == nil
        {
            
            self.languageAlert(title: "Alert".localized(), message: "Select the language\nYou can change your language from language settings.", btnTitle: "العربية", buttonTwo: "English", successCallback: self.englishLanguage, callBack: self.arabicLanguage)
            
        }
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.pushCame(notification:)), name: Notification.Name(IS_SUPPORT_PUSH), object: nil)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.setLanaguage()
        self.backgroundColorOrImageSetting(view: self.view, imageView: self.bgImage)
        headerView.frame.size.width = UIScreen.main.bounds.size.width
        IQKeyboardManager.shared().isEnabled = true
        self.setTabbarToVisible(isVisible: true)
        
        
        if UserDefaults.standard.value(forKey: FIRST_TIME) == nil
        {
            
            UserDefaults.standard.set("first", forKey: FIRST_TIME)
            UserDefaults.standard.synchronize()
            
        }
        else
        {
            //self.getFeaturedProducts()
            
            if isLanguageChange != MSJSharedManager.getArabic()
            {
                self.getAds()
                isLanguageChange = MSJSharedManager.getArabic()
            }
            else
            {
                if self.adsArray.count == 0
                {
                    self.getAds()
                }
                else if self.productsArray.count == 0
                {
                    self.getFeaturedProducts()
                }
                
            }
        }
        
        
        
        
        self.addMultiplRightBraButtons(with: UIImage.init(named: SEARCH_BLUE), secondImg: UIImage.init(named: CART_BlUE), ref: self, action1:
            {
                let vc  = self.storyboard?.instantiateViewController(withIdentifier: "MSJSearchVC")
                self.navigationController?.pushViewController(vc!, animated: true)
                
        })
        {
            
            let vc  = self.storyboard?.instantiateViewController(withIdentifier: "MSJAddToCartVC")
            self.navigationController?.pushViewController(vc!, animated: true)
            
        }
                
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
  
    
    @objc func pullToRefresh(_ refreshControl: UIRefreshControl)
    {
        isRefresControl = false
        self.getAds()
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        
        if featuredProductCollectionView == collectionView
        {
            return self.productsArray.count
        }
        else
        {
            return self.adsArray.count
            
        }
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if featuredProductCollectionView == collectionView
        {
            
            
            
            var cell = MSJProductCell()
            
            cell = collectionView .dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! MSJProductCell
            
            
            let productObject = self.productsArray[indexPath.row]
            
            
            cell.cartView.layer.cornerRadius = cell.cartView.frame.size.height / 2
            cell.cartView.clipsToBounds = true
            
            cell.labelLogo.isHidden = true
            cell.logoImageView.isHidden = true
            
            cell.labelModelNumber.text = productObject.SubCategoryTitle
            cell.labelModelNumber.isHidden = true
            cell.labelDetail.text = productObject.Title
            cell.labelPrice.text = String(describing: productObject.Price) + " " + "SAR".localized()
            cell.labelLogo.text = productObject.SubCategoryTitle
            
            cell.labelModelNumber.font = UIFont.init(name: MSJSharedManager.regularFont(), size: cell.labelModelNumber.font.pointSize)
            cell.labelDetail.font = UIFont.init(name: MSJSharedManager.regularFont(), size: cell.labelDetail.font.pointSize)
            cell.labelPrice.font = UIFont.init(name: MSJSharedManager.boldFont(), size: cell.labelPrice.font.pointSize)
            
            cell.labelLogo.font = UIFont.init(name: MSJSharedManager.boldFont(), size: cell.labelLogo.font.pointSize)
            
            if productObject.ImageName != ""
            {
                let urlString = IMG_BASE_URL + productObject.ImageName
                let url = URL(string: urlString)
                cell.productImageView.kf.indicatorType = .activity
                cell.productImageView.kf.setImage(with: url, placeholder: PLACEHOLDER_IMAGE, options: [.transition(.fade(0.7))])
            }
            else
            {
                cell.productImageView.image = PLACEHOLDER_IMAGE
            }
            
//            if productObject.brandLogo != ""
//            {
//                pinRemoteImg(url: IMG_BASE_URL + (productObject.brandLogo ?? ""), imgView: cell.logoImageView, placeholder: BRAND_PLACEHOLDER_IMAGE)
//                cell.logoImageView.isHidden = false
//            }
//            else
//            {
//                cell.labelLogo.isHidden = false
//
//            }
            
            
            return cell
            
        }
        else
        {
            
            let cell = collectionView .dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! MSJProductCell
            let adObject = self.adsArray[indexPath.row] as! MSJProduct
            
            cell.labelProductTitleBold.text = adObject.productName!
            cell.labelDetail.text = adObject.productDescription!
            
            cell.labelProductTitleBold.font = UIFont.init(name: MSJSharedManager.boldFont(), size: cell.labelProductTitleBold.font.pointSize)
            cell.labelDetail.font = UIFont.init(name: MSJSharedManager.regularFont(), size: cell.labelDetail.font.pointSize)
            if adObject.imagePath != nil
            {
                
                pinRemoteImg(url: IMG_BASE_URL + adObject.imagePath!, imgView: cell.productImageView, placeholder: FEATURED_PLACEHOLDER_IMAGE)
                
            }
            else
            {
                cell.productImageView.image = FEATURED_PLACEHOLDER_IMAGE
            }
            
            
            
            return cell
        }
        
    }
    
    
    ////////////////////////////// MARK - Header View Reuseable ////////////////////////////
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        
        
        
        switch kind {
            
        case UICollectionView.elementKindSectionHeader:
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Header", for: indexPath)
            header.addSubview(self.headerView)
            
            return header
            
        case UICollectionView.elementKindSectionFooter:
            
            return UICollectionReusableView()
            
        default:
            return UICollectionReusableView()
        }
        
        
        
        return UICollectionReusableView()
        
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        
        if collectionView == self.featuredProductCollectionView
        {
            let yourWidth = UIScreen.main.bounds.size.width / 2.3
            
            let yourHeight = yourWidth + 60
            
            
            
            
            return CGSize(width: yourWidth, height: yourHeight)
        }
        
        
        return CGSize(width:UIScreen.main.bounds.size.width , height:220)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        if collectionView == self.featuredProductCollectionView
        {
            
            return CGSize(width: collectionView.frame.width, height: 320)
        }
        
        return CGSize(width: collectionView.frame.width, height: 0)
        
        
    }
    
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        //        if adsCollectionView == collectionView
        //        {
        //            initalIndex = indexPath.row
        //            self.expandImages()
        //        }
        //        else
        //        {
        DispatchQueue.main.async
            {
//                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MSJPorductDetailVC") as! MSJPorductDetailVC
//                vc.categoryObjectById = self.featuredProductsArray[indexPath.row] as? MSJProduct
//                self.navigationController?.pushViewController(vc, animated: true)
                
        }
        
        // }
        
    }
    
    
    
    
    
    @objc func buttonViewDetails(button: UIButton)
    {
        
        DispatchQueue.main.async
            {
//                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MSJPorductDetailVC") as! MSJPorductDetailVC
//                vc.categoryObjectById = self.adsArray[button.tag] as? MSJProduct
//                self.navigationController?.pushViewController(vc, animated: true)
                
        }
        
    }
    public func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
                let pageWidth  = adsCollectionView.frame.size.width
                let currentPage = (adsCollectionView.contentOffset.x) / CGFloat(pageWidth)
                if (0.0 != fmodf(Float(currentPage), 1.0))
                {
        
                      pageControl.currentPage = Int(currentPage + 1);
        
                }
                else
                {
        
                     pageControl.currentPage = Int(currentPage);
                }
        
    }
    
    
    
    
    @IBAction func buttonFileComplaint(_ sender: Any)
    {
        if (UserDefaults.standard.value(forKey: USER_DATA) == nil)
        {
            self.moveToLoginScreen()
            return
            
        }
        
        DispatchQueue.main.async
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MSJComplaintVC")
                self.navigationController?.pushViewController(vc!, animated: true)
        }
        
    }
    
    @IBAction func buttonCheckLatesTicket(_ sender: Any)
    {
        if (UserDefaults.standard.value(forKey: USER_DATA) == nil)
        {
            
            self.moveToLoginScreen()
            return
            
        }
        //    NotificationCenter.default.post(name: Notification.Name(SUPPORT), object: nil)
        
        
    
    }
    
    
    
    
    func getFeaturedProducts()
    {
        
        let url = BASE_URL + PRODUCTS
        
        let params = ["IsFeatured": "1",
                      "CompanyID":       MSJSharedManager.getAppSetting().CompanyID,
                      "NoVersion": "true"]

        
        ApiManager.getRequest(urlString: url, isAlertShow: self.isRefresControl, parameters: params, successCallback: { (dict) in
            
            print(dict)
            self.isRefresControl = true
            
            let featuredDict = dict["products"] as! NSArray
            
            for item in featuredDict {
                let mainProductDict = item as! NSDictionary
                self.productsArray.append(ProductModel.parseProductData(dict: mainProductDict))
            }
            
            if MSJSharedManager.getArabic()
            {
                
                AnimatableReload.reload(collectionView: self.featuredProductCollectionView, animationDirection: ANIMATION_DOWN)
                //                 self.featuredProductCollectionView.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition.left, animated: false)
            }
            else
            {
                AnimatableReload.reload(collectionView: self.featuredProductCollectionView, animationDirection: ANIMATION_DOWN)
            }
            
            

        }) { (errorString) in
            
            
            
            print(errorString)
            
        }
    }
    
    
    
    
    func getAds()
    {
        
        let url = BASE_URL + ADS
        
        
        
        MSJApiManager .sharedInstance.getRequest(urlString: url, isAlertShow: self.isRefresControl, parameters: nil, successCallback: { (dict) in
            
            
            
            self.adsArray.removeAll()
//            self.featuredProductsArray.removeAll()
            
            self.featuredProductCollectionView.reloadData()
            //self.adsCollectionView.reloadData()
            
            
            let adsList = dict["ads"] as! NSArray
            
            for item in adsList
            {
                var adDict = item as! NSDictionary
                adDict = adDict.removeNull()
                let adObject = MSJProduct()
                adObject.mainCategoryId = adDict["ad_id"] as? String
                adObject.productId = adDict["product_id"] as? String
                adObject.imagePath = adDict["image"] as? String
                if MSJSharedManager.getArabic() == true
                {
                    
                    adObject.productName = adDict["ad_title_ar"] as? String
                    adObject.productDescription = adDict["ad_description_ar"] as? String
                }
                else
                {
                    adObject.productName = adDict["ad_title_en"] as? String
                    adObject.productDescription = adDict["ad_description_en"] as? String
                    
                }
                
                
                
                self.adsArray.append(adObject)
                
            }
            
         
            
            
            
                        if MSJSharedManager.getArabic()
                        {
                            // self.adsArray =  self.adsArray.reversed()
                            AnimatableReload.reload(collectionView: self.adsCollectionView, animationDirection: ANIMATION_RIGHT)
            
            
                        }
                        else
                        {
                            AnimatableReload.reload(collectionView: self.adsCollectionView, animationDirection: ANIMATION_LEFT)
                        }
            
            
            
            self.pageControl.numberOfPages = self.adsArray.count
            
           // self.adsCollectionView.reloadData()
            
            self.getFeaturedProducts()
            
            
        }) { (errorString) in
            
            
            self.getFeaturedProducts()
            
        }
    }
    
    
    // Zoom Images
    func expandImages()
    {
        var count =  self.adsArray.count
        count = count - 1
        var images = [SKPhoto]()
        for  index in 0...count
        {
            
            let adObject = self.adsArray[index] as!  MSJProduct
            
            if adObject.imagePath != nil
            {
                let url = IMG_BASE_URL + adObject.imagePath!
                let photo = SKPhoto.photoWithImageURL(url)
                photo.shouldCachePhotoURLImage = true
                images.append(photo)
            }
            
            
        }
        
        
        let browser = SKPhotoBrowser(photos: images)
        browser.initializePageIndex(initalIndex)
        present(browser, animated: true, completion: {})
    }
    
    
    func setLanaguage()
    {
        
        print(MSJSharedManager.mediumFont())
        labelFeaturedProducts.text = "Featured Products".localized()
        
        //
        //        labelFeaturedProduct.attributedText = makeAttributedLable(complete: "Featured Products".localized(), CompFont: UIFont.init(name: MSJSharedManager.regularFont(), size: 15.0)!, CompColor: UIColor.white, sub: "Featured".localized(), subFont: UIFont.init(name: MSJSharedManager.mediumFont(), size: 15.0)!, SubColor: UIColor.white)
        //
        //       labelTechnicalDetail.text = "Now, For any complaints and issues, you can file a new complaint to get technical help from our team.".localized()
        //
        //
        //
        //
        //        labelTechnicalSupport.text = "Technical Support Request".localized()
        //        labelCheckLastComplaint.text = "View status of old Ticket‎".localized()
        //        labelNewComplaintTicket.text = "Open a new Ticket".localized()
        //        labelNoAdsAvailable.text = "No Ads Available".localized()
        //        labelNoProductAndFeaturedAvailable.text = "No Featured Products Available".localized()
        //
        //        labelTechnicalSupport.font = UIFont.init(name:MSJSharedManager.mediumFont(), size:labelTechnicalSupport.font.pointSize)
        //        labelCheckLastComplaint.font = UIFont.init(name:MSJSharedManager.regularFont(), size:labelCheckLastComplaint.font.pointSize)
        //        labelNewComplaintTicket.font = UIFont.init(name: MSJSharedManager.regularFont(), size:labelNewComplaintTicket.font.pointSize)
        //
        //        if MSJSharedManager.getArabic()
        //        {
        //            labelTechnicalSupport.textAlignment = NSTextAlignment.right
        //            labelFeaturedProduct.textAlignment = NSTextAlignment.right
        //        }
        //        else
        //        {
        //            labelTechnicalSupport.textAlignment = NSTextAlignment.left
        //            labelFeaturedProduct.textAlignment = NSTextAlignment.left
        //        }
        
    }
    
    @objc func pushCame(notification: Notification)
    {
        if (UserDefaults.standard.value(forKey: USER_DATA) == nil)
        {
            
            self.moveToLoginScreen()
            return
            
        }
        //    NotificationCenter.default.post(name: Notification.Name(SUPPORT), object: nil)
        
  
        
    }
    
    
    func englishLanguage()
    {
        self.getAds()
        
    }
    
    func arabicLanguage()
    {
        self.setLanguage(isArabic: true)
        self.opensetRTL()
    }
    
    
}





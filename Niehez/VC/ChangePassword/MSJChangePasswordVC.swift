//
//  MSJChangePasswordVC.swift
//  MSJ
//
//  Created by Mac on 09/03/2018.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit
import IDZSwiftCommonCrypto

class MSJChangePasswordVC: BaseVC {

    @IBOutlet weak var buttonUpdate: MSJRegularButton!
    @IBOutlet weak var textFieldConfirmPassword: MSJCustomTextField!
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var textFieldNewPassword: MSJCustomTextField!
    @IBOutlet weak var textFieldOldPassword: MSJCustomTextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        setLanguage()
        self.backButtonOnNavigationBar()
        self.setAttributedTitleForRightBar(complete: "Change Password".localized(), CompFont: UIFont.init(name: MSJSharedManager.regularFont(), size: 15.0)!, CompColor: .white, sub: "Change".localized(), subFont: UIFont.init(name: MSJSharedManager.boldFont(), size: 15.0)!, SubColor: .white)
        
        self.setTabbarToVisible(isVisible: false)
        self.backgroundColorOrImageSetting(view: self.view, imageView: self.bgImage)
    }

 
    
    @IBAction func updateButton(_ sender: Any)
    {
        
        self.view.endEditing(true)
        let userObject = MSJSharedManager.getUser()
        
        if self.whiteSpaceValidae(textfield: self.textFieldOldPassword.text!)==false
        {
            self.showAlert(title: "Error".localized(), message: "Enter old password".localized())
            
            
        }
        else if (textFieldOldPassword.text != nil)
        {
            let md5s2 : Digest = Digest(algorithm:.md5)
            md5s2.update(string: textFieldOldPassword.text!)
            let digests2 = md5s2.final()
            
            print( hexString(fromArray: digests2))
            
            if hexString(fromArray: digests2) == userObject.Password
            {
                 if self.whiteSpaceValidae(textfield: self.textFieldNewPassword.text!)==false
                {
                    self.showAlert(title: "Error".localized(), message: "Enter new password".localized())
                    
                    
                }
                else if self.textFieldNewPassword.text != self.textFieldConfirmPassword.text
                {
                    self.showAlert(title: "Error".localized(), message: "Password mismatch".localized())
                    

                }
                else
                {
                    
                    var dataDictionary = [String: String]()
                    dataDictionary["OldPassword"] = self.textFieldOldPassword.text
                    dataDictionary["NewPassword"] = self.textFieldNewPassword.text
                    dataDictionary["UserID"] = userObject.UserID
                    
//                    buttonUpdate.startAnimation()
                    let qualityOfServiceClass = DispatchQoS.QoSClass.background
                    let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
                    backgroundQueue.async(execute: {
                        
                        
                        
                        let url = BASE_URL + CHANGE_PASSWORD
                                                
                        ApiManager.postServiceCall(urlString: url, isAlertShow: false, parameters: dataDictionary, header: nil, successCallback: { (dict) in
                            print(dict)
                            
                            self.alertWithSingleBackButtonFuction(title: "Success".localized(), message: "Password updated successfully".localized(), successCallback: self.goBack)
                            
                            var userObject:MSJUser = MSJUser.parseDataOfUserInfo(userDict: dict )
                            
                            userObject =  MSJSharedManager.setUser(userObject: userObject)
                            
                            self.saveCustomerData(dict: dict)
                            
                            
                            
                            
                        }) { (error) in
                            
                            
                            self.showAlert(title: "Error".localized(), message:error)
                        }
                    })
                }
                
            }
            else
            {
                  self.showAlert(title: "Error".localized(), message: "Old password not match with current password".localized())
            }
            
        }
       

       
    }
    
    func goBack()
    {
       self.navigationController?.popViewController(animated: true)
    }
    
    func setLanguage()
    {
        if MSJSharedManager.getArabic()
        {
            
            textFieldNewPassword.textAlignment = NSTextAlignment.right
            textFieldOldPassword.textAlignment = NSTextAlignment.right
            textFieldConfirmPassword.textAlignment = NSTextAlignment.right
            
            
            
            
        }
        else
        {
            
            
            textFieldConfirmPassword.textAlignment = NSTextAlignment.left
            textFieldOldPassword.textAlignment = NSTextAlignment.left
            textFieldNewPassword.textAlignment = NSTextAlignment.left
            
            
            
        }
        
        textFieldNewPassword.placeholder = "New Password".localized()
        textFieldOldPassword.placeholder = "Old Password".localized()
        textFieldConfirmPassword.placeholder = "Confirm Password".localized()
        buttonUpdate.setTitle("Update".localized(), for: UIControl.State.normal)
    }

}

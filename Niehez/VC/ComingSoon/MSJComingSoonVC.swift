//
//  MSJComingSoonVC.swift
//  MSJ
//
//  Created by Mac on 24/05/2018.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class MSJComingSoonVC: BaseVC {

    @IBOutlet weak var labelComingSoong: MSJBoldLabel!
    override func viewDidLoad()
    {
        super.viewDidLoad()
         self.addLeftBarIcon()
        labelComingSoong.text = "Coming Soon!".localized()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

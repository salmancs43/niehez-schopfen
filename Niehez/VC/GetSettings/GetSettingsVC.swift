//
//  GetSettingsVC.swift
//  Niehez
//
//  Created by Macbook on 04/11/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class GetSettingsVC: BaseVC {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var logoImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
        
        self.generateToken()
    }
    
    
    func generateToken() {
        
        MSJApiManager.alamofirePostRequestNewToken(parameters: [:], isShowAI: true, mainView: self.view, successCallback: { (dict) in
            
            print(dict)
            
            let token = dict["token"] as? String
            
            UserDefaults.standard.set(token, forKey: USERDEFAULTS_TOKEN)
            UserDefaults.standard.synchronize()
            print(UserDefaults.standard.string(forKey: USERDEFAULTS_TOKEN) ?? "")
//            UserDefaults.standard.removeObject(forKey: USERDEFAULTS_USERID)
            
            self.getApiSetting()
            
        }) { (error) in
            
            self.showAlert(title: "Error".localized(), message: error)
            
        }
        
    }
    
    func getApiSetting()
    {
        
        let params = ["CompanyID": "2", "NoVersion": "true"]
        
        ApiManager.getRequest(urlString: BASE_URL + GET_API_SETTING, isAlertShow: true, parameters: params) { dict in
            print(dict)
            
            let object = GetAppSetting.parseAppSetting(response: dict)
            MSJSharedManager.setAppSetting(object: object)

            self.loadSplashData(object: object)
            
        } errorCallBack: { error in
            print(error)
        }

    }
    
    
    func loadSplashData(object: GetAppSetting) {
        
        
        
        // Set Logo
        
        self.logoImage.downLoadImageIntoImageView(url: IMG_BASE_URL + object.SplashLogo)
        
        
        //Splash Background Color Start
        let firstHex = hexStringToUIColors(hex: object.SplashBackgroundHex)
        let secondHex = hexStringToUIColors(hex: object.Splash2ndHex)
        
        if object.SplashBackgroundHex != "" && object.Splash2ndHex != "" {
            
            self.bgView.applyGradient(hex1: firstHex, hex2: secondHex)
            
        }
        else if object.SplashBackgroundImage != ""
        {
           self.bgImage.downLoadImageIntoImageView(url: IMG_BASE_URL + object.SplashBackgroundImage)
        }
        else {
                    
            if object.SplashBackgroundHex != "" {
                bgView.backgroundColor = firstHex
            } else if object.Splash2ndHex != "" {
                bgView.backgroundColor = secondHex
            } else {
                bgView.backgroundColor = UIColor.white
            }
            
        }
        
        
        //Splash Move Forward Time
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0 + Double(object.SplashDuration)!) {
            
            if UserDefaults.standard.value(forKey: USER_DATA) != nil
            {
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "MSJTabBar")
                {
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }
                
                
            }
            else
            {
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "MSJLogInVC")
                {
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }
                
            }
            
           
        }
        

    }
    

}

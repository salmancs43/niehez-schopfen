//
//  MSJLanguageSettingVC.swift
//  MSJ
//
//  Created by Mac on 14/02/2018.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class MSJLanguageSettingVC: BaseVC {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bgImage: UIImageView!
    
    var languageArray = Array<Any>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        languageArray = ["English", "العربية"]
        tableView.tableFooterView = UIView()
    
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.backButtonOnNavigationBar()
        self.setAttributedTitleForRightBar(complete: "Language Setting".localized(), CompFont: UIFont.init(name: MSJSharedManager.regularFont(), size: 15.0)!, CompColor: .white, sub: "Language".localized(), subFont: UIFont.init(name: MSJSharedManager.boldFont(), size: 15.0)!, SubColor: .white)
        
        self.setTabbarToVisible(isVisible: false)
        self.backgroundColorOrImageSetting(view: self.view, imageView: self.bgImage)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    

}

extension MSJLanguageSettingVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return languageArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") else { return UITableViewCell() }
        
        let labelTitle =  cell.viewWithTag(10) as! MSJRegularLabel
        labelTitle.text = languageArray[indexPath.row] as? String
        
//        if MSJSharedManager.getArabic() {
//
//            if indexPath.row == 1 {
//                labelTitle.textColor = MSJColor.white
//            } else {
//                labelTitle.textColor = MSJColor.white
//            }
//
//        } else {
//
//            if indexPath.row == 0 {
//                labelTitle.textColor = MSJColor.white
//            } else {
//                labelTitle.textColor = MSJColor.white
//            }
//
//        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var isArabic: Bool = false
        isArabic = indexPath.row == 0 ? false : true
        
        self.setLanguage(isArabic: isArabic)
        self.opensetLTR()
        
        NotificationCenter.default.post(name: Notification.Name(NOTIFICATION), object: nil)
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

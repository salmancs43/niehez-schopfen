//
//  MSJOtherVC.swift
//  MSJ
//
//  Created by Mac on 21/12/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class MSJOtherVC: BaseVC , UITableViewDelegate , UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var bgView: UIView!
    
    var otherArray = Array<Any>()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.addLeftBarIcon()
        tableView.tableFooterView = UIView()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.setTableView()
        self.backgroundColorOrImageSetting(view: self.view, imageView: self.bgImage)
        self.setTabbarToVisible(isVisible: true)
                
    }
    
   

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return otherArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        
        let  labelTitle =  cell?.viewWithTag(10) as! MSJRegularLabel
        
        labelTitle.text = otherArray[indexPath.row] as? String
        
        labelTitle.font = UIFont.init(name: MSJSharedManager.regularFont(), size: labelTitle.font.pointSize)
        
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if indexPath.row == 0
        {
            DispatchQueue.main.async
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "MSJLanguageSettingVC") as! MSJLanguageSettingVC
                    self.navigationController?.pushViewController(vc, animated: true)
                    
            }
            
            
        }
        else if indexPath.row == 1
        {
            
            if (UserDefaults.standard.value(forKey: USER_DATA) != nil)
            {
                DispatchQueue.main.async
                    {
                        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "NiehezSignUpViewController") as! NiehezSignUpViewController
                        vc.isFromEditProfile = true
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                }
                
            }
            else
            {
                
                DispatchQueue.main.async
                    {
                        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "MSJLogInVC") as! MSJLogInVC
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                }
                
                
            }
            
        }
        else if indexPath.row == 2
        {
            
            if (UserDefaults.standard.value(forKey: USER_DATA) != nil)
            {
                DispatchQueue.main.async
                    {
                        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "MSJChangePasswordVC") as! MSJChangePasswordVC
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                }
                
                
            }
            else
            {
                DispatchQueue.main.async
                    {
                        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "MSJLogInVC") as! MSJLogInVC
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                }
                
                
            }
            
            
            
        }
        else if indexPath.row == 3
        {
            DispatchQueue.main.async
                {
                    let vc  = self.storyboard?.instantiateViewController(withIdentifier: "MSJOrderVC") as! MSJOrderVC
                    vc.isBackButton = true
                    self.navigationController?.pushViewController(vc, animated: true)
                    
            }
        }
        else if indexPath.row == 4
        {
            DispatchQueue.main.async
                {
                    let vc  = self.storyboard?.instantiateViewController(withIdentifier: "MSJSupportVC") as! MSJSupportVC
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                    
            }
            
        }
            
            
        else if indexPath.row == 5
        {
            
            let alert = UIAlertController(title: "Are you sure you want to coutinue?".localized(), message: "", preferredStyle: .actionSheet)
            
            
            
            let logOut = UIAlertAction(title: "Log Out".localized(), style: .destructive, handler: handlLogOut)
            let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: handlCancel)
            
            alert.addAction(logOut)
            alert.addAction(cancelAction)
            
            
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = CGRect(x: 2.0, y: 2.0, width: self.view.bounds.size.width / 2.0, height: self.view.bounds.size.height / 2.0)
            
            self.present(alert, animated: true, completion: nil)
        }
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        if indexPath.row == 3 || indexPath.row == 4
        {
            return 0
        }
        return 60
    }
    
    func setTableView()
    {
        if (UserDefaults.standard.value(forKey: USER_DATA) != nil)
        {
            otherArray = ["Language Settings".localized(),"Edit Profile".localized(),"Change Password".localized(),"My Order".localized() , "Complaints".localized() ,"Log Out".localized()]
        }
        else
        {
            otherArray = ["Language Settings".localized(),"Log In".localized()]
        }
        
        tableView.reloadData()
    }
    
    
    func handlLogOut(alertAction: UIAlertAction!) -> Void
    {
        self.logOut()
        NotificationCenter.default.post(name: Notification.Name(REMOVE_SUPPORT), object: nil)
        self.setTableView()
        
    }
    
    func handlCancel(alertAction: UIAlertAction!) -> Void
    {
        
    }
    
    
    
}

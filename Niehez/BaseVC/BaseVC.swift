//
//  BaseVC.swift
//  SwiftStructure
//
//  Created by Mac on 9/27/17.
//  Copyright © 2017 Zynq. All rights reserved.
//

import UIKit


import CFAlertViewController
import CoreLocation
import MapKit
import Kingfisher
import NVActivityIndicatorView
import Firebase
import Localize_Swift
import NYAlertViewController


class BaseVC: UIViewController,CLLocationManagerDelegate {
    
    var firstBarBtn: (()->())?
    var secondBarBtn: (()->())?
    var customBarBtn: (()->())?
    var gestureAction: ((_ tapGestureRecognizer: UITapGestureRecognizer)->())?
    
    let locationManager = CLLocationManager()
    var latitude : Double = 0.0
    var lognitude : Double = 0.0
    
    
    var refreshControlFunction: (()->())?
      
    @objc var refreshControlBase = UIRefreshControl()
  
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getUserLocation()
        self.setUpNavBarAppearance()
        
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    
    
    
    func backgroundColorOrImageSetting(view:UIView , imageView:UIImageView){
        
        let object = MSJSharedManager.getAppSetting()
        
        //Set Background Color
        let firstHex = hexStringToUIColors(hex: object.LoginBackgroundHex)
        let secondHex = hexStringToUIColors(hex: object.Login2ndHex)
        
      
        imageView.isHidden = true
        if object.LoginBackgroundHex != "" && object.Login2ndHex != ""{
            
        
            view.applyGradient(hex1: firstHex, hex2: secondHex)
        }
        else if object.SplashBackgroundImage != ""
        {
            imageView.isHidden = false
           imageView.downLoadImageIntoImageView(url: IMG_BASE_URL + object.LoginBackgroundImage)
        }
        else{
             
            
            if object.LoginBackgroundHex != ""
            {
                view.backgroundColor = firstHex
            }
            else if object.Login2ndHex != ""
            {
                view.backgroundColor = secondHex
            }
            else{
                view.backgroundColor = UIColor.black
            }
        }
 
    }
 
   
    func getUserLocation()
    {
        isAuthorizedtoGetUserLocation()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        }
        
    }
    func hexStringToUIColors (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

//    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool
//    {
//        return true
//    }
//    
    
  
    
    // MARK: - Navigation lable styling.

    func makeAttributedLable(complete:String,CompFont:UIFont,CompColor:UIColor,sub:String,subFont:UIFont,SubColor:UIColor) -> NSAttributedString {
        
        let CompleteString = complete
        let specificString = sub
        let longestWordRange = (CompleteString as NSString).range(of: specificString)
        let attributedString = NSMutableAttributedString(string: CompleteString, attributes: [NSAttributedString.Key.font : CompFont, NSAttributedString.Key.foregroundColor : CompColor])
        attributedString.setAttributes([NSAttributedString.Key.font : subFont, NSAttributedString.Key.foregroundColor : SubColor], range: longestWordRange)
        return attributedString
    }
   
 
    // MARK: - Navigation left side iCon.
    
    func addLeftBarIcon() {
       
        let logoImageView = UIImageView()
        logoImageView.downLoadImageIntoImageView(url: MSJSharedManager.getAppSetting().LoginLogo)
        logoImageView.frame = CGRect(x:0.0,y:0.0, width:logoImageView.frame.size.width,height:40)
        logoImageView.contentMode = .scaleAspectFit
        logoImageView.clipsToBounds = true
        let imageItem = UIBarButtonItem.init(customView: logoImageView)
        let widthConstraint = logoImageView.widthAnchor.constraint(equalToConstant:110)
        let heightConstraint = logoImageView.heightAnchor.constraint(equalToConstant: 26)
        heightConstraint.isActive = true
        widthConstraint.isActive = true
        navigationItem.leftBarButtonItem =  imageItem
    }
    // MARK: - Navigation custom rightBarButtonItem.
    
    func addMultiplRightBraButtons(with firstImg:UIImage!,secondImg:UIImage?,ref:UIViewController,action1: @escaping ()->(),action2: @escaping ()->()) {
        self.firstBarBtn = action1
        self.secondBarBtn = action2
        if firstImg != nil && secondImg == nil
        {
           
            let button1 = UIButton(type: UIButton.ButtonType.custom)
         
            
            button1.downLoadImageIntoButton(url: MSJSharedManager.getAppSetting().TopBarCartIcon)
           
//            button1.setImage(firstImg, for: UIControl.State.normal)
            button1.addTarget(ref, action: #selector(self.first), for: UIControl.Event.touchDown)
            button1.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
            button1.imageView?.contentMode = .scaleAspectFit
            button1.imageView?.clipsToBounds = true
            button1.clipsToBounds = true
            button1.imageEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
            let barButton1 = MJBadgeBarButton()
            barButton1.setup(customButton: button1)
            
            if UserDefaults.standard.value(forKey: CART_COUNT) != nil
            {
                barButton1.badgeValue = UserDefaults.standard.value(forKey: CART_COUNT) as! String
            }
            
            barButton1.badgeOriginX = 20.0
            barButton1.badgeOriginY = -4
           // navigationItem.rightBarButtonItems = [barButton1]
            
        }else{
            let button1 = UIButton(type: UIButton.ButtonType.custom)
            button1.downLoadImageIntoButton(url: MSJSharedManager.getAppSetting().TopBarCartIcon)
            button1.imageView?.contentMode = .scaleAspectFit
            button1.imageView?.clipsToBounds = true
            button1.clipsToBounds = true
            button1.imageEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
            button1.addTarget(ref, action: #selector(self.first), for: UIControl.Event.touchDown)
            button1.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
            
            
            let barButton1 = UIBarButtonItem(customView: button1)
            
            let button2 = UIButton(type: UIButton.ButtonType.custom)
            
            button2.downLoadImageIntoButton(url: MSJSharedManager.getAppSetting().TopBarSearchIcon)
            button2.imageView?.contentMode = .scaleAspectFit
            button2.imageView?.clipsToBounds = true
            button2.clipsToBounds = true
            button2.imageEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
           
            button2.addTarget(ref, action: #selector(self.second), for: UIControl.Event.touchDown)
            button2.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
          
            let barButton2 = MJBadgeBarButton()
            barButton2.setup(customButton: button2)
            
            if UserDefaults.standard.value(forKey: CART_COUNT) != nil
            {
               barButton2.badgeValue = UserDefaults.standard.value(forKey: CART_COUNT) as! String
            }
            
            barButton2.badgeOriginX = 20.0
            barButton2.badgeOriginY = -4
//            if (UserDefaults.standard.value(forKey: USER_DATA) != nil)
//            {
                 navigationItem.rightBarButtonItems = [barButton1,barButton2]
           // }
//            else
//            {
//                navigationItem.rightBarButtonItems = [barButton1]
//
//            }
           
            
        }
        
    }
    
    

    @objc func first() {
        firstBarBtn?()
    }
    @objc func second(){
        secondBarBtn?()
    }
    
    func addRightSideImageOnTextfield(with Image:String!,textField:UITextField,ref:UIViewController,action1: @escaping (_ tapGestureRecognizer: UITapGestureRecognizer)->()){
        
        self.gestureAction = action1
        let cityPaddingView: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let dropDownImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 18, height: 18))
        dropDownImageView.image = UIImage(named: Image)
        dropDownImageView.contentMode = UIView.ContentMode.scaleAspectFit
        cityPaddingView.addSubview(dropDownImageView)
        textField.rightView = cityPaddingView
        textField.rightViewMode = .always
        
        let cityGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapImage(tapGestureRecognizer:)))
        dropDownImageView.isUserInteractionEnabled = true
        dropDownImageView.addGestureRecognizer(cityGesture)
        
    }
    
    @objc func tapImage(tapGestureRecognizer: UITapGestureRecognizer){

        gestureAction?(tapGestureRecognizer)
    }
    
    
    // Add Custom Right button with style.
    
    func addRightNavBarCustomButton(title:String,fontFamily:String,fontColor:UIColor,ref:UIViewController,buttonAction: @escaping ()->()) {
        self.customBarBtn = buttonAction
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: title, style: .plain, target: ref, action: #selector(self.custom))
        // Nav Bar Right button styling.
        ref.navigationItem.rightBarButtonItem!.setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: fontFamily , size: 17)!], for: .normal);
        ref.navigationItem.rightBarButtonItem!.setTitleTextAttributes([NSAttributedString.Key.foregroundColor:fontColor], for: .normal)
        
        
        
    }
    
    @objc func custom() {
        customBarBtn?()
    }
    // Email Validation.
    func isValidEmail(testStr:String) -> Bool {
        print("validate emilId: \(testStr)")
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
    // Phonenumber Validation.
    
    func phoneValidate(value: String) -> Bool
    {
        if value.count == 10 
        {
            let phoneText = value.prefix(2)
            if phoneText == "05"
            {
                return true
            }
           
        }
         return false
        
//        let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,14}$"
//        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
//        let result =  phoneTest.evaluate(with: value)
//        return result
    }
    
    // White space validation.
    func whiteSpaceValidae(textfield:String) -> Bool {
        if (textfield.trimmingCharacters(in: .whitespaces).isEmpty){
            return false
        }else{
            return true
        }
    }
   
    
    
 // Mark - Alerts
    
    func showAlert (title : String!, message : String!) -> Void
    {
        
        let alertViewController = NYAlertViewController()
        
        // Set a title and message
        alertViewController.title = title
        alertViewController.message = message
        
        // Customize appearance as desired
        alertViewController.buttonCornerRadius = 5.0
        alertViewController.cancelButtonColor = self.hexStringToUIColors(hex: MSJSharedManager.getAppSetting().AlertPositiveButtonColor)
        alertViewController.view.tintColor = self.view.tintColor
        
        alertViewController.titleFont = UIFont(name:  MSJSharedManager.boldFont(), size: 20.0)
        alertViewController.messageFont = UIFont(name:  MSJSharedManager.regularFont(), size: 16.0)
        alertViewController.cancelButtonTitleFont = UIFont(name:  MSJSharedManager.boldFont(), size: 16.0)
        alertViewController.buttonTitleFont = UIFont(name:  MSJSharedManager.boldFont(), size: 16.0)
        
        
        
        alertViewController.swipeDismissalGestureEnabled = false
        alertViewController.backgroundTapDismissalGestureEnabled = false
        
        // Add alert actions
        let cancelAction = NYAlertAction(
            title: "Close".localized(),
            style: .cancel,
            handler: { (action) in
               
                self.dismiss(animated: true, completion: nil)
               
                
        })
        alertViewController.addAction(cancelAction)
        
     
        self.present(alertViewController, animated: true, completion: nil)
        
    }
    
    

    func alertWithSingleBackButtonFuction (title : String!, message : String!,successCallback : @escaping () -> Void) -> Void
    {
        
        let alertViewController = NYAlertViewController()
        
        // Set a title and message
        alertViewController.title = title
        alertViewController.message = message
        
        // Customize appearance as desired
        alertViewController.buttonCornerRadius = 5.0
        alertViewController.cancelButtonColor =  self.hexStringToUIColors(hex: MSJSharedManager.getAppSetting().AlertPositiveButtonColor)
        alertViewController.view.tintColor = self.view.tintColor
        
        alertViewController.titleFont = UIFont(name:  MSJSharedManager.boldFont(), size: 20.0)
        alertViewController.messageFont = UIFont(name:  MSJSharedManager.regularFont(), size: 16.0)
        alertViewController.cancelButtonTitleFont = UIFont(name:  MSJSharedManager.boldFont(), size: 16.0)
        alertViewController.buttonTitleFont = UIFont(name:  MSJSharedManager.boldFont(), size: 16.0)
        
   
        
        
        alertViewController.swipeDismissalGestureEnabled = false
        alertViewController.backgroundTapDismissalGestureEnabled = false
        
        // Add alert actions
        let cancelAction = NYAlertAction(
            title: "Close".localized(),
            style: .cancel,
            handler: { (action) in
                
                self.dismiss(animated: true, completion: nil)
                successCallback()
                
        })
        alertViewController.addAction(cancelAction)
        
        
        self.present(alertViewController, animated: true, completion: nil)
        
    }
    
    
    func twoButtonAlert (title : String!, message : String!, btnTitle : String!, buttonTwo : String!, successCallback : @escaping () -> Void) -> Void {
        
        let alertViewController = NYAlertViewController()
        
        // Set a title and message
        alertViewController.title = title
         alertViewController.message = message
        
        // Customize appearance as desired
        alertViewController.buttonCornerRadius = 5.0
        alertViewController.cancelButtonColor =  self.hexStringToUIColors(hex: MSJSharedManager.getAppSetting().AlertPositiveButtonColor)
        alertViewController.destructiveButtonColor =  self.hexStringToUIColors(hex: MSJSharedManager.getAppSetting().AlertNegativeButtonColor)
        alertViewController.view.tintColor = self.view.tintColor
        
        alertViewController.titleFont = UIFont(name:  MSJSharedManager.boldFont(), size: 20.0)
        alertViewController.messageFont = UIFont(name:  MSJSharedManager.regularFont(), size: 16.0)
        alertViewController.buttonTitleFont = UIFont(name:  MSJSharedManager.boldFont(), size: 16.0)
        alertViewController.destructiveButtonTitleFont = UIFont(name:  MSJSharedManager.boldFont(), size: 16.0)
      
        
        
        alertViewController.swipeDismissalGestureEnabled = false
        alertViewController.backgroundTapDismissalGestureEnabled = false
        
        // Add alert actions
        
        // Add alert actions
        let cancelAction = NYAlertAction(
            title: btnTitle,
            style: .cancel,
            handler: { (action) in
                
                self.dismiss(animated: true, completion: nil)
                
        })
        
        let doneAction = NYAlertAction(
            title: buttonTwo,
            style: .destructive,
            handler: { (action) in
                self.dismiss(animated: true, completion: nil)
                successCallback()
                
        })
        alertViewController.addAction(cancelAction)
        alertViewController.addAction(doneAction)
        
        
        
        
        
        
        // Present the alert view controller
        self.present(alertViewController, animated: true, completion: nil)
        
        
        
        
    }
   
    
    func attributedStringWithNumberOfLineSpacing (text:String) -> NSAttributedString
    {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 3
        if MSJSharedManager.getArabic()
        {
            paragraphStyle.alignment = .right
        }
        else
        {
            paragraphStyle.alignment = .left
        }
        let attrString = NSMutableAttributedString(string: text, attributes: [NSAttributedString.Key.font: UIFont(name:MSJSharedManager.regularFont(), size: 12)!])
//        let attrString = NSMutableAttributedString(string: text)
        attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle,
                                range:NSMakeRange(0, attrString.length))
        return attrString
    }
    
    
    
    
    func languageAlert (title : String!, message : String!, btnTitle : String!, buttonTwo : String!, isUsedBagColor:Bool = false ,  successCallback : @escaping () -> Void, callBack : @escaping () -> Void) -> Void {
        
        let alertViewController = NYAlertViewController()
        
        // Set a title and message
        alertViewController.title = title
        alertViewController.message = message
        
        // Customize appearance as desired
        alertViewController.buttonCornerRadius = 5.0
        alertViewController.cancelButtonColor =  self.hexStringToUIColors(hex: MSJSharedManager.getAppSetting().AlertPositiveButtonColor)
        
        if isUsedBagColor
        {
             alertViewController.destructiveButtonColor =  self.hexStringToUIColors(hex: MSJSharedManager.getAppSetting().AlertNegativeButtonColor)
        }
        else
        {
            alertViewController.destructiveButtonColor =  self.hexStringToUIColors(hex: MSJSharedManager.getAppSetting().AlertNegativeButtonColor)
        }
       
        
        alertViewController.view.tintColor = self.view.tintColor
        
        alertViewController.titleFont = UIFont(name:  MSJSharedManager.boldFont(), size: 20.0)
        alertViewController.messageFont = UIFont(name:  MSJSharedManager.regularFont(), size: 16.0)
        alertViewController.destructiveButtonTitleFont = UIFont(name:  MSJSharedManager.boldFont(), size: 12.0)
        alertViewController.cancelButtonTitleFont = UIFont(name:  MSJSharedManager.boldFont(), size: 12.0)
      
        
        
        
        alertViewController.swipeDismissalGestureEnabled = false
        alertViewController.backgroundTapDismissalGestureEnabled = false
        
        
        
        // Add alert actions
        
        let cancelAction = NYAlertAction(
            title: btnTitle,
            style: .cancel,
            handler: { (action) in
                
                self.dismiss(animated: true, completion: nil)
                callBack()
                
        })
        
        let doneAction = NYAlertAction(
            title: buttonTwo,
            style: .destructive,
            handler: { (action) in
                self.dismiss(animated: true, completion: nil)
                successCallback()
                
        })
        alertViewController.addAction(cancelAction)
        alertViewController.addAction(doneAction)

        
        // Present the alert view controller
        self.present(alertViewController, animated: true, completion: nil)
        
        
        
        
    }
    
    
    func addRefreshControl(isCollectionView:Bool , collectionView:UICollectionView? , tableView:UITableView? , titleOfRefresh:String = "",  refrence:UIViewController ,action: @escaping ()->())-> Void {
           
           
           self.refreshControlFunction = action
           
           
           refreshControlBase.tintColor = UIColor.white
           
           refreshControlBase.addTarget(refrence, action: #selector(refreshControlMethod), for: .valueChanged)
           
           if isCollectionView
           {
               collectionView?.refreshControl = refreshControlBase
           }
           else
           {
               tableView?.refreshControl = refreshControlBase
           }
           
       }
       
       
       @objc func refreshControlMethod()
       {
           
           refreshControlFunction?()
           
           
       }
       
       
       func endRefreshing()
       {
           refreshControlBase.endRefreshing()
       }
    
    

   
    // Save Customer Data In User Default
    
    func saveCustomerData(dict:NSDictionary)
    {
       
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: dict)
              
        UserDefaults.standard.set(encodedData, forKey: USER_DATA)
        UserDefaults.standard.synchronize()


    }
    
    // Remove Customer Data In User Default
    
    func logOut()
    {
        
        UserDefaults.standard.removeObject(forKey: USER_DATA)
        UserDefaults.standard.synchronize()
        
        UserDefaults.standard.setValue("0", forKey: CART_COUNT)
        UserDefaults.standard.synchronize()

    }
    
    // Get Customer Id
    
    func customerId () ->String
    {
        if  UserDefaults.standard.value(forKey: USER_DATA) != nil
        {
            let data = UserDefaults.standard.value(forKey: USER_DATA) as? Data
            let userDict =   NSKeyedUnarchiver.unarchiveObject(with: data!) as? NSDictionary
            
            if let customerDict = userDict?["user_info"] as? NSDictionary
            {
                 return  customerDict["user_id"] as? String ?? ""
            }
          
        }
        return ""
     
        
    }
    
    
    // ConertTimeZones
    
    func convertDateAndTimeIntoTimeStamp(date:NSDate) -> NSDictionary {
        var dict = [String: String]()
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.short
        
        
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.doesRelativeDateFormatting = true;
        
        let dateStr : String = dateFormatter.string(from: date as Date)
        let timeStamp =  String(format:"%.f", date.timeIntervalSince1970)
        
        dict[DATE] = dateStr
        dict[TIMESTAMP] = timeStamp
        return dict as NSDictionary
        
    }
    
    
    func convertTimeStampIntoDateString(unixtimeInterval:String , isTime:Bool) -> String
    {
        let date = Date(timeIntervalSince1970: TimeInterval(unixtimeInterval)!)
        let dateFormatter = DateFormatter()
      //  dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.locale = NSLocale.current
        if isTime
        {
            dateFormatter.timeStyle = DateFormatter.Style.short
        }
        
        dateFormatter.dateStyle = DateFormatter.Style.medium
        
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    
    
    func covertDate(unixtimeInterval:String ) -> String
    {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
      
        let date = dateFormatter.date(from: unixtimeInterval)
       
        
       

        let formatter = DateFormatter()
        formatter.doesRelativeDateFormatting = true;
        formatter.dateStyle = DateFormatter.Style.medium
        var dateStr = ""
        if date != nil
        {
             dateStr  = dateFormatter.string(from: date!)
        }
        
        return dateStr
    }
    
    
    
    
    
    
    //if we have no permission to access user location, then ask user for permission.
    func isAuthorizedtoGetUserLocation() {
        
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse     {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    
    //this method will be called each time when a user change his location access preference.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            print("User allowed us to access location")
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            //do whatever init activities here.
        }
    }
    
    
    //this method is called by the framework on     locationManager.requestLocation();
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("Did location updates is called")
        
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        let coord = locationObj.coordinate
        
        print(coord.latitude)
        print(coord.longitude)
        latitude = coord.latitude
        lognitude = coord.longitude
        
        MSJSharedManager.sharedManager.latitude = coord.latitude
        MSJSharedManager.sharedManager.lognitude = coord.longitude
        //store the user location here to firebase or somewhere
    }
    
    @nonobjc func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {
        print(newLocation)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Did location updates is called but failed getting location \(error)")
    }
 // Pin Remote For download Images.
    func pinRemoteImg(url:String, imgView:UIImageView, placeholder:UIImage?)
    {
//        let encodedUrl = url.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
//
//        let finalUrl = URL(string: encodedUrl)!
//
//
//
//       imgView.kf.indicatorType = .activity
//
//        imgView.kf.setImage(with: finalUrl ,placeholder: nil,options: [.transition(ImageTransition.fade(1.0))],progressBlock: { receivedSize, totalSize in
//
//        },
////        completionHandler: { image, error, cacheType, imageURL in
////
////            print(error)
////
////        })
        

    }
    func verifyUrl (urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            // create NSURL instance
            if let url = NSURL(string: urlString) {
                // check if your application can open the NSURL instance
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    
// Register user on firebase
//    func regiserUserOnFirebase (successCallback : @escaping (User) -> Void, errorCallBack : @escaping (String) -> Void) -> Void
//    {
//        if (UserDefaults.standard.value(forKey: USER_DATA) != nil)
//        {
//            let dict = UserDefaults.standard.value(forKey: USER_DATA) as! NSDictionary
//            let customerDict = dict["customer"] as! NSDictionary
//
//            Auth.auth().createUser(withEmail: customerDict["customerEmail"] as! String, password: "12345678", completion: { (user, error) in
//                if let err = error
//                {
//
//                    errorCallBack(err.localizedDescription)
//                    return
//                }
//                else
//                {
//                    successCallback(user!)
//                }
//            })
//
//        }
//
//    }
    
// Sign In user on firebase
//    func signInOnFirebase (successCallback : @escaping (User) -> Void, errorCallBack : @escaping (String) -> Void) -> Void
//    {
//
//        if (UserDefaults.standard.value(forKey: USER_DATA) != nil)
//        {
//            let dict = UserDefaults.standard.value(forKey: USER_DATA) as! NSDictionary
//            let customerDict = dict["customer"] as! NSDictionary
//
//            Auth.auth().signIn(withEmail:customerDict["customerEmail"] as! String, password: "12345678", completion: { (user, error) in
//                if let err = error
//                {
//                     ARSLineProgress.hide()
//                    errorCallBack(err.localizedDescription)
//                    return
//                }
//                else
//                {
//                    successCallback(user!)
//                }
//            })
//
//        }
//
//    }
    
    
    func createChatButton()
    {
       
        
        let imageView = UIImageView()
        if MSJSharedManager.getArabic()
        {
            if UIScreen.main.bounds.size.height == 812
            {
                imageView.frame = CGRect(x:20,y:UIScreen.main.bounds.size.height - 160,width:70,height:70)
            }
            else
            {
                imageView.frame = CGRect(x:20,y:UIScreen.main.bounds.size.height - 130,width:60,height:60)
            }
        }
        else
        {
            if UIScreen.main.bounds.size.height == 812
            {
                imageView.frame = CGRect(x:UIScreen.main.bounds.size.width - 80,y:UIScreen.main.bounds.size.height - 160,width:70,height:70)
            }
            else
            {
                imageView.frame = CGRect(x:UIScreen.main.bounds.size.width - 80,y:UIScreen.main.bounds.size.height - 130,width:60,height:60)
            }
        }


        imageView.image = UIImage.init(named: "Chat")
        imageView.contentMode = UIView.ContentMode.scaleAspectFit
        imageView.tag = 1
        self.view.addSubview(imageView)
        // UIApplication.shared.keyWindow?.addSubview(imageView)

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(chatImage(tapGestureRecognizer:)))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tapGesture)
    }
    
    @objc func chatImage(tapGestureRecognizer: UITapGestureRecognizer)
    {
        
        self.showAlert(title: "Alert".localized(), message: "Account Setup Required".localized())
        print("button chat tap.")
//        // Your action
//        if (UserDefaults.standard.value(forKey: USER_DATA) == nil)
//        {
//            
//           self.moveToLoginScreen()
//            
//        }
//        else
//        {
//            
//        }
    }
    
    
    
    func moveToLoginScreen()
    {
        DispatchQueue.main.async
        {
            let vc =  self.storyboard?.instantiateViewController(withIdentifier: "MSJLogInVC") as! MSJLogInVC
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
       
        
    }
    
    
 
    

    
    func removeWithoutLoginId()
    {
        UserDefaults.standard.removeObject(forKey: WITHOUT_LOGIN_ID)
        UserDefaults.standard.synchronize()
    }
    
    func setRTL()
    {
        
        
        UINavigationBar.appearance().semanticContentAttribute = .forceRightToLeft
        UIView.appearance().semanticContentAttribute = .forceRightToLeft
        UITextField.appearance().semanticContentAttribute = .forceRightToLeft
        
        
   
    
    }
    
    
    func setLTR()
    {
        UINavigationBar.appearance().semanticContentAttribute = .forceLeftToRight
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
         UITextField.appearance().semanticContentAttribute = .forceLeftToRight
        
       
    }
    
    func opensetRTL()
    {
        UIView.appearance().semanticContentAttribute = .forceRightToLeft
        UINavigationBar.appearance().semanticContentAttribute = .forceRightToLeft
        UITextField.appearance().semanticContentAttribute = .forceRightToLeft
        if let vc = storyboard?.instantiateViewController(withIdentifier: "MSJTabBar")
        {

            
            UIView.transition(with: UIApplication.shared.keyWindow!, duration: 0.3, options: .transitionFlipFromRight, animations: {
               
                UIApplication.shared.keyWindow!.rootViewController = vc
                
            }, completion: { completed in
               
            })

          
        }
    }
    
    func opensetLTR()
    {
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        UINavigationBar.appearance().semanticContentAttribute = .forceLeftToRight
        
        UITextField.appearance().semanticContentAttribute = .forceLeftToRight
        
        if let vc = storyboard?.instantiateViewController(withIdentifier: "MSJTabBar")
        {
            UIView.transition(with: UIApplication.shared.keyWindow!, duration: 0.3, options: .transitionFlipFromLeft, animations: {
                
                UIApplication.shared.keyWindow!.rootViewController = vc
                
            }, completion: { completed in
                
            })
            
           // UIApplication.shared.keyWindow?.rootViewController = vc
        }
    }
    
    
    
   
    
    
    func getCartCount()
    {
        var url = ""
        if (UserDefaults.standard.value(forKey: USER_DATA) != nil)
        {
            
            url = BASE_URL + CART_COUNT_API + "?user_id=" + self.customerId()
            
        }
        else
        {
            if UserDefaults.standard.value(forKey: WITHOUT_LOGIN_ID) != nil
            {
                let tempId = String(describing:UserDefaults.standard.value(forKey: WITHOUT_LOGIN_ID)!)
                url = BASE_URL + CART_COUNT_API + "?user_id=" + tempId
                
            }
            
            
        }
      
        if url != ""
        {
           
            
            MSJApiManager .sharedInstance.getRequest(urlString: url, isAlertShow: false, parameters: nil, successCallback: { (dict) in
                
                
              
                let cartCount = dict["cart_count"] as! NSNumber
                
                UserDefaults.standard.setValue(String(describing:cartCount) , forKey: CART_COUNT)
                UserDefaults.standard.synchronize()
                
                  NotificationCenter.default.post(name: Notification.Name(CART_NOTIFICATION), object: nil)
                
            }) { (errorString) in
                
               
                
            }
        
        }
       
    }
    
    
    func openAppstoreAccount()
    {
        
        if let url = URL(string: "itms-apps://itunes.apple.com/app/id1420001669"),
            UIApplication.shared.canOpenURL(url)
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        
    }
    
}

// Extensions
extension UIViewController
{
    
  
    func setAttributedTitleForRightBar(complete:String,CompFont:UIFont,CompColor:UIColor,sub:String,subFont:UIFont,SubColor:UIColor)  {
        let titleLabel = UILabel()
        let CompleteString = complete
        let specificString = sub
        let longestWordRange = (CompleteString as NSString).range(of: specificString)
        let attributedString = NSMutableAttributedString(string: CompleteString, attributes: [NSAttributedString.Key.font : CompFont, NSAttributedString.Key.foregroundColor : CompColor])
        attributedString.setAttributes([NSAttributedString.Key.font : subFont, NSAttributedString.Key.foregroundColor : SubColor], range: longestWordRange)
        titleLabel.attributedText = attributedString
        let rightItem = UIBarButtonItem(customView: titleLabel)
        self.navigationItem.rightBarButtonItem = rightItem
        
        
    }
    
    // Set Tab Bar Visible Or Not
    
    func setTabbarToVisible(isVisible : Bool)
    {
        self.tabBarController?.tabBar.isHidden = !isVisible
        let view = self.tabBarController?.view.viewWithTag(-1)
        view?.isHidden = !isVisible
    }
    
    
    func backButtonOnNavigationBar() {
        
        var back_button_image: UIImage = UIImage()
        
        if MSJSharedManager.getArabic() {
            back_button_image = UIImage(named: ARABIC_BACK_BLUE)!
        } else {
            back_button_image = UIImage(named: BACK_BLUE)!
        }
        
        let backItem = UIBarButtonItem(image: back_button_image.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(backButtonClick(sender:)))
        self.navigationItem.leftBarButtonItem = backItem
        
    }

    @objc func backButtonClick(sender: UIButton) {
        self.setTabbarToVisible(isVisible: true)
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupCenterImageButton() {
        
        let logoContainer = UIView(frame: CGRect(x: 0, y: 0, width: 120, height: 25))
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 120, height: 25))
        let button_image = UIImage(named: "Logo")
        button.setImage(button_image, for: .normal)
//        button.addTarget(self, action: #selector(goHome), for: .touchUpInside)
        logoContainer.addSubview(button)
        self.navigationItem.titleView = logoContainer
        
    }
    
    // set language of arabic
    
    func setLanguage(isArabic:Bool)
    {
        if isArabic
        {
             UserDefaults.standard.set(["ar"], forKey: APPLE_LANGUAGES)
            UserDefaults.standard.synchronize()
            
            UserDefaults.standard.setValue("ar", forKey: LANGUAGE)
            UserDefaults.standard.synchronize()
            MSJSharedManager.setArabicPage(isArabic: true)
            Localize.setCurrentLanguage("ar")
        }
        else
        {
            UserDefaults.standard.set(["en"], forKey: APPLE_LANGUAGES)
            UserDefaults.standard.synchronize()
            
            UserDefaults.standard.setValue("en", forKey: LANGUAGE)
            UserDefaults.standard.synchronize()
            MSJSharedManager.setArabicPage(isArabic: false)
            Localize.setCurrentLanguage("en")
        }
       
    }
    
    
    
    

}
extension NSDictionary {
    
    func removeNull() -> NSDictionary
    {
        let mainDict = NSMutableDictionary.init(dictionary: self)
        for _dict in mainDict {
            if _dict.value is NSNull {
                
                mainDict.setValue("", forKey: _dict.key as! String)
                // mainDict.removeObject(forKey: _dict.key)
            }
            if _dict.value is NSDictionary {
                let test1 = (_dict.value as! NSDictionary).filter({ $0.value is NSNull }).map({ $0 })
                let mutableDict = NSMutableDictionary.init(dictionary: _dict.value as! NSDictionary)
                for test in test1
                {
                    mutableDict.setValue("", forKey: test.key as! String)
                    //mutableDict.removeObject(forKey: test.key)
                }
                mainDict.removeObject(forKey: _dict.key)
                mainDict.setValue(mutableDict, forKey: _dict.key as? String ?? "")
            }
            if _dict.value is NSArray {
                let mutableArray = NSMutableArray.init(object: _dict.value)
                for (index,element) in mutableArray.enumerated() where element is NSDictionary {
                    let test1 = (element as! NSDictionary).filter({ $0.value is NSNull }).map({ $0 })
                    let mutableDict = NSMutableDictionary.init(dictionary: element as! NSDictionary)
                    for test in test1
                    {
                        mutableDict.setValue("", forKey: test.key as! String)
                        //mutableDict.removeObject(forKey: test.key)
                    }
                    mutableArray.replaceObject(at: index, with: mutableDict)
                }
               // mainDict.setValue("", forKey: _dict.key as! String)
                //  mainDict.removeObject(forKey: _dict.key)
                mainDict.setValue(mutableArray, forKey: _dict.key as? String ?? "")
            }
        }
        return mainDict as NSDictionary
    }
}

extension UIApplication
{
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}

extension UIViewController {
    open override func awakeFromNib()
    {
        super.awakeFromNib()
        if Localize.currentLanguage() == "ar"
        {
            navigationController?.view.semanticContentAttribute = .forceRightToLeft
            navigationController?.navigationBar.semanticContentAttribute = .forceRightToLeft
        }
        else
        {
            navigationController?.view.semanticContentAttribute = .forceLeftToRight
            navigationController?.navigationBar.semanticContentAttribute = .forceLeftToRight
        }
    }
}







extension UITextField
{
     
        func addLeftAndRightView(image:UIImage = UIImage.init(named: DROP_DOWN_ICON)!)
        {
               let paddingView: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
               let dropDownImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 12, height: 12))

               let imageView = UIImageView(image: image)
               dropDownImageView.image = imageView.image
//               dropDownImageView.setImageColor(color: UIColor.Ferozi())


               dropDownImageView.contentMode = UIView.ContentMode.scaleAspectFit
               paddingView.addSubview(dropDownImageView)
               paddingView.isUserInteractionEnabled = false
               dropDownImageView.isUserInteractionEnabled = false
            
                self.rightView = paddingView
                self.rightViewMode = .always
                self.leftView = nil
            
        }
}

extension UILabel {
    
    func setHTMLFromString(htmlText: String) {
            
        let modifiedFont = String(format:"<span style=\"font-family: '-apple-system', 'HelveticaNeue'; font-size: \(self.font?.pointSize ?? 16)\">%@</span>", htmlText)

        let attrStr = try! NSAttributedString(data: modifiedFont.data(using: .unicode, allowLossyConversion: true)!, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)

        self.attributedText = attrStr
        
    }
    
}

extension String {
    
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    var htmlToAttributedString: NSAttributedString? {

        guard let data = data(using: .utf8) else { return NSAttributedString() }
        
        do {
            
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
            
        } catch {
            return NSAttributedString()
        }
        
    }
    
}

extension BaseVC {
    
    func setUpNavBarAppearance() {
        
        let navigationBarAppearace = UINavigationBar.appearance()
        
        // change navigation bar icons color
        navigationBarAppearace.tintColor = .black
        // change navigation item title color
        navigationBarAppearace.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.black,
           // NSAttributedString.Key.font: UIFont(name: SharedManager.heavyFont(), size: 18)!
        ]
        // to hide navbar bottom border
        navigationBarAppearace.shadowImage = UIImage()
          
        if #available(iOS 15.0, *) {
        
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = self.hexStringToUIColors(hex: MSJSharedManager.getAppSetting().TopTabBarColor)
            appearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
            navigationBarAppearace.standardAppearance = appearance
            navigationBarAppearace.scrollEdgeAppearance = appearance
            
        } else {
        
            // Fallback on earlier versions
            
            navigationBarAppearace.isTranslucent = false
            // to change the background color of navbar
            navigationBarAppearace.barTintColor = self.hexStringToUIColors(hex: MSJSharedManager.getAppSetting().TopTabBarColor)
            
        }
        
    }
    
}

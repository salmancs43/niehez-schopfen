//
//  Constant.swift
//  MSJ
//
//  Created by Mac on 2/17/1439 AH.
//  Copyright © 1439 Mac. All rights reserved.
//

import Foundation
import UIKit

enum ALERT_TYPE : Int
{
    case AlertTypeWarning
    case AlertTypeSuccess
    case AlertTypeNotice
    case AlertTypeInfo
   
}

let APP_ID = "683458"
let KEY = "cdeb4b42705f7d980ced"
let SECRET_KEY = "99443431ecbfd9c927fd"
let CLUSTER = "ap2"



let PAYTAB_EMAIL =  "dev@zynq.net"
let PAYTAB_SECRETKEY =  "c7YRQHdLzzMN3TyMS6VmhZQEvgUMjj1vq0mgHz2h9P4cIDZBJUY1TOFp4AboTdTkdgBVQDcZshcbUbME5KFZzloEWA2PNV91cKX2"



let GOOGLE_PLACES_API_KEY = "AIzaSyBt17pBtDXT203xEJh4XlPnXuW9502SSM0"
//let GOOGLE_PLACES_API_KEY = "AIzaSyC8NdKXr6Kio-lela3WU6qzXRijSe6_Yvo"
//let GOOGLE_PLACES_API_KEY = "AIzaSyAD-TJBPi7Nc3j5-SWYKgq5mGMWw8KOgQc"

let FULLNAME_LENGTH = 25



let DEVICE_TYPE = "ios"
let UPDATE_APP_MESSAGE = "update"


let USER_DATA = "login"
let LOGO_IMAGE = "Logo"
let BACK_BLUE = "back_blue"
let ARABIC_BACK_BLUE = "arabic_blue"
let SEARCH_BLUE = "search_ blue"
let CART_BlUE = "cart_blue"
let DROP_DOWN_ICON = "dropDown"
let LOCATION_ICON = "location"
let FIREBASE_DATABASEKEY = "conversations"
let FIREBASE_MESSAGEKEY = "messages"
let DATE = "date"
let TIMESTAMP = "timestamp"
let LANGUAGE = "Apple"
let APPLE_LANGUAGES = "AppleLanguages"
let NOTIFICATION = "notification"
let CART_NOTIFICATION = "cartnotification"
let CART_COUNT = "cart"
let SUPPORT = "support"
let REMOVE_SUPPORT = "remove"
let TOKEN = "token"
let IS_PUSH = "push"
let IS_INTERNET_COMING = "internet"
let IS_SUPPORT_PUSH = "supportpush"

let NOTIFICATION_DATA = "notification"
let ANIMATION_DOWN = "down"
let ANIMATION_LEFT = "left"
let ANIMATION_RIGHT = "right"
let FIRST_TIME = "firsttime"
let CITY_ARRAY = "cityarray"
let WITHOUT_LOGIN_ID = "withoutLoginId"
let VERIFICATION_CODE_SEND = "resendVerificationCode"





//let BASE_URL = "https://Dkakeen.schopfen.com/api/"
//let IMG_BASE_URL = "https://Dkakeen.schopfen.com/"

//let BASE_URL = "https://Ecom.Schopfen.com/api/"
let BASE_URL = "https://ecommerce.schopfen.com/api/"
let IMG_BASE_URL = "https://ecommerce.schopfen.com/"

//let IMG_BASE_URL = "https://Ecom.schopfen.com/"

let GENERATE_TOKEN = "generateTokenByApi"
let USERDEFAULTS_TOKEN = "user_token"
let COMPANY_ID = "company_id"
let GET_API_SETTING = "getAppSettings"
let MAIN_CATEGORY = "getCategories"
let POST_USER_SIGN_UP = "userRegistration"
let POST_USER_SIGN_IN = "login"
let PRODUCTS = "getProducts"
let PROMOTIONS = "getPromotions"
let ADS = "ads"
let UPDATE_USER_PROFILE_DATA = "updateUser"
let CHANGE_PASSWORD = "changePassword"
let GET_COMPLAINT_TYPE = "getComplaintTypes"
let GET_VISIT_TYPE = "visit_complaint_type"
let POST_CREATE_COMPLAINT = "addTicket"
let GET_COMPLAINT_BY_ID = "getUserTickets"
let GET_COMMENT = "getTicketComments"
let POST_COMMENT = "addTicketComment"
let UPDATE_READ_STATUS = "updateComment"
let GET_ALL_BRAND = "brands"
let FILTER = "products"
let CHAT_REQUEST = "createChatRequest"
let GET_CUSTOMER_INFO_BY_ID = "getUser"
let CHAT_CLOSE = "closeUserChat"
let GET_QUNENED_NUMBER = "getQueuedChats"
let FORGOT_PASSWORD = "forgotPassword"
let UPDATE_TICKET = "updateTicket"
//let GET_USER_INFO = "getUser"
//let ADD_TO_CART = "addToCart"
let EDIT_CART = "updateCart"
let UPDATE_CART = "updateCartItem"

let GET_SINGLE_COMPLAINT = "getTicketInfo"
let GET_CART_ITEM_LIST = "getCartItems"
let REMOVE_CART_ITEM_BY_ID = "deleteCartItem"
let GET_ALL_CUST_ADDRESS = "getAllAddress"
let POST_ORDER = "placeOrder"
let POST_ADDRESS = "addAddress"
let REMOVE_ADDRESS = "deleteAddress"
let UPDATE_ADDRESS = "updateAddress"
let CITIES = "ksaCities"
let CART_COUNT_API = "getCartCount"
let DISTRICTS = "getDistricts"

let GET_CITIES = "getCities"
let GET_STORES = "getStores"
let GET_PRODUCT_AVAILABILITY = "getProductAvailability"
let SIGN_UP_URL = "signUp"
let SHIPMENT_METHOD = "getShippingMethods"
let LOGIN_URL = "login"
let ADD_TO_CART = "addToCart"
let Update_Profile = "updateProfile"
let GET_PAGE_DETAIL = "getPageDetail"
let GET_ORDER_DETAILS = "getOrderDetail"


let SEARCH_PRODUCTS = "Product/GetProductBySearch"


let GET_ORDERS = "getOrders"

let GET_ALL_SUBCATEGORIES = "Categories/GetAllSubCategories"

let SUB_MAIN_CATEGORY = "Categories/GetSubCategoryByMainCatId"
let GET_PRODUCT_BY_SUBID = "Product/GetProductBySubCatId"
let GET_PRODUCT_BY_ID = "Product/GetProductById"







let PLACEHOLDER_IMAGE = UIImage(named: "camera")
let BRAND_PLACEHOLDER_IMAGE = UIImage(named: "Bitmap")
let FEATURED_PLACEHOLDER_IMAGE = UIImage(named: "dummy")
let USER_IMAGE_PLACEHOLDER_IMAGE = UIImage(named: "userImage")







//
//  AppDelegate.swift
//  MSJ
//
//  Created by Mac on 2/17/1439 AH.
//  Copyright © 1439 Mac. All rights reserved.
//

import UIKit
import Firebase
import FirebaseCore
import FirebaseMessaging
import Localize_Swift
import UserNotifications


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , UNUserNotificationCenterDelegate , MessagingDelegate {

    var window: UIWindow?
    
     var vc = BaseVC()


    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
      
        UIApplication.shared.isStatusBarHidden = true
        if UserDefaults.standard.value(forKey: LANGUAGE) != nil
        {
            let language = UserDefaults.standard.value(forKey: LANGUAGE) as? String
          
            if language == "ar"
            {
                  UserDefaults.standard.set(["ar"], forKey: APPLE_LANGUAGES)
                UserDefaults.standard.synchronize()
                
                UserDefaults.standard.setValue("ar", forKey: LANGUAGE)
                UserDefaults.standard.synchronize()
                MSJSharedManager.setArabicPage(isArabic: true)
                Localize.setCurrentLanguage("ar")
                vc.setRTL()
            }
            else
            {
                UserDefaults.standard.set(["en"], forKey: APPLE_LANGUAGES)
                UserDefaults.standard.synchronize()
                
               UserDefaults.standard.setValue("en", forKey: LANGUAGE)
                UserDefaults.standard.synchronize()
                MSJSharedManager.setArabicPage(isArabic: false)
                Localize.setCurrentLanguage("en")
                vc.setLTR()
            }
        }
        else
        {
           UserDefaults.standard.set(["en"], forKey: APPLE_LANGUAGES)
            UserDefaults.standard.synchronize()
            
           UserDefaults.standard.setValue("en", forKey: LANGUAGE)
            UserDefaults.standard.synchronize()
            MSJSharedManager.setArabicPage(isArabic: false)
            Localize.setCurrentLanguage("en")
            vc.setLTR()
            
        }
        
        

    
    
     
        
        let attributes = [NSAttributedString.Key.font: UIFont(name: MSJSharedManager.regularFont(), size: 15)!]
        UINavigationBar.appearance().titleTextAttributes = attributes
      
    
      
        
        if UserDefaults.standard.value(forKey: CART_COUNT) == nil
        {
            UserDefaults.standard.setValue("0", forKey: CART_COUNT)
            UserDefaults.standard.synchronize()
        }
        
        
        FirebaseApp.configure()
        
        
       
        
        UNUserNotificationCenter.current().delegate = self
               let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
               UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (isAllow, error) in
                   
                   if isAllow {
                       
                       
                       Messaging.messaging().delegate = self
                       MSJSharedManager.getToken()
                       
                   }
                   
               }
               
               application.registerForRemoteNotifications()
      
   
      
        
    
        
        return true
    }
   

    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
           
           
           
           UserDefaults.standard.setValue(fcmToken, forKey: TOKEN)
           
           
           
           
       }

    func applicationDidBecomeActive(_ application: UIApplication)
    {
        if UserDefaults.standard.value(forKey: USER_DATA) != nil
        {
            if Reachability.isConnectedToNetwork()
            {
                
                if UserDefaults.standard.value(forKey: USER_DATA) != nil
                {
                    let data = UserDefaults.standard.value(forKey: USER_DATA) as? Data
                    if let userDict =   NSKeyedUnarchiver.unarchiveObject(with: data!) as? NSDictionary
                    {
                        var userObject:MSJUser = MSJUser.parseDataOfUserInfo(userDict: userDict )
                                          
                        userObject =  MSJSharedManager.setUser(userObject: userObject)
                        self.getUserData()
                        
                    }
        
                  
                }
              
                print("Internet Connection Available!")
                
               
                let vc = UIApplication.topViewController()
                
              
                
            }
            else
            {
                if UserDefaults.standard.value(forKey: USER_DATA) != nil
                {
                    let dict = UserDefaults.standard.value(forKey: USER_DATA)
                    var userObject:MSJUser = MSJUser.parseDataOfUserInfo(userDict: dict as! NSDictionary )
                    
                    userObject =  MSJSharedManager.setUser(userObject: userObject)
                }
                
                print("Internet Connection not Available!")
              
                
            
            }
        }
        
        
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
        var bgTask: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier(rawValue: 0)
        bgTask = application.beginBackgroundTask(expirationHandler: {
            application.endBackgroundTask(bgTask)
            bgTask = UIBackgroundTaskIdentifier.invalid
        })
    }
    

    
    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }
    

    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        //this method is called when notification is about to appear in foreground
        
        
        completionHandler([.alert, .badge, .sound]) // Display notification as
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
    {
     
        
 
        completionHandler(UIBackgroundFetchResult.noData)
    }
    
    
  
 
    func getUserData()
    {
    
        let userObject = MSJSharedManager.getUser()
        
        let url = BASE_URL + GET_CUSTOMER_INFO_BY_ID + "?user_id=" + userObject.UserID!
        
        MSJApiManager.sharedInstance.getRequest(urlString: url, isAlertShow: false, parameters: nil, successCallback: { (dict) in
            
            print(dict)
          
            var userObject:MSJUser = MSJUser.parseDataOfUserInfo(userDict: dict )
            
            userObject =  MSJSharedManager.setUser(userObject: userObject)
            
            self.vc.saveCustomerData(dict: dict )
            
            
        }) { (error) in
            
            
        }
    }
  

}


//
//  MSJAddressOneObject.swift
//  MSJ
//
//  Created by Mac on 1/10/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class UserAddressModel: NSObject {

    var AddressID: String = ""
    var AddressType: String = ""
    var BuildingNo : String = ""
    var CityID: String = ""
    var CityTitle: String = ""
    var DistrictID: String = ""
    var DistrictTitle: String = ""
    var Email: String = ""
    var FullName: String = ""
    var IsDefault: String = ""
    var Latitude: String = ""
    var Longitude: String = ""
    var MobileNo: String = ""
    var POBox: String = ""
    var RecipientName: String = ""
    var Street: String = ""
    var UseForPaymentCollection: String = ""
    var UserID: String = ""
    var ZipCode: String = ""
    
    class func parseUserAddressData(userDict:NSDictionary) -> UserAddressModel
    {
        
        let userInfoObj = UserAddressModel()
        
        userInfoObj.AddressID = userDict["AddressID"] as? String ?? ""
        userInfoObj.AddressType = userDict["AddressType"] as? String ?? ""
        userInfoObj.CityTitle = userDict["CityTitle"] as? String ?? ""
        userInfoObj.BuildingNo = userDict["BuildingNo"] as? String ?? ""
        userInfoObj.CityID = userDict["CityID"] as? String ?? ""
        userInfoObj.DistrictID = userDict["DistrictID"] as? String ?? ""
        userInfoObj.DistrictTitle = userDict["DistrictTitle"] as? String ?? ""
        userInfoObj.Email = userDict["Email"] as? String ?? ""
        userInfoObj.FullName = userDict["FullName"] as? String ?? ""
        userInfoObj.IsDefault = userDict["IsDefault"] as? String ?? ""
        userInfoObj.Latitude = userDict["Latitude"] as? String ?? ""
        userInfoObj.Longitude = userDict["Longitude"] as? String ?? ""
        userInfoObj.POBox = userDict["POBox"] as? String ?? ""
        userInfoObj.RecipientName = userDict["RecipientName"] as? String ?? ""
        userInfoObj.Street = userDict["Street"] as? String ?? ""
        userInfoObj.UseForPaymentCollection = userDict["UseForPaymentCollection"] as? String ?? ""
        userInfoObj.UserID = userDict["UserID"] as? String ?? ""
        userInfoObj.ZipCode = userDict["ZipCode"] as? String ?? ""
        userInfoObj.MobileNo = userDict["MobileNo"] as? String ?? ""
        

        return userInfoObj
        
        
    }
    
}

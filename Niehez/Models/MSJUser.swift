//
//  MSJUser.swift
//  MSJ
//
//  Created by Mac on 12/20/17.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class MSJUser: NSObject
{

    var AuthToken : String?
    var CityID : String?
    var CityTitle : String?
    var Code : String?
    var CompanyID : String?
    var CompanyName : String?
    var CompressedImage : String?
    var DateOfBirth : String?
    var DeviceToken : String?
    var DeviceType : String?
    var DistrictID : String?
    var Email : String?
    var FromSocial : String?
    var FullName : String?
    var Gender : String?
    var Hide : String?
    var Image : String?
    
    var IsActive : String?
    var IsEmailVerified : String?
    var IsMobileVerified : String?
    var LastUnsuccessfulLogin : String?
    var LoyaltyPoints : String?
    var Mobile : String?
    var Notification : String?
    var OS : String?
    var OnlineStatus : String?
    var Password : String?
    var RoleID : String?
    var SecondaryEmail : String?
    var SocialID : String?
    var SortOrder : String?
    var StoreID : String?
    var TempUserKey : String?
    var TermsAccepted : String?
    var UserID : String?
    var UserType : String?


    
    
    
    class func parseDataOfUserInfo(userDict:NSDictionary) -> MSJUser
    {
        
        print(userDict)
        let customerDict = userDict["user_info"] as! NSDictionary
     
        
        let userInfoObj = MSJUser()
        userInfoObj.AuthToken = customerDict["AuthToken"] as? String
        userInfoObj.CityID = customerDict["CityID"] as? String
        userInfoObj.CityTitle = customerDict["CityTitle"] as? String
        userInfoObj.Code = customerDict["Code"] as? String
        userInfoObj.CompanyID = customerDict["CompanyID"] as? String
        userInfoObj.CompanyName = customerDict["CompanyName"] as? String
        userInfoObj.CompressedImage = customerDict["CompressedImage"] as? String
        userInfoObj.DateOfBirth = customerDict["DateOfBirth"] as? String
        userInfoObj.DeviceToken = customerDict["DeviceToken"] as? String
        userInfoObj.DeviceType = customerDict["DeviceType"] as? String
        userInfoObj.DistrictID = customerDict["DistrictID"] as? String
        userInfoObj.Email = customerDict["Email"] as? String
        userInfoObj.FromSocial = customerDict["FromSocial"] as? String
        userInfoObj.FullName = customerDict["FullName"] as? String
        userInfoObj.Gender = customerDict["Gender"] as? String
        userInfoObj.Hide = customerDict["Hide"] as? String
        userInfoObj.Image = customerDict["Image"] as? String
        
        userInfoObj.IsActive = customerDict["IsActive"] as? String
        userInfoObj.IsEmailVerified = customerDict["IsEmailVerified"] as? String
        userInfoObj.IsMobileVerified = customerDict["IsMobileVerified"] as? String
        userInfoObj.LastUnsuccessfulLogin = customerDict["LastUnsuccessfulLogin"] as? String
        userInfoObj.LoyaltyPoints = customerDict["LoyaltyPoints"] as? String
        userInfoObj.Mobile = customerDict["Mobile"] as? String
        userInfoObj.Notification = customerDict["Notification"] as? String
        userInfoObj.OS = customerDict["OS"] as? String
        userInfoObj.OnlineStatus = customerDict["OnlineStatus"] as? String
        userInfoObj.Password = customerDict["Password"] as? String
        userInfoObj.RoleID = customerDict["RoleID"] as? String
        userInfoObj.SecondaryEmail = customerDict["SecondaryEmail"] as? String
        userInfoObj.SocialID = customerDict["SocialID"] as? String
        userInfoObj.SortOrder = customerDict["SortOrder"] as? String
        userInfoObj.StoreID = customerDict["StoreID"] as? String
        userInfoObj.TempUserKey = customerDict["TempUserKey"] as? String
        userInfoObj.TermsAccepted = customerDict["TermsAccepted"] as? String
        userInfoObj.UserID = customerDict["UserID"] as? String
        userInfoObj.UserType = customerDict["UserType"] as? String

        if userInfoObj.IsActive == "0" {
            
            BaseVC().logOut()
            BaseVC().navigationController?.popToRootViewController(animated: true)
            
        }
        
        return userInfoObj
        
        
    }
 
    
}

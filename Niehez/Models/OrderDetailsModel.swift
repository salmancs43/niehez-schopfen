//  Created on 23/07/2022.

import UIKit

class OrderDetailsModel: NSObject {

    var orderInnerDetailObj = OrderInnerDetailModel()
    var orderExtraChargesArray = [OrderExtraChargesModel]()
    var orderItemsArray = [OrderItemsModel]()
    
    class func parseOrderDetailData(dict: NSDictionary) -> OrderDetailsModel {
        
        let orderData = OrderDetailsModel()
        
        if let orderInnerDetail = dict["order_detail"] as? NSDictionary {
            
            orderData.orderInnerDetailObj = OrderInnerDetailModel.parseOrderInnerDetailData(dict: orderInnerDetail)
            
        }
        
        if let orderExtraCharges = dict["order_extra_charges"] as? NSArray {
            
            for item in orderExtraCharges {
            
                if let itemDict = item as? NSDictionary {
                    
                    orderData.orderExtraChargesArray.append(OrderExtraChargesModel.parseOrderExtraChargesData(dict: itemDict))
                    
                }
                
            }
            
        }
        
        if let orderItems = dict["order_items"] as? NSArray {
            
            for item in orderItems {
                
                if let itemDict = item as? NSDictionary {
                    
                    orderData.orderItemsArray.append(OrderItemsModel.parseOrderItemsData(dict: itemDict))
                    
                }
                
            }
            
        }
        
        return orderData
        
    }
    
}

class OrderInnerDetailModel: NSObject {
    
    var AddressCity: String = ""
    var AddressDistrict: String = ""
    var AddressID: String = ""
    var AddressIDForPaymentCollection: String = ""
    var AddressType: String = ""
    var AssignedDriverEmail: String = ""
    var AssignedDriverMobile: String = ""
    var AssignedDriverName: String = ""
    var BranchDeliveryDistrictID: String = ""
    var BuildingNo: String = ""
    var CityID: String = ""
    var CollectFromStore: String = ""
    var CompanyID: String = ""
    var CouponCodeDiscountPercentage: String = ""
    var CouponCodeUsed: String = ""
    var CreatedAt: String = ""
    var DeliveryOTP: String = ""
    var Description: String = ""
    var DiscountAvailed: String = ""
    var DistrictID: String = ""
    var DistrictTitle: String = ""
    var DriverID: String = ""
    var DriverImage: String = ""
    var Email: String = ""
    var ForAdmin: String = ""
    var ForUser: String = ""
    var FullName: String = ""
    var HasTicket: String = ""
    var Hide: String = ""
    var IsClosed: String = ""
    var IsDefault: String = ""
    var IsPosOrder: String = ""
    var IsRead: String = ""
    var Latitude: String = ""
    var Longitude: String = ""
    var Mobile: String = ""
    var MobileNo: String = ""
    var OnlineStatus: String = ""
    var OrderCreateDate: String = ""
    var OrderID: String = ""
    var OrderNumber: String = ""
    var OrderStatusAr: String = ""
    var OrderStatusEn: String = ""
    var OrderStatusID: String = ""
    var POBox: String = ""
    var PaymentMethod: String = ""
    var RecipientName: String = ""
    var ReturnedReason: String = ""
    var ShipmentMethodID: String = ""
    var ShipmentMethodTitle: String = ""
    var ShippedThroughApi: String = ""
    var Status: String = ""
    var StoreAddress: String = ""
    var StoreAdminEmail: String = ""
    var StoreAdminFullName: String = ""
    var StoreCityTitle: String = ""
    var StoreID: String = ""
    var StoreTitle: String = ""
    var Street: String = ""
    var TicketCreatedAt: String = ""
    var TicketID: String = ""
    var TicketNumber: String = ""
    var TotalAmount: String = ""
    var TotalShippingCharges: String = ""
    var TotalTaxAmount: String = ""
    var TransactionID: String = ""
    var UseForPaymentCollection: String = ""
    var UserCity: String = ""
    var UserID: String = ""
    var VatNo: String = ""
    var ZipCode: String = ""
    
    class func parseOrderInnerDetailData(dict: NSDictionary) -> OrderInnerDetailModel {
        
        let obj = OrderInnerDetailModel()
        
        obj.AddressCity = dict["AddressCity"] as? String ?? ""
        obj.AddressDistrict = dict["AddressDistrict"] as? String ?? ""
        obj.AddressID = dict["AddressID"] as? String ?? ""
        obj.AddressIDForPaymentCollection = dict["AddressIDForPaymentCollection"] as? String ?? ""
        obj.AddressType = dict["AddressType"] as? String ?? ""
        obj.AssignedDriverEmail = dict["AssignedDriverEmail"] as? String ?? ""
        obj.AssignedDriverMobile = dict["AssignedDriverMobile"] as? String ?? ""
        obj.AssignedDriverName = dict["AssignedDriverName"] as? String ?? ""
        obj.BranchDeliveryDistrictID = dict["BranchDeliveryDistrictID"] as? String ?? ""
        obj.BuildingNo = dict["BuildingNo"] as? String ?? ""
        obj.CityID = dict["CityID"] as? String ?? ""
        obj.CollectFromStore = dict["CollectFromStore"] as? String ?? ""
        obj.CompanyID = dict["CompanyID"] as? String ?? ""
        obj.CouponCodeDiscountPercentage = dict["CouponCodeDiscountPercentage"] as? String ?? ""
        obj.CouponCodeUsed = dict["CouponCodeUsed"] as? String ?? ""
        obj.CreatedAt = dict["CreatedAt"] as? String ?? ""
        obj.DeliveryOTP = dict["DeliveryOTP"] as? String ?? ""
        obj.Description = dict["Description"] as? String ?? ""
        obj.DiscountAvailed = dict["DiscountAvailed"] as? String ?? ""
        obj.DistrictID = dict["DistrictID"] as? String ?? ""
        obj.DistrictTitle = dict["DistrictTitle"] as? String ?? ""
        obj.DriverID = dict["DriverID"] as? String ?? ""
        obj.DriverImage = dict["DriverImage"] as? String ?? ""
        obj.Email = dict["Email"] as? String ?? ""
        obj.ForAdmin = dict["ForAdmin"] as? String ?? ""
        obj.ForUser = dict["ForUser"] as? String ?? ""
        obj.FullName = dict["FullName"] as? String ?? ""
        obj.HasTicket = dict["HasTicket"] as? String ?? ""
        obj.Hide = dict["Hide"] as? String ?? ""
        obj.IsClosed = dict["IsClosed"] as? String ?? ""
        obj.IsDefault = dict["IsDefault"] as? String ?? ""
        obj.IsPosOrder = dict["IsPosOrder"] as? String ?? ""
        obj.IsRead = dict["IsRead"] as? String ?? ""
        obj.Latitude = dict["Latitude"] as? String ?? ""
        obj.Longitude = dict["Longitude"] as? String ?? ""
        obj.Mobile = dict["Mobile"] as? String ?? ""
        obj.MobileNo = dict["MobileNo"] as? String ?? ""
        obj.OnlineStatus = dict["OnlineStatus"] as? String ?? ""
        obj.OrderCreateDate = dict["OrderCreateDate"] as? String ?? ""
        obj.OrderID = dict["OrderID"] as? String ?? ""
        obj.OrderNumber = dict["OrderNumber"] as? String ?? ""
        obj.OrderStatusAr = dict["OrderStatusAr"] as? String ?? ""
        obj.OrderStatusEn = dict["OrderStatusEn"] as? String ?? ""
        obj.OrderStatusID = dict["OrderStatusID"] as? String ?? ""
        obj.POBox = dict["POBox"] as? String ?? ""
        obj.PaymentMethod = dict["PaymentMethod"] as? String ?? ""
        obj.RecipientName = dict["RecipientName"] as? String ?? ""
        obj.ReturnedReason = dict["ReturnedReason"] as? String ?? ""
        obj.ShipmentMethodID = dict["ShipmentMethodID"] as? String ?? ""
        obj.ShipmentMethodTitle = dict["ShipmentMethodTitle"] as? String ?? ""
        obj.ShippedThroughApi = dict["ShippedThroughApi"] as? String ?? ""
        obj.Status = dict["Status"] as? String ?? ""
        obj.StoreAddress = dict["StoreAddress"] as? String ?? ""
        obj.StoreAdminEmail = dict["StoreAdminEmail"] as? String ?? ""
        obj.StoreAdminFullName = dict["StoreAdminFullName"] as? String ?? ""
        obj.StoreCityTitle = dict["StoreCityTitle"] as? String ?? ""
        obj.StoreID = dict["StoreID"] as? String ?? ""
        obj.StoreTitle = dict["StoreTitle"] as? String ?? ""
        obj.Street = dict["Street"] as? String ?? ""
        obj.TicketCreatedAt = dict["TicketCreatedAt"] as? String ?? ""
        obj.TicketID = dict["TicketID"] as? String ?? ""
        obj.TicketNumber = dict["TicketNumber"] as? String ?? ""
        obj.TotalAmount = dict["TotalAmount"] as? String ?? ""
        obj.TotalShippingCharges = dict["TotalShippingCharges"] as? String ?? ""
        obj.TotalTaxAmount = dict["TotalTaxAmount"] as? String ?? ""
        obj.TransactionID = dict["TransactionID"] as? String ?? ""
        obj.UseForPaymentCollection = dict["UseForPaymentCollection"] as? String ?? ""
        obj.UserCity = dict["UserCity"] as? String ?? ""
        obj.UserID = dict["UserID"] as? String ?? ""
        obj.VatNo = dict["VatNo"] as? String ?? ""
        obj.ZipCode = dict["ZipCode"] as? String ?? ""
        
        return obj
        
    }
    
}

class OrderExtraChargesModel: NSObject {
    
    var Amount: String = ""
    var Factor: String = ""
    var OrderExtraChargeID: String = ""
    var OrderID: String = ""
    var TaxShipmentChargesID: String = ""
    var Title: String = ""
    
    class func parseOrderExtraChargesData(dict: NSDictionary) -> OrderExtraChargesModel {
        
        let obj = OrderExtraChargesModel()
        
        obj.Amount = dict["Amount"] as? String ?? ""
        obj.Factor = dict["Factor"] as? String ?? ""
        obj.OrderExtraChargeID = dict["OrderExtraChargeID"] as? String ?? ""
        obj.OrderID = dict["OrderID"] as? String ?? ""
        obj.TaxShipmentChargesID = dict["TaxShipmentChargesID"] as? String ?? ""
        obj.Title = dict["Title"] as? String ?? ""
        
        return obj
        
    }
    
}

class OrderItemsModel: NSObject {
    
    var AddressID: String = ""
    var AddressIDForPaymentCollection: String = ""
    var Amount: String = ""
    var BranchDeliveryDistrictID: String = ""
    var CollectFromStore: String = ""
    var CompanyID: String = ""
    var CouponCodeDiscountPercentage: String = ""
    var CouponCodeUsed: String = ""
    var CreatedAt: String = ""
    var CustomizedBoxID: String = ""
    var CustomizedOrderProductIDs: String = ""
    var CustomizedShapeImage: String = ""
    var DeliveryOTP: String = ""
    var Description: String = ""
    var DiscountAvailed: String = ""
    var DriverID: String = ""
    var Hide: String = ""
    var IsCorporateItem: String = ""
    var IsCorporateProduct: String = ""
    var IsPosOrder: String = ""
    var IsRead: String = ""
    var ItemType: String = ""
    var OrderID: String = ""
    var OrderItemID: String = ""
    var OrderNumber: String = ""
    var PaymentMethod: String = ""
    var Price: String = ""
    var PriceType: String = ""
    var ProductID: String = ""
    var Quantity: String = ""
    var ReturnedReason: String = ""
    var Ribbon: String = ""
    var SKU: String = ""
    var ShipmentMethodID: String = ""
    var ShippedThroughApi: String = ""
    var Status: String = ""
    var StoreID: String = ""
    var Title: String = ""
    var TotalAmount: String = ""
    var TotalShippingCharges: String = ""
    var TotalTaxAmount: String = ""
    var TransactionID: String = ""
    var UserID: String = ""
    
    class func parseOrderItemsData(dict: NSDictionary) -> OrderItemsModel {
        
        let obj = OrderItemsModel()
        
        obj.AddressID = dict["AddressID"] as? String ?? ""
        obj.AddressIDForPaymentCollection = dict["AddressIDForPaymentCollection"] as? String ?? ""
        obj.Amount = dict["Amount"] as? String ?? ""
        obj.BranchDeliveryDistrictID = dict["BranchDeliveryDistrictID"] as? String ?? ""
        obj.CollectFromStore = dict["CollectFromStore"] as? String ?? ""
        obj.CompanyID = dict["CompanyID"] as? String ?? ""
        obj.CouponCodeDiscountPercentage = dict["CouponCodeDiscountPercentage"] as? String ?? ""
        obj.CouponCodeUsed = dict["CouponCodeUsed"] as? String ?? ""
        obj.CreatedAt = dict["CreatedAt"] as? String ?? ""
        obj.CustomizedBoxID = dict["CustomizedBoxID"] as? String ?? ""
        obj.CustomizedOrderProductIDs = dict["CustomizedOrderProductIDs"] as? String ?? ""
        obj.CustomizedShapeImage = dict["CustomizedShapeImage"] as? String ?? ""
        obj.DeliveryOTP = dict["DeliveryOTP"] as? String ?? ""
        obj.Description = dict["Description"] as? String ?? ""
        obj.DiscountAvailed = dict["DiscountAvailed"] as? String ?? ""
        obj.DriverID = dict["DriverID"] as? String ?? ""
        obj.Hide = dict["Hide"] as? String ?? ""
        obj.IsCorporateItem = dict["IsCorporateItem"] as? String ?? ""
        obj.IsCorporateProduct = dict["IsCorporateProduct"] as? String ?? ""
        obj.IsPosOrder = dict["IsPosOrder"] as? String ?? ""
        obj.IsRead = dict["IsRead"] as? String ?? ""
        obj.ItemType = dict["ItemType"] as? String ?? ""
        obj.OrderID = dict["OrderID"] as? String ?? ""
        obj.OrderItemID = dict["OrderItemID"] as? String ?? ""
        obj.OrderNumber = dict["OrderNumber"] as? String ?? ""
        obj.PaymentMethod = dict["PaymentMethod"] as? String ?? ""
        obj.Price = dict["Price"] as? String ?? ""
        obj.PriceType = dict["PriceType"] as? String ?? ""
        obj.ProductID = dict["ProductID"] as? String ?? ""
        obj.Quantity = dict["Quantity"] as? String ?? ""
        obj.ReturnedReason = dict["ReturnedReason"] as? String ?? ""
        obj.Ribbon = dict["Ribbon"] as? String ?? ""
        obj.SKU = dict["SKU"] as? String ?? ""
        obj.ShipmentMethodID = dict["ShipmentMethodID"] as? String ?? ""
        obj.ShippedThroughApi = dict["ShippedThroughApi"] as? String ?? ""
        obj.Status = dict["Status"] as? String ?? ""
        obj.StoreID = dict["StoreID"] as? String ?? ""
        obj.Title = dict["Title"] as? String ?? ""
        obj.TotalAmount = dict["TotalAmount"] as? String ?? ""
        obj.TotalShippingCharges = dict["TotalShippingCharges"] as? String ?? ""
        obj.TotalTaxAmount = dict["TotalTaxAmount"] as? String ?? ""
        obj.TransactionID = dict["TransactionID"] as? String ?? ""
        obj.UserID = dict["UserID"] as? String ?? ""
        
        return obj
        
    }
    
}

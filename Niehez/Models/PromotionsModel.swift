//
//  PromotionsModel.swift
//  Niehez
//
//  Created by Macbook on 08/06/2022.
//  Copyright © 2022 Mac. All rights reserved.
//

import UIKit

class PromotionsModel: NSObject
{
    
    var ProductTitle: String = ""
    var PromotionExpiresAt: String = ""
    var PromotionID: Int = -1
    var PromotionImage: String = ""
    var PromotionTextID: Int = -1
    var PromotionType: Int = -1
    var PromotionUrl: String = ""
    var CompanyID: String = ""
    var CreatedAt: String = ""
    var CreatedBy: String = ""
    var Description: String = ""
    var Hide: Int = -1
    var ImageName: String = ""
    var IsActive: String = ""
    var Price: String = ""
    var PriceType: String = ""
    var ProducrTextID: String = ""
    var SortOrder: String = ""
    var SystemLanguageID: String = ""
    var Title: String = ""
    var UpdatedAt: String = ""
    var UpdatedBy: String = ""
    
    
    class func parsePromotionData(dict:NSDictionary) -> PromotionsModel
    {
        
        let promotionModel = PromotionsModel()
        promotionModel.ProductTitle = dict["ProductTitle"] as? String ?? ""
        promotionModel.PromotionExpiresAt = dict["PromotionExpiresAt"] as? String ?? ""
        promotionModel.PromotionID = dict["PromotionID"] as? Int ?? -1
        promotionModel.PromotionImage = dict["PromotionImage"] as? String ?? ""
        promotionModel.PromotionTextID = dict["PromotionTextID"] as? Int ?? -1
        promotionModel.PromotionType = dict["PromotionType"] as? Int ?? -1
        promotionModel.PromotionUrl = dict["PromotionUrl"] as? String ?? ""
        promotionModel.CompanyID = dict["CompanyID"] as? String ?? ""
        promotionModel.CreatedAt = dict["CreatedAt"] as? String ?? ""
        promotionModel.CreatedBy = dict["CreatedBy"] as? String ?? ""
        promotionModel.Description = dict["Description"] as? String ?? ""
        promotionModel.Hide = dict["Hide"] as? Int ?? -1
        promotionModel.ImageName = dict["ImageName"] as? String ?? ""
        promotionModel.IsActive = dict["IsActive"] as? String ?? ""
        promotionModel.Price = dict["Price"] as? String ?? ""
        promotionModel.PriceType = dict["PriceType"] as? String ?? ""
        promotionModel.ProducrTextID = dict["ProducrTextID"] as? String ?? ""
        promotionModel.SortOrder = dict["SortOrder"] as? String ?? ""
        promotionModel.SystemLanguageID = dict["SystemLanguageID"] as? String ?? ""
        promotionModel.Title = dict["Title"] as? String ?? ""
        promotionModel.UpdatedAt = dict["UpdatedAt"] as? String ?? ""
        promotionModel.UpdatedBy = dict["UpdatedBy"] as? String ?? ""
                
    
                
        return promotionModel
        
    }
    
}


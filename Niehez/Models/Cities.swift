//
//  Cities.swift
//  ShopBox Driver App
//
//  Created by Macbook on 09/06/2020.
//  Copyright © 2020 Macbook. All rights reserved.
//

import UIKit

class Cities: NSObject {
    
    var id : String = ""
    var title : String = ""
    
    
    class func parseData(response:NSDictionary) -> [Cities]
    {
        
        
        var tempArray = [Cities]()
        
        if let cityArray = response["cities"] as? NSArray
        {
            
            for item in cityArray
            {
                if let dictionary = item as? NSDictionary
                {
                    let object = Cities()
                    
                    object.id = dictionary["id"] as? String ?? ""
                    
                    if MSJSharedManager.getArabic()
                    {
                         object.title = dictionary["arb_name"] as? String ?? ""
                    }
                    else
                    {
                         object.title = dictionary["eng_name"] as? String ?? ""
                    }
                   
                    
                    tempArray.append(object)
                }
            }
            
            
            
        }
        return tempArray
    }
}

import UIKit

class CitiesModel: NSObject {
    
    var CityID : String = ""
    var CityTextID : String = ""
    var Hide : String = ""
    var SortOrder : String = ""
    var SystemLanguageID : String = ""
    var Title : String = ""
    
    
    class func parseData(response:NSDictionary) -> [CitiesModel]
    {
        
        
        var tempArray = [CitiesModel]()
        
        if let cityArray = response["cities"] as? NSArray
        {
            
            for item in cityArray
            {
                if let dictionary = item as? NSDictionary
                {
                    let object = CitiesModel()
                    
                    object.CityID = dictionary["CityID"] as? String ?? ""
                    object.CityTextID = dictionary["CityTextID"] as? String ?? ""
                    object.Hide = dictionary["Hide"] as? String ?? ""
                    object.SortOrder = dictionary["SortOrder"] as? String ?? ""
                    object.SystemLanguageID = dictionary["SystemLanguageID"] as? String ?? ""
                    object.Title = dictionary["Title"] as? String ?? ""
                    
                    tempArray.append(object)
                }
            }
            
            
            
        }
        return tempArray
    }
}

class DistrictModel: NSObject {
    
    var CityID : String = ""
    var DistrictID: String = ""
    var DistrictTextID : String = ""
    var Hide : String = ""
    var SortOrder : String = ""
    var SystemLanguageID : String = ""
    var Title : String = ""
    
    
    class func parseData(response:NSDictionary) -> [DistrictModel]
    {
        
        
        var tempArray = [DistrictModel]()
        
        if let cityArray = response["districts"] as? NSArray
        {
            
            for item in cityArray
            {
                if let dictionary = item as? NSDictionary
                {
                    let object = DistrictModel()
                    
                    object.CityID = dictionary["CityID"] as? String ?? ""
                    object.DistrictID = dictionary["DistrictID"] as? String ?? ""
                    object.DistrictTextID = dictionary["DistrictTextID"] as? String ?? ""
                    object.Hide = dictionary["Hide"] as? String ?? ""
                    object.SortOrder = dictionary["SortOrder"] as? String ?? ""
                    object.SystemLanguageID = dictionary["SystemLanguageID"] as? String ?? ""
                    object.Title = dictionary["Title"] as? String ?? ""
                    
                    tempArray.append(object)
                }
            }
            
            
            
        }
        return tempArray
    }
}


class Districts: NSObject {
    
    var id : String = ""
    var title : String = ""
    
    
    class func parseData(response:NSDictionary) -> [Districts]
    {
        
        
        var tempArray = [Districts]()
        
        if let cityArray = response["districts"] as? NSArray
        {
            
            for item in cityArray
            {
                if let dictionary = item as? NSDictionary
                {
                    let object = Districts()
                    
                    object.id = dictionary["district_id"] as? String ?? ""
                    
                    if MSJSharedManager.getArabic()
                    {
                         object.title = dictionary["arb_name"] as? String ?? ""
                    }
                    else
                    {
                         object.title = dictionary["eng_name"] as? String ?? ""
                    }
                   
                    
                    tempArray.append(object)
                }
            }
            
            
            
        }
        return tempArray
    }
    
    
    
    
    
    
    
}

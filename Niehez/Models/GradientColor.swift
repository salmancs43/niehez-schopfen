//
//  GradientColor.swift
//  Niehez
//
//  Created by Macbook on 05/11/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

extension UIView {
    func applyGradient(hex1:UIColor,hex2:UIColor) {
        let gradient = CAGradientLayer()
        gradient.colors = [hex1.cgColor,
                           hex2.cgColor]   // your colors go here
        //gradient.locations = [0.0, 0.5, 1.0]
        gradient.frame = self.frame
        self.layer.insertSublayer(gradient, at: 0)
    }
}

//
//  ProductModel.swift
//  Niehez
//
//  Created by Macbook on 30/03/2022.
//  Copyright © 2022 Mac. All rights reserved.
//

import UIKit

class ProductModel: NSObject
{
    var AddOnProductID: String = ""
    var BoxIDs: String = ""
    var BrandID: String = ""
    var BrandTitle: String = ""
    var CategoryID: String = ""
    var CategoryTitle: String = ""
    var CompanyID: String = ""
    var CorporateMinQuantity: String = ""
    var CorporatePrice: String = ""
    var CreatedAt: String = ""
    var CreatedBy: String = ""
    var CrossSellingProductID: String = ""
    var Description: String = ""
    var Hide: String = ""
    var ImageName: String = ""
    var Ingredients: String = ""
    var IsActive: String = ""
    var IsCorporateProduct: String = ""
    var IsCustomizedProduct: String = ""
    var IsFeatured: String = ""
    var Keywords: String = ""
    var MetaDescription: String = ""
    var MetaKeywords: String = ""
    var MetaTags: String = ""
    var MinimumOrderQuantity: String = ""
    var OutOfStock: String = ""
    var Price: String = ""
    var PriceType: String = ""
    var ProducrTextID: String = ""
    var ProductID: String = ""
    var PurchaseCount: String = ""
    var SKU: String = ""
    var ServingSize: String = ""
    var SortOrder: String = ""
    var Specifications: String = ""
    var SubCategoryID: String = ""
    var SubCategoryTitle: String = ""
    var TagIDs: String = ""
    var SystemLanguageID: String = ""
    var Title: String = ""
    var ProductType: String = ""
    var UpdatedAt: String = ""
    var UpdatedBy: String = ""
    var ProductImages = [ProductModel]()
    var FileID: String = ""
    var ImageType: String = ""
    var SiteImageID: String = ""
    var VariantData = [ProductVariantData]()
    var AddOnData = [ProductAddOnData]()
    var VariantForSearchProduct: String = ""
    var Warnings: String = ""
    
    class func parseProductData(dict:NSDictionary) -> ProductModel
    {
        
        let productModel = ProductModel()
        
        productModel.AddOnProductID = dict["AddOnProductID"] as? String ?? ""
        productModel.BoxIDs = dict["BoxIDs"] as? String ?? ""
        productModel.BrandID = dict["BrandID"] as? String ?? ""
        productModel.BrandTitle = dict["BrandTitle"] as? String ?? ""
        productModel.CategoryID = dict["CategoryID"] as? String ?? ""
        productModel.CategoryTitle = dict["CategoryTitle"] as? String ?? ""
        productModel.CompanyID = dict["CompanyID"] as? String ?? ""
        productModel.CorporateMinQuantity = dict["CorporateMinQuantity"] as? String ?? ""
        productModel.CorporatePrice = dict["CorporatePrice"] as? String ?? ""
        productModel.CreatedAt = dict["CreatedAt"] as? String ?? ""
        productModel.CreatedBy = dict["CreatedBy"] as? String ?? ""
        productModel.CrossSellingProductID = dict["CrossSellingProductID"] as? String ?? ""
        productModel.Description = dict["Description"] as? String ?? ""
        productModel.Hide = dict["Hide"] as? String ?? ""
        productModel.ImageName = dict["ImageName"] as? String ?? ""
        productModel.Ingredients = dict["Ingredients"] as? String ?? ""
        productModel.IsActive = dict["IsActive"] as? String ?? ""
        productModel.IsCorporateProduct = dict["IsCorporateProduct"] as? String ?? ""
        productModel.IsCustomizedProduct = dict["IsCustomizedProduct"] as? String ?? ""
        productModel.IsFeatured = dict["IsFeatured"] as? String ?? ""
        productModel.Keywords = dict["Keywords"] as? String ?? ""
        productModel.MetaDescription = dict["MetaDescription"] as? String ?? ""
        productModel.MetaKeywords = dict["MetaKeywords"] as? String ?? ""
        productModel.MetaTags = dict["MetaTags"] as? String ?? ""
        productModel.MinimumOrderQuantity = dict["MinimumOrderQuantity"] as? String ?? ""
        productModel.OutOfStock = dict["OutOfStock"] as? String ?? ""
        productModel.Price = dict["Price"] as? String ?? ""
        productModel.PriceType = dict["PriceType"] as? String ?? ""
        productModel.ProducrTextID = dict["ProducrTextID"] as? String ?? ""
        productModel.ProductID = dict["ProductID"] as? String ?? ""
        productModel.PurchaseCount = dict["PurchaseCount"] as? String ?? ""
        productModel.SKU = dict["SKU"] as? String ?? ""
        productModel.SortOrder = dict["SortOrder"] as? String ?? ""
        productModel.Specifications = dict["Specifications"] as? String ?? ""
        productModel.SubCategoryID = dict["SubCategoryID"] as? String ?? ""
        productModel.SubCategoryTitle = dict["SubCategoryTitle"] as? String ?? ""
        productModel.SystemLanguageID = dict["SystemLanguageID"] as? String ?? ""
        productModel.TagIDs = dict["TagIDs"] as? String ?? ""
        productModel.Title = dict["Title"] as? String ?? ""
        productModel.ProductType = dict["Type"] as? String ?? ""
        productModel.UpdatedAt = dict["UpdatedAt"] as? String ?? ""
        productModel.UpdatedBy = dict["UpdatedBy"] as? String ?? ""
                
        if let productImagesArray = dict["ProductImages"] as? NSArray
        {
            for imagesItem in productImagesArray
            {
                if let imagesDict = imagesItem as? NSDictionary
                {
                    
                    let item = ProductModel()
                    
                    item.ImageName = imagesDict["ImageName"] as? String ?? ""
                    item.FileID = imagesDict["FileID"] as? String ?? ""
                    item.ImageType = imagesDict["ImageType"] as? String ?? ""
                    item.SiteImageID = imagesDict["SiteImageID"] as? String ?? ""
            
                    productModel.ProductImages.append(item)
                    
                }
            }
                
        }
        
        if let VariantData = dict["VariantData"] as? NSArray {
            
            for item in VariantData {
                
                if let singleItem = item as? NSDictionary {
                    
                    let singleVariantDataObj = ProductVariantData.parseVariantData(dict: singleItem)
                    productModel.VariantData.append(singleVariantDataObj)
                    
                }
                
            }
            
        }
        
        if let AddOnData = dict["AddOn"] as? NSArray {
            
            for item in AddOnData {
                
                if let singleItem = item as? NSDictionary {
                    
                    let singleAddOnDataObj = ProductAddOnData.parseAddOnData(dict: singleItem)
                    productModel.AddOnData.append(singleAddOnDataObj)
                    
                }
                
            }
            
        }
        
        productModel.VariantForSearchProduct = dict["VariantForSearchProduct"] as? String ?? ""
        productModel.Warnings = dict["Warnings"] as? String ?? ""
        
        return productModel
        
    }
    
}

class ProductVariantData: NSObject {
    
    var AttributeID: String = ""
    var AttributeTitle: String = ""
    var Price: String = ""
    var VariantID: String = ""
    var VariantTitle: String = ""
    
    var isSelected: Bool = false
    
    class func parseVariantData(dict: NSDictionary) -> ProductVariantData {
        
        let obj = ProductVariantData()
        
        obj.AttributeID = dict["AttributeID"] as? String ?? ""
        obj.AttributeTitle = dict["AttributeTitle"] as? String ?? ""
        obj.Price = dict["Price"] as? String ?? ""
        obj.VariantID = dict["VariantID"] as? String ?? ""
        obj.VariantTitle = dict["VariantTitle"] as? String ?? ""
     
        return obj
        
    }
    
}

class ProductAddOnData: NSObject {
    
    var AddOnProductID: String = ""
    var AttributeForSearchProduct: String = ""
    var BoxIDs: String = ""
    var BrandID: String = ""
    var BrandTitle: String = ""
    var CategoryID: String = ""
    var CategoryTitle: String = ""
    var CompanyID: String = ""
    var CorporateMinQuantity: String = ""
    var CorporatePrice: String = ""
    var CreatedAt: String = ""
    var CreatedBy: String = ""
    var CrossSellingProductID: String = ""
    var Description: String = ""
    var Hide: String = ""
    var ImageName: String = ""
    var Ingredients: String = ""
    var IsActive: String = ""
    var IsCorporateProduct: String = ""
    var IsCustomizedProduct: String = ""
    var IsFeatured: String = ""
    var Keywords: String = ""
    var MetaDescription: String = ""
    var MetaKeywords: String = ""
    var MetaTags: String = ""
    var MinimumOrderQuantity: String = ""
    var OutOfStock: String = ""
    var Packaging: String = ""
    var Price: String = ""
    var PriceType: String = ""
    var ProducrTextID: String = ""
    var ProductID: String = ""
    var PurchaseCount: String = ""
    var SKU: String = ""
    var ServingSize: String = ""
    var SortOrder: String = ""
    var Specifications: String = ""
    var SubCategoryID: String = ""
    var SubCategoryTitle: String = ""
    var SuggestedUse: String = ""
    var SystemLanguageID: String = ""
    var TagIDs: String = ""
    var Title: String = ""
    var Typee: String = ""
    var UpdatedAt: String = ""
    var UpdatedBy: String = ""
    var VariantData: String = ""
    var VariantForSearchProduct: String = ""
    var Warnings: String = ""
    
    var isSelected: Bool = false
    
    class func parseAddOnData(dict: NSDictionary) -> ProductAddOnData {
        
        let obj = ProductAddOnData()
        
        obj.AddOnProductID = dict["AddOnProductID"] as? String ?? ""
        obj.AttributeForSearchProduct = dict["AttributeForSearchProduct"] as? String ?? ""
        obj.BoxIDs = dict["BoxIDs"] as? String ?? ""
        obj.BrandID = dict["BrandID"] as? String ?? ""
        obj.BrandTitle = dict["BrandTitle"] as? String ?? ""
        obj.CategoryID = dict["CategoryID"] as? String ?? ""
        obj.CategoryTitle = dict["CategoryTitle"] as? String ?? ""
        obj.CompanyID = dict["CompanyID"] as? String ?? ""
        obj.CorporateMinQuantity = dict["CorporateMinQuantity"] as? String ?? ""
        obj.CorporatePrice = dict["CorporatePrice"] as? String ?? ""
        obj.CreatedAt = dict["CreatedAt"] as? String ?? ""
        obj.CreatedBy = dict["CreatedBy"] as? String ?? ""
        obj.CrossSellingProductID = dict["CrossSellingProductID"] as? String ?? ""
        obj.Description = dict["Description"] as? String ?? ""
        obj.Hide = dict["Hide"] as? String ?? ""
        obj.ImageName = dict["ImageName"] as? String ?? ""
        obj.Ingredients = dict["Ingredients"] as? String ?? ""
        obj.IsActive = dict["IsActive"] as? String ?? ""
        obj.IsCorporateProduct = dict["IsCorporateProduct"] as? String ?? ""
        obj.IsCustomizedProduct = dict["IsCustomizedProduct"] as? String ?? ""
        obj.IsFeatured = dict["IsFeatured"] as? String ?? ""
        obj.Keywords = dict["Keywords"] as? String ?? ""
        obj.MetaDescription = dict["MetaDescription"] as? String ?? ""
        obj.MetaKeywords = dict["MetaKeywords"] as? String ?? ""
        obj.MetaTags = dict["MetaTags"] as? String ?? ""
        obj.MinimumOrderQuantity = dict["MinimumOrderQuantity"] as? String ?? ""
        obj.OutOfStock = dict["OutOfStock"] as? String ?? ""
        obj.Packaging = dict["Packaging"] as? String ?? ""
        obj.Price = dict["Price"] as? String ?? ""
        obj.PriceType = dict["PriceType"] as? String ?? ""
        obj.ProducrTextID = dict["ProducrTextID"] as? String ?? ""
        obj.ProductID = dict["ProductID"] as? String ?? ""
        obj.PurchaseCount = dict["PurchaseCount"] as? String ?? ""
        obj.SKU = dict["SKU"] as? String ?? ""
        obj.ServingSize = dict["ServingSize"] as? String ?? ""
        obj.SortOrder = dict["SortOrder"] as? String ?? ""
        obj.Specifications = dict["Specifications"] as? String ?? ""
        obj.SubCategoryID = dict["SubCategoryID"] as? String ?? ""
        obj.SubCategoryTitle = dict["SubCategoryTitle"] as? String ?? ""
        obj.SuggestedUse = dict["SuggestedUse"] as? String ?? ""
        obj.SystemLanguageID = dict["SystemLanguageID"] as? String ?? ""
        obj.TagIDs = dict["TagIDs"] as? String ?? ""
        obj.Title = dict["Title"] as? String ?? ""
        obj.Typee = dict["Typee"] as? String ?? ""
        obj.UpdatedAt = dict["UpdatedAt"] as? String ?? ""
        obj.UpdatedBy = dict["UpdatedBy"] as? String ?? ""
        obj.VariantData = dict["VariantData"] as? String ?? ""
        obj.VariantForSearchProduct = dict["VariantForSearchProduct"] as? String ?? ""
        obj.Warnings = dict["Warnings"] as? String ?? ""
     
        return obj
        
    }
    
}

//
//  StoresModel.swift
//  Niehez
//
//  Created by Macbook on 12/05/2022.
//  Copyright © 2022 Mac. All rights reserved.
//

import UIKit

class StoresModel: NSObject {
    
    var CityID : String = ""
    var Address : String = ""
    var CityTitle : String = ""
    var CompanyID : String = ""
    var StoreID : String = ""
    var Title : String = ""
    
    
    class func parseData(response:NSDictionary) -> [StoresModel]
    {
        
        
        var tempArray = [StoresModel]()
        
        if let storeArray = response["stores"] as? NSArray
        {
            
            for item in storeArray
            {
                if let dictionary = item as? NSDictionary
                {
                    let object = StoresModel()
                    
                    object.CityID = dictionary["CityID"] as? String ?? ""
                    object.Address = dictionary["Address"] as? String ?? ""
                    object.CityTitle = dictionary["CityTitle"] as? String ?? ""
                    object.CompanyID = dictionary["CompanyID"] as? String ?? ""
                    object.StoreID = dictionary["StoreID"] as? String ?? ""
                    object.Title = dictionary["Title"] as? String ?? ""
                    
                    tempArray.append(object)
                }
            }
            
            
            
        }
        return tempArray
    }
}

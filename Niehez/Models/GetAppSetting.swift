//
//  GetAppSetting.swift
//  Niehez
//
//  Created by Macbook on 04/11/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation

class GetAppSetting: NSObject {
    
    var AlertBackgroundColor : String = ""
    var AlertNegativeButtonColor : String = ""
    var AlertPositiveButtonColor : String = ""
    var AlertTextColor :String = ""
    var AppDescription : String = ""
    var AppIcon : String = ""
    var AppKeywords : String = ""
    var AppName : String = ""
    var AppSettingID :String = ""
    var AppStoreTitle = ""
    var CompanyID : String = ""
    var CreatedAt = ""
    var CreatedBy : String?
    var DomainColor :String = ""
    var FieldStyle: String = ""
    
    var Login2ndHex: String = ""
    var LoginBackgroundHex: String = ""
    var LoginBackgroundImage : String = ""
    var LoginLogo : String = ""
    var MainElementsBackground2ndColor : String = ""
    var MainElementsBackgroundColor : String = ""
    var MainElementsBackgroundImage : String = ""
    var ShowActualProducts : String = ""
    var Splash2ndHex : String = ""
    var SplashBackgroundHex : String = ""
    var SplashBackgroundImage : String = ""
    var SplashDuration : String = "0.0"
    var SplashLogo: String = ""
    var TabBarColor : String = ""
    var TabImageCatalougeHoverIcon : String = ""
    var TabImageCatalougeIcon : String = ""
    var TabImageCatalougeSelectedIcon : String = ""
    var TabImageHomeHoverIcon : String = ""
    var TabImageHomeIcon : String = ""
    var TabImageHomeSelectedIcon : String = ""
    var TabImageOrdersHoverIcon : String?
    var TabImageOrdersIcon : String = ""
    var TabImageOrdersSelectedIcon : String = ""
    var TabImageSettingHoverIcon : String = ""
    var TabImageSettingIcon : String = ""
    var TabImageSettingSelectedIcon : String = ""
    var TopBarCartIcon : String = ""
    var TopBarSearchIcon : String = ""
    var TopTabBarColor : String = ""
    var table : String = ""
    
    
    class func parseAppSetting(response: NSDictionary) -> GetAppSetting {
        
        let getAppSetting = GetAppSetting()
        
        if let dict = response["setting_detail"] as? NSDictionary {
            
            getAppSetting.AlertBackgroundColor = dict["AlertBackgroundColor"] as? String ?? ""
            getAppSetting.AlertNegativeButtonColor = dict["AlertNegativeButtonColor"] as? String ?? ""
            getAppSetting.AlertPositiveButtonColor = dict["AlertPositiveButtonColor"] as? String ?? ""
            getAppSetting.AlertTextColor = dict["AlertTextColor"] as? String ?? ""
            getAppSetting.AppDescription = dict["AppDescription"] as? String ?? ""
            getAppSetting.AppIcon = dict["AppIcon"] as? String ?? ""
            getAppSetting.AppKeywords = dict["AppKeywords"] as? String ?? ""
            getAppSetting.AppName = dict["AppName"] as? String ?? ""
            getAppSetting.AppSettingID = dict["AppSettingID"] as? String ?? ""
            getAppSetting.AppStoreTitle = dict["AppStoreTitle"] as? String ?? ""
            getAppSetting.CompanyID = dict["CompanyID"] as? String ?? ""
            getAppSetting.CreatedAt = dict["CreatedAt"] as? String ?? ""
            getAppSetting.CreatedBy = dict["CreatedBy"] as? String ?? ""
            getAppSetting.DomainColor = dict["DomainColor"] as? String ?? ""
            getAppSetting.FieldStyle = dict["FieldStyle"] as? String ?? ""
            
            getAppSetting.Login2ndHex = dict["Login2ndHex"] as? String ?? ""
            getAppSetting.LoginBackgroundHex = dict["LoginBackgroundHex"] as? String ?? ""
            getAppSetting.LoginBackgroundImage = dict["LoginBackgroundImage"] as? String ?? ""
            getAppSetting.LoginLogo = dict["LoginLogo"] as? String ?? ""
            getAppSetting.MainElementsBackground2ndColor = dict["MainElementsBackground2ndColor"] as? String ?? ""
            getAppSetting.MainElementsBackgroundColor = dict["MainElementsBackgroundColor"] as? String ?? ""
            getAppSetting.MainElementsBackgroundImage = dict["MainElementsBackgroundImage"] as? String ?? ""
            getAppSetting.ShowActualProducts = dict["ShowActualProducts"] as? String ?? ""
            getAppSetting.Splash2ndHex = dict["Splash2ndHex"] as? String ?? ""
            getAppSetting.SplashBackgroundHex = dict["SplashBackgroundHex"] as? String ?? ""
            getAppSetting.SplashBackgroundImage = dict["SplashBackgroundImage"] as? String ?? ""
            getAppSetting.SplashDuration = dict["SplashDuration"] as? String ?? ""
            getAppSetting.SplashLogo = dict["SplashLogo"] as? String ?? ""
            getAppSetting.TabBarColor = dict["TabBarColor"] as? String ?? ""
            getAppSetting.TabImageCatalougeHoverIcon = dict["TabImageCatalougeHoverIcon"] as? String ?? ""
            getAppSetting.TabImageCatalougeIcon = dict["TabImageCatalougeIcon"] as? String ?? ""
            
            getAppSetting.TabImageCatalougeSelectedIcon = dict["TabImageCatalougeSelectedIcon"] as? String ?? ""
            getAppSetting.TabImageHomeHoverIcon = dict["TabImageHomeHoverIcon"] as? String ?? ""
            getAppSetting.TabImageHomeIcon = dict["TabImageHomeIcon"] as? String ?? ""
            getAppSetting.TabImageHomeSelectedIcon = dict["TabImageHomeSelectedIcon"] as? String ?? ""
            getAppSetting.TabImageOrdersHoverIcon = dict["TabImageOrdersHoverIcon"] as? String ?? ""
            getAppSetting.TabImageOrdersIcon = dict["TabImageOrdersIcon"] as? String ?? ""
            getAppSetting.TabImageOrdersSelectedIcon = dict["TabImageOrdersSelectedIcon"] as? String ?? ""
            getAppSetting.TabImageSettingHoverIcon = dict["TabImageSettingHoverIcon"] as? String ?? ""
            getAppSetting.TabImageSettingIcon = dict["TabImageSettingIcon"] as? String ?? ""
            getAppSetting.TabImageSettingSelectedIcon = dict["TabImageSettingSelectedIcon"] as? String ?? ""
            getAppSetting.TopBarCartIcon = dict["TopBarCartIcon"] as? String ?? ""
            getAppSetting.TopBarSearchIcon = dict["TopBarSearchIcon"] as? String ?? ""
            getAppSetting.TopTabBarColor = dict["TopTabBarColor"] as? String ?? ""
            getAppSetting.table = dict["table"] as? String ?? ""
            
            
        }
        
        return getAppSetting
        
    }
    
}

//
//  MSJAddToCartObject.swift
//  MSJ
//
//  Created by Mac on 1/2/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class MSJAddToCartObject: NSObject {

    var CorporateMinQuantity : String?
    var CustomizedBoxID : String?
    var CustomizedOrderProductIDs : String?
    var CustomizedShapeImage : String?
    var ImageName : String?
    var IsCorporateItem : String?
    var IsCorporateProduct : String?
    var ItemType : String?
    var Price : String?
    var PriceType : String?
    var ProductID : String?
    var Quantity : String?
    var Rating : String?
    var Ribbon : String?
    var TempItemPrice: String?
    var TempOrderID: String?
    var Title: String?
    
    class func parseCartData(dict:NSDictionary) -> MSJAddToCartObject
    {
        
        let obj = MSJAddToCartObject()
        
        obj.CorporateMinQuantity = dict["CorporateMinQuantity"] as? String
        obj.CustomizedBoxID = dict["CustomizedBoxID"] as? String
        obj.CustomizedOrderProductIDs = dict["CustomizedOrderProductIDs"] as? String
        obj.CustomizedShapeImage = dict["CustomizedShapeImage"] as? String
        obj.ImageName = dict["ImageName"] as? String
        obj.IsCorporateItem = dict["IsCorporateItem"] as? String
        obj.IsCorporateProduct = dict["IsCorporateProduct"] as? String
        obj.ItemType = dict["ItemType"] as? String
        obj.Price = dict["Price"] as? String
        obj.PriceType = dict["PriceType"] as? String
        obj.ProductID = dict["ProductID"] as? String
        obj.Quantity = dict["Quantity"] as? String
        obj.Rating = dict["Rating"] as? String
        obj.Ribbon = dict["Ribbon"] as? String
        obj.TempItemPrice = dict["TempItemPrice"] as? String
        obj.TempOrderID = dict["TempOrderID"] as? String
        obj.Title = dict["Title"] as? String
        
        return obj
        
    }
    
}

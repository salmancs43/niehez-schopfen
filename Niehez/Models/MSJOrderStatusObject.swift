//
//  MSJOrderStatusObject.swift
//  MSJ
//
//  Created by Mac on 1/18/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class MSJOrderStatusObject: NSObject {
    
   var  AddressCity = ""
    var AddressDistrict = ""
    var AddressID = ""
    var AddressIDForPaymentCollection = ""
    var AddressType = ""
    var AssignedDriverEmail = ""
    var AssignedDriverMobile = ""
    var AssignedDriverName = ""
    var BranchDeliveryDistrictID = ""
    var BuildingNo = ""
    var CityID = ""
    var CollectFromStore = ""
    var CompanyID = ""
    var CouponCodeDiscountPercentage = ""
    var CouponCodeUsed = ""
    var CreatedAt = ""
    var DeliveryOTP = ""
    var  Description = ""
    var  DiscountAvailed = ""
    var  DistrictID = ""
    var  DistrictTitle = ""
    var  DriverID = ""
    var  Email = ""
    var ForAdmin = ""
    var  ForUser = ""
    var FullName = ""
    var HasTicket = ""
    var Hide = ""
    var  IsClosed = ""
    var  IsDefault = ""
    var   IsPosOrder = ""
    var  IsRead = ""
    var Latitude = ""
    var Longitude = ""
    var  Mobile = ""
    var MobileNo = ""
    var OnlineStatus = ""
    var OrderCreateDate = ""
    var  OrderID = ""
    var  OrderNumber = ""
    var  OrderStatusAr = ""
    var  OrderStatusEn = ""
    var  OrderStatusID = ""
    var  POBox = ""
    var  PaymentMethod = ""
    var  RecipientName = ""
    var ReturnedReason = ""
    var ShipmentMethodID = ""
    var ShipmentMethodTitle = ""
    var  ShippedThroughApi = ""
    var  Status = ""
    var  StoreAddress = ""
    var  StoreAdminEmail = ""
    var  StoreAdminFullName = ""
    var  StoreCityTitle = ""
    var  StoreID = ""
    var  StoreTitle = ""
    var Street = ""
    var  TicketCreatedAt = ""
    var  TicketID = ""
    var  TicketNumber = ""
    var  TotalAmount = ""
    var  TotalShippingCharges = ""
    var  TotalTaxAmount = ""
    var  TransactionID = ""
    var  UseForPaymentCollection = ""
    var  UserCity = ""
    var  UserID = ""
    var  VatNo = ""
    var  ZipCode = ""
    
 
  
    
  
    
    
    class func parseData(response:NSDictionary) -> [MSJOrderStatusObject]
    {
        
        
        var tempArray = [MSJOrderStatusObject]()
        
        if let orderArray = response["orders"] as? NSArray
        {
            
            for item in orderArray
            {
                if let dictionary = item as? NSDictionary
                {
                    let object = MSJOrderStatusObject()
                    
                    object.AddressCity = dictionary["AddressCity"] as? String ?? ""
                    object.AddressDistrict = dictionary["AddressDistrict"] as? String ?? ""
                    object.AddressID = dictionary["AddressID"] as? String ?? ""
                    object.AddressIDForPaymentCollection = dictionary["AddressIDForPaymentCollection"] as? String ?? ""
               
                    object.AddressType = dictionary["AddressType"] as? String ?? ""
                    object.AssignedDriverEmail = dictionary["AssignedDriverEmail"] as? String ?? ""
                    object.AssignedDriverMobile = dictionary["AssignedDriverMobile"] as? String ?? ""
                    
                    object.AssignedDriverName = dictionary["AssignedDriverName"] as? String ?? ""
                    object.BranchDeliveryDistrictID = dictionary["BranchDeliveryDistrictID"] as? String ?? ""
                    object.BuildingNo = dictionary["BuildingNo"] as? String ?? ""
                    object.CityID = dictionary["CityID"] as? String ?? ""
                    
                    object.CollectFromStore = dictionary["CollectFromStore"] as? String ?? ""
                    object.CompanyID = dictionary["CompanyID"] as? String ?? ""
                    object.CouponCodeDiscountPercentage = dictionary["CouponCodeDiscountPercentage"] as? String ?? ""
                    object.CouponCodeUsed = dictionary["CouponCodeUsed"] as? String ?? ""
                    object.CreatedAt = dictionary["CreatedAt"] as? String ?? ""
                    
                    
                    
                    object.DeliveryOTP = dictionary["DeliveryOTP"] as? String ?? ""
                    object.Description = dictionary["Description"] as? String ?? ""
                    object.DiscountAvailed = dictionary["DiscountAvailed"] as? String ?? ""
                    object.DistrictID = dictionary["DistrictID"] as? String ?? ""
               
                    object.DistrictTitle = dictionary["DistrictTitle"] as? String ?? ""
                    object.DriverID = dictionary["DriverID"] as? String ?? ""
                    object.Email = dictionary["Email"] as? String ?? ""
                    
                    object.ForAdmin = dictionary["ForAdmin"] as? String ?? ""
                    object.ForUser = dictionary["ForUser"] as? String ?? ""
                    object.FullName = dictionary["FullName"] as? String ?? ""
                    object.HasTicket = dictionary["HasTicket"] as? String ?? ""
                    
                    object.Hide = dictionary["Hide"] as? String ?? ""
                    object.IsClosed = dictionary["IsClosed"] as? String ?? ""
                    object.IsDefault = dictionary["IsDefault"] as? String ?? ""
                    object.IsPosOrder = dictionary["IsPosOrder"] as? String ?? ""
                    object.IsRead = dictionary["IsRead"] as? String ?? ""
                    
                    
                    
                    
                    object.Latitude = dictionary["Latitude"] as? String ?? ""
                    object.Longitude = dictionary["Longitude"] as? String ?? ""
                    object.Mobile = dictionary["Mobile"] as? String ?? ""
                    object.MobileNo = dictionary["MobileNo"] as? String ?? ""
               
                    object.OnlineStatus = dictionary["OnlineStatus"] as? String ?? ""
                    object.OrderCreateDate = dictionary["OrderCreateDate"] as? String ?? ""
                    object.OrderID = dictionary["OrderID"] as? String ?? ""
                    
                    object.OrderNumber = dictionary["OrderNumber"] as? String ?? ""
                    object.OrderStatusAr = dictionary["OrderStatusAr"] as? String ?? ""
                    object.OrderStatusEn = dictionary["OrderStatusEn"] as? String ?? ""
                    object.OrderStatusID = dictionary["OrderStatusID"] as? String ?? ""
                    
                    object.POBox = dictionary["POBox"] as? String ?? ""
                    object.PaymentMethod = dictionary["PaymentMethod"] as? String ?? ""
                    object.RecipientName = dictionary["RecipientName"] as? String ?? ""
                    object.ReturnedReason = dictionary["ReturnedReason"] as? String ?? ""
                    object.ShipmentMethodID = dictionary["ShipmentMethodID"] as? String ?? ""
                    
                    
                    object.ShipmentMethodTitle = dictionary["ShipmentMethodTitle"] as? String ?? ""
                    object.ShippedThroughApi = dictionary["ShippedThroughApi"] as? String ?? ""
                    object.Status = dictionary["Status"] as? String ?? ""
                    object.StoreAddress = dictionary["StoreAddress"] as? String ?? ""
               
                    object.StoreAdminEmail = dictionary["StoreAdminEmail"] as? String ?? ""
                    object.StoreAdminFullName = dictionary["StoreAdminFullName"] as? String ?? ""
                    object.StoreCityTitle = dictionary["StoreCityTitle"] as? String ?? ""
                    
                    object.StoreID = dictionary["StoreID"] as? String ?? ""
                    object.StoreTitle = dictionary["StoreTitle"] as? String ?? ""
                    object.Street = dictionary["Street"] as? String ?? ""
                    object.TicketCreatedAt = dictionary["TicketCreatedAt"] as? String ?? ""
                    
                    object.TicketID = dictionary["TicketID"] as? String ?? ""
                    object.TicketNumber = dictionary["TicketNumber"] as? String ?? ""
                    object.TotalAmount = dictionary["TotalAmount"] as? String ?? ""
                    object.TotalShippingCharges = dictionary["TotalShippingCharges"] as? String ?? ""
                    object.TotalTaxAmount = dictionary["TotalTaxAmount"] as? String ?? ""
                    
                    
                    object.TransactionID = dictionary["TransactionID"] as? String ?? ""
                    
                    object.UseForPaymentCollection = dictionary["UseForPaymentCollection"] as? String ?? ""
                    object.UserCity = dictionary["UserCity"] as? String ?? ""
                    object.UserID = dictionary["UserID"] as? String ?? ""
                    object.VatNo = dictionary["VatNo"] as? String ?? ""
                    object.ZipCode = dictionary["ZipCode"] as? String ?? ""
             
                    
                    tempArray.append(object)
                }
            }
            
            
            
        }
        return tempArray
    }
    
    
    
    
    

}

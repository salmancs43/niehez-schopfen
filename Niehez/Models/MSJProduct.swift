//
//  MSJProduct.swift
//  MSJ
//
//  Created by Mac on 06/12/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class MSJProduct: NSObject
{
    var brandId          : String?
    var imagePath      : String?
    var productDescription         : String?
    var productFlag           : Bool?
    var productId            : String?
  @objc  var productModel   : String?
  @objc  var productName           : String?
  @objc  var mainProductName           : String?
    var productPrice    : String?
    var subCategoryId         : String?
    var mainCategoryId         : String?
    var brandLogo         : String?
    var brandTitle         : String?
    var logoString         : String?
   

}

//
//  MSJProductDetailsObject.swift
//  MSJ
//
//  Created by Mac on 12/11/17.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class MSJProductDetailsObject: NSObject {

    var brandId : String?
    var isActive : Bool?
    var imagePath : String?
    var productDescription : String?
    var mainProductId : String?
    var productModel : String?
    var productName : String?
    var productPrice : String?
    var subCategoryId : String?
    var brandName : String?
    var brandLogo : String?
    
    
    var productColorArry = Array<Any>()
    var colorMasterId : String?
    var productColorId : String?
    var mainProductColorId : String?
    var productColorHexa : String?
    
    var productFeatureArry = Array<Any>()
    var featureDescription : String?
    var featureTitle : String?
    var imageURL : String?
    var featureId : String?
    var mainProductFeatureId : String?
    
    var productImageArry = Array<Any>()
    var mainProductImageId : String?
    var productImageId : String?
    var productImagePath : String?
    
    var productSpecificationArry = Array<Any>()
    var mainSprcificationId : String?
    var productSpecDescription : NSAttributedString?
    var productSpecificationId : String?


    
    
}

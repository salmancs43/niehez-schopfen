//
//  MSJComplaint.swift
//  MSJ
//
//  Created by Mac on 12/18/17.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class MSJComplaint: NSObject {
    
    var complaintDescription : String?
    var complaintTitle : String?
    var complaintTypeId : String?
    var complaintPlacholder : String?
    
    // complaint list by customer id.
    var address : String?
    var city : String?
    var complaintId : String?
    var supportComplaintTypeId : String?
    var customerId : String?
    var email : String?
    var fullName : String?
    var message : String?
    var mobile : String?
    var productBrand : String?
    var requestNo : String?
     var createdDate : String?
    var status : String?
    var lat : String?
    var lng : String?
    var apartment_no : String?
    var ticket_type : String?
    var visit_time : String?
    var unread : String?
    var closed_request : String?
    
    var ticket_type_parent : String?
    var ticket_type_child : String?

    
    
    
    
    
    
    
}

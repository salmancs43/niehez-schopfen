//
//  CategoryModel.swift
//  Niehez
//
//  Created by Macbook on 27/03/2022.
//  Copyright © 2022 Mac. All rights reserved.
//

import UIKit

class CategoryModel: NSObject
{
    var CategoryID: String = ""
    var CategoryPrice: String = ""
    var CategoryTextID: String = ""
    var CompanyID: String = ""
    var CreatedAt: String = ""
    var CreatedBy: String = ""
    var Description: String = ""
    var Hide: String = ""
    var Image: String = ""
    var IsActive: String = ""
    var IsFeatured: String = ""
    var ParentID: String = ""
    var SortOrder: String = ""
    var SystemLanguageID: String = ""
    var Title: String = ""
    
    class func parseCategoryData(dict:NSDictionary) -> CategoryModel
    {
        
        let categoryModel = CategoryModel()
        
        categoryModel.CategoryID = dict["CategoryID"] as? String ?? ""
        categoryModel.CategoryPrice = dict["CategoryPrice"] as? String ?? ""
        categoryModel.CategoryTextID = dict["CategoryTextID"] as? String ?? ""
        categoryModel.CompanyID = dict["CompanyID"] as? String ?? ""
        categoryModel.CreatedAt = dict["CreatedAt"] as? String ?? ""
        categoryModel.CreatedBy = dict["CreatedBy"] as? String ?? ""
        categoryModel.Description = dict["Description"] as? String ?? ""
        categoryModel.Hide = dict["Hide"] as? String ?? ""
        categoryModel.Image = dict["Image"] as? String ?? ""
        categoryModel.IsActive = dict["IsActive"] as? String ?? ""
        categoryModel.IsFeatured = dict["IsFeatured"] as? String ?? ""
        categoryModel.ParentID = dict["ParentID"] as? String ?? ""
        categoryModel.SortOrder = dict["SortOrder"] as? String ?? ""
        categoryModel.SystemLanguageID = dict["SystemLanguageID"] as? String ?? ""
        categoryModel.Title = dict["Title"] as? String ?? ""
                
        return categoryModel
        
    }
}

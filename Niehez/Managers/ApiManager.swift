//
//  ApiManager.swift
//  Niehez
//
//  Created by Macbook on 29/03/2022.
//  Copyright © 2022 Mac. All rights reserved.
//

import UIKit
import Alamofire

class ApiManager {
    
    static let sharedInstance = ApiManager()
    
    class func getRequest (urlString : String!, isAlertShow:Bool,parameters:Parameters?, successCallback : @escaping (NSDictionary) -> Void, errorCallBack : @escaping (String) -> Void) -> Void {
        
        if let param = parameters {
            print(param)
        }
        print(urlString ?? "")
        MSJSharedManager.sharedManager.isUpdateApp = false
        
        if !Reachability.isConnectedToNetwork()
        {
            print("Internet Connection not  Available!")
            return
            
        }
        else
        {
            print("Internet Connection  Available!")
        }
        
        if isAlertShow
        {
            MSJSharedManager.showHUD()
        }
            
        var header: HTTPHeaders = [:]
        
        if (UserDefaults.standard.value(forKey: USER_DATA) == nil)
        {
            header = ["Verifytoken": UserDefaults.standard.string(forKey: USERDEFAULTS_TOKEN) ?? ""]
        }
        else
        {
            header = ["Verifytoken": MSJSharedManager.getUser().AuthToken ?? ""]
        }
        
        
        AF.request(urlString, method: .get, parameters:parameters, headers: header ).responseJSON {response in
            
            print(response)
            MSJSharedManager.hideHud()
            
            switch response.result {
                
                
            case .success(let value):
                
                
                if let JSON = value as? [String: Any] {
                    
                    
                    let keyExists = JSON["status"]
                    
                    if keyExists != nil
                    {
                        
                        
                        let statusCode = JSON["status"] as! NSNumber
                        
                        
                        if statusCode == 200 {
                            
                            successCallback(JSON as NSDictionary)
                            
                        }
                        else
                        {
                            var msg  = "Oops!!Something went wrong.".localized()
                            let keyExists = JSON["message"]
                            if keyExists != nil
                            {
                                msg = JSON["message"] as! String
                            }
                            
                            let keyExistsVersion = JSON["version_ok"]
                            if keyExistsVersion != nil
                            {
                                let bool = JSON["version_ok"] as! Bool
                                if bool == false
                                {
                                    MSJSharedManager.sharedManager.isUpdateApp = true
                                }
                            }
                            
                            errorCallBack(msg)
                        }
                    }
                    else
                    {
                        
                        errorCallBack("Oops!!Something went wrong.".localized())
                    }
                }
                
            case .failure(let error):
                errorCallBack(error.localizedDescription)
            }
            
        }
        
    }
    
    class func SignUpApi(url: String, profileImage: UIImage, params: [String:Any], showLoader: Bool = true, completion: @escaping (NSDictionary) -> Void, errorCallBack : @escaping (String) -> Void) -> Void {
        let myURL = BASE_URL + url
        
        print(myURL)
        
        if !Reachability.isConnectedToNetwork()
        {
            print("Internet Connection not  Available!")
            return
            
        }
        else
        {
            print("Internet Connection  Available!")
        }
        
        if showLoader
        {
            MSJSharedManager.showHUD()
        }
            
        var header: HTTPHeaders = [:]
        
        if (UserDefaults.standard.value(forKey: USER_DATA) == nil)
        {
            header = ["Verifytoken": UserDefaults.standard.string(forKey: USERDEFAULTS_TOKEN) ?? ""]
        }
        else
        {
            header = ["Verifytoken": MSJSharedManager.getUser().AuthToken ?? ""]
        }
        
        AF.upload(multipartFormData: { multipartFormData in
            //uploading images=
            
            let profileImageData = profileImage.jpegData(compressionQuality: 0.3)
            multipartFormData.append(profileImageData!, withName: "Image", fileName: "image.png", mimeType: "image/jpeg")
            
            print(params)
            
            //uploading params
            for (key, value) in params {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
        },to: myURL, method: .post, headers: header)
        
            .uploadProgress(queue: .main, closure: { progress in
                //Current upload progress of file
                print("Upload Progress: \(progress.fractionCompleted)")
            })
            .responseJSON(completionHandler: { data in
                //Do what ever you want to do with response
                MSJSharedManager.hideHud()
                print(data)
                switch data.result {
                case .success(let value):
                    
                    
                    if let JSON = value as? [String: Any] {
                        
                        
                        let keyExists = JSON["status"]
                        
                        if keyExists != nil
                        {
                            
                            let statusCode = JSON["status"] as! NSNumber
                            
                            
                            if statusCode == 200 {
                                
                                completion(JSON as NSDictionary)
                                
                            }
                            else
                            {
                                var msg  = "Oops!!Something went wrong.".localized()
                                let keyExists = JSON["message"]
                                if keyExists != nil
                                {
                                    msg = JSON["message"] as! String
                                }
                                
                                let keyExistsVersion = JSON["version_ok"]
                                if keyExistsVersion != nil
                                {
                                    let bool = JSON["version_ok"] as! Bool
                                    if bool == false
                                    {
                                        MSJSharedManager.sharedManager.isUpdateApp = true
                                    }
                                }
                                
                                errorCallBack(msg)
                            }
                        }
                        else
                        {
                            
                            errorCallBack("Oops!!Something went wrong.".localized())
                        }
                    }
                    
                case .failure(let error):
                    errorCallBack(error.localizedDescription)
                }
                
            })
    }
    
    class func postServiceCall (urlString : String!, isAlertShow:Bool, parameters : Parameters,header : String!, successCallback : @escaping (NSDictionary) -> Void, errorCallBack : @escaping (String) -> Void) -> Void {
        
        
        MSJSharedManager.sharedManager.isUpdateApp = false
        if !Reachability.isConnectedToNetwork()
        {
            print("Internet Connection not  Available!")
            return
            
        }
        else
        {
            print("Internet Connection  Available!")
        }
        if isAlertShow
        {
            
            
             MSJSharedManager.showHUD()
        }
        
        var header: HTTPHeaders = [:]
        
        if (UserDefaults.standard.value(forKey: USER_DATA) == nil)
        {
            header = ["Verifytoken": UserDefaults.standard.string(forKey: USERDEFAULTS_TOKEN) ?? ""]
        }
        else
        {
            header = ["Verifytoken": MSJSharedManager.getUser().AuthToken ?? ""]
            print(MSJSharedManager.getUser().AuthToken)
        }
        
        var params = parameters
        
        params["NoVersion"] =  "true"
        
        print(urlString!)
        print(params)
        
        AF.request(urlString, method: .post, parameters: params,headers: header).responseJSON {response in
            
            MSJSharedManager.hideHud()
            print(response)
            switch response.result {
                
                
            case .success(let value):
                
                
                if let JSON = value as? [String: Any] {
                    
                    
                    let keyExists = JSON["status"]
                    
                    if keyExists != nil
                    {
                        
                        
                        let statusCode = JSON["status"] as! NSNumber
                        print(statusCode)
                        
                        if statusCode == 200 {
                            
                            successCallback(JSON as NSDictionary)
                            
                        }
                        else
                        {
                            var msg  = "Oops!!Something went wrong.".localized()
                            let keyExists = JSON["message"]
                            if keyExists != nil
                            {
                                msg = JSON["message"] as? String ?? ""
                            }
                            
                            let keyExistsVersion = JSON["version_ok"]
                            if keyExistsVersion != nil
                            {
                                let bool = JSON["version_ok"] as! Bool
                                if bool == false
                                {
                                    MSJSharedManager.sharedManager.isUpdateApp = true
                                }
                            }
                            print(msg)
                            errorCallBack(msg)
                        }
                    }
                    else
                    {
                        
                        errorCallBack("Oops!!Something went wrong.".localized())
                    }
                }
            case .failure(let error):
            
                print(error.localizedDescription)
                errorCallBack(error.localizedDescription)
                
            }
            
        }
        
        
        
    }
    
}

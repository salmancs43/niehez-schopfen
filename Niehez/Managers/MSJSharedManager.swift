//
//  MSJSharedManager.swift
//  MSJ
//
//  Created by Mac on 20/12/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit
import DropDown
import Firebase
import FirebaseMessaging


var user = MSJUser()
var isArabicLanguage : Bool = false

var dropDown = DropDown()

class MSJSharedManager: NSObject
{

    static let sharedManager = MSJSharedManager()
    

    var latitude : Double = 0.0
    var lognitude : Double = 0.0
    
    var isUpdateApp : Bool = false
    var cartArray = Array<Any>()
    var visitArray = Array<Any>()
    
    var category : String?
    var subCategory : String?
    var brand : String?
    
    var appSetting = GetAppSetting()
    
    var url : String?
    
    static let hud = LottieHUD("4495-shopping-basket")

    
    private override init ()
    {
        super.init()
        
        
    }
    
    
    
    class func setCategory(cat:String?)
    {
        MSJSharedManager.sharedManager.category = cat
        
    }
    
    class func setSubCategory(subCat:String?)
    {
        MSJSharedManager.sharedManager.subCategory = subCat
        
    }
    
    class func setBrand(brand:String?)
    {
        MSJSharedManager.sharedManager.brand = brand
        
    }
    
    class func setAppSetting(object:GetAppSetting){
        MSJSharedManager.sharedManager.appSetting = object
    }
    class func getAppSetting()->GetAppSetting{
        return MSJSharedManager.sharedManager.appSetting
    }
    
    
    class func setArabicPage(isArabic:Bool)
    {
        isArabicLanguage = isArabic
    
      
        
    }
    
    class  func getArabic()-> Bool
    {
        return isArabicLanguage
        
    }

    
    class func setUser(userObject:MSJUser)-> MSJUser
    {
        user = userObject
        return userObject
        
    }
    class  func getUser()-> MSJUser
    {
        return user
        
    
    }
    
    @objc class func regularFont() -> String
    {
        if MSJSharedManager.getArabic()
        {
            return "HelveticaNeue"
            
            return "GESSTwoMedium-Medium"
           // return "Avenir"
        }
        else
        {
            return "HelveticaNeue"
        }
        
    }
    
    @objc class func mediumFont() -> String
    {
        if MSJSharedManager.getArabic()
        {
            return "HelveticaNeue-Medium"
            
            return "GESSTwoBold-Bold"
           //return "Avenir-Medium"
        }
        else
        {
            return "HelveticaNeue-Medium"
        }
        
    }
    
    @objc class func boldFont() -> String
    {
        
        if MSJSharedManager.getArabic()
        {
             return "HelveticaNeue-Bold"
            
            return "GESSTwoBold-Bold"
           //  return "Avenir-Heavy"
        }
        else
        {
            return "HelveticaNeue-Bold"
        }
        
    }
    
    class func showHUD()
    {
        
        hud.showHUD()
    }
    
    
    class func hideHud()
    {
        hud.stopHUD()
    }
    
    
    @objc class func getAppVersion() -> String
    {
        let version: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        return version
    }
    
    class func getFCMTokenFromUserDefaults() -> String {
        
        if let fcmToken = UserDefaults.standard.value(forKey: TOKEN) {
            
            return fcmToken as! String
            
        } else {
            return Messaging.messaging().fcmToken ?? ""
        }
        
    }
    
    
    class func getToken()  {
//
//         InstanceID.instanceID().instanceID { (result, error) in
//
//             if let error = error {
//
//
//                 print("Error fetching remote instance ID: \(error)")
//
//             } else if let result = result {
//
//                 print("Remote instance ID token: \(result.token)")
//
//
//                 UserDefaults.standard.set(result.token, forKey: TOKEN)
//                 UserDefaults.standard.synchronize()
//
//             }
//
//         }
         
     }
    
    // show drop down
    
   
    func setUpDropDownForArray ( array : [String]?, view : UIView!, onSelection : @escaping ( _ str : String, _ index : Int) -> Void) {
        
    
        guard let ary = array else
        {
            dropDown.hide()
            return
        }
        
        if ary.count == 0 {
            dropDown.hide()
            return
        }
        
        dropDown.anchorView = view
        dropDown.width = view.bounds.size.width
        
        dropDown.dataSource = ary
       
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.topOffset = CGPoint(x: 0, y:-view.bounds.height)
      //  dropDown.cellNib = UINib(nibName: "DropDownCustomCell", bundle: nil)
        
        dropDown.selectionAction = { (index: Int, item: String) in
            onSelection(item, index)
            dropDown.hide()
           
        }
        
        dropDown.show()
    }
    
 
    
   
    
   
}

//
//  MSJApiManager.swift
//  MSJ
//
//  Created by Mac on 06/12/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import Reachability
import Foundation

var vc = BaseVC()
class MSJApiManager: NSObject
{
    static let sharedInstance = MSJApiManager()
    
    func postServiceCall (urlString : String!, isAlertShow:Bool, companyId : String , parameters : Parameters,header : String!, successCallback : @escaping (NSDictionary) -> Void, errorCallBack : @escaping (String) -> Void) -> Void {
        
        
        MSJSharedManager.sharedManager.isUpdateApp = false
        if !Reachability.isConnectedToNetwork()
        {
            print("Internet Connection not  Available!")
            return
            
        }
        else
        {
            print("Internet Connection  Available!")
        }
        if isAlertShow
        {
            
            
             MSJSharedManager.showHUD()
        }
        
        
        
        var params = parameters
        if companyId != ""
        {
            params[COMPANY_ID] =  companyId
        }
        
        
        
        
        
        AF.request(urlString, method: .post, parameters: params,headers: nil).responseJSON {response in
            
            print(response)
            MSJSharedManager.hideHud()
            
            
            
            switch response.result {
                
                
            case .success(let value):
                
                
                if let JSON = value as? [String: Any] {
                    
                    
                    let keyExists = JSON["status"]
                    
                    if keyExists != nil
                    {
                        
                        
                        let statusCode = JSON["status"] as! Bool
                        
                        
                        if statusCode == true {
                            
                            successCallback(JSON as NSDictionary)
                            
                        }
                        else
                        {
                            var msg  = "Oops!!Something went wrong.".localized()
                            let keyExists = JSON["message"]
                            if keyExists != nil
                            {
                                msg = JSON["message"] as! String
                            }
                            
                            let keyExistsVersion = JSON["version_ok"]
                            if keyExistsVersion != nil
                            {
                                let bool = JSON["version_ok"] as! Bool
                                if bool == false
                                {
                                    MSJSharedManager.sharedManager.isUpdateApp = true
                                }
                            }
                            
                            errorCallBack(msg)
                        }
                    }
                    else
                    {
                        
                        errorCallBack("Oops!!Something went wrong.".localized())
                    }
                    
                    
                    
                    
                    
                }
                
                
            case .failure(let error):
                
                
                errorCallBack(error.localizedDescription)
                
            }
            
        }
        
        
        
    }
    
    func getRequest (urlString : String!, isAlertShow:Bool,parameters:Parameters?, successCallback : @escaping (NSDictionary) -> Void, errorCallBack : @escaping (String) -> Void) -> Void {
        
        if let param = parameters {
            print(param)
        }
        print(urlString ?? "")
        MSJSharedManager.sharedManager.isUpdateApp = false
        
        if !Reachability.isConnectedToNetwork()
        {
            print("Internet Connection not  Available!")
            return
            
        }
        else
        {
            print("Internet Connection  Available!")
        }
        
        if isAlertShow
        {
            MSJSharedManager.showHUD()
        }
            
        
        AF.request(urlString, method: .get, parameters:parameters).responseJSON {response in
            
            print(response)
            MSJSharedManager.hideHud()
            
            switch response.result {
                
                
            case .success(let value):
                
                
                if let JSON = value as? [String: Any] {
                    
                    
                    let keyExists = JSON["status"]
                    
                    if keyExists != nil
                    {
                        
                        
                        let statusCode = JSON["status"] as! Bool
                        
                        
                        if statusCode == true {
                            
                            successCallback(JSON as NSDictionary)
                            
                        }
                        else
                        {
                            var msg  = "Oops!!Something went wrong.".localized()
                            let keyExists = JSON["message"]
                            if keyExists != nil
                            {
                                msg = JSON["message"] as! String
                            }
                            
                            let keyExistsVersion = JSON["version_ok"]
                            if keyExistsVersion != nil
                            {
                                let bool = JSON["version_ok"] as! Bool
                                if bool == false
                                {
                                    MSJSharedManager.sharedManager.isUpdateApp = true
                                }
                            }
                            
                            errorCallBack(msg)
                        }
                    }
                    else
                    {
                        
                        errorCallBack("Oops!!Something went wrong.".localized())
                    }
                    
                    
                    
                    
                    
                }
                
                
            case .failure(let error):
                
                
                errorCallBack(error.localizedDescription)
                
                
                
                
            }
            
        }
        
    }
    
    class func alamofirePostRequestNewToken(parameters: Parameters, isShowAI: Bool, mainView: UIView, successCallback: @escaping (NSDictionary) -> Void, errorCallBack: @escaping (String) -> Void) {
        
        
        var url = BASE_URL + GENERATE_TOKEN
        url = url.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        
        MSJSharedManager.showHUD()
        
        let header: HTTPHeaders = ["Newtoken": "true"]
        
        
        var params = parameters
        
//        params["Userid"] = "1"
        params["NoVersion"] = "true"
        
        AF.request(url, method: .post, parameters: params, headers: header).responseJSON { response in
            
            print(url)
            print(params)
            print(response)
            
            MSJSharedManager.hideHud()
            
            
            switch response.result {
                
            case .success(let value):
                
                if let JSON = value as? [String: Any] {
                    
                    
                    if let isSTATUS = JSON["status"] {
                        
                        let STATUS = isSTATUS as? Int
                        if STATUS == 200 {
                            
                            successCallback(JSON as NSDictionary)
                            return
                            
                        } else if STATUS == 401 {
                            
//                                    MSJApiManager.logOutFromDevice()
                            return
                        }
                        
                    }
                }
                else
                {
                    errorCallBack("Oops!!Something went wrong.".localized())
                }
                
            case .failure(let error):
                errorCallBack(error.localizedDescription)
            }
        }
    }
}


//
//  customImageView.swift
//  Niehez
//
//  Created by Macbook on 06/06/2022.
//  Copyright © 2022 Mac. All rights reserved.
//

import UIKit

class CustomImageView: UIImageView {
    override init(image: UIImage?) {
        super.init(image: image)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.contentMode = .scaleAspectFill
    }
}

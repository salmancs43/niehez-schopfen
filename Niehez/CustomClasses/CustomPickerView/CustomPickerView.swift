//
//  CustomPickerView.swift
//  Niehez
//
//  Created by Macbook on 02/04/2022.
//  Copyright © 2022 Mac. All rights reserved.
//

import UIKit
import AAPickerView

class CustomAAPickerView : AAPickerView {
    
    override var datePicker: UIDatePicker? {
        get {
            return self.inputView as? UIDatePicker
        }
        set {
            
            inputView = newValue
            if #available(iOS 13.4, *) {
                datePicker?.preferredDatePickerStyle = .wheels
            } else {
                // Fallback on earlier versions
            }
            datePicker?.datePickerMode = UIDatePicker.Mode.date
            
            var components = DateComponents()
            
            components.year = -18
            let maxDate = Calendar.current.date(byAdding: components, to: Date())
            
            datePicker?.maximumDate = maxDate
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/dd/YYYY"
            
        }
    }
}

extension UIDatePicker {

   func setDate(from string: String, format: String, animated: Bool = true) {

      let formater = DateFormatter()

      formater.dateFormat = format

      let date = formater.date(from: string) ?? Date()

      setDate(date, animated: animated)
   }
}

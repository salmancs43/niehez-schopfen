//
//  MSJBoldLabel.swift
//  MSJ
//
//  Created by Mac on 2/17/1439 AH.
//  Copyright © 1439 Mac. All rights reserved.
//

import UIKit

class MSJBoldLabel: UILabel
{
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.font = UIFont.init(name: MSJSharedManager.boldFont(), size: self.font.pointSize)
        
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

//
//  MSJColor.swift
//  MSJ
//
//  Created by Mac on 2/17/1439 AH.
//  Copyright © 1439 Mac. All rights reserved.
//

import UIKit

class MSJColor: UIColor
{

}

extension UIColor
{
//    static let TAB_BAR_BACKGROUND_COLOR =  UIColor.init(red: 27.0/255.0, green: 65.0/255.0, blue: 144.0/255.0, alpha: 1.0)
//    static let SELECT_TAB_VIEW_COLOR =  UIColor.init(red: 71.0/255.0, green: 103.0/255.0, blue: 168.0/255.0, alpha: 1.0)
    static let TAB_BAR_BACKGROUND_COLOR =  UIColor.init(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    static let SELECT_TAB_VIEW_COLOR =  UIColor.init(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
//     static let GREEN_BUTTON_COLOR =  UIColor.init(red: 51.0/255.0, green: 168.0/255.0, blue: 68.0/255.0, alpha: 1.0)
    static let GREEN_BUTTON_COLOR =  UIColor.init(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    
      static let LIGHT_GRAY_COLOR =  UIColor.init(red: 173.0/255.0, green: 173.0/255.0, blue: 173.0/255.0, alpha: 0.5)
    
    static let STATUS_BAR_GREEN =  UIColor.init(red: 93.0/255.0, green: 193.0/255.0, blue: 105.0/255.0, alpha: 1.0)
     static let STATUS_BAR_RED =  UIColor.init(red: 237.0/255.0, green: 108.0/255.0, blue: 104.0/255.0, alpha: 1.0)
    
    static let TAB_BAR_TEXT =  UIColor.init(red: 74.0/255.0, green: 161.0/255.0, blue: 151.0/255.0, alpha: 1.0)
    
    
    
    static let LIGHT_GREEN_COLOR =  UIColor.init(red: 92.0/255.0, green: 200.0/255.0, blue: 140.0/255.0, alpha: 1.0)
    static let DARK_GREEN_COLOR =  UIColor.init(red: 66.0/255.0, green: 145.0/255.0, blue: 142.0/255.0, alpha: 1.0)
    
    
    static let GO_TO_BAG_COLOR =  UIColor.init(red: 255.0/255.0, green: 193.0/255.0, blue: 7.0/255.0, alpha: 1.0)
    
}


//
//  MSJCellViewShadow.swift
//  MSJ
//
//  Created by Mac on 22/01/2018.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class MSJCellViewShadow: UIView {

    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        layer.cornerRadius = 20
        layer.masksToBounds = false
        layer.shadowColor = UIColor.init(red: 225.0/255.0, green: 225.0/255.0, blue: 225.0/255.0, alpha: 1.0).cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset =  CGSize(width: 0, height: 1)
        layer.shadowRadius = 1
        
        
    }

}

//
//  MSJCorner.swift
//  MSJ
//
//  Created by Mac on 05/04/2018.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class MSJCorner: UIView {


    
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        if MSJSharedManager.getArabic()
        {
            
              self.roundCornersView([.topRight, .bottomRight], radius: 20)
        
        }
        else
        {
            
              self.roundCornersView([.topLeft, .bottomLeft], radius: 20)
            
        }
      
    }
    
    
}

extension UIView {
    
    
    
    func roundCornersView(_ corners: UIRectCorner, radius: CGFloat)
    {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
        
    }
    
    
}


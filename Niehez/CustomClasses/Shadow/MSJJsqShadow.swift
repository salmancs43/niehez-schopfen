//
//  MSJJsqShadow.swift
//  MSJ
//
//  Created by Mac on 08/02/2018.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class MSJJsqShadow: UIView {

    override func awakeFromNib()
    {
        super.awakeFromNib()
        layer.cornerRadius = 15
        layer.masksToBounds = false;
        layer.shadowColor = UIColor.init(red: 225.0/255.0, green: 225.0/255.0, blue: 225.0/255.0, alpha: 1.0).cgColor
        layer.shadowOpacity = 0.15
        layer.shadowOffset =  CGSize.zero
        layer.shadowRadius = 1
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

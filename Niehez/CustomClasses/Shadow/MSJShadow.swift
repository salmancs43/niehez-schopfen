//
//  MSJShadow.swift
//  MSJ
//
//  Created by Mac on 07/11/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class MSJShadow: UIView {

    override func awakeFromNib()
    {
        super.awakeFromNib()
       
      
        
      
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.roundCorners([.bottomRight, .bottomLeft], radius: 20)
    }
   

}

extension UIView {
  
    
 
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat)
    {
            let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            layer.mask = mask
        
    }
    
    
    
    func dropShadowView(color:UIColor,isRoundConer:Bool)
    {

        layer.masksToBounds = false;
        layer.shadowColor = UIColor.init(red: 225.0/255.0, green: 225.0/255.0, blue: 225.0/255.0, alpha: 1.0).cgColor
        layer.shadowOpacity = 0.15
        layer.shadowOffset =  CGSize.zero
        layer.shadowRadius = 1
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
    }
        
    
   
    func dropShadow(color:UIColor,isRoundConer:Bool)
    {
  
        if isRoundConer
        {
            layer.cornerRadius = layer.frame.size.height/2
            layer.masksToBounds = true;
        }
        
        layer.shadowOffset = CGSize(width: 0, height: 1)
        layer.shadowColor = color.cgColor;
        layer.shadowRadius = 1.0;
        layer.shadowOpacity = 0.15;
        layer.shadowPath = UIBezierPath (roundedRect: layer.bounds, cornerRadius: layer.cornerRadius).cgPath
        let bColor = self.backgroundColor?.cgColor
        layer.backgroundColor  = nil
        layer.backgroundColor = bColor

    }
    
    
    func dropShadowBottom(color:UIColor)
    {
        layer.shadowColor = color.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 3)
        layer.shadowOpacity = 0.15
        layer.shadowRadius = 0.0
        layer.masksToBounds = false
    }
    
    
    func makeBorderOfView(color:UIColor)
    {
        layer.borderColor = color.cgColor
        layer.borderWidth = 1.0
        layer.cornerRadius = layer.frame.size.height/2
        layer.masksToBounds = false
      
        
    }
    
  
    func dropShadowComplete(color: UIColor)
    {
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 1
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        
        self.layer.rasterizationScale = UIScreen.main.scale


    }
    
    func dropShadowProduct()
    {
       layer.cornerRadius = layer.frame.size.height/2
        layer.masksToBounds = false
       //layer.cornerRadius = 35
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = 1
       
      // self.layer.shadowPath =  UIBezierPath(roundedRect: self.layer.bounds, cornerRadius: layer.frame.size.height/2).cgPath
       
    }
}

//
//  MSJCustomTextView.swift
//  MSJ
//
//  Created by Mac on 07/11/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class MSJCustomTextView: UITextView {

    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        self.font =  UIFont.init(name: MSJSharedManager.regularFont(), size: 15.0)
        
        
    }

}


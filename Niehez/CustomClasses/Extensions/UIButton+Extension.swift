//
//  UIButton+Extension.swift
//  Niehez
//
//  Created by Muhammad Salman on 19/07/2022.
//  Copyright © 2022 Mac. All rights reserved.
//

import Foundation
import Kingfisher
import UIKit

extension UIButton{
  
    
    
    func downLoadImageIntoButton(url:String, placeholder:UIImage? = PLACEHOLDER_IMAGE)
    {
        if url == ""
        {
            
            self.setImage(PLACEHOLDER_IMAGE, for: .normal)
            return
        }
        
        var finalUrl = ""
        
        finalUrl =  IMG_BASE_URL + url
        
        
        print(finalUrl)
        
        
        
        finalUrl = finalUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!
        
        //  keepCurrentImageWhileLoading
        //  fromMemoryCacheOrRefresh
        
        let downloadUrl = URL(string: finalUrl )
        
        
      
        
        
        var options: KingfisherOptionsInfo = []
        options.append(.transition(.fade(1)))
        options.append(.loadDiskFileSynchronously)
        
        self.kf.setImage(with: downloadUrl, for: .normal, placeholder: PLACEHOLDER_IMAGE, options: options, progressBlock: nil) { result in
            
            switch result {
            case .success(let value):
                // The source object which contains information like `url`.
                print(value.source)
                
                
            case .failure(let error):
                print(error) // The error happens
            }
            
        }
    
       
        
    }
    
    

}

//
//  ImageVIew+Extension.swift
//  Niehez
//
//  Created by Muhammad Salman on 19/07/2022.
//  Copyright © 2022 Mac. All rights reserved.
//

import Foundation
import Kingfisher

extension UIImageView{
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
    
    func downLoadImageIntoImageView(url:String, placeholder:UIImage? = PLACEHOLDER_IMAGE)
    {
        if url == ""
        {
            
            return self.image = PLACEHOLDER_IMAGE
        }
        
        var finalUrl = ""
        
        finalUrl =  IMG_BASE_URL + url
        
        
        print(finalUrl)
        
        
        
        finalUrl = finalUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!
        
        //  keepCurrentImageWhileLoading
        //  fromMemoryCacheOrRefresh
        
        let downloadUrl = URL(string: finalUrl )
        
        
        self.kf.indicatorType = .activity
        
        
        var options: KingfisherOptionsInfo = []
        options.append(.transition(.fade(1)))
        options.append(.loadDiskFileSynchronously)
        
        
        self.kf.setImage(with: downloadUrl, placeholder: PLACEHOLDER_IMAGE, options:options) { result in
            switch result {
            case .success(let value):
                // The source object which contains information like `url`.
                print(value.source)
                
                
            case .failure(let error):
                print(error) // The error happens
            }
        }
        
       
        
    }
    
    
    func downLoadQRImageAndReturnResult(url:String, placeholder:UIImage?  , returnResult: @escaping (UIImage) -> Void , errorResult: @escaping (String) -> Void )
    {
        if url == ""
        {
            
            return
        }
        
        var finalUrl = ""
        
        finalUrl = url
        
        
        print(finalUrl)
        
        
        
        finalUrl = finalUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!
        
        //  keepCurrentImageWhileLoading
        //  fromMemoryCacheOrRefresh
        
        let downloadUrl = URL(string: finalUrl )
        
        
        self.kf.indicatorType = .activity
        
        
        var options: KingfisherOptionsInfo = []
        options.append(.transition(.fade(1)))
        options.append(.loadDiskFileSynchronously)
        
        
        self.kf.setImage(with: downloadUrl, placeholder: nil, options:options) { result in
            switch result {
            case .success(let value):
                print(value.image)
                
                returnResult(value.image)
                // The source object which contains information like `url`.
                print(value.source)
            case .failure(let error):
                print(error) // The error happens
                errorResult(error.localizedDescription)
            }
        }
        
        
    }
}

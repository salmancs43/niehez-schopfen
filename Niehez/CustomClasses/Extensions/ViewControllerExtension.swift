//
//  ViewControllerExtension.swift
//  Niehez
//
//  Created by Macbook on 02/04/2022.
//  Copyright © 2022 Mac. All rights reserved.
//

import UIKit

extension UIViewController {
    func addImagePicker(title: String, msg: String, imagePicker: UIImagePickerController) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera".localized(), style: .default , handler:{ (UIAlertAction)in
            print("Camera button clicked")
            imagePicker.allowsEditing = true
            imagePicker.sourceType = .camera
            self.present(imagePicker, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Photos".localized(), style: .default , handler:{ (UIAlertAction)in
            print("Gallery button clicked")
            imagePicker.allowsEditing = true
            imagePicker.sourceType = .photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
}

 //
//  MSJCustomPickerTextField.swift
//  MSJ
//
//  Created by Mac on 12/18/17.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit
import AAPickerView

class MSJCustomPickerTextField: AAPickerView {

    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        self.font =  UIFont.init(name: MSJSharedManager.regularFont(), size: (self.font?.pointSize)!)
        
        
    }

}

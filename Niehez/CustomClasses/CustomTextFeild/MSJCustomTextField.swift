//
//  MSJCustomTextField.swift
//  MSJ
//
//  Created by Mac on 07/11/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class MSJCustomTextField: UITextField {

  
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        self.font =  UIFont.init(name: MSJSharedManager.regularFont(), size: 15.0)
       // self.keyboardType = UIKeyboardType.asciiCapable
        
        if MSJSharedManager.getArabic() {
            self.textAlignment = NSTextAlignment.right
        } else {
            self.textAlignment = NSTextAlignment.left
        }
        
    }

}

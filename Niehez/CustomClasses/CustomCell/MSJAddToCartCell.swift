//
//  MSJAddToCartCell.swift
//  MSJ
//
//  Created by Mac on 1/2/18.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class MSJAddToCartCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!

    @IBOutlet var widthContraint: NSLayoutConstraint!
    @IBOutlet weak var labelProductName: MSJRegularLabel!
    

    @IBOutlet weak var blueView: UIView!
    @IBOutlet weak var buttonMore: UIButton!
    @IBOutlet weak var labelPrice: MSJMediumLabel!
   
   
    @IBOutlet var buttonMinus: UIButton!
    @IBOutlet var buttonPlus: UIButton!
    @IBOutlet var textFieldQuantity: UITextField!
    @IBOutlet var viewCart: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

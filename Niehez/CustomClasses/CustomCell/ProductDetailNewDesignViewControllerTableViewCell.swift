//  Created on 19/08/2022.

import UIKit

class ProductDetailNewDesignViewControllerTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    
    @IBOutlet weak var imagesCollectionViewOuterView: UIView!
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    
    @IBOutlet weak var itemsLeftLabel: UILabel!
    
    @IBOutlet weak var descriptionTitleLabel: UILabel!
    @IBOutlet weak var descriptionTextLabel: UILabel!
    @IBOutlet weak var specificationTitleLabel: UILabel!
    @IBOutlet weak var specificationTextLabel: UILabel!
    
    @IBOutlet weak var multiselectionCellOuterView: UIView!
    @IBOutlet weak var mutiselectionCellTitleLabel: UILabel!
    
    @IBOutlet weak var typeCellOuterView: UIView!
    @IBOutlet weak var typeCellButtonOuterView: UIView!
    @IBOutlet weak var typeCellButtonInnerView: UIView!
    @IBOutlet weak var typeCellButton: UIButton!
    @IBOutlet weak var typeCellTypeLabel: UILabel!
    @IBOutlet weak var typeCellPriceLabel: UILabel!
    
    @IBOutlet weak var textFieldQuantity: MSJCustomTextField!
    @IBOutlet weak var totalPriceLabel: UILabel!
    
}

//
//  MSJComplaintDetailCell.swift
//  MSJ
//
//  Created by Mac on 13/02/2018.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class MSJComplaintDetailCell: UITableViewCell {

    @IBOutlet weak var labelComplaintDetail: MSJRegularLabel!
    
    @IBOutlet weak var labelComplaintType: MSJBoldLabel!
    
    @IBOutlet weak var labelProductName: MSJBoldLabel!
    
    @IBOutlet weak var labelDate: MSJRegularLabel!
    @IBOutlet weak var labelRequestNo: MSJBoldLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  MSJProductDetailCollectionCell.swift
//  MSJ
//
//  Created by Mac on 08/11/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class MSJProductDetailCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productView: UIView!
    @IBOutlet weak var colerView: UIView!
    @IBOutlet weak var selectedView: UIView!
    @IBOutlet weak var featuredImageView: UIImageView!
    
    
}

//
//  MSJVisitCell.swift
//  MSJ
//
//  Created by Mac on 19/02/2018.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class MSJVisitCell: UITableViewCell
{

    @IBOutlet weak var labelTime: MSJRegularLabel!
    @IBOutlet weak var labelDate: MSJRegularLabel!
    @IBOutlet weak var labelAppartment: MSJRegularLabel!
    @IBOutlet weak var labelRequestType: MSJRegularLabel!
    @IBOutlet weak var labelDateAndTime: MSJRegularLabel!
    @IBOutlet weak var labelTitle: MSJBoldLabel!
    @IBOutlet weak var mainView: MSJCellViewShadow!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  MSJChatCell.swift
//  MSJ
//
//  Created by Mac on 06/02/2018.
//  Copyright © 2018 Mac. All rights reserved.
//

import UIKit

class MSJChatCell: UITableViewCell {

  
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var labelMessage: MSJRegularLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

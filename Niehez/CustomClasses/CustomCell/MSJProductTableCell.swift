//
//  MSJProductTableCell.swift
//  MSJ
//
//  Created by Mac on 08/11/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class MSJProductTableCell: UITableViewCell {
   
    @IBOutlet weak var mainContainer: UIView!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var labelProductTitle: MSJRegularLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

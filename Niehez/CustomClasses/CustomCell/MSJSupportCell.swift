//
//  MSJSupportCell.swift
//  MSJ
//
//  Created by Mac on 07/11/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class MSJSupportCell: UITableViewCell
{

    @IBOutlet weak var top_yes_button_height: NSLayoutConstraint!
    @IBOutlet weak var yes_height: NSLayoutConstraint!
    @IBOutlet weak var labelRefrence: UILabel!
    @IBOutlet weak var labelDateAndTime: MSJRegularLabel!
    @IBOutlet weak var labelDescription: MSJRegularLabel!
    
    @IBOutlet weak var buttonNo: MSJRegularButton!
    @IBOutlet weak var buttonYes: MSJRegularButton!
    @IBOutlet weak var labelCloseTicket: MSJBoldLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

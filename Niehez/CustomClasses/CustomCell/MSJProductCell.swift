//
//  MSJProductCell.swift
//  MSJ
//
//  Created by Mac on 08/11/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class MSJProductCell: UICollectionViewCell {
    
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var labelModelNumber: MSJMediumLabel!
    
    @IBOutlet var viewDetails: MSJRegularButton!

    @IBOutlet weak var labelProductTitle: MSJRegularLabel!
    @IBOutlet weak var buttonAddCart: UIButton!
    @IBOutlet weak var labelPrice: MSJBoldLabel!
    @IBOutlet weak var labelDetail: MSJRegularLabel!
    
    @IBOutlet weak var cartView: UIView!
    
    @IBOutlet weak var labelLogo: MSJBoldLabel!
    @IBOutlet weak var labelProductTitleBold: MSJBoldLabel!
 
    
    
    
}

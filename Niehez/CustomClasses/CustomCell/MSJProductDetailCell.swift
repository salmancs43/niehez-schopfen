//
//  MSJProductDetailCell.swift
//  MSJ
//
//  Created by Mac on 08/11/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class MSJProductDetailCell: UITableViewCell
{
    
    @IBOutlet weak var labelSpecificationDetail: MSJRegularLabel!
    @IBOutlet weak var dotImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  MSJHomeCollectionReusableView.swift
//  MSJ
//
//  Created by Muhammad Salman on 12/06/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class MSJHomeCollectionReusableView: UICollectionReusableView {
        
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var adsCollectionView: UICollectionView!
}

//
//  MSJMediumButton.swift
//  MSJ
//
//  Created by Mac on 07/11/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class MSJMediumButton: UIButton {

    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.titleLabel?.font = UIFont.init(name:MSJSharedManager.mediumFont(), size: (self.titleLabel?.font.pointSize)!)
        
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

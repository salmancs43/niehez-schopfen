//
//  PlacePickerTextField.swift
//  EliteRide
//
//  Created by Awais Shahid on 7/6/17.
//  Copyright © 2017 ZaproDigital. All rights reserved.
//

import Foundation
import UIKit
import DropDown

class PlacePickerTextField : MSJCustomTextField {
    
    private var onSelection:(String)->() = {_ in}
    private var onTextChange:(String)->() = {_ in}
    private var onEmptyTextField:()->() = {}
    private var onCompletionFethcingDetails:(MPlace)->() = {_ in}
    var shouldShow = false

    var typeText = String()
    
    private let dropDown = DropDown()
    
    @objc private func textFieldDidChange(){
        shouldShow = true
        guard let _ = text else {
            return
        }
        startFindingPlaces(text: text!)
        onTextChange(text!)
    }
    
    
    @objc private func textFieldDidEndEditing(){
        shouldShow = false
         dropDown.hide()
    }
    
    private func setUpDropDown (_ array : [MPlace]?) {
        
        if shouldShow == false {
            return
        }
        
        guard let ary = array else {
            return
        }
        
        if ary.count == 0 {
            return
        }
        
        dropDown.anchorView = self
        dropDown.width = self.bounds.width 
        
        var strAry = [String]()
        for place in ary {
            strAry.append(place.city!)
        }
        
        dropDown.dataSource = strAry
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.topOffset = CGPoint(x: 0, y:-self.bounds.height)
      //  dropDown.cellNib = UINib(nibName: "DropDownCustomCell", bundle: nil)
        
        
        dropDown.selectionAction = { (index: Int, item: String) in
            self.text=item
            self.dropDown.hide()
            for i in 0...ary.count {
                if i == index {
                    let place = ary[i]
                    //self.getPlaceDetailsByID(id: place.id)
                    self.onCompletionFethcingDetails(place)
                    break
                }
            }
            self.onSelection(item)
        }
        dropDown.show()
    }
    
    
    private func getPlaceDetailsByID(id : String!) {
        var dataTask:URLSessionDataTask?
        
        if let dataTask1 = dataTask {
            dataTask1.cancel()
        }
        
        let urlString = "https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyDgOeL1TnDOy7ePEdvdcNs9sE2EDypRJ2Y&placeid=\(id!)"
        let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        let request = URLRequest(url: url!)
        
        dataTask = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
            if let data = data{
                do{
                    var place = MPlace()
                    let result1 = try JSONSerialization.jsonObject(with: data) as! [String:Any]
                    if let status = result1["status"] as? String{
                        if status == "OK"{
                            
                            let result = result1["result"] as! NSDictionary
                            
                            
                            if let address_components = result["address_components"] as? [NSDictionary] {
                                
                                for dict in address_components{
                                    
                                    let types = dict["types"] as! [String]
                                    
                                    if types[0]  == "postal_code" {
                                        place.postcode = dict["long_name"] as? String
                                    }
                                }
                            }
                            
                            if let place_id = result["place_id"] as? String {
                                place.id = place_id
                            }
                            
                            
                            
                            if let name = result["formatted_address"] as? String {
                                place.name = name
                            }
                            
                            if let geometery = result["geometry"] as? NSDictionary {
                                if let loc = geometery["location"] as? NSDictionary {
                                    place.latitude = loc["lat"] as? Double
                                    place.longitude = loc["lng"] as? Double
                                }
                            }
                            
                            self.onCompletionFethcingDetails(place)
                            return
                        }
                    }
                    
                    self.onCompletionFethcingDetails(place)
                    
                }
                catch let error as NSError{
                    print("Error: \(error.localizedDescription)")
                }
            }
        })
        dataTask?.resume()
    }
    
    
    private func startFindingPlaces (text : String!) {
        
        if text.isEmpty {
            onEmptyTextField()
            return
        }
        
        var dataTask:URLSessionDataTask?
        
        if let dataTask1 = dataTask {
            dataTask1.cancel()
        }
        var language:String
        if MSJSharedManager.getArabic()
        {
            language = "ar"
        }
        else
        {
            language = "en"
        }
        let urlString = "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=AIzaSyDWJjL2YrT5FefvnQ71XtYtt7E73cwcFXE&input=\(text!)&types=(\(typeText))&components=country:SA&language=\(language)"
        let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        let request = URLRequest(url: url!)
        
        dataTask = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
            if let data = data{
                do{
                    
                    let result = try JSONSerialization.jsonObject(with: data) as! [String:Any]
                    if let status = result["status"] as? String{
                        if status == "OK"{
                            if let predictions = result["predictions"] as? NSArray {
                                var places = [MPlace]()
                                for dict in predictions as! [NSDictionary]{
                                    
                                   
                                    var place = MPlace()
                                    place.id =  dict["place_id"] as? String
                                    place.name = dict["description"] as? String
                                  
                                    let cityDict = dict["structured_formatting"] as! NSDictionary
                                    place.city = cityDict["main_text"] as? String
                                    place.country = cityDict["secondary_text"] as? String
                                   
                                    places.append(place)
                                }
                                DispatchQueue.main.async(execute: { () -> Void in
                                    self.setUpDropDown(places)
                                })
                                return
                            }
                        }
                    }
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.setUpDropDown(nil)
                    })
                }
                catch let error as NSError{
                    print("Error: \(error.localizedDescription)")
                }
            }
        })
        dataTask?.resume()
    }
    
    
    func setUpForAutoCompletePlaces( type:String, onChange : @escaping (_ str : String) -> Void, onSelect : @escaping (_ str : String) -> Void, onEmpty : @escaping () -> Void, completeDetails : @escaping (_ place : MPlace) -> Void )  {
        let typeStr = type
         typeText = typeStr 
        commonInit()
        onTextChange = onChange
        onSelection = onSelect
        onEmptyTextField = onEmpty
        onCompletionFethcingDetails = completeDetails
        
    }
    
    private func commonInit() {
        self.clearButtonMode = .whileEditing
        self.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        self.addTarget(self, action: #selector(textFieldDidEndEditing), for: .editingDidEnd)
    }
    
    
    
    
    struct MPlace {
        var id                  : String?
        var name                : String?
        var postcode            : String?
        var city                : String?
        var country            : String?
        var latitude            : Double?
        var longitude           : Double?
    }
    
    
    
    
}

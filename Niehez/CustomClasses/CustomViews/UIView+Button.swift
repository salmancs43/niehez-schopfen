//  Created on 19/07/2022.

import UIKit

class UIView_Button: UIView {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        if MSJSharedManager.getAppSetting().FieldStyle == "Half Round" {
            self.layer.cornerRadius = layer.frame.size.height / 4
        } else {
            self.layer.cornerRadius = layer.frame.size.height / 2
        }
        
        self.clipsToBounds = true
        self.backgroundColor = BaseVC().hexStringToUIColors(hex: MSJSharedManager.getAppSetting().AlertPositiveButtonColor)
        
    }

}

//
//  CustomViewExtension.swift
//  Niehez
//
//  Created by Macbook on 01/04/2022.
//  Copyright © 2022 Mac. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func fullRoundView() {
        
        self.layer.cornerRadius = self.frame.size.height / 2
        
    }
    
    func halfRoundView() {
        
        self.layer.cornerRadius = 8
        
    }
}

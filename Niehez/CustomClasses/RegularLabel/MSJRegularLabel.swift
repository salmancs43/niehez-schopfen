//
//  MSJRegularLabel.swift
//  MSJ
//
//  Created by Mac on 2/17/1439 AH.
//  Copyright © 1439 Mac. All rights reserved.
//

import UIKit

class MSJRegularLabel: UILabel {

   
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
       
          self.font = UIFont.init(name: MSJSharedManager.regularFont() , size: self.font.pointSize)
        
      
        
    }

}
